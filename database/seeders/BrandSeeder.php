<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\BrandTranslation;
use App\Models\Page;
use App\Models\PageTranslation;
use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service1= Brand::create([
            //"src" => "images/services/air_conditions.png" ,
            "ac_type" => "shbak"
        ]);
        BrandTranslation::create([
            "name" => "Lennox" ,
            "locale" => "en" ,
            "brand_id" => $service1->id
        ]);
        BrandTranslation::create([
            "name" => "لينوكس" ,
            "locale" => "ar" ,
            "brand_id" => $service1->id
        ]);
        // ============================


        $service1= Brand::create([
            //"src" => "images/services/air_conditions.png" ,
            "ac_type" => "shbak"
        ]);
        BrandTranslation::create([
            "name" => "Carrier" ,
            "locale" => "en" ,
            "brand_id" => $service1->id
        ]);
        BrandTranslation::create([
            "name" => "كرير" ,
            "locale" => "ar" ,
            "brand_id" => $service1->id
        ]);

        // ============================

        $brand= Brand::create([
            "ac_type" => "sahrawi"
        ]);
        BrandTranslation::create([
            "name" => "Trane" ,
            "locale" => "en" ,
            "brand_id" => $brand->id
        ]);
        BrandTranslation::create([
            "name" => "تران" ,
            "locale" => "ar" ,
            "brand_id" => $brand->id
        ]);
        // ============================

        $brand= Brand::create([
            "ac_type" => "sahrawi"
        ]);
        BrandTranslation::create([
            "name" => "American Standard" ,
            "locale" => "en" ,
            "brand_id" => $brand->id
        ]);
        BrandTranslation::create([
            "name" => "أمريكان" ,
            "locale" => "ar" ,
            "brand_id" => $brand->id
        ]);

        // ============================
        $brand= Brand::create([
            //"src" => "images/services/air_conditions.png" ,
            "ac_type" => "sahrawi"
        ]);
        BrandTranslation::create([
            "name" => "American Standard" ,
            "locale" => "en" ,
            "brand_id" => $brand->id
        ]);
        BrandTranslation::create([
            "name" => "أمريكان" ,
            "locale" => "ar" ,
            "brand_id" => $brand->id
        ]);
        // ============================

        $brand = Brand::create([
            //"src" => "images/services/air_conditions.png" ,
            "ac_type" => "markazi"
        ]);
        BrandTranslation::create([
            "name" => "LG" ,
            "locale" => "en" ,
            "brand_id" => $brand->id
        ]);
        BrandTranslation::create([
            "name" => "LG" ,
            "locale" => "ar" ,
            "brand_id" => $brand->id
        ]);


        //======================

        $page = Page::create([
            "segment" =>"terms_Conditions"
        ]);

        PageTranslation::create([
            "name" => "الشروط و الاحكام" ,
            "description" => "الشروط و الاحكام" ,
            "locale" => "ar" ,
            "page_id" => $page->id
        ]);
        PageTranslation::create([
            "name" => "Terms and Conditions" ,
            "description" => "Terms and Conditions" ,
            "locale" => "en" ,
            "page_id" => $page->id
        ]);


        $page2 = Page::create([
            "segment" =>"about"
        ]);

        PageTranslation::create([
            "name" => "من نحن" ,
            "description" => "من نحن" ,
            "locale" => "ar" ,
            "page_id" => $page2->id
        ]);
        PageTranslation::create([
            "name" => "about" ,
            "description" => "about" ,
            "locale" => "en" ,
            "page_id" => $page2->id
        ]);
    }
}
