<?php

namespace Database\Seeders;

use App\Models\Service;
use App\Models\ServiceTranslation;
use App\Models\SurveyAnswer;
use App\Models\SurveyAnswerTranslation;
use App\Models\SurveyQuestion;
use App\Models\SurveyQuestionTranslation;
use Illuminate\Database\Seeder;

class SurveySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $x = SurveyQuestion::create([
            "type" => "choose" ,
            "ordering"=>1
        ]);
        SurveyQuestionTranslation::create([
            "text" => "هل انت راضي عن خدمتنا لك ؟" ,
            "locale" => "ar" ,
            "survey_question_id" => $x->id
        ]);

        SurveyQuestionTranslation::create([
            "text" => "Are you satisfied with our service to you?" ,
            "locale" => "en" ,
            "survey_question_id" => $x->id
        ]);

        $ans = SurveyAnswer::create([
            "ordering" => 1 ,
            "survey_question_id" => $x->id
        ]);

        SurveyAnswerTranslation::create([
            "locale" =>  "ar" ,
            "text"  =>  "نعم" ,
            "survey_answer_id" =>$ans->id ,
        ]);

        SurveyAnswerTranslation::create([
            "locale" =>  "en" ,
            "text"  =>  "yes" ,
            "survey_answer_id" =>$ans->id ,
        ]);

        $ans2 = SurveyAnswer::create([
            "ordering" => 2 ,
            "survey_question_id" => $x->id
        ]);

        SurveyAnswerTranslation::create([
            "locale" =>  "ar" ,
            "text"  =>  "لا" ,
            "survey_answer_id" =>$ans2->id ,
        ]);

        SurveyAnswerTranslation::create([
            "locale" =>  "en" ,
            "text"  =>  "no" ,
            "survey_answer_id" =>$ans2->id ,
        ]);

        // ======================================

        $x2 = SurveyQuestion::create([
            "type" => "choose" ,
            "ordering"=>1
        ]);
        SurveyQuestionTranslation::create([
            "text" => "ما مدى احتراف المدير التنفيذي في توفير الحل لك؟" ,
            "locale" => "ar" ,
            "survey_question_id" => $x2->id
        ]);

        SurveyQuestionTranslation::create([
            "text" => "How professional is the CEO in providing you with the solution?" ,
            "locale" => "en" ,
            "survey_question_id" => $x2->id
        ]);

        $ans3 = SurveyAnswer::create([
            "ordering" => 1 ,
            "survey_question_id" => $x2->id
        ]);

        SurveyAnswerTranslation::create([
            "locale" =>  "ar" ,
            "text"  =>  "محترف جدا" ,
            "survey_answer_id" => $ans3->id ,
        ]);

        SurveyAnswerTranslation::create([
            "locale" =>  "en" ,
            "text"  =>  "Very professional" ,
            "survey_answer_id" => $ans3->id ,
        ]);
        //====


        $ans4 = SurveyAnswer::create([
            "ordering" => 2 ,
            "survey_question_id" => $x2->id
        ]);

        SurveyAnswerTranslation::create([
            "locale" =>  "ar" ,
            "text"  =>  "احترافي بدرجة كافية" ,
            "survey_answer_id" =>$ans4->id ,
        ]);

        SurveyAnswerTranslation::create([
            "locale" =>  "en" ,
            "text"  =>  "Professional enough" ,
            "survey_answer_id" =>$ans4->id ,
        ]);

        //=====
        $ans5 = SurveyAnswer::create([
            "ordering" => 3 ,
            "survey_question_id" => $x2->id
        ]);

        SurveyAnswerTranslation::create([
            "locale" =>  "ar" ,
            "text"  =>  "لا محترف ولا غير محترف" ,
            "survey_answer_id" =>$ans5->id ,
        ]);

        SurveyAnswerTranslation::create([
            "locale" =>  "en" ,
            "text"  =>  "Neither professional nor unprofessional" ,
            "survey_answer_id" =>$ans5->id ,
        ]);

        // =====

        $ans6 = SurveyAnswer::create([
            "ordering" => 3 ,
            "survey_question_id" => $x2->id
        ]);

        SurveyAnswerTranslation::create([
            "locale" =>  "ar" ,
            "text"  =>  "ليس محترفًا جدًا" ,
            "survey_answer_id" =>$ans6->id ,
        ]);

        SurveyAnswerTranslation::create([
            "locale" =>  "en" ,
            "text"  =>  "Not very professional" ,
            "survey_answer_id" =>$ans6->id ,
        ]);

        //  ========

        $x3 = SurveyQuestion::create([
            "type" => "text" ,
            "ordering" => 4
        ]);
        SurveyQuestionTranslation::create([
            "text" => "هل كانت القطعة المستخدمة فى الصيانه سهل التثبيت؟" ,
            "locale" => "ar" ,
            "survey_question_id" => $x3->id
        ]);

        SurveyQuestionTranslation::create([
            "text" => "Was the part used for maintenance easy to install?" ,
            "locale" => "en" ,
            "survey_question_id" => $x3->id
        ]);

    }
}
