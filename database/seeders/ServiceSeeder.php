<?php

namespace Database\Seeders;

use App\Models\Service;
use App\Models\ServiceTranslation;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service1= Service::create([
            "src" => "images/services/air_conditions.png"
        ]);
        ServiceTranslation::create([
            "name" => "Air conditions" ,
            "locale" => "en" ,
            "service_id" => $service1->id
        ]);
        ServiceTranslation::create([
            "name" => "أجهزه تكييف" ,
            "locale" => "ar" ,
            "service_id" => $service1->id
        ]);
        // ============================

        $service2 = Service::create([
            "src" => "images/services/plumbing_sanitation.png"
        ]);
        ServiceTranslation::create([
            "name" => "Plumbing and sanitation" ,
            "locale" => "en" ,
            "service_id" => $service2->id
        ]);

        ServiceTranslation::create([
            "name" => "السباكه و الصرف الصحي" ,
            "locale" => "ar" ,
            "service_id" => $service2->id
        ]);

        // ============================

        $service3 = Service::create([
            "src" => "images/services/electricity.png"
        ]);
        ServiceTranslation::create([
            "name" => "Electricity" ,
            "locale" => "en" ,
            "service_id" => $service3->id
        ]);

        ServiceTranslation::create([
            "name" => "كهرباء" ,
            "locale" => "ar" ,
            "service_id" => $service3->id
        ]);

        // ============================

        $service4 = Service::create([
            "src" => "images/services/emergency_visit.png"
        ]);
        ServiceTranslation::create([
            "name" => "Emergency visit" ,
            "locale" => "en" ,
            "service_id" => $service4->id
        ]);

        ServiceTranslation::create([
            "name" => "زياره طارئه" ,
            "locale" => "ar" ,
            "service_id" => $service4->id
        ]);

        // ============================

        $service5 = Service::create([
            "src" => "images/services/civil_architectural_works.png"
        ]);
        ServiceTranslation::create([
            "name" => "Civil and architectural works" ,
            "locale" => "en" ,
            "service_id" => $service5->id
        ]);

        ServiceTranslation::create([
            "name" => "أعمال مدنيه و معماريه" ,
            "locale" => "ar" ,
            "service_id" => $service5->id
        ]);

        // ============================

        $service6 = Service::create([
            "src" => "images/services/cleaning_sterilization_services.png"
        ]);
        ServiceTranslation::create([
            "name" => "Cleaning and sterilization services" ,
            "locale" => "en" ,
            "service_id" => $service6->id
        ]);

        ServiceTranslation::create([
            "name" => "خدمات التحكم و الاتصال" ,
            "locale" => "ar" ,
            "service_id" => $service6->id
        ]);

        // ============================

        $service7 = Service::create([
            "src" => "images/services/carpentry_services.png"
        ]);
        ServiceTranslation::create([
            "name" => "Carpentry services" ,
            "locale" => "en" ,
            "service_id" => $service7->id
        ]);

        ServiceTranslation::create([
            "name" => "خدمات النجاره" ,
            "locale" => "ar" ,
            "service_id" => $service7->id
        ]);


        // ============================

        $service8 = Service::create([
            "src" => "images/services/appliances.png"
        ]);
        ServiceTranslation::create([
            "name" => "Appliances" ,
            "locale" => "en" ,
            "service_id" => $service8->id
        ]);

        ServiceTranslation::create([
            "name" => "أجهزه منزليه" ,
            "locale" => "ar" ,
            "service_id" => $service8->id
        ]);

    }
}
