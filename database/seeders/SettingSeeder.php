<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\BrandTranslation;
use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            "site_name" => "Kafo",
            "email" => "kafo@gmail.com",
            "phone" => "908908290",
            "phone2" => "908908291",
            "meta_description" => "meta description",
            "meta_keywords" => " meta keywords",
        ]);

    }
}
