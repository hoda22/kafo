<?php

namespace Database\Seeders;

use App\Enums\PermissionEnum;
use App\Enums\RolesEnum;
use App\Enums\SubscriptionTypeEnum;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::factory()->create([
            'name' => 'Admin User',
            'last_name' => 'User',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456'),
        ]);

//        $bo = User::factory()->create([
//            'name'      => 'User',
//            'email'     => 'user@user.com',
//            'password'  => Hash::make('12345678'),
//        ]);

        foreach (PermissionEnum::values(RolesEnum::SUPER_ADMIN->value) as $item) {
            if ( is_numeric( $item[1] ) ) {
                $role = $item[1] ;
            }else{
                $role = 0 ;
            }
            $permission = Permission::create([ 'name' => $item[0] , 'parent_id' => $role ]);
        }

        foreach (RolesEnum::cases() as $role) {
            $role_created = Role::create(['name' => $role->value]);
            $role_created->syncPermissions(PermissionEnum::values($role->value));
        }

        $admin->assignRole(RolesEnum::SUPER_ADMIN->value);
//        $bo->assignRole(RolesEnum::BUSINESS_OWNER->value);
    }
}
