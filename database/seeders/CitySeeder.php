<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\CityTranslation;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    
    public function run()
    {
        $city1= City::create([]);
        CityTranslation::create([
            "name" => "Riyadh" ,
            "locale" => "en" ,
            "city_id" => $city1->id
        ]);

        CityTranslation::create([
            "name" => "الرياض" ,
            "locale" => "ar" ,
            "city_id" => $city1->id
        ]);
        // ============================

        $city2 = City::create([]);

        CityTranslation::create([
            "name" => "Asir" ,
            "locale" => "en" ,
            "city_id" => $city2->id
        ]);

        CityTranslation::create([
            "name" => "عسير" ,
            "locale" => "ar" ,
            "city_id" => $city2->id
        ]);

        // ============================

        $city3 = City::create([]);

        CityTranslation::create([
            "name" => "Jeddah" ,
            "locale" => "en" ,
            "city_id" => $city3->id
        ]);

        CityTranslation::create([
            "name" => "جده" ,
            "locale" => "ar" ,
            "city_id" => $city3->id
        ]);

        // ============================

        $city4 = City::create([]);
        CityTranslation::create([
            "name" => "AL Madinah" ,
            "locale" => "en" ,
            "city_id" => $city4->id
        ]);

        CityTranslation::create([
            "name" => "المدينه المنوره" ,
            "locale" => "ar" ,
            "city_id" => $city4->id
        ]);

        // ============================

        $city5 = City::create([]);
        CityTranslation::create([
            "name" => "Tabuk" ,
            "locale" => "en" ,
            "city_id" => $city5->id
        ]);

        CityTranslation::create([
            "name" => "تبوك" ,
            "locale" => "ar" ,
            "city_id" => $city5->id
        ]);

        // ============================


    }
}
