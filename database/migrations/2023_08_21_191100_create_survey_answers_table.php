<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('survey_answers', function (Blueprint $table) {
            $table->id();
            $table->integer("ordering")->index();
            $table->integer("survey_question_id")->index();
            $table->timestamps();
        });

        Schema::create('survey_answer_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale')->index();
            $table->text('text');
            $table->foreignId('survey_answer_id')->constrained('survey_answers');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('survey_answers');
    }
};
