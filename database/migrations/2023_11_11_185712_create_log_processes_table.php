<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('log_processes', function (Blueprint $table) {
            $table->id();
            $table->integer("order_id")->index();
            $table->text("code")->index()->nullable();
            $table->text("checkout_id")->nullable();
            $table->text("build_number")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('log_processes');
    }
};
