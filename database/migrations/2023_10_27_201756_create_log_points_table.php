<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('log_points', function (Blueprint $table) {
            $table->id();
            $table->integer("client_id");
            $table->integer("before_points");
            $table->integer("discount_points")->nullable();
            $table->integer("new_points")->nullable();
            $table->integer("order_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('log_points');
    }
};
