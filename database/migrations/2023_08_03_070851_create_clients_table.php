<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string("first_name" , 90);
            $table->string("last_name" , 90);
            $table->string("residency_number" , 100)->nullable();
            $table->string("nationality" , 150)->nullable();
            $table->string("phone" , 150)->nullable();
            $table->string("email");
            $table->boolean("verify")->default(0);
            $table->string("password" , 150);
            $table->string("avatar" , 150)->nullable();
            $table->string("code" , 150)->nullable();
            $table->string("type" )->default("provider");
            $table->string("active")->default("active");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('clients');
    }
};
