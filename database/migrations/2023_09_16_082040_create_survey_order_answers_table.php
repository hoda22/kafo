<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('survey_order_answers', function (Blueprint $table) {
            $table->id();
            $table->integer('survey_order_id')->constrained('survey_order');
            $table->foreignId('question_id')->constrained('survey_questions');

            $table->integer('answer_id')->index()->nullable();
            $table->text('text')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('survey_order_answers');
    }
};
