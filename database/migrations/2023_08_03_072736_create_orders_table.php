<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_id')->constrained('clients');
            $table->foreignId('city_id')->constrained('cities');
            $table->foreignId('service_id')->constrained('services');
            $table->foreignId('provider_id')->constrained('clients')->nullable();



            $table->float('price')->index()->nullable();
            $table->float('amount_paid')->nullable();

            $table->text('client_comment')->nullable();

            $table->text('management_comment')->nullable();
            $table->text('address')->nullable();
            $table->integer('method_payment')->index();
            $table->text('description')->nullable();
            $table->text('reject_reson')->nullable();



//            $table->text('technical_comment')->nullable();
            $table->text('provider_comment')->nullable();
            $table->integer('provider_rate')->nullable();


            $table->date('service_date')->nullable();
            $table->time('service_time')->nullable();


            $table->string('status')->default(1);
            $table->string('provider_status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
