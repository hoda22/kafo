<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('survey_questions', function (Blueprint $table) {
            $table->id();
            $table->string("type");
            $table->integer("ordering")->index();
            $table->timestamps();
        });

        Schema::create('survey_question_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale')->index();
            $table->text('text');
            $table->foreignId('survey_question_id')->constrained('survey_questions');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('survey_questions');
    }
};
