<?php

namespace App\Providers;

use App\Models\Client;
use App\Models\Notification;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{

    public function register(): void
    {
        //
    }

    public function boot(): void
    {

        Validator::extend('range', function ($attribute, $value, $parameters, $validator) {
            // Parameters: $value is the value being validated, $parameters[0] is the lower bound, and $parameters[1] is the upper bound
            $lowerBound = $parameters[0];
            $upperBound = $parameters[1];

            return $value >= $lowerBound && $value <= $upperBound;
        });

        Validator::replacer('range', function ($message, $attribute, $rule, $parameters) {
            // Customize the error message
            return str_replace([':attribute', ':min', ':max'], [$attribute, $parameters[0], $parameters[1]], $message);
        });


        $adminNotifications = Notification::where("notifiable_type" , "App\Models\User")
            ->latest()
            ->get() ;


        $clientIds = Client::where("active","active")->pluck('id') ;
        View::composer(['dashboard.*'], function ($view) use ($adminNotifications , $clientIds) {
            $view->with([
                'adminNotifications' => $adminNotifications,
                'clientIds' => $clientIds,
            ]);
        });

        Paginator::useBootstrap();


//        $firebaseFactory = (new Factory())
//            ->withServiceAccount(config('firebase.service_account'))
//            ->withDatabaseUri(config('firebase.database_url'));
//
//        $database = $firebaseFactory->createDatabase();
    }
}
