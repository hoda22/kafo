<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class UserNotification extends Notification
{
    use Queueable;

    private $orderID;
    private $status;
    private $statusProvider;
    private $msg_en;


    public function __construct( $orderID , $status,  $statusProvider , $msg_en , $type = "order" )
    {
        $this->orderID          = $orderID;
        $this->status           = $status;
        $this->statusProvider   = $statusProvider;
        $this->msg_en           = $msg_en;
        $this->type             = $type;
    }

    public function via(object $notifiable): array
    {
        return ['database'];
    }

    public function toDatabase(object $notifiable)
    {
        return [
            'order_id'          => $this->orderID  ,
            'status'            => $this->status ,
            'status_provider'   => $this->statusProvider,
            'msg_en'            => $this->msg_en,
            'type'              => $this->type,
        ];
    }


}
