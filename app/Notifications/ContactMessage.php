<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ContactMessage extends Notification
{
    use Queueable;
    /**
     * Create a new notification instance.
     */
    public function __construct( $message , $from )
    {
        $this->message = $message;
        $this->from = $from;
    }

    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            //->line($this->message) // Here are the lines you can safely override
            ->from( $this->from )
            ->greeting(__('mails.greeting'))
            ->line($this->message)
            ->line(__('mails.thank'))
            ->salutation(__('mails.salutation'));
    }

}
