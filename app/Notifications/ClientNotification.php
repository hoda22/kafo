<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ClientNotification extends Notification
{
    use Queueable;

    private $messages;
    private $message_en;
    private $title;
    private $set_at;
    /**
     * Create a new notification instance.
     */
    public function __construct($message ,$message_en = null , $title = null )
    {
        $this->message      = $message;
        $this->message_en   = $message_en;
        $this->title        = $title;
        $dateTime = date('Y-m-d H:i:s') ;
        $this->set_at = Carbon::parse(Carbon::parse($dateTime)->setTimezone('UTC')->format('Y-m-d H:i:s'), 'UTC');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['database'];
    }


    public function toDatabase($notifiable)
    {
        return [
            'message'       => $this->message ,
            'message_en'    => $this->message_en ,
            'title'         => $this->title ,
            'set_at'        => $this->set_at
        ];
    }


}
