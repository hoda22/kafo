<?php

namespace App\Enums;

enum ClientTypeEnum: string
{
    case CLIENT = 'client';
    case PROVIDER = 'provider';
}
