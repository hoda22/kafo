<?php

namespace App\Enums;

enum AcTypeEnum: string
{
    case shbak = 'شباك';
    case sbalt = 'سبلت';
    case dolabi = 'دولابي';
    case casit = 'كاسيت مخفي';
    case markazi = 'وحدة تكييـف مركزي';
    case sahrawi = 'صحراوي';
}
