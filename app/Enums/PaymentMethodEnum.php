<?php

namespace App\Enums;

use function Nette\Utils\match;

enum PaymentMethodEnum: int
{

    case CASH = 1;
    case VISA = 2;

//    public function label(): string {
//        return static::getLabel($this);
//    }
//
//    public static function getLabel($value ):string {
//        return match( $value ) {
//            self::CASH => 'Cash' ,
//            self::VISA => 'Visa' ,
//        };
//    }

}
