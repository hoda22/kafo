<?php

namespace App\Enums;

enum ActiveEnum: string
{
    case ACTIVE = 'active';
    case INACTIVE = 'inactive';

}
