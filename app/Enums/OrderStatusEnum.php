<?php

namespace App\Enums;

enum OrderStatusEnum: int
{
    case RequestIsDone = 1; //[1 , 'تم الطلب'
    case UnderRevision =  2; //[2 , 'تحت المراجعه' ];
    case TechnicianAssigned = 3; //=====
    case TechnicianApproved = 4; //[ 3, 'تم تخصيص فني'];
    case TechnicianWay = 5; //[ 4,'الفني في الطريق'];
    case TechnicianArrived = 6; // [ 5,'تم الوصوالفني'];
    case Implemented = 7; //[ 6,'تم التنفيذ'];
    case Cancel = 8; //[ 7,'تم إلغاء'];
    case Reject = 9; //[ 8,'رفض'];
    case RejectFromMangment = 10; //[ 8,'مرفوض من قبل الاداره'];

    //================
    case Modification = 11;

    case AprovedModificationFromClient = 12;
    case RejectedModificationFromClient = 13;
    case AprovedModificationFromAdmin = 14;
    case RejectedModificationFromAdmin = 15;

    case SendCostModificationToClient = 16;
    case CancelFromClient = 17;

}
