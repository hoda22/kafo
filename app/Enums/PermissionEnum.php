<?php

namespace App\Enums;

enum PermissionEnum
{
    public static function values(string $role): array
    {
        $permissions = [
            'Super Admin' => [

                ['clients' , 0],
                ['clients-list' ,1],
                ['clients-create',1],
                ['clients-edit',1],
                ['clients-delete',1],

                //6
                ['users' , 0],
                ['users-list' , 6],
                ['users-create' ,6],
                ['users-edit' ,6],
                ['users-delete' ,6],

                //11
                ['cities', 0],
                ['cities-list', 0],
                ['cities-create' ,10],
                ['cities-edit' ,10],
                ['cities-delete' ,10],

                //21
                ['providers' , 0],
                ['providers-list' ,''],
                ['providers-show' ,''] ,
                ['providers-create' ,''] ,
                ['providers-edit' ,''] ,
                ['providers-delete' ,''] ,

                //25
                ['sliders', 0] ,
                ['sliders-list', ''] ,
                ['sliders-create' ,''] ,
                ['sliders-edit' ,''] ,
                ['sliders-delete' ,''] ,


                ['roles', 0],
                ['roles-list', 0],
                ['roles-create' ,''],
                ['roles-edit' ,''],
                ['roles-delete' ,''],

                ['settings', 0],
                ['settings-edit' ,''],

                ['notifications', 0],
                ['notifications-list' ,''],
                ['notifications-send' ,''],

                ['spare-parts' , 0],
                ['spare-parts-list',''],
                ['spare-parts-create',''],
                ['spare-parts-edit',''],
                ['spare-parts-delete',''],


                ['pages', 0],
                ['pages-list',''],

                ['brands' , 0] ,
                ['brands-list',""],
                ['brands-create',""],
                ['brands-edit',""],
                ['brands-delete',""],


                ['survey' , 0] ,
                ['survey-list',""],
                ['survey-create',""],
                ['survey-edit',""],
                ['survey-delete',""],


                ['services',0],
                ['services-list',''] ,
                ['services-create',''] ,
                ['services-edit',''] ,
                ['services-delete',''] ,

                ['orders' , 0] ,
                ['orders-list', ''] ,
                ['orders-create',''] ,
                ['orders-edit',''] ,
                ['orders-delete',''] ,
            ]

        ];

        return $permissions[$role];
    }
}
