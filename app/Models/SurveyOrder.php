<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SurveyOrder extends Model
{

    use HasFactory;
    protected $table ='survey_order' ;
    protected $guarded = ['id'] ;

//    protected $casts = [
//        'answers' => 'json',
//    ];
//
//    public function question()
//    {
//        return $this->belongsTo(SurveyQuestion::class, 'answers->question_id');
//    }

    public function surveyOrderAnswers() {
        return $this->hasMany(SurveyOrderAnswer::class ,'survey_order_id','id' ) ;
    }

}
