<?php

namespace App\Models;

use App\Traits\ActiveStatusTrait;
use Illuminate\Database\Eloquent\Model;

class AddressClient extends Model
{
    use ActiveStatusTrait;
    protected $guarded = ["id"] ;
    protected $table ='addresses_client' ;
    protected $hidden = ['created_at','updated_at'] ;


    public function city()
    {
        return $this->belongsTo(City::class) ;
    }
}
