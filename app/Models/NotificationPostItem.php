<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationPostItem extends Model
{
    use HasFactory;
    protected $table ='notification_post_items' ;

    protected $guarded = ['id'] ;


    public function client()
    {
        return $this->belongsTo(Client::class) ;
    }
}
