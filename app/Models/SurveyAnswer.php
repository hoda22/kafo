<?php

namespace App\Models;

use App\Traits\ActiveStatusTrait;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
class SurveyAnswer extends Model implements TranslatableContract
{
    use Translatable, ActiveStatusTrait;
    protected $guarded = ['id'] ;
    public $translatedAttributes = ['text'] ;

    public function translatesItems(){
        return $this->hasMany(SurveyQuestionTranslation::class );
    }
}
