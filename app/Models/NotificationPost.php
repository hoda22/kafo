<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationPost extends Model
{
    use HasFactory;
    protected $table ='notafication_posts' ;

    protected $guarded = ['id'] ;


    public function notificationPostItems()
    {
        return $this->hasMany(NotificationPostItem::class ) ;
    }
}
