<?php

namespace App\Models;

use App\Traits\PaymentMethodTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Enums\OrderStatusEnum;
use Illuminate\Database\Eloquent\Builder;

class Order extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    use PaymentMethodTrait ;

    public function service()
    {
        return $this->belongsTo(Service::class) ;
    }

    public function trackingOrder()
    {
        return $this->hasMany(TrackingOrder::class) ;
    }

    public function city()
    {
        return $this->belongsTo(City::class )  ;
    }

    public function addressOrder()
    {
        return $this->belongsTo(AddressClient::class , "address" , "id" )  ;
    }


    public function addressItem()
    {
        return $this->belongsTo(AddressClient::class ,"address_id" )  ;
    }

    public function client()
    {
        return $this->belongsTo(Client::class ) ;
    }

    public function provider()
    {
        return $this->belongsTo(Client::class , "provider_id" ) ;
    }

    public function survey()
    {
        return $this->belongsTo( SurveyOrder::class  ) ;
    }

    public function surveyItem()
    {
        return $this->belongsTo( SurveyOrder::class,"order_id" ,"id" ) ;
    }

    public function sparePart()
    {
        return $this->belongsToMany( SparePart::class , "spare_part_order_details" ,"order_details_id" ) ;
    }

    public function scopeFilter(Builder $query, $filter)
    {
        $filter->filter($query);
    }


    public function scopeCurrentOrders($query)
    {
        return $query->where(function ( $query1 ) {
            $query1->where("status", OrderStatusEnum::TechnicianArrived->value )
                    ->orWhere("status", OrderStatusEnum::TechnicianAssigned->value )
                    ->orWhere("status", OrderStatusEnum::TechnicianApproved->value )
                    ->orWhere("status", OrderStatusEnum::AprovedModificationFromClient->value )
                    ->orWhere("status", OrderStatusEnum::RejectedModificationFromClient->value )
                    ->orWhere("status", OrderStatusEnum::AprovedModificationFromAdmin->value )
                    ->orWhere("status", OrderStatusEnum::RejectedModificationFromAdmin->value )
                    ->orWhere("status", OrderStatusEnum::SendCostModificationToClient->value )
                    ->orWhere("status", OrderStatusEnum::Modification->value )
                    ->orWhere("status", OrderStatusEnum::TechnicianWay->value ) ;
            }) ;
    }


    public function scopeFinishOrders($query)
    {
        return $query->where(function ( $query1 ) {
            $query1->where("status", OrderStatusEnum::Implemented->value );
//                    ->orWhere("status", OrderStatusEnum::TechnicianWay->value ) ;
            }) ;
    }


    public function scopeCancelOrders($query)
    {
        return $query->where(function ( $query1 ) {
            $query1->where("status", OrderStatusEnum::Cancel->value )
                   ->orWhere("status", OrderStatusEnum::Reject->value ) ;
            }) ;
    }

    public function surveyOrder()
    {
        return $this->belongsTo(SurveyOrder::class ,"id") ;
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetails::class )->where("type","new") ;
    }

    public function orderDetailsAll()
    {
        return $this->hasMany(OrderDetails::class );
    }

    public function orderDetailsItems()
    {
        return $this->hasMany(OrderDetails::class )
            ->where("is_edit" , 1)
            ->where(function ( $query ){
                $query->where("admin_status" ,"approved")
                    ->orWhere("admin_status" ,"reject_from_admin")
                    ->orWhere("admin_status" ,"under_revision")
                ;
            })->orWhere("admin_status" ,"rejected_client");

    }


    public function orderDetailsItemsEdit()
    {
        return $this->hasMany(OrderDetails::class )
            ->where(function ( $query ){
                $query->where("is_edit" , 0)
                    ->where("type" , "edit");
            });
            //->orWhere("admin_status" ,"rejected_client");

    }

    public function orderDetailsEdit()
    {
        return $this->hasMany(OrderDetails::class )->where("type","edit") ;
    }

    public function orderDetailsEditNotPaid()
    {
        return $this->hasMany(OrderDetails::class )->where("type","edit")
            ->where("payment_method", 0)->where("paid_status", 0 ) ;
    }

    public function orderImages()
    {
        return $this->hasMany(OrderImage::class );
    }
    
    public function getPaymentMethodAttribute()
    {
        if ( $this->method_payment == 1 ) {
            return __('app.cash');
        }elseif ( $this->method_payment == 2 ) {
            return __('app.visa');
        }else{
            return __('app.not_yet');
        }
    }


    public function getPaidDoneAttribute()
    {
        if ( $this->is_paid == 1 ) {
            return __('app.paid');
        }else{
            return __('app.not_paid');
        }
    }

    public function supServicesInEdit()
    {
        return $this->hasMany(OrderDetails::class ,"order_id" )->where("type","edit") ;
    }

    public function getStatusOrderAttribute()
    {
        if ( $this->status == 1 ) {
            $text = 'تم الطلب' ;
        }elseif (  $this->status == 2 ) {
            $text = 'تحت المراجعه' ;
        } elseif (  $this->status == 3 ) {
            $text = 'تم تخصيص فني' ;
        } elseif (  $this->status == 4 ) {
            $text = 'تم تخصيص فني' ;
        }elseif (  $this->status == 5 ) {
            $text = 'الفني في الطريق' ;
        }elseif (  $this->status == 6 ) {
            $text = 'تم وصول الفني' ;
        }elseif (  $this->status == 7 ) {
            $text = 'تم التنفيذ' ;
        }elseif (  $this->status == 8 ) {
            $text = 'تم الإلغاء من قبل الفني' ;
        }elseif (  $this->status == 9 ) {
            $text = 'رفض من الفني' ;
        }elseif (  $this->status == 10 ) {
            $text = 'مرفوض من قبل الاداره' ;
        }elseif (  $this->status == 11 ) {
            $text = 'طلب تعديل' ;
        }elseif (  $this->status == 12 ) {
            $text = 'تم الموافقه علي التكلفه من العميل' ;
        }elseif (  $this->status == 13 ) {
            $text = 'تم رفض التكلفه الجديده من قبل العميل' ;
        }elseif (  $this->status == 11 ) {
            $text = 'طلب تعديل' ;
        }elseif (  $this->status == 14 ) {
            $text = 'تم ارسال الموافقه علي التعديل للفني' ;
        }elseif (  $this->status == 15 ) {
            $text = 'تم ارسال رفض التكلفه الجديده للفني' ;
        }elseif (  $this->status == 16 ) {
            $text = 'تم ارسال التكلفه الجديده للعميل' ;
        } elseif (  $this->status == 17 ) {
            $text = 'تم الرفض من قبل العميل' ;
        }else{
            $text = 'تم الطلب' ;
        }
        return $text ;
    }
}
