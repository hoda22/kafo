<?php

namespace App\Models;

use App\Traits\ActiveStatusTrait;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use ActiveStatusTrait;
    protected $guarded = ["id"] ;
    protected $hidden = ['created_at','updated_at'] ;

    public function scopeActive($query)
    {
        return $query->where('active',"active");
    }

    public function couponsClients()
    {
        return $this->belongsToMany(Client::class) ;
    }


    public function getClientsAttribute()
    {
        return $this->couponsClients()->pluck('client_id') ;
    }
}
