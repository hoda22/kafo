<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrackingOrder extends Model
{
    protected $guarded = ["id"] ;
    protected $table = 'tracking_orders' ;

    public function provider()
    {
        return $this->belongsTo(Client::class ,"provider_id" );
    }
}
