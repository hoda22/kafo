<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChannelChat extends Model
{
    use HasFactory;
    protected $table = "channels" ;
    protected $guarded = ['id'];
    protected $appends = ['sender_name' ,'client_id'];

    public function scopeAdmin( $query )
    {
        return $query->where('sender_id', 0)->orWhere('receiver_id', 0);
    }

    public function getSenderNameAttribute()
    {
        if ( $this->sender_id == 0 ) {
            $client = Client::find( $this->receiver_id ) ;
        }else{
            $client = Client::find( $this->sender_id ) ;
        }

        return $client->full_name ?? "";
    }

    public function getClientIdAttribute()
    {
        if ( $this->sender_id == 0 ) {
            $sender_id = $this->receiver_id ;
        }else{
            $sender_id = $this->sender_id ;
        }

        return $sender_id;
    }

    public function lastMessage()
    {
        return $this->belongsTo(ChannelMessage::class  ,"id" ,"channel_id")
            ->latest();
    }

    public function channelMessages()
    {
        return $this->hasMany(ChannelMessage::class , 'channel_id') ;
    }

    public function getMessageLastChannelAttribute()
    {

    }
}
