<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMessagesItem extends Model
{
    use HasFactory;
    protected $table ='user_messages_items' ;

    protected $guarded = ['id'] ;


    public function user()
    {
        return $this->belongsTo(User::class) ;
    }
}
