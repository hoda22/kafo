<?php

namespace App\Models;

use App\Traits\ActiveStatusTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;


use App\Enums\ClientTypeEnum;
use Laravel\Sanctum\HasApiTokens;

class Client extends Authenticatable
{
    use HasFactory , HasApiTokens, Notifiable ,SoftDeletes, ActiveStatusTrait;

    protected $guarded = ['id'] ;

    public function scopeProvider($query)
    {
        return $query->where('type', ClientTypeEnum::PROVIDER->value );
    }

    public function scopeClient($query)
    {
        return $query->where('type', ClientTypeEnum::CLIENT->value );
    }

    public function getFullNameAttribute()
    {
        return $this->first_name ." ". $this->last_name ;
    }

    public function getAvatarImageAttribute()
    {
        if ( $this->avatar ){
            return asset($this->avatar );
        }
        return  "/kafo-development/assets/images/logo-sm.png" ;
    }

    public function getClientRateAvgAttribute()
    {
        return $this->providerOrders()->avg('client_rate');
    }

    public function clientOrders(){
        return $this->hasMany(Order::class , "client_id" ) ;
    }

    public function providerOrders(){
        return $this->hasMany(Order::class , "provider_id" ) ;
    }

    public function getProviderHoursOrdersAttribute(){
        $completionTimes = [];
        foreach ( $this->providerOrders()->get() as $key => $item ) {
            $completionTimes[] = $item->completion_time ;
        }
        return array_sum($completionTimes) ;
    }

    public function setPhoneAttribute($value){
        $this->attributes['phone'] = str_replace(' ', '', $value);
    }

    public function scopeFilter(Builder $query, $filter)
    {
        $filter->filter($query);
    }


    public function city()
    {
        return $this->belongsTo(City::class) ;
    }
}
