<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SparePartTranslation extends Model
{
    protected $table = 'spare_part_translations';
    protected $guarded = ['id'];
    public $timestamps = false;

}
