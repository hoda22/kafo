<?php

namespace App\Models;

use App\Traits\ActiveStatusTrait;
use App\Traits\AcTypeTrait;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use App\Enums\ActiveEnum;
use App\Enums\AcTypeEnum;


class Brand extends Model implements TranslatableContract
{
    use Translatable;
    use AcTypeTrait;
    use ActiveStatusTrait;

    protected $guarded =['id'];
    protected $attributes = [
        'active' => 'inactive'
    ];

    public $translatedAttributes = ['name'];

    public function translatesItems(){
        return $this->hasMany(BrandTranslation::class );
    }

    public function spareParts()
    {
        return $this->hasMany(SparePart::class );
    }
}
