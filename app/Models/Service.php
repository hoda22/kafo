<?php

namespace App\Models;

use App\Traits\ActiveStatusTrait;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use App\Enums\ActiveEnum;
class Service extends Model implements TranslatableContract
{
    use Translatable , ActiveStatusTrait;
    protected $guarded =['id'];
    public $translatedAttributes = ['name'];

    public function translatesItems(){
        return $this->hasMany(ServiceTranslation::class );
    }

    public function scopeParent( $query )
    {
        return $query->where("parent_id" , 0)->orWhereNull("parent_id") ;
    }

    public function scopeChildren( $query )
    {
        return $query->where("parent_id" , ">" ,0);
    }

    public function scopeChildrenItems( $query )
    {
        return $query->where("parent_id" , ">" ,0);
    }
    public function scopeActive( $query )
    {
        return $query->where("active" , ActiveEnum::ACTIVE->value);
    }

    public function getActiveStatusAttribute()
    {
        return $this->active == ActiveEnum::ACTIVE->value ? ['className' => 'success' , 'text' => 'نشط'] : ['className' => 'danger' , 'text' =>  'غير نشط'] ;
    }

    public function parentItem()
    {
        return $this->belongsTo( Service::class , "parent_id"  , "id" );
    }

    public function children()
    {
        return $this->hasMany( Service::class , "parent_id"  , "id" );
    }

    public function activeChildren()
    {
        return $this->hasMany( Service::class , "parent_id"  , "id" )
            ->where("active","active");
    }
}
