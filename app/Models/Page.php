<?php

namespace App\Models;

use App\Traits\ActiveStatusTrait;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
class Page extends Model implements TranslatableContract
{
    use Translatable;
    use ActiveStatusTrait;
    protected $guarded =['id'];
    public $translatedAttributes = ['name', 'description'];

    public function translatesItems(){
        return $this->hasMany(PageTranslation::class );
    }

}
