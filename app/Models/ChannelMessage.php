<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChannelMessage extends Model
{
    use HasFactory;
    protected $table = "channel_messages" ;
    protected $guarded = ['id'];

    public function sender()
    {
        return $this->belongsTo(Client::class , "sender_id", "id");
    }

    public function getAvatar()
    {
        $baseUrl = "https://mragaey.com/kafo/";
        if ( $this->sender_id == 0 ) {
            $avatar = $baseUrl."assets/images/logo.png" ;
        }else {
            $avatar = $baseUrl.$this->sender->avatar;

        }
        return $avatar ;
    }
}
