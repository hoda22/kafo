<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class OrderDetails extends Model
{
    protected $table = 'order_details' ;
    use HasFactory;
    protected $guarded = ['id'];
    protected $hidden = ["created_at","updated_at"] ;

    protected $with = "service" ;
    protected $appends = ['service_item','service_parent'] ;

    public function service()
    {
        return $this->belongsTo(Service::class) ;
    }


    public function services()
    {
        return explode(',', $this->service_id);
    }

    public function getServices()
    {
        return Service::whereIn( 'id', $this->services() )->get();
    }


    public function sparePartsOrderDetials()
    {
        return $this->hasMany(SparePartOrderDetails::class ,"order_details_id") ;
    }

    public function getServiceItemAttribute()
    {
        return optional( $this->service() )->first();
    }

    public function getServiceParentAttribute()
    {
        return optional( optional( $this->service )->parentItem() )->first() ?? "";
    }

    public function getImagesAttribute($value)
    {
        return unserialize($value) ?? [];
    }
}
