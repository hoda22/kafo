<?php

namespace App\Models;

use App\Enums\ActiveEnum;
use App\Traits\ActiveStatusTrait;
use App\Traits\AcTypeTrait;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;


class SparePart extends Model implements TranslatableContract
{
    use Translatable;
    use AcTypeTrait;
    use ActiveStatusTrait;

    protected $guarded =['id'];
    public $translatedAttributes = ['name'];

    public function translatesItems(){
        return $this->hasMany(SparePartTranslation::class );
    }

    public function scopeActive($query)
    {
        return $query->where('active', ActiveEnum::ACTIVE->value);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function orders()
    {
        return $this->hasMany(SparePartOrder::class ) ;
    }


    public function service()
    {
        return $this->belongsTo(Service::class ) ;
    }

    public function subService()
    {
        return $this->belongsTo(Service::class ) ;
    }
}
