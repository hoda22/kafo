<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyAnswerTranslation extends Model
{
    protected $table = 'survey_answer_translations';
    protected $guarded = ['id'];
    public $timestamps = false;
}
