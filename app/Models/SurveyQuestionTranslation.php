<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionTranslation extends Model
{
    protected $table = 'survey_question_translations';
    protected $guarded = ['id'];
    public $timestamps = false;
}
