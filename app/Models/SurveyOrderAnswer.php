<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class SurveyOrderAnswer extends Model
{
    protected $table ='survey_order_answers';
    protected $guarded = ["id"] ;
    protected $hidden = ['created_at','updated_at'] ;

    public function question()
    {
        return$this->belongsTo(SurveyQuestion::class) ;
    }

    public function answer()
    {
        return$this->belongsTo(SurveyAnswer::class) ;
    }
}