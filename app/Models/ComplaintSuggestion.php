<?php

namespace App\Models;

use App\Traits\ActiveStatusTrait;
use Illuminate\Database\Eloquent\Model;

class ComplaintSuggestion extends Model
{
    protected $guarded = ["id"] ;
    protected $hidden = ['created_at','updated_at'] ;

    protected $with =['client'] ;

    public function client()
    {
        return $this->belongsTo(Client::class) ;
    }

    public function scopeItemsAvailable( $query )
    {
        return $query->where("type","suggestion")
            ->orWhere("type","complaint");
    }


    public function answer()
    {
        return $this->belongsTo(ComplaintSuggestion::class ,"replay_id") ;
    }
}
