<?php

namespace App\Models;

use App\Traits\ActiveStatusTrait;
use Illuminate\Database\Eloquent\Model;

class OrderImage extends Model
{
    use ActiveStatusTrait;
    protected $guarded = ["id"] ;
    protected $table ='order_images' ;
    protected $hidden = ['created_at','updated_at'] ;

}
