<?php

namespace App\Models;

use App\Traits\ActiveStatusTrait;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use ActiveStatusTrait;
    protected $guarded = ["id"] ;
    protected $hidden = ['created_at','updated_at'] ;

//    protected $with = ['attachment'] ;

//    public function attachment(){
//        return $this->belongsTo(Attachment::class ,'id','type_id' )
//            ->where("type","slider") ;
//    }

    public function scopeActive($query)
    {
        return $query->where('active',"active");
    }
}
