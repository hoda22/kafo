<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $guarded = ["id"] ;

    public function children()
    {
        return $this->hasMany(Permission::class , 'parent_id' );
    }
}
