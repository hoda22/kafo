<?php

namespace App\Models;

use App\Traits\ActiveStatusTrait;
use Illuminate\Database\Eloquent\Model;

class SparePartOrderDetails extends Model
{
    protected $with=['sparePart'];
    protected $guarded = ["id"] ;
    protected $hidden = ['created_at','updated_at'] ;
    protected $table ='spare_part_order_details' ;


    public function sparePart()
    {
        return $this->belongsTo(SparePart::class) ;
    }
}
