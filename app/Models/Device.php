<?php

namespace App\Models;

use App\Traits\ActiveStatusTrait;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $guarded = ["id"] ;
    protected $hidden = ['created_at','updated_at'] ;

}
