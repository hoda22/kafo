<?php

namespace App\Models;

use App\Traits\ActiveStatusTrait;
use Illuminate\Database\Eloquent\Model;

class UserMessage extends Model
{
    use ActiveStatusTrait;
    protected $guarded = ["id"] ;
    protected $hidden = ['created_at','updated_at'] ;


    public function userMessagesItems()
    {
        return $this->hasMany(UserMessagesItem::class) ;
    }
}
