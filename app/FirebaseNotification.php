<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class FirebaseNotification extends Model
{
    public function sendNote($tokens = [], $data = [] , $contents , $title = null , $deviceType = 0 , $note_type = "order" , $chatData=[] ){
        //FCM api URL
        $url = 'https://fcm.googleapis.com/fcm/send';
        //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
        $server_key = 'AAAAE6pXTBo:APA91bHWjI-R7fDDH3o3wPj1YOLOLcmpsb7l33QoRjbU-dwDOnwPi5krFcvkzf3suQDru5NKl52yhitI_CHHi6KN9u7PAgItbxvmLSkhjEH8nYW-tmVqSSDeUw7YxP-vR6eLWWF4J7vz';

        //header with content_type api key
        $headers = array(
            'Content-Type:application/json; charset=utf-8',
            'Authorization:key='.$server_key
        );

        $notification_data = [

        ] ;

        $notification = [
            'body' => $contents,
//            'click_action' => 'MAIN_ACTIVITY',
            'sound'             => true,
            'type'              =>'notifications',
            'title'             => $title,
            'status_code'       => isset( $data['status_code'] ) ? $data['status_code'] : 0 ,
            'order_id'          => isset( $data['order_id'] ) ? $data['order_id'] : 0 ,

            'chat_data_img'             => isset( $chatData['avatar'] ) ? $chatData['avatar'] : "" ,
            'chat_data_name'            => isset( $chatData['name'] ) ? $chatData['name'] : "" ,
            'chat_data_message_type'    => isset( $chatData['message_type'] ) ? $chatData['message_type'] : "" ,

            'note_type'         => $note_type ,
        ];

        $extraNotificationData = ['type'=>'notifications'];

        $fields = [
            'registration_ids' => $tokens, //multple token array
            //'to'        => $tokens, //single token
//            'notification' => $notification,
            'data' => $notification ,

            "priority"=> "high"
        ];

        if ($deviceType == 1) { // for ios
            $fields['notification'] = $notification ;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

//        dd( $result ) ;
        return $result;
    }

}
