<?php

namespace App\Services;

use App\Models\Page;

class PageService
{
    protected $model;
    public function __construct()
    {
        $this->model = new Page();
    }

    public function index($data)
    {
        $search = "";
        if ( isset( $data['name'] ) ){
            $search = $data['name'] ;
        }
        return $this->model->latest()
            ->whereTranslationLike('name' , '%'.$search.'%')
            ->paginate();
    }



    public function store($data)
    {

        $this->model->create( [
                'en' => [
//                    'name'       => $data['name_en'],
                    'description'       => $data['description_en'],
                ],
                'ar' => [
//                    'name'       => $data['name'],
                    'description'       => $data['description'] ,
                ]
       ] );
    }

    public function update( $id , $data)
    {
        $item = $this->model->findOrFail($id);
        $item->update([
            'en' => [
//                'name'        => $data['name_en'],
                'description' => $data['description_en'],
            ],
            'ar' => [
//                'name'        => $data['name'],
                'description' => $data['description'],
            ]
        ]);
    }

    public function delete($id)
    {
        $item = $this->model->findOrFail($id);
        foreach ( $item->translatesItems as  $translate){
            $translate->delete() ;
        }
        $item->delete();
    }
}
