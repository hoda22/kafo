<?php

namespace App\Services;

use App\Models\Setting;

class SettingService
{
    protected $model;

    public function __construct()
    {
        $this->model = new Setting();
    }

    public function index()
    {
        return $this->model->first();
    }

    public function update( $data )
    {
        $item = $this->model->firstOrNew();
        $item->update($data);
    }
}
