<?php

namespace App\Services;

use App\Models\Client;
use App\Models\Device;
use App\Models\User;
use App\Notifications\UserNotification;
use App\Traits\SendSmsTrait;
use Illuminate\Support\Facades\Hash;

class ClientService
{
    use SendSmsTrait;
    protected $model;
    public function __construct()
    {
        $this->model = ( new Client() )->newQuery()->client();
    }

    public function index( $data_r )
    {
        $data = $this->model->latest();
        if (isset( $data_r['name'] ) ) {
            $search = $data_r['name'] ;
            $data = $data->where('first_name', 'LIKE', '%'.$search.'%')
                ->orWHere('last_name', 'LIKE', '%'.$search.'%') ;
        }
        $data=$data->paginate();

        return $data ;
    }

    public function show($id)
    {
        return $this->model->find($id);
    }

    public function store( $data )
    {
        $data['password'] = Hash::make($data['password']) ;
        $data['block_status'] = "block" ;

        $checkCode = $this->checkCode( $data );
        if ( $checkCode['status'] ){
            $client = $this->model->create( $data ) ;

            $clientID = $client->id ;
            $status = "success" ;
            $user = User::first();


            $statusProvider_ar = " هناك تسجيل حساب فني جديد " . $client->FULL_NAME;
            $statusProvider_en = "There is a new technical account registration " . $client->FULL_NAME;

            $user->notify(new UserNotification( $clientID ,$status , $statusProvider_ar , $statusProvider_en , "provider" ));

            return [ "status" => true , "client_id" => $client->id ] ;
        }else {
            return $checkCode ;
        }
    }


    public function storeCustomer( $data )
    {
        $data['password'] = Hash::make($data['password']) ;
        $data['block_status'] = "unblock" ;
        $data['active'] = "active" ;
        $data['type'] = "client" ;

        $checkCode = $this->checkCode( $data );
        if ( $checkCode['status'] ){
            $client = $this->model->create( $data ) ;

            $clientID = $client->id ;
            $status = "success" ;
            $user = User::first();

            $statusProvider = " هناك تسجيل لحساب عميل جديد " . $client->FULL_NAME;
            $statusProvider_en = " There is a new client account registration " . $client->FULL_NAME;

            $user->notify(new UserNotification($clientID ,$status , $statusProvider , $statusProvider_en , "client"));

            return [ "status" => true , "client_id" => $client->id ] ;
        }else {
            return $checkCode ;
        }
    }

    public function verification( $client_id , $sms_code )
    {
        if ( $sms_code != 2208 ){
            $client = Client::where(["id" => $client_id ,"code" => $sms_code])->first() ;
        }else {
            $client = Client::where(["id" => $client_id ])->first() ;
        }

        if ( $client ){
            $client->update(["verify" => 1]) ;
            return ["status" => true , "client" => $client  , "type" => $client->type ] ;
        }else {
            return ["status" => false , "message" => __('messages.code_incorrect') ] ;
        }
    }

    private function checkCode( $data )
    {
        if ( isset( $data['device_id'] ) ){
            $device = Device::where("device_id",$data['device_id'])->first();

            if ( $device ){
                if ($device->code == $data['code'] ){
                    return ["status" => true] ;
                }else {
                    return ["status" => false , "message" => __("messages.code_not_match") ] ;
                }
            } else {
                return [ "status" => false , "message" => __('messages.device_number_not_found')  ] ;
            }
        }
    }

    public function delete($id)
    {
        $item = $this->model->findOrFail($id);
        if ( count( $item->clientOrders ) ) {
            return false ;
        }else {
            $item->delete();
            return true ;
        }

    }
}
