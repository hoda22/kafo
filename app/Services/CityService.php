<?php

namespace App\Services;

use App\Models\City;

class CityService
{
    protected $model;
    public function __construct()
    {
        $this->model = new City();
    }

    public function index($data)
    {
        $query = $this->model->latest();
        if ( isset($data['name'] ) ){
            $query = $query->whereTranslationLike('name' , '%'.$data['name'].'%') ;
        }
        return $query->paginate(20);
    }

    public function store($data)
    {
        $this->model->create($data + [
            'en' => [
                'name'       => $data['name_en'],
            ],
            'ar' => [
                'name'       => $data['name'],
            ]
       ]);
    }

    public function update( $id , $data)
    {

        if ($data["zone_polygon"] === null) {
            unset($data["zone_polygon"]);
        }
        $item = $this->model->findOrFail($id);
        $item->update($data +[
            'en' => [
                'name'       => $data['name_en'],
            ],
            'ar' => [
                'name'       => $data['name'],
            ]
        ]);
    }

    public function show( $id )
    {
        return $this->model->findOrFail($id);
    }


    public function delete($id)
    {
        $item = $this->model->findOrFail($id);
        foreach ( $item->translatesItems as  $translate){
            $translate->delete() ;
        }
        $item->delete();
    }

    public function getCitiesByCountyId( $county_id )
    {
        return $this->model->where("county_id",$county_id)->get() ;
    }
}
