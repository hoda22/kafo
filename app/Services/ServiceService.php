<?php

namespace App\Services;

use App\Models\Service;
use App\Traits\UploadFile;

class ServiceService
{
    use UploadFile;
    protected $model;
    public function __construct()
    {
        $this->model = new Service();
    }

    public function index($data)
    {
        $query = $this->model->withCount("children")->latest();
        if ( isset($data['name'] ) ){
            $query = $query->whereTranslationLike('name' , '%'.$data['name'].'%') ;
        }

        if ( isset($data['active'] ) ){
            $query = $query->where('active' , $data['active'] ) ;
        }

        if ( isset($data['service_id'] ) ){
            $query = $query->where('id' , $data['service_id'] )
                ->orWhere("parent_id",$data['service_id']) ;
        }
        return $query->parent()->paginate();
    }

    public function store($data)
    {
        if ( isset( $data['src'] ) ){
            $file = $data['src'] ;
            $data["src"] = $this->upload($file , "services");
        }


        $this->model->create( $data + [
            'en' => [ 'name' => $data['name_en'] ],
            'ar' => [ 'name' => $data['name'] ]
       ] );
    }

    public function update( $id , $data )
    {
        $item = $this->model->findOrFail($id);
        if (  isset($data['src'] ) ) {
            $file = $data['src'] ;
            $data["src"] = $this->upload($file , "services");
        }

        $item->update($data+[
            'en' => [
                'name'       => $data['name_en'],
            ],
            'ar' => [
                'name'       => $data['name'],
            ]
        ]);
    }

    public function delete($id)
    {
        $item = $this->model->findOrFail($id);

        if ( count( $item->children ) ) {
            return false ;
        }else {
            foreach ( $item->translatesItems as  $translate){
                $translate->delete() ;
            }
            $item->delete();
            return true ;
        }
    }


    public function supServices($id , $data)
    {
        $query = $this->model->where("parent_id",$id)->latest();
        if ( isset($data['name'] ) ){
            $query = $query->whereTranslationLike('name' , '%'.$data['name'].'%') ;
        }

        if ( isset($data['active'] ) ){
            $query = $query->where('active' , $data['active'] ) ;
        }
        return $query->paginate();
    }
}
