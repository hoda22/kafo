<?php

namespace App\Services;

use App\Models\Coupon;

class CouponService
{
    protected $model;
    public function __construct()
    {
        $this->model = new Coupon();
    }

    public function index($data)
    {
        $query = $this->model->latest();
        if ( isset($data['name'] ) ) {
            $query = $query->whereTranslationLike('name' , '%'.$data['name'].'%') ;
        }
        return $query->paginate(20);
    }

    public function store($data)
    {
        $item = $this->model->create($data);
        if ( isset( $data['clients_id'] ) && count( $data['clients_id'] )  ) {
            $item->couponsClients()->sync($data['clients_id']);
        }
    }

    public function update( $id , $data)
    {
        $item = $this->model->findOrFail($id);
        $item->update($data );

        if ( isset( $data['clients_id'] ) && count( $data['clients_id'] )  ) {
            $item->couponsClients()->sync($data['clients_id']);
        }
    }

    public function delete($id)
    {
        $item = $this->model->findOrFail($id);
        $item->delete();
    }
}
