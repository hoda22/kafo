<?php

namespace App\Services;

use App\Models\Slider;
use App\Traits\UploadFile;

class SliderService
{
    use UploadFile;
    protected $model;
    public function __construct()
    {
        $this->model = new Slider();
    }

    public function index()
    {
        return $this->model->latest()->paginate();
    }

    public function store($data)
    {
        if ( isset($data['src']) ){
            $file = $data['src'] ;
            $data["src"] = $this->upload($file , "sliders"  , 600 , 300 , false);
        }
        $this->model->create($data);
    }

    public function update( $id , $data)
    {
        $item = $this->model->findOrFail($id);
        if (  isset($data['src'] ) ) {
            $file = $data['src'] ;
            $data["src"] = $this->upload($file , "sliders");
        }
        $item->update($data);
    }

    public function delete($id)
    {
        $item = $this->model->findOrFail($id);
        $item->delete();
    }
}
