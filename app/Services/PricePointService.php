<?php

namespace App\Services;

use App\Models\PricePoint;

class PricePointService
{
    protected $model;
    public function __construct()
    {
        $this->model = new PricePoint();
    }

    public function index($data)
    {
        $query = $this->model->latest();
        if ( isset($data['name'] ) ) {
            $query = $query->whereTranslationLike('name' , '%'.$data['name'].'%') ;
        }
        return $query->paginate(20);
    }

    public function store($data)
    {
        $this->model->create($data);
    }

    public function update( $id , $data)
    {
        $item = $this->model->findOrFail($id);
        $item->update($data );
    }

    public function delete($id)
    {
        $item = $this->model->findOrFail($id);
        $item->delete();
    }
}
