<?php

namespace App\Services;

use App\Models\SurveyAnswer;
use App\Models\SurveyAnswerTranslation;
use App\Models\SurveyQuestion;
use App\Models\SurveyQuestionTranslation;

class SurveyQuestionService
{
    protected $model;
    public function __construct()
    {
        $this->model = new SurveyQuestion();
    }

    public function index()
    {
        $query = $this->model->with("answers")->latest()->paginate();
        return $query ;
    }

    public function store($data)
    {
        $qu = $this->model->create($data + [
            'en' => [
                'text'       => $data['text_en'],
            ],
            'ar' => [
                'text'       => $data['text'],
            ]
       ]);

        $i = 0 ;
        if ( isset($data['answers']) && count( $data['answers'] ) ){
            foreach ( $data['answers']["ar"] as $key => $ans ){
                $item = ["survey_question_id" => $qu->id , "ordering" => $data['answers_ordering'][$i] ] ;
                SurveyAnswer::create($item + [
                    'en' => [
                        'text'       => $data['answers']["en"][$key],
                    ],
                    'ar' => [
                        'text'       => $ans,
                    ]
                ]);
                $i++ ;
            }
        }


    }

    public function update($id ,$data)
    {
        $item = $this->model->findOrFail($id);
        $data['ordering'] = $data['ordering1'];

        $item->update($data+[
            'en' => [
                'text'       => $data['text_en'],
            ],
            'ar' => [
                'text'       => $data['text'],
            ]
        ]);

//        survey-questions

        if ( isset( $data['item_id_s']) && count( $data['item_id_s'] ) ) {
            foreach ($data['item_id_s'] as $key => $val) {
                $ss = SurveyAnswerTranslation::where("id", $key)->first();
                $ss->text = $val;
                $ss->save();
            }
        }

        if ( isset( $data['ordering_ans']) && count( $data['ordering_ans'] ) ) {
            foreach ($data['ordering_ans'] as $key_ => $_ans) {
                $ansItem = SurveyAnswer::find($key_);
                if ($ansItem) {
                    $ansItem->ordering = $_ans;
                    $ansItem->save();
                }
            }
        }

        //========= delete ansers

//         dd( $data['delete_ans'] );

        if ( isset( $data['delete_ans']) && count( $data['delete_ans'] ) ) {
            foreach ($data['delete_ans'] as $delete_ans) {
                $ans = SurveyAnswer::find($delete_ans);

                foreach ($ans->translatesItems as $translate) {
                    $translate->delete();
                }

                $ans->delete();
            }
        }

        //===========================

        // add new answers
        $i = 0 ;
        if ( isset( $data['answers']) && count( $data['answers'] ) ){
            foreach ( $data['answers']["ar"] as $key => $ans ){
                $item = ["survey_question_id" => $item->id , "ordering" => $data['answers_ordering'][$i] ] ;
                SurveyAnswer::create($item + [
                        'en' => [
                            'text'       => $data['answers']["en"][$key],
                        ],
                        'ar' => [
                            'text'       => $ans,
                        ]
                    ]);
                $i++ ;
            }
        }
    }

    public function delete($id)
    {
        $item = $this->model->findOrFail($id);

        if ( count($item->surveyOrderAnswers ) ) {
            return false ;
        } else {
//            foreach ( $item->translatesItems as  $translate){
//                $translate->delete() ;
//            }
//            $item->delete();
            return true ;
        }
    }
}
