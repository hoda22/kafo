<?php

namespace App\Services;

use App\Models\Order;

class OrderService
{
    protected $model;
    public function __construct()
    {
        $this->model = new Order();
    }

    public function index($filter_data)
    {
        $data = $this->model ;
        if ( isset($filter_data['client_id'] ) ) {
            $data = $data->where("client_id",$filter_data['client_id']);
        }

        if ( isset($filter_data['provider_id'] ) ) {
            $data = $data->where("provider_id",$filter_data['provider_id']);
        }

        if ( isset($filter_data['status'] ) ) {
            $data = $data->where("status",$filter_data['status']);
        }

        if ( isset($filter_data['paymentMethod'] ) ) {
            $data = $data->where("method_payment",$filter_data['paymentMethod']);
        }

        return $data->with("city","client","provider","orderDetails")
            ->orderBy("id","DESC")
            ->whereNot("status" , 0)
            ->paginate(50);
    }

    public function show($id)
    {
        return $this->model->with('surveyOrder')->find($id);
    }
}
