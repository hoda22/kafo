<?php

namespace App\Services;


use App\Models\SparePart;

class SparePartService
{
    protected $model;
    public function __construct()
    {
        $this->model = new SparePart();
    }

    public function index($data)
    {
        $query = $this->model->with('brand','service','subService')->latest();

        if ( isset($data['name'] )){
            $query = $query->whereTranslationLike('name' , '%'.$data['name'].'%');
        }

        $query = $query->paginate();

        return $query ;
    }

    public function store($data)
    {
        $this->model->create($data+[
            'en' => [
                'name'       => $data['name_en'],
            ],
            'ar' => [
                'name'       => $data['name'],
            ]
       ]);
    }

    public function update($id ,$data)
    {
        $item = $this->model->findOrFail($id);

        $item->update($data+[
            'en' => [
                'name'       => $data['name_en'],
            ],
            'ar' => [
                'name'       => $data['name'],
            ]
        ]);
    }

    public function delete($id)
    {
        $item = $this->model->findOrFail($id);
        foreach ( $item->translatesItems as  $translate){
            $translate->delete() ;
        }
        $item->delete();
    }
}
