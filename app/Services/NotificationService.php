<?php

namespace App\Services;

use App\FirebaseNotification;
use App\Models\Client;
use App\Models\NotificationPost;
use App\Models\NotificationPostItem;
use App\Notifications\ClientNotification;
use App\Notifications\SendMessageNotification;
use App\Notifications\SendNotification;
use App\Traits\SendSmsTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;

class NotificationService
{
    protected $model;
    use SendSmsTrait;

    public function __construct()
    {
        $this->model = new NotificationPost();
    }

    public function index($data)
    {
        $query = $this->model->latest();
        if ( isset( $data['category'] ) ){
            $query->where('category' , $data['category'] ) ;
        }
        return $query->paginate();
    }

    public function store($data)
    {
        $dateTime = date('Y-m-d H:i:s') ;
        $dataValid = $data ;
        $dataValid['set_at'] = Carbon::parse(Carbon::parse($dateTime)->setTimezone('UTC')->format('Y-m-d H:i:s'), 'UTC');
        $notificationPost = $this->model->create($dataValid);


        $clients = Client::query();
        if ( $data['category'] == 'clients' ) {

            $clients = $clients->client();
        } else if ($data['category'] == 'providers'){
            $clients = $clients->provider();
        }
        $clients = $clients
            ->where("active", "active")
            ->where("block_status","unblock")
            ->get() ;

        $message = $data['text'] ;
        $message_en = $data['text'] ;
        /* */
        if ( $data['how_send'] == 'notice' ) {
            Notification::send( $clients , new ClientNotification( $message , $message_en ));

            foreach ( $clients as $client ) {
                if ( $client->player_id ) {
                    NotificationPostItem::create(["client_id" => $client->id , "notification_post_id" => $notificationPost->id ]);
                    $notify_fire = new FirebaseNotification();
                    $notify_fire->sendNote([$client->player_id], 'Kafo', $message ,__('app.application_management') );
                }
            }
        }else if ( $data['how_send'] == 'email' ) { // email

            foreach ( $clients as $client ) {
                if ($client->email) {
                    $client->notify(new SendNotification($message));
                    NotificationPostItem::create(["client_id" => $client->id, "notification_post_id" => $notificationPost->id]);

                    $client->notify(new SendMessageNotification($message));
                }
            }
        } else { // sms
            foreach ( $clients as $client ) {
                $sendCode = $this->send($message, $client->phone);
//                dd( $sendCode ) ;
//                dd($client->phone   ) ;

                info("Status send > ". $sendCode) ;
                if ($sendCode) {
                    NotificationPostItem::create(["client_id" => $client->id, "notification_post_id" => $notificationPost->id]);
                }
            }
        }

    }
//
//$table->string("name");
//$table->string("lat");
//$table->string("lon");
//$table->string("address");

    public function show($id)
    {
        return $this->model->with("notificationPostItems.client")->find($id);
    }
}