<?php

namespace App\Services;

use App\Models\Brand;
use App\Models\ComplaintSuggestion;
use App\Traits\UploadFile;

class ComplaintSuggestionService
{
    use UploadFile;
    protected $model;
    public function __construct()
    {
        $this->model = new ComplaintSuggestion();
    }

    public function index($data)
    {
        $query = $this->model->latest();
        if ( isset( $data['name'] ) )  {
            $query->whereTranslationLike('name' , '%'.$data['name'].'%') ;
        }
        return $query->itemsAvailable()->paginate();
    }

    public function replay($data)
    {
        $item = $this->model->create($data) ;

        $org = $this->model->find($data['replay_id']);
        $org->replay_id = $item->id;
        $org->save();
    }

    public function update($data, $id)
    {
        $item = $this->model->findOrFail($id);
        if (  isset($data['src'] ) ) {
            $file = $data['src'] ;
            $data["src"] = $this->upload($file , "brands");
        }

        $item->update($data+[
            'en' => [
                'name'       => $data['name_en'],
            ],
            'ar' => [
                'name'       => $data['name'],
            ]
        ]);
    }

    public function delete($id)
    {
        $item = $this->model->with('spareParts')->findOrFail($id);
        if ( count($item->spareParts) ) {
            return false ;
        }else {
            foreach ( $item->translatesItems as  $translate){
                $translate->delete() ;
            }
            $item->delete();
            return true ;
        }

    }

    public function getCitiesByCountyId( $county_id )
    {
        return $this->model->where("county_id",$county_id)->get() ;
    }
}
