<?php

namespace App\Services;

use App\Models\Client;
use App\Models\NotificationPost;
use App\Models\NotificationPostItem;
use App\Models\User;
use App\Models\UserMessage;
use App\Models\UserMessagesItem;
use App\Notifications\AdminNotification;
use App\Notifications\ClientNotification;
use App\Notifications\UserMessageNotification;
use App\Traits\SendSmsTrait;
use Illuminate\Support\Facades\Notification;

class UserMessageService
{
    protected $model;
    use SendSmsTrait;
    public function __construct()
    {
        $this->model = new UserMessage();
    }

    public function index($data)
    {
        $query = $this->model->latest();
        if ( isset( $data['category'] ) ){
            $query->where('category' , $data['category'] ) ;
        }
        return $query->paginate();

    }

    public function store( $data )
    {
        $userMessage = $this->model->create($data);
        $users = User::query();

        $users = $users->whereNot("id",auth()->id())->where("active", "active")->get() ;

        $message = $data['text'] ;
        if ( $data['how_send'] == 'email' ) { // email
            foreach ( $users as $user ) {
                UserMessagesItem::create(["user_id" => $user->id , "user_message_id" => $userMessage->id ]);
                // To Mail
                $user->notify(new UserMessageNotification($message));
            }
        } else { // sms
            foreach ( $users as $user ) {
                if ($user->phone){
                        $sendCode = $this->send($message, $user->phone);
                    if ($sendCode) {
                        UserMessagesItem::create(["user_id" => $user->id, "user_message_id" => $userMessage->id]);
                    }
                }
            }
        }
    }

    public function show($id)
    {
        return $this->model->with("userMessagesItems.user")->find($id);
    }

//    public function delete($id)
//    {
//        $item = $this->model->findOrFail($id);
//        $item->delete();
//    }
}