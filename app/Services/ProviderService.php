<?php

namespace App\Services;

use App\Models\Client;

class ProviderService
{
    protected $model;
    public function __construct()
    {
        $this->model = ( new Client() )->newQuery()->provider();
    }

    public function index( $data_r )
    {
        $data = $this->model->orderBy("id","DESC");
            if (isset( $data_r['name'] ) ) {
                $search = $data_r['name'] ;
                $data = $data->where('first_name', 'LIKE', '%'.$search.'%')
                    ->orWHere('last_name', 'LIKE', '%'.$search.'%') ;
            }
         return $data->paginate();
    }

    public function show($id)
    {
        return $this->model->find($id);
    }

    public function delete($id)
    {
        $item = $this->model->findOrFail($id);
        if ( count( $item->clientOrders ) ) {
            return false ;
        }else {
            $item->delete();
            return true ;
        }
    }
}
