<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class RangeInDatabase implements Rule
{
    protected $table;
    protected $column;

    public function __construct($table, $column)
    {
        $this->table = $table;
        $this->column = $column;
    }

    public function passes($attribute, $value)
    {

        $result = DB::table($this->table)
            ->where("from" ,"<", request()->from )
            ->where("to",">", request()->from )
            ->first();


        $result2 = DB::table($this->table)
            ->where("from" ,"<", request()->to )
            ->where("to",">", request()->to )
            ->first();

        if ( $result ) {
            return false;
        }
        return true;
    }

    public function message()
    {
        return __('app.validation.field_from_range');
    }
}