<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class RangeITonDatabase implements Rule
{
    protected $table;
    protected $column;

    public function __construct($table, $column)
    {
        $this->table = $table;
        $this->column = $column;
    }

    public function passes($attribute, $value)
    {
        $result2 = DB::table($this->table)
            ->where("from" ,"<", request()->to )
            ->where("to",">", request()->to )
            ->first();

        if ( $result2  ) {
            return false;
        }
        return true;
    }

    public function message()
    {
        return __('app.validation.field_to_range');
    }
}