<?php

namespace App\Traits;


trait AcTypeTrait
{
    public function getAcTypeTextAttribute()
    {
        $text= '' ;
        if ( $this->ac_type == 'shbak' )
        {
            $text = 'شباك' ;
        } else if( $this->ac_type == 'sbalt' )
        {
            $text = 'سبلت' ;
        } else if( $this->ac_type == 'dolabi' )
        {
            $text = 'دولابي' ;
        }
        else if( $this->ac_type == 'casit' )
        {
            $text = 'كاسيت مخفي' ;
        }
        else if( $this->ac_type == 'markazi' )
        {
            $text = 'وحدة تكييـف مركزي' ;
        }
        else
        {
            $text = 'صحراوي' ;
        }

        return $text;
    }
}