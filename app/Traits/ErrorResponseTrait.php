<?php

namespace App\Traits;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
trait ErrorResponseTrait
{
    protected function failedAuthorization()
    {
        if (in_array('api', $this->segments())) {
            throw new HttpResponseException(response()->json([
                'success' => false,
                'message' => 'Unauthorized',
            ], 403));
        }

        parent::failedAuthorization();
    }

    protected function failedValidation(Validator $validator)
    {
        if (in_array('api', $this->segments())) {
            $massages = "";
            foreach ( $validator->errors()->messages() as $key => $val ){
                $massages = ["massage" => $val[0]] ;
            }
            throw new HttpResponseException(response()->json([
                'status' => 0,
                //                'errors' => $validator->errors()->messages(),
                'errors' => $massages,
            ], 200));
        }
        parent::failedValidation($validator);
    }
}