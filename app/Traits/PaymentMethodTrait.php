<?php

namespace App\Traits;


trait PaymentMethodTrait
{
    public function getPaymentMethodsAttribute()
    {
        return [
            1 => __('app.cash'),
            2 => __('app.visa'),
            3 => __('app.mada'),
            5 => __('app.wallet'),
        ] ;
    }
}