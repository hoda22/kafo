<?php

namespace App\Traits;


trait OrderStatusTrait
{
    public function OrderStatusItems()
    {
        return [
            1   => __('app.request_done'),
            2   => __('app.under_revision_'),
            4   => __('app.technician_assigned_'),
            6   => __('app.technician_arrived'),
            7   => __('app.implemented'),
            10  => __('app.rejected_by_administration'),
            11  => __('app.modification'),
            17   => __('app.rejected_customer'),
        ] ;
    }
}