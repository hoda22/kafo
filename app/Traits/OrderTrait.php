<?php

namespace App\Traits;


use App\Models\Coupon;
use App\Models\LoyaltyPoint;
use App\Models\Order;

use App\Models\OrderDetails;
use Illuminate\Support\Facades\Request as UserRequest;

trait OrderTrait
{
    private function refund()
    {
        $user = UserRequest::user() ;
        return $this->getValuePoints( $user->points );
    }

    private function getValuePoints( $points )
    {
        $nearestNumber = LoyaltyPoint::where("active" ,"active")
            ->where('num_point', '<', $points)
            ->orderByDesc('num_point')
            ->first();
        return $nearestNumber;
    }

    private function updateUserPoints( $points )
    {
        $user = UserRequest::user() ;

        if ( $user->points >= $points ){
            $user->points = $user->points - $points ;
            $user->save();
            return true;
        }
        return false ;
    }

    private function getTotalOrder($order_id = null)
    {
        if ( $order_id ) {
            $order = Order::find($order_id) ;
        }else{
            $user = UserRequest::user() ;
            $order = $this->getOrder($user->id) ;
        }

        $totalItems = [] ;
        foreach ( $order->orderDetails as $orderDetails ){
            $totalItems[] = $orderDetails->price ;
        }
        dd( $totalItems ) ;
        $totalNet = array_sum($totalItems );

        $order->total = $totalNet ;
        $order->save() ;
        return $totalNet ;
    }

    private function getOrder()
    {
        $user = UserRequest::user() ;
        $client_id = $user->id ;
        $order = Order::with("orderDetails")->with("orderImages")->firstOrCreate(
            ['client_id' =>  $client_id , 'status' => 0 ],
            ['client_id' =>  $client_id , 'status' => 0]
        );
        return $order ;
    }

    private function coupon( $coupon_id , $total )
    {
        $coupon = Coupon::find( $coupon_id ) ;
        if ( $coupon->type == 'amount' )
        {
            $total = $total - $coupon->number ;
            $total_discount = $coupon->number ;
        } else {
            $totalAmount = $total; // Total amount
            $percentage = $coupon->number ; // Percentage you want to calculate
            $total_discount = ($percentage / 100) * $totalAmount;
        }
        return ["total" => $total , "coupon_discount" => $total_discount , "type" => $coupon->type ] ;
    }


    public function getTotalOrderItems( $orderId )
    {
        $orderDetailsNews = OrderDetails::where("order_id" , $orderId)
            ->where( "type" , "new")
            ->get() ;

        $orderDetailsEdit = OrderDetails::where("order_id" , $orderId)
            ->where( "type" , "edit")
//            ->where( "admin_status" , "!=" ,"rejected_client")
            ->get() ;

        $orderDetailsSum = [] ;
        foreach ( $orderDetailsNews as $orderDetailsNew ){
            $orderDetailsSum[] = $orderDetailsNew->price ;
        }

        foreach ( $orderDetailsEdit as $orderDetails ){
            if ( $orderDetails->admin_status == "rejected_client" ) {
                $orderDetailsSum[] = $orderDetails->visit ;
            } else{
                $orderDetailsSum[] = $orderDetails->price ;
            }
        }



        return array_sum( $orderDetailsSum ) ;
    }

}