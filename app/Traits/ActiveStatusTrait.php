<?php

namespace App\Traits;

use App\Enums\ActiveEnum;
trait ActiveStatusTrait
{
    public function getActiveStatusAttribute()
    {
        return $this->active == ActiveEnum::ACTIVE->value ? ['className' => 'success' , 'text' => 'نشط'] : ['className' => 'danger' , 'text' =>  'غير نشط'] ;
    }

    public function scopeOrdering($query)
    {
        return $query->orderBy('ordering','ASC');
    }

    public function scopeActive($query)
    {
        return $query->where('active', ActiveEnum::ACTIVE->value);
    }
}