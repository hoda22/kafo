<?php

namespace App\Traits;

use App\Models\Attachment;
use Intervention\Image\Facades\Image as ResizeImage;

trait UploadFile
{
    public function upload( $img , $type , $width = 200, $height = 200 , $resize = true )
    {
        if ($img) {
            $imageName = time().'.'.$img->extension();
            $pathFolder = "images/".$type."/" ;


            $path = public_path(  "../".  $pathFolder.time().'.'.$img->extension());
            // RIGHT :: $path = public_path(  "../".  $pathFolder.time().'.'.$img->extension());
//           $path = asset($pathFolder.time().'.'.$img->extension());

            if ( $resize ){
                ResizeImage::make($img)->resize($width, $height)->save($path);
            }else {
                ResizeImage::make($img)->save($path);
            }

            return $pathFolder.$imageName;
        }
    }
}
