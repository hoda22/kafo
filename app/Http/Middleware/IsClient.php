<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as UserRequest;
use Illuminate\Http\Response;

class IsClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = UserRequest::user() ;
        if ( $user ) {
            if ( $user->type == "client" ) {
                return $next($request);
            }
            return response()->json( ["status" => 0 , "message" => "forbidden" ], 403);
        }
        return response()->json( ["status" => 0 , "message" => "forbidden" ], 403);
    }
}
