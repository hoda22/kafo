<?php

namespace App\Http\Requests\Api\Order;

use App\Traits\ErrorResponseTrait;
use Illuminate\Foundation\Http\FormRequest;

class RescheduleOrderRequest extends FormRequest
{
    use ErrorResponseTrait;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "date" => 'required',
            "time" => 'required',
        ];
    }
}
