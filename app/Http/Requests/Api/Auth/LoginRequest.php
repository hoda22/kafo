<?php

namespace App\Http\Requests\Api\Auth;

use App\Traits\ErrorResponseTrait;
use Illuminate\Foundation\Http\FormRequest;
class LoginRequest extends FormRequest
{
    use ErrorResponseTrait;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'phone'             => 'required',
            'password'          => 'required',
            'code'              => 'required',
        ];
    }
}
