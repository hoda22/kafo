<?php

namespace App\Http\Requests\Api\Auth;

use App\Traits\ErrorResponseTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request as UserRequest;


class ProfileRequest extends FormRequest
{
    use ErrorResponseTrait;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $user = UserRequest::user() ;

//        dd( $user->id ) ;
        return [

            'first_name'        => 'required',
            'last_name'         => 'required',

            'phone'             => 'required|unique:clients,phone,'.$user->id.',id,deleted_at,NULL', //required|email|unique:users,email,NULL,id,deleted_at,NULL

            'email'             => 'required|email|unique:clients,email,'.$user->id.',id,deleted_at,NULL', //
        ];
    }
}
