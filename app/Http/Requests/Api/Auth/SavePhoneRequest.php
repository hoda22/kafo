<?php

namespace App\Http\Requests\Api\Auth;


use App\Traits\ErrorResponseTrait;
use Illuminate\Foundation\Http\FormRequest;

class SavePhoneRequest extends FormRequest
{
    use ErrorResponseTrait;
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'phone' => 'required',
            'code' => 'required',
        ];
    }
}
