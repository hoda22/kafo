<?php

namespace App\Http\Requests\Api\Auth;


use App\Traits\ErrorResponseTrait;
use Illuminate\Foundation\Http\FormRequest;

class ForgetPasswordRequest extends FormRequest
{
    use ErrorResponseTrait;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'device_id'     => 'required',
            'code'          => 'required',
            'phone'         => 'required',
        ];
    }
}
