<?php

namespace App\Http\Requests\Api\Auth;


use App\Traits\ErrorResponseTrait;
use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    use ErrorResponseTrait;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'old_password'          => 'required',
            'new_password'          => 'required|min:8',
            'password_confirmation' => 'required|same:new_password',
        ];
    }
}
