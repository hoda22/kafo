<?php

namespace App\Http\Requests\Api\Auth;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegisterCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    protected function failedAuthorization()
    {
        if (in_array('api', $this->segments())) {
            throw new HttpResponseException(response()->json([
                'status' => 0,
                'message' => 'Unauthorized',
            ], 403));
        }

        parent::failedAuthorization();
    }

    protected function failedValidation(Validator $validator)
    {
        $messages = [] ;
        foreach ( $validator->errors()->getMessages() as $key => $message ){
            $messages[$key] =   $message[0];
        }
        if (in_array('api', $this->segments())) {
            throw new HttpResponseException(response()->json([
                'status' => 0,
                'errors' => $messages,
            ], 200));
        }

        parent::failedValidation($validator);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [

            'first_name'        => 'required', //
            'last_name'         => 'required',
            'phone'             => 'required|unique:clients,phone,NULL,id,deleted_at,NULL', //required|email|unique:users,email,NULL,id,deleted_at,NULL

            'email'             => 'required|email|unique:clients,email,NULL,id,deleted_at,NULL', //

            'terms_conditions'  => 'required',
            'device_id'         => 'required',
            'password'          => 'required|min:6', //
            'password_confirmation' => 'required|same:password', //

        ];
    }
}
