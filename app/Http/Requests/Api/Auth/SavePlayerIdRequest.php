<?php

namespace App\Http\Requests\Api\Auth;


use App\Traits\ErrorResponseTrait;
use Illuminate\Foundation\Http\FormRequest;

class SavePlayerIdRequest extends FormRequest
{
    use ErrorResponseTrait;
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'player_id' => 'required',
            'type_device' => 'nullable',
        ];
    }
}
