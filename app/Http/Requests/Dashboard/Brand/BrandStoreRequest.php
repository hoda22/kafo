<?php

namespace App\Http\Requests\Dashboard\Brand;

use Illuminate\Foundation\Http\FormRequest;

class BrandStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name'       => 'required|unique:brand_translations',
            'name_en'    => 'required|unique:brand_translations,name',
            'ac_type'    => 'required',
            'active'     => 'nullable',
            'ordering'   => 'nullable|numeric',
            'src'        => 'required|mimes:jpeg,png,jpg,gif',
        ];
    }
}
