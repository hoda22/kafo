<?php

namespace App\Http\Requests\Dashboard\Provider;

use Illuminate\Foundation\Http\FormRequest;

class ProviderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'first_name'        => 'required',
            'last_name'         => 'required',
            'nationality'       => 'required',

            'phone'             => 'required|unique:clients,phone,NULL,id,deleted_at,NULL', //required|email|unique:users,email,NULL,id,deleted_at,NULL
            'email'             => 'required|email|unique:clients,email,NULL,id,deleted_at,NULL', //
            'residency_number'  => 'required|unique:clients,residency_number,NULL,id,deleted_at,NULL', //


            'years_experience'  => 'nullable',
            'date_birth'        => 'nullable',
            'jop'               => 'nullable',
            'password'          => 'required|confirmed|min:8',
            'avatar'          => 'nullable',

            'lat'           => 'nullable',
            'lon'           => 'nullable',

            'address'       => 'nullable',
            'active'        => 'required',
        ];
    }
}
