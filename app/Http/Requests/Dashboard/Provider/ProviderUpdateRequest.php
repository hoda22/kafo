<?php

namespace App\Http\Requests\Dashboard\Provider;

use Illuminate\Foundation\Http\FormRequest;

class ProviderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $id = $this->route('provider') ;
        return [
            'first_name'        => 'required',
            'last_name'         => 'required',
            'nationality'       => 'required',

            'email'             => 'required|unique:clients,email,'.$id.',id,deleted_at,NULL' ,
            'phone'             => 'required|unique:clients,phone,'.$id.',id,deleted_at,NULL' ,
            'residency_number'  => 'required|unique:clients,residency_number,'.$id.',id,deleted_at,NULL' ,


            'years_experience'  => 'nullable',
            'date_birth'        => 'nullable',
            'jop'               => 'nullable',
            'city_id'           => 'nullable',

            'password'          => 'nullable|confirmed|min:8',

            'lat'           => 'nullable',
            'lon'           => 'nullable',




            'address'       => 'nullable',
            'active'        => 'required',
        ];
    }
}
