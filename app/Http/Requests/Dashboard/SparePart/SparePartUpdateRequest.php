<?php

namespace App\Http\Requests\Dashboard\SparePart;

use Illuminate\Foundation\Http\FormRequest;

class SparePartUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [

            'name'              => 'required',
            'name_en'           => 'required',

            'sub_service_id'    => 'required',
            'service_id'        => 'required',


//            'ac_type'    => 'required',
//            'brand_id'   => 'required|exists:brands,id',
            'active'     => 'required',
            'ordering'   => 'nullable|numeric',


            
//            'name'       => 'required',
//            'name_en'    => 'required',
//            'ac_type'    => 'required',
//            'brand_id'   => 'required|exists:brands,id',
//            'active'     => 'required',
//            'ordering'   => 'nullable|numeric',
        ];
    }
}
