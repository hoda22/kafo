<?php

namespace App\Http\Requests\Dashboard\PricePoint;

use App\Rules\RangeInDatabase;
use App\Rules\RangeITonDatabase;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PricePointStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'from' => ['required', new RangeInDatabase('price_points', 'from' )],
            'points_num'    => 'required',
            'active'        => 'nullable',

//            'to' =>[
//                'required',
//                'numeric',
//                Rule::
//            ],
            'to' => ['required', new RangeITonDatabase('price_points', 'to' )],
//            'to' =>[
//                'required',
//                'numeric',
//                Rule::exists('price_points', 'your_column')
//                    ->where(function ($query) {
//                        $query->whereBetween( 'to', [ request('from'), request('to')] );
//                    }),
//            ],
        ];
    }
}
