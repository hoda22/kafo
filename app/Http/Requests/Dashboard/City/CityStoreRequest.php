<?php

namespace App\Http\Requests\Dashboard\City;

use Illuminate\Foundation\Http\FormRequest;

class CityStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name'          => 'required|unique:city_translations',
            'name_en'       => 'required|unique:city_translations,name',
            'ordering'      => 'nullable|numeric',
            'active'        => 'nullable',
            'zone_polygon'  => 'nullable',
        ];
    }
}
