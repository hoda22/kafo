<?php

namespace App\Http\Requests\Dashboard\City;

use Illuminate\Foundation\Http\FormRequest;

class CityUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $id = $this->route('city') ;
        return [
            'name'       => 'required',
            'name_en'    => 'required',
            'ordering'   => 'nullable|numeric',
            'active'     => 'nullable',
            'zone_polygon'     => 'nullable',
        ];
    }
}
