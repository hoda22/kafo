<?php

namespace App\Http\Requests\Dashboard\Page;

use Illuminate\Foundation\Http\FormRequest;

class PageStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
//            'name'              => 'required|unique:page_translations,name',
//            'name_en'           => 'required|unique:page_translations,name',
//
//            'ordering'          => 'nullable|numeric' ,
            'description'       => 'nullable',
            'description_en'    => 'nullable',
//            'active'            => 'required',
        ];
    }
}
