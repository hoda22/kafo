<?php

namespace App\Http\Requests\Dashboard\Client;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class ClientStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'first_name'        => 'required',
            'last_name'         => 'required',
            'email'             => 'required|unique:clients,email',
            'phone'             => 'required|unique:clients,phone',
            'password'          => [ 'required',
                Password::min(8)
                    ->letters()
                    ->mixedCase()
                    ->numbers()
                    ->symbols()
            ],
            'address'           => 'nullable',
            'active'            => 'required',
        ];


    }

}
