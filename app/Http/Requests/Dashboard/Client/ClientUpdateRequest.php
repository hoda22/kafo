<?php

namespace App\Http\Requests\Dashboard\Client;

use Illuminate\Foundation\Http\FormRequest;

class ClientUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $id = $this->route('client') ;
        return [
            'first_name'        => 'required',
            'last_name'         => 'required',
            'email'             => 'required|unique:clients,email,'.$id ,
            'phone'             => 'required|unique:clients,phone,'.$id ,

            'password'          => 'nullable|confirmed|min:8',

            'active'        => 'required',
        ];
    }
}
