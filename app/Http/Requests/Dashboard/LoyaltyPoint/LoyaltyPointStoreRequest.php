<?php

namespace App\Http\Requests\Dashboard\LoyaltyPoint;

use Illuminate\Foundation\Http\FormRequest;

class LoyaltyPointStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'num_point'     => 'required',
            'num_sar'       => 'required',
            'ordering'      => 'required',
            'active'        => 'nullable',
            'service_id'    => 'nullable',
        ];
    }
}
