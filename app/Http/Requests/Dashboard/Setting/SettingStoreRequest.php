<?php

namespace App\Http\Requests\Dashboard\Setting;

use Illuminate\Foundation\Http\FormRequest;

class SettingStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'site_name'         => 'required',
            'email'             => 'required',
            'phone'             => 'required',
            'phone2'            => 'nullable',
            'meta_description'  => 'nullable',
            'meta_keywords'     => 'nullable',



            'facebook'            => 'nullable',
            'twitter'  => 'nullable',
            'visit'  => 'required',
            'linkedin'     => 'nullable',
            'instagram'     => 'nullable',
        ];
    }
}
