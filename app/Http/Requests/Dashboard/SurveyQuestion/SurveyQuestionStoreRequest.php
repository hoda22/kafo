<?php

namespace App\Http\Requests\Dashboard\SurveyQuestion;

use Illuminate\Foundation\Http\FormRequest;

class SurveyQuestionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'text'              => 'required',
            'text_en'           => 'required',
            'type'              => 'required',
            'ordering'          => 'nullable',
            'active'            => 'nullable',
            'answers'           => 'nullable',
            'answers_ordering'   => 'nullable',
        ];
    }
}
