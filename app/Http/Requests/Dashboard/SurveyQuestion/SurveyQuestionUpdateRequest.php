<?php

namespace App\Http\Requests\Dashboard\SurveyQuestion;

use Illuminate\Foundation\Http\FormRequest;

class SurveyQuestionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'text'       => 'required',
            'text_en'    => 'required',
            'ordering'   => 'nullable|numeric',
            'ordering1'   => 'nullable|numeric',
            'active'     => 'nullable',
            'answer'     => 'nullable',
            'type'     => 'nullable',
            'item_id_s'     => 'nullable',
            'ordering_ans'     => 'nullable',
        ];
    }
}
