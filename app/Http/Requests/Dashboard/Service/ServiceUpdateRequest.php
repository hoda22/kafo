<?php

namespace App\Http\Requests\Dashboard\Service;

use Illuminate\Foundation\Http\FormRequest;

class ServiceUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name'       => 'required',
            'name_en'    => 'required',
            'parent_id'  => 'nullable',
            'price'      => 'nullable',
            'active'     => 'nullable',
            'ordering'   => 'nullable|numeric',
            'src'        => 'nullable|mimes:jpeg,png,jpg,gif',
        ];
    }
}
