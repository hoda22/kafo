<?php

namespace App\Http\Requests\Dashboard\Service;

use Illuminate\Foundation\Http\FormRequest;

class ServiceStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name'       => 'required|unique:service_translations,name',
            'name_en'    => 'required|unique:service_translations,name',
            'parent_id'  => 'nullable',
            'price'      => 'nullable',
            'ordering'   => 'nullable|numeric',
            'active'     => 'nullable',
//            'src'        => 'nullable|mimes:jpeg,png,jpg,gif',
            'src'        => 'required_if:parent_id,==,0|mimes:jpeg,png,jpg,gif',
        ];
    }
}
