<?php

namespace App\Http\Requests\Dashboard\Coupon;

use Illuminate\Foundation\Http\FormRequest;

class CouponUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'clients_id'    => 'nullable|array' ,
            'code'          => 'required',
            'expire_date'   => 'required',
            'start_date'    => 'required',
            'type'          => 'required',
            'number'        => 'required',
            'active'        => 'nullable',
        ];
    }
}
