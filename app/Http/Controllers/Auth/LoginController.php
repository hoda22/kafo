<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Login\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $validate = $this->validateData($request->all());

        if ($validate->fails()) {
            return redirect('login')
                ->withErrors($validate)
                ->withInput();
        }else{
            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                return redirect()->route('home');
            }else{
                return redirect('login')->withErrors(['loginfail' => 'Invalid login details']);
            }
        }
    }


    protected function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();
        $request->session()->regenerate();

        return redirect('/login');
    }

    public function validateData($data)
    {
        $rulse = [
            'email' => 'required|email',
            'captcha' => 'required|captcha',
            'password' => 'required'
        ];

        $msg = [
            'email.required' => 'The email field is required.',
            'email.captcha' => 'The captcha field is required.',
            "captcha.validation"  => "رمز التأكيد غير صحيح"
        ];

        return Validator::make($data,$rulse);
    }
}
