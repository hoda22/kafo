<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Coupon\CouponStoreRequest;
use App\Http\Requests\Dashboard\Coupon\CouponUpdateRequest;
use App\Models\Client;
use App\Models\Coupon;
use App\Services\CouponService;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.coupons.';

    public function __construct()
    {
        $this->model = new Coupon();
        /**/
        $this->middleware('permission:coupons-list|coupons-create|coupons-edit|coupons-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:coupons-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:coupons-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:coupons-delete', ['only' => ['destroy']]);

    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index( Request $request , CouponService $CouponService )
    {
        $items = $CouponService->index( $request->all() ) ;
        $clients = Client::active()->client()->where("block_status" , "unblock")->get();
        return $this->view('index', compact('items' , 'clients'));
    }

    public function store( CouponStoreRequest $request, CouponService $CouponService )
    {
        $CouponService->store($request->validated()) ;
        flash(__('messages.added_successfully'))->success();
        return back();
    }

    public function update( CouponUpdateRequest $request, $id, CouponService $CouponService )
    {
        $CouponService->update($id , $request->validated()) ;
        flash(__('messages.updated_successfully'))->success();
        return back();
    }

    public function destroy($id , CouponService $CouponService)
    {
        $CouponService->delete($id);
        flash(__("messages.deleted_successfully"))->success();
        return back();
    }

    public function updateAxios( Request $request , $id )
    {
        $item = $this->model->findOrFail($id);
        $data = [] ;
        foreach ( $request->all() as $key => $value )
        {
            $data = [ $key => $value ] ;
        }
        $item->update($data);
        return response()->json(["status" => true , "message" => __('messages.updated_successfully') ]) ;
    }
}
