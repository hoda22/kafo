<?php

namespace App\Http\Controllers\Dashboard;

use App\Exports\ExportService;
use App\Exports\ExportSupService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Service\ServiceStoreRequest;
use App\Http\Requests\Dashboard\Service\ServiceUpdateRequest;
use App\Models\Service;
use App\Services\ServiceService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ServiceController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.services.';

    public function __construct()
    {
        $this->model = new Service();

        $this->middleware('permission:services-list|services-create|services-edit|services-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:services-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:services-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:services-delete', ['only' => ['destroy']]);

    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index(Request $request , ServiceService $serviceService)
    {
        $items = $serviceService->index($request->all()) ;
        $services = Service::parent()->get() ;
        return $this->view('index1', compact('items' ,'services'));
    }

    public function store( ServiceStoreRequest $request, ServiceService $serviceService )
    {
        $serviceService->store($request->validated()) ;
        flash(__('messages.added_successfully'))->success();
        return back();
    }

    public function update(ServiceUpdateRequest $request, $id, ServiceService $serviceService )
    {
        $serviceService->update($id , $request->validated()) ;
        flash(__('messages.updated_successfully'))->success();
        return back();
    }

    public function destroy($id , ServiceService $serviceService )
    {
        $is_deleted = $serviceService->delete($id);
        if ( $is_deleted ) {
            flash(__('messages.deleted_successfully'))->success();
        }else{
            flash(__('messages.cant_deleted'))->error();
        }
        return back();
    }

    public function export()
    {
        return Excel::download(new ExportService() , 'services-'.date("Y-m-d m:i:s").'.xlsx') ;
    }

    public function supServicesExport( $id )
    {
        return Excel::download(new ExportSupService($id) , 'sup-services-'.date("Y-m-d m:i:s").'.xlsx') ;
    }

    public function updateAxios( Request $request , $id )
    {
        $item = $this->model->findOrFail($id);
        $data = [] ;
        foreach ( $request->all() as $key => $value )
        {
            $data = [ $key => $value ] ;
        }
        $item->update($data);
        return response()->json(["status" => true , "message" => __('messages.updated_successfully') ]) ;
    }

    public function supServices( Request $request ,$id = null )
    {
        $mainServices = Service::active()->parent()->get();
        if ( $id ) {
            $query = $this->model->where("parent_id",$id)->latest();
        }else{
            $query = $this->model->childrenItems()->latest();
        }
        if (  $request->filled('name')   ){
            $query = $query->whereTranslationLike('name' , '%'.$request->name.'%') ;
        }

        if ( $request->filled('active' ) ){
            $query = $query->where('active' , $request->active ) ;
        }
        $items =  $query->paginate();
        $titlePage = "" ;
        $itemMain = "" ;
        if( $id ){
            $itemMain = Service::find($id) ;
            $titlePage = $itemMain->name ;
        }

        return $this->view('supServices', compact('items', 'itemMain' ,'titlePage' ,'mainServices'));
    }
}
