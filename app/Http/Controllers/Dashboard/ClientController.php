<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Client\ClientStoreRequest;
use App\Http\Requests\Dashboard\Client\ClientUpdateRequest;
use App\Models\Client;
use App\Models\Order;
use App\Services\ClientService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ClientController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.clients.';

    public function __construct()
    {
        $this->model = ( new Client() )->newQuery()->client();

        $this->middleware('permission:clients-list|clients-create|clients-edit|clients-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:clients-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:clients-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:clients-delete', ['only' => ['destroy']]);
        $this->middleware('permission:clients-show', ['only' => ['show']]);
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index(Request $request , ClientService $clientService )
    {
        $items = $clientService->index($request->all()) ;
        return $this->view('index', compact('items'));
    }

    public function create()
    {
        return $this->view('create');
    }

    public function store( ClientStoreRequest $request, ClientService $clientService )
    {
        $data = $request->validated();
        $data['verify'] = 1;
        $data['password'] = Hash::make($data['password']);
        $data['type'] = "client";
        Client::create($data) ;
        flash(__('messages.added_successfully'))->success();
        return redirect(route("clients.index"));
    }

    public function update( ClientUpdateRequest $request, $id )
    {
        $item = $this->model->find($id) ;
        $data = $request->validated() ;


        if (  ! $request->password ) {
              unset($data['password']);
        }else{
            $data['password'] = Hash::make($data['password']);
        }

        $item->update($data) ;
        flash(__('messages.added_successfully'))->success();
        return redirect(route("clients.index"));
    }

    public function edit( $id )
    {
        $item = $this->model->find($id) ;
        return $this->view('edit' , compact('item'));
    }

    public function show( $id , ClientService $clientService )
    {
        $item = $clientService->show($id) ;
        $orders = Order::where("client_id",$id)->get();
        return $this->view('show', compact('item','orders'));
    }

    public function destroy($id , ClientService $clientService )
    {
        $is_deleted = $clientService->delete($id);
        if ( $is_deleted ) {
            flash(__('messages.deleted_successfully'))->success();
        }else{
            flash(__('messages.cant_deleted'))->error();
        }
        return back();
    }


    public function updateAxios( Request $request , $id )
    {

        $item = $this->model->findOrFail($id);
        $data = [] ;
        foreach ( $request->all() as $key => $value )
        {
            $data = [ $key => $value ] ;
        }
        $item->update($data);


        // To remove token from app
        if ( $item->active == "inactive" ){
            $item->tokens()->delete();
        }


        if ( $request->active == 'active' ){
            return response()->json(["status" => true , "message" => __('messages.activation_successfully') ]) ;
        }else{
            return response()->json(["status" => true , "message" => __('messages.activation_successfully_deactivated') ]) ;
        }

    }

    public function getClient( Request $request )
    {
        $client = Client::findOrFail( $request->id );
        return ["name" => $client->full_name  , "avater" => $client->avater] ;
    }


    public function updateAxiosBlock( Request $request , $id )
    {
        $item = $this->model->findOrFail($id);
        $item->block_status = $request->block_status ;
        $item->save();

        if ( $item->block_status == "block" ){
            $item->tokens()->delete();
        }


        if ( $request->active == 'active' ){
            return response()->json(["status" => true , "message" => __('messages.block_successfully_deactivated') ]) ;
        }else{
            return response()->json(["status" => true , "message" => __('messages.block_successfully_delete') ]) ;
        }
    }
}
