<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Page\PageStoreRequest;
use App\Http\Requests\Dashboard\Page\PageUpdateRequest;
use App\Models\Page;
use App\Services\PageService;
use Illuminate\Http\Request;

class PageController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.pages.';

    public function __construct()
    {
        $this->model = new Page();
        /**/
        $this->middleware('permission:pages-list|pages-create|pages-edit|pages-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:pages-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:pages-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:pages-delete', ['only' => ['destroy']]);

    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function create()
    {
        return $this->view('create');
    }


    public function index(Request $request , PageService $pageService)
    {
        $items = $pageService->index($request->all()) ;
        return $this->view('index', compact('items'));
    }


    public function store(PageStoreRequest $request, PageService $pageService )
    {
        $pageService->store($request->validated()) ;
        flash(__('messages.added_successfully'))->success();
        return back();
    }


    public function edit( $id )
    {
        $item = $this->model->find($id) ;
        return $this->view('edit' , compact('item'));
    }

    public function update(PageUpdateRequest $request, $id, PageService $pageService)
    {
        $pageService->update($id ,$request->validated()) ;
        flash(__('messages.updated_successfully'))->success();
        return back();
    }

    public function destroy($id , PageService $pageService )
    {
        $pageService->delete($id);
        flash(__('messages.updated_successfully'))->success();
        return back();
    }
}
