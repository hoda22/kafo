<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\SurveyQuestion\SurveyQuestionUpdateRequest;
use App\Http\Requests\Dashboard\SurveyQuestion\SurveyQuestionStoreRequest;
use App\Models\SurveyQuestion;
use App\Enums\StatusesEnum;
use App\Services\SurveyQuestionService;
use Illuminate\Http\Request;

class SurveyQuestionController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.surveyQuestion.';

    public function __construct()
    {
        $this->model = new SurveyQuestion();
        $this->middleware('permission:survey-list|survey-create|survey-edit|survey-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:survey-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:survey-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:survey-delete', ['only' => ['destroy']]);

    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index( Request $request , SurveyQuestionService $surveyQuestionService)
    {
        $items = $surveyQuestionService->index( $request->all() ) ;
        return $this->view('index', compact('items'));
    }

    public function store( SurveyQuestionStoreRequest $request, SurveyQuestionService $surveyQuestionService )
    {
        $surveyQuestionService->store($request->validated()) ;
        flash(__('messages.added_successfully'))->success();
        return back();
    }

    public function update( SurveyQuestionUpdateRequest $request, $id , SurveyQuestionService $surveyQuestionService )
    {
        $surveyQuestionService->update($id , $request->validated()) ;
        flash(__('messages.updated_successfully'))->success();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id , SurveyQuestionService $surveyQuestionService )
    {
        $pros = $surveyQuestionService->delete($id);
        if ( $pros ){
            flash(__('messages.deleted_successfully'))->success();
        }else{
            flash(__('messages.cant_deleted'))->error();
        }

        return back();
    }
}
