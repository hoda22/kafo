<?php

namespace App\Http\Controllers\Dashboard;

use App\Exports\ExportClient;
use App\Exports\ExportRequest;
use App\Exports\ExportServiceRequest;
use App\Exports\ExportSparePart;
use App\Exports\ExportProvider;
use App\Exports\ExportFinance;

use App\Filters\ClientFilter;
use App\Filters\RequestFilter;
use App\Models\OrderDetails;
use App\Models\SparePart;
use App\Models\SparePartOrderDetails;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Order;
use App\Models\Service;

use App\Enums\OrderStatusEnum;

class ReportController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.reports.';

    public function __construct()
    {
        /*
        $this->middleware('permission:sliders-list|sliders-create|sliders-edit|sliders-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:sliders-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:sliders-delete', ['only' => ['destroy']]);
        */

        $this->middleware('permission:report-clients', ['only' => ['clients']]);
        $this->middleware('permission:report-services', ['only' => ['services']]);
        $this->middleware('permission:report-finance', ['only' => ['finance']]);
        $this->middleware('permission:report-provider', ['only' => ['provider']]);
        $this->middleware('permission:report-spare-pats', ['only' => ['sparePart']]);
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index(  )
    {
        return $this->view('index');
    }

    public function clients( ClientFilter $filter )
    {
        $items = Client::latest()->client()->filter($filter)->paginate(20);
        $statisticsClient = [];

        $blockClients = Client::latest()->client()->where("block_status" ,"block")->count();
        $unBlockClients = Client::latest()->client()->where("block_status" ,"unblock")->count();

        $month = 1;
        while ($month <= 12) {
            $statisticsClient[] = Client::client()->whereYear('created_at', today()->year)
                ->whereMonth('created_at', $month)->count();
            $month++;
        }
        $statisticsClient = json_encode($statisticsClient);

        return $this->view('clients', compact('items' ,'statisticsClient' ,'blockClients','unBlockClients' ));
    }

    public function requests( RequestFilter $filter )
    {
        $items = Order::with('client' , 'provider' , 'service')->latest()->filter($filter)->paginate(20);

        return $this->view('requests', compact('items'));
    }

    public function services( RequestFilter $filter )
    {
        $items = Order::with('client' , 'provider','orderDetails' ,'service')
            ->whereNot("status","0")
            ->latest()->filter($filter)->paginate(15);

        $sup_services = Service::active()->children()->get();

        $serviceCount = [] ;
        $sup_services_names = [] ;

        $orderWaiting = Order::where("status", OrderStatusEnum::RequestIsDone->value )
                    ->orWhere("status", OrderStatusEnum::UnderRevision->value)
                    ->orWhere("status", OrderStatusEnum::TechnicianAssigned->value)
                    ->count() ;


        $orderCurrent = Order::where("status", OrderStatusEnum::TechnicianApproved->value )
                    ->orWhere("status", OrderStatusEnum::TechnicianWay->value)
                    ->orWhere("status", OrderStatusEnum::TechnicianArrived->value)
                    ->count() ;


        $orderDone = Order::where("status", OrderStatusEnum::Implemented->value )->count() ;

        foreach ( $sup_services as $sup_service ){

            $serviceCount[]= OrderDetails::where("service_id" , $sup_service->id )->count();
            $sup_services_names[] = $sup_service->name ;
        }
//        return $sup_services_names ;
        $serviceCount = json_encode($serviceCount);
        $sup_services_names = json_encode($sup_services_names);

        return $this->view('services', compact('items', 'sup_services_names','serviceCount', 'sup_services' , 'orderWaiting' ,'orderCurrent','orderDone'));
    }

    public function finance( RequestFilter $filter )
    {
        $items = Order::with('client' , 'provider' , 'service')->latest()
            ->filter($filter)
            ->where("is_paid", 1)
            ->paginate(20);
        return $this->view('finance', compact('items'));
    }

    public function provider( RequestFilter $filter )
    {
        $items = Client::latest()->provider()->with("providerOrders")->filter($filter)->paginate(20);


        $statisticsClient = [];

        $blockProviders = Client::latest()->provider()->where("block_status" ,"block")->count();
        $unBlockProviders = Client::latest()->provider()->where("block_status" ,"unblock")->count();

        $month = 1;
        while ($month <= 12) {
            $statisticsClient[] = Client::client()->whereYear('created_at', today()->year)
                ->whereMonth('created_at', $month)->count();
            $month++;
        }
        $statisticsClient = json_encode($statisticsClient);


        return $this->view('providers', compact('items' ,'statisticsClient' ,'blockProviders','unBlockProviders'));
    }

    public function sparePart( RequestFilter $filter )
    {
        $items = Order::latest()->filter($filter)
            ->with("client" ,"provider" ,"sparePart","orderDetailsEdit")
            ->has("sparePart")->paginate(20);


        $spareParts = SparePart::get();
        $spare_parts_count = [] ;
        $spare_parts_names = [] ;

        foreach ( $spareParts as $sparePart ){
            $spare_parts_count[] = SparePartOrderDetails::where("spare_part_id",$sparePart->id)->count();
            $spare_parts_names[] = SparePart::where("id",$sparePart->id)->first()->name;
        }

        $spare_parts_names = json_encode($spare_parts_names);
        $spare_parts_count = json_encode($spare_parts_count);

        return $this->view('spareParts', compact('items','spare_parts_count','spare_parts_names'));
    }

    public function clientsExport(Request $requset)
    {
        return Excel::download(new ExportClient($requset->all()) , 'clients-'.date("Y-m-d m:i:s").'.xlsx') ;
    }

    public function requestsExport(Request $requset)
    {
        return Excel::download(new ExportRequest($requset->all()) , 'requests-'.date("Y-m-d m:i:s").'.xlsx') ;
    }

    public function financeExport(Request $requset)
    {
        return Excel::download(new ExportFinance($requset->all()) , 'finance-'.date("Y-m-d m:i:s").'.xlsx') ;
    }

    public function ServicesExport(Request $requset)
    {
        return Excel::download(new ExportServiceRequest($requset->all()) , 'services-'.date("Y-m-d m:i:s").'.xlsx') ;
    }

    public function sparePartsExport(Request $requset)
    {
        return Excel::download(new ExportSparePart($requset->all()) , 'spareParts-'.date("Y-m-d m:i:s").'.xlsx') ;
    }

    public function providersExport(Request $requset)
    {
        return Excel::download(new ExportProvider($requset->all()) , 'providers-'.date("Y-m-d m:i:s").'.xlsx') ;
    }

    public function financesExport(Request $requset)
    {
        return Excel::download(new ExportFinance($requset->all()) , 'providers-'.date("Y-m-d m:i:s").'.xlsx') ;
    }
}
