<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\PricePoint\PricePointStoreRequest;
use App\Http\Requests\Dashboard\PricePoint\PricePointUpdateRequest;
use App\Enums\StatusesEnum;
use App\Models\PricePoint;
use App\Models\Service;
use App\Services\PricePointService;
use Illuminate\Http\Request;

class PricePointController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.pricePoint.';

    public function __construct()
    {
        $this->model = new PricePoint();

        $this->middleware('permission:loyalty-points-list|loyalty-points-create|loyalty-points-edit|loyalty-points-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:loyalty-points-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:loyalty-points-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:loyalty-points-delete', ['only' => ['destroy']]);

    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index( Request $request , PricePointService $pricePointServiceService )
    {
        $items = $pricePointServiceService->index( $request->all() ) ;
        $services = Service::parent()->active()->get() ;
        return $this->view('index', compact('items' ,'services'));
    }

    public function store( PricePointStoreRequest $request, PricePointService $pricePointServiceService )
    {
        $pricePointServiceService->store($request->validated()) ;
        flash(__('messages.added_successfully'))->success();
        return back();
    }

    public function update(PricePointUpdateRequest $request, $id, PricePointService $pricePointServiceService )
    {
        $pricePointServiceService->update($id , $request->validated()) ;
        flash(__('messages.updated_successfully'))->success();
        return back();
    }

    public function destroy($id , PricePointService $pricePointServiceService)
    {
        $pricePointServiceService->delete($id);
        flash(__("messages.deleted_successfully"))->success();
        return back();
    }

    public function updateAxios( Request $request , $id )
    {
        $item = $this->model->findOrFail($id);
        $data = [] ;
        foreach ( $request->all() as $key => $value )
        {
            $data = [ $key => $value ] ;
        }
        $item->update($data);
        return response()->json(["status" => true , "message" => __('messages.updated_successfully') ]) ;
    }
}
