<?php

namespace App\Http\Controllers\Dashboard;

use App\FirebaseNotification;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Client;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\OrderImage;
use App\Models\Service;

use App\Models\SurveyOrder;
use App\Models\SurveyOrderAnswer;
use App\Models\TrackingOrder;
use App\Notifications\ClientNotification;
use App\Services\OrderService;
use App\Traits\OrderStatusTrait;
use Illuminate\Http\Request;
use App\Enums\OrderStatusEnum;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.orders.';

    use OrderStatusTrait;

    public function __construct()
    {

        $this->model = new Order();

        $this->middleware('permission:orders-list|orders-delete', ['only' => ['index', 'show']]);
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index( Request $request , OrderService $orderService )
    {
        $items = $orderService->index( $request->all() ) ;

        $orderStatusItems = $this->OrderStatusItems();
        $clients = Client::client()->get();
        $providers = Client::provider()->get();
        $services = Service::parent()->get();

        return $this->view('index', compact('items' , 'providers' , 'clients' ,'services' ,'orderStatusItems'));
    }

    public function show( $id ,OrderService $orderService)
    {
        $data = Order::with("service","client","provider","trackingOrder",'addressItem',"orderDetailsItemsEdit" ,"orderDetailsItems")
            ->where("id",$id)->first() ;

        $services = Service::childrenItems()->get();

        $images = OrderImage::where("order_id", $id )->get() ;

//        return $images ;

        $survey =  SurveyOrder::where("order_id",$id)->first();
        $answers = [] ;
        if ( $survey ){
            $answers = SurveyOrderAnswer::where("survey_order_id" , $survey->id)->get();
        }
        $lat = $data->lat;
        $lng = $data->lon;
        $city ="";
        if ( $lat && $lng ) {
            $city = City::query()
                ->whereNotNull('zone_polygon')
                ->where('zone_polygon', 'LIKE', 'POLYGON%')
                ->whereRaw("ST_Intersects(ST_GEOMFROMTEXT('POINT(".$lat." ".$lng.")'), ST_GEOMFROMTEXT(zone_polygon))")
                ->first();
        }

        return $this->view('show', compact('data' ,'answers','city','services' ,"images"));
    }


    public function update( Request $request , $id )
    {
        $data = Order::with("service")->where("id",$id)->first() ;
        $data->management_comment = $request->management_comment ;
        $data->save() ;
        flash(__('messages.commented_administration'))->success();
        return redirect()->to(route("orders.show" , $data->id) );
    }

    public function destroy($id , OrderService $orderService )
    {
        $orderService->delete($id);
        flash(__('Deleted Successfully'))->success();
        return back();
    }

    public function assign( Request $request )
    {
        $orderId = $request->orderId ;
        $providerId = $request->providerId ;
        $reAssign = $request->reAssign ;
        $order = Order::find($orderId);

        if ( $reAssign == 1 ) {
            $this->reAsign($order->provider_id  , $orderId) ;
        }


        $order->provider_id = $providerId ;
        $order->status = OrderStatusEnum::UnderRevision->value ;
        $order->save() ;

        //===========================================
        $provider = Client::where("id" ,$providerId)->first();
        $message = 'تم ارسال طلب جديد لك و في انتظار موافقتك علي الطلب . رقم الطلب ' . $order->id ;
        $message_en = 'A new request has been sent to you and is awaiting your approval of the request. order number' . $order->id ;
        $titleNote =  'app.note_new_request' ;
        $provider->notify(new ClientNotification($message , $message_en , $titleNote));

        if ( $provider->player_id ) {
            $titleNote =  __('app.note_new_request') ;
            $notify_fire = new FirebaseNotification();
            $notify_fire->sendNote([$provider->player_id], 'Kafo', $message , $titleNote , $provider->type_device ,"order");
        }

        return response()->json(["status" => true , "message" => __() ]) ;
    }


    public function reject( Request $request )
    {
        $reason = $request->reason ;
        $id = $request->orderId ;
        $order = Order::find($id);
        $order->status = OrderStatusEnum::RejectFromMangment->value ;
        $order->management_reject_reson = $reason  ;
        $order->save() ;


        $client = Client::find($order->client->id) ;

        if ( $client->player_id ) {
            $message = 'تم رفض طلبك بسبب ' . $reason;
            $message_en = 'Your request was rejected because' . $reason;
            $client->notify(new ClientNotification($message , $message_en));
        }

        return response()->json(["status" => true , "message" => __('app.order_rejected') ]) ;
    }

    public function updateCollection( Request $request )
    {
        $id = $request->id ;
        $order = Order::where("id" , $id)->first();
        $order->is_paid = 1 ;
        $order->save();
        return response()->json(["status" => true ]) ;
    }

    public function rejectCanceled( Request $request )
    {
        $id = $request->id ;
        $order = Order::where("id" , $id)->first();
        $order->status = OrderStatusEnum::TechnicianApproved->value ;
        $order->save();

        $titleNote = 'app.note_reject_request_from_admin' ;

        $message = 'تم رفض طلب الإلغاء المقدم للطلب ' . $order->id ;
        $message_en = 'The request to cancel the order was rejected by the administration '  . $order->id ;


        $order->provider->notify(new ClientNotification($message , $message_en , $titleNote));

        if ( $order->provider->player_id ) {
            $titleNote = __('app.note_reject_request_from_admin') ;
            $notify_fire = new FirebaseNotification();
            $dataNote = [
                "status_code" => 3 ,
                "order_id" => $order->id
            ];
            $notify_fire->sendNote([$order->provider->player_id], $dataNote , $message , $titleNote ,$order->provider->type_device);
        }

        return response()->json(["status" => true ]) ;
    }

    public function newCost( Request $request )
    {
        $id = $request->orderId ;
        $order = Order::where("id" , $id)->first();
        $order->new_cost = $request->newCost ;
        $order->status = OrderStatusEnum::SendCostModificationToClient->value ;
        $order->save();

        //==================================

        $orderDetails = $order->orderDetailsEdit()->where("admin_status","under_revision")->first();
        if ( $orderDetails ) {
            $orderDetails->price = $request->newCost;
            $orderDetails->admin_status = "approved";
            $orderDetails->save();
        }


        //==================================
        $message = "تم ارسال التكلفه الجديده علي الطلب لتصبح " .$request->newCost ."  ". __('app.SAR');
        $message_en = "The new cost has been sent to the order " . $request->newCost .  " SAR";

        $titleNote =  "app.note_response_edit_request_from_admin"  ;

        // notify client
        $order->client->notify(new ClientNotification($message , $message_en , $titleNote));
        if ( $order->client->player_id ) {
            $titleNote =  __($titleNote)  ;
            $notify_fire = new FirebaseNotification();
            $notify_fire->sendNote([$order->client->player_id], 'Kafo', $message , $titleNote ,$order->client->type_device);
        }

        // notify provider
        if ( $order->provider->player_id ) {
            $titleNote =  __($titleNote)  ;
            $notify_fire = new FirebaseNotification();
            $notify_fire->sendNote([$order->provider->player_id], 'Kafo', $message , $titleNote);
        }
        $this->changeOrderDetails( $order->id , 1 ) ;
        return response()->json(["status" => true ]) ;
    }

    public function approveNewCostFromClient( Request $request )
    {
        $id = $request->order_id ;
        $order = Order::where("id" , $id)->first();
        $order->status = 5 ; //todo;
        $order->save();

        //==================================
        $message = " تم ارسال التكلفه الجديده علي الطلب لتصبح  "   . $request->newCost ."  ريال " ;
        $message_en = "The new cost has been sent to the order  ". $request->newCost ." SAR";

        $titleNote =  "app.note_response_edit_request_from_admin"  ;


        $order->client->notify(new ClientNotification($message , $message_en , $titleNote));
        if ( $order->client->player_id ) {
            $titleNote =  __("app.note_response_edit_request_from_admin")  ;
            $notify_fire = new FirebaseNotification();
            $notify_fire->sendNote([$order->client->player_id], 'Kafo', $message ,$titleNote , $order->client->type_device);
        }
        return response()->json(["status" => true ]) ;
    }

    public function modificationStatus( Request $request )
    {
        $id = $request->orderId ;
        $order = Order::where("id" , $id)->first();
        if ( $request->status == 1 ){
            $order->status = OrderStatusEnum::AprovedModificationFromAdmin->value;
            $message =  'وافق علي تعديل الطلب . رقم الطلب ' .$order->id;
            $orderStatus =  2;
            $message_en =  'The administrator agreed to amend the request . Order ID ' . $order->id ;
            $titleNote =  "app.note_approve_modify_request" ;
            $this->changeOrderDetails( $order->id , 1 ) ;
        } else {
            $order->status = OrderStatusEnum::RejectedModificationFromAdmin->value;
            $message = 'رفض طلب التعديل رقم الطلب ' . $order->id;
            $orderStatus =  2;
            $message_en =   'reject modify request Order Id '.$order->id;
            $titleNote =  "app.note_reject_modify_request" ;
            $this->changeOrderDetails( $order->id , 0 ) ;
        }
        $order->save();
        //==================================
        $order->provider->notify(new ClientNotification($message , $message_en , $titleNote));
        if ( $order->provider->player_id ) {

            $dataNote = [
                "status_code" => $orderStatus ,
                 "order_id" => $order->id
            ];

            $titleNote = __($titleNote) ;
            $notify_fire = new FirebaseNotification();
            $notify_fire->sendNote([$order->provider->player_id], $dataNote, $message , $titleNote  );
        }
        return response()->json(["status" => true ]) ;
    }


    public function modificationStatusRejectAdmin( Request $request )
    {
        $id = $request->orderId ;

        $order = Order::where("id" , $id)->first();
        $order->status = OrderStatusEnum::RejectedModificationFromAdmin->value;
        $message =  'رفض الادمن تعديل الطلب . رقم الطلب ' .$order->id;
        $orderStatus = 3;
        $message_en = 'The admin refused to modify the request. Order ID ' . $order->id ;
        $titleNote = "app.note_reject_modify_request" ;
        $order->save();
        //=================================

        $orderDetails = OrderDetails::where("order_id" , $id)
            ->where("admin_status" , "under_revision")
            ->get();

        foreach ( $orderDetails as $details_item ) {
            $details_item->admin_status = "reject_from_admin" ;
            $details_item->save();
        }
        //==================================
        if ( $order->client ) {
            $order->client->notify(new ClientNotification($message , $message_en , $titleNote));

            $dataNote = [
                "status_code" => $orderStatus ,
                "order_id" => $order->id
            ];
            $titleNote = __($titleNote) ;
            $notify_fire = new FirebaseNotification();
            $notify_fire->sendNote([$order->client->player_id], $dataNote, $message , $titleNote ,$order->client->type_device );
        }
        //==================================
        if ( $order->provider  ) {
            $order->provider->notify(new ClientNotification($message , $message_en , $titleNote));
            $dataNote = [
                "status_code" => $orderStatus ,
                "order_id" => $order->id
            ];
            $titleNote = __($titleNote) ;
            $notify_fire = new FirebaseNotification();
            $notify_fire->sendNote([$order->provider->player_id], $dataNote, $message , $titleNote  );
        }

        return response()->json(["status" => true ]) ;
    }

    public function getProviders( Request $request )
    {
        $date = $request->date ;
        $time = $request->time;
        $lat = $request->lat;
        $lng = $request->lon;


        $citiesIDs = DB::table('cities')
            ->whereNotNull('zone_polygon')
            ->where('zone_polygon', 'LIKE', 'POLYGON%')
            ->whereRaw("ST_Intersects(ST_GEOMFROMTEXT('POINT(".$lat." ".$lng.")'), ST_GEOMFROMTEXT(zone_polygon))")
            ->pluck("id")
            ->toArray(); 
        $providers = Client::provider()->whereIn("city_id" ,$citiesIDs)->get();



        $providerNotValid = Order::query()
            ->where("service_date",$date)
            ->where("service_time",$time)
           ->currentOrders()
//             ->where("status", 4)
            ->whereNotNull("provider_id")
           ->pluck("provider_id")
//            ->get();
            ->toArray();


        $html = "<option value=''>".__("app.choose")."</option>";
        foreach ( $providers as $provider ){
            if ( !in_array( $provider->id , $providerNotValid ) ) {
                $html.= "<option value='".$provider->id."'>";
//                $html.=   ( in_array( $provider->id , $providerNotValid )  )? __('app.unavailable') : __('app.available') ;
                $html.= $provider->full_name;
                $html.= "</option>";
            }
        }
        return response()->json(["status" => true , "data" => $html ] ) ;
    }

    public function subServices( Request $request )
    {
        $id = $request->id ;
        $subItemId = $request->subItemId ?? null ;

        $services = Service::where("parent_id", $id )->active()->get();

        $html = "<option value=''>".__("app.choose")."</option>";
        foreach ( $services as $service ){
            $selected = ($service->id == $subItemId) ? "selected" : "";
            $selected = "selected";
            $html.= "<option  " . $selected . " value='".$service->id."'  >";
            $html.= $service->name;
            $html.= "</option>";
        }
        return response()->json(["status" => true , "data" => $html , "count" => $services->count() ] ) ;
    }

    private function changeOrderDetails($order_id , $status )
    {
        $orderDetails = OrderDetails::where("order_id",$order_id )
            ->where("type","edit")
            ->where("admin_status","under_revision")
            ->get();

        if ( $status == 1 ) {
            $statusText =  "approved" ;
        }else{
            $statusText =  "rejected" ;
        }
        foreach ( $orderDetails as $orderDetails_ ){
            $orderDetails_->admin_status = $statusText ;
            $orderDetails_->save();
        }
    }

    private function reAsign( $providerId , $order_id  )
    {
        $message = "تم قبول طلب الإلغاء المقدم للطلب  " . $order_id ;
        $message_en = " Your cancellation request has been accepted. order number " . $order_id;
        $titleNote = "app.note_accept_cancellation_request" ;

        $order = Order::where("id",$order_id)->first();

        TrackingOrder::create([
            "order_id" => $order_id ,
            "provider_id" => $providerId ,
            "reason" => $order->reject_reson ,
            "status_id" => 8
            ]);

        $provider = Client::where("id",$providerId)->first();
        $provider->notify(new ClientNotification($message , $message_en , $titleNote));
        if ( $provider->player_id ) {

            $dataNote = [
                "status_code" => 2 ,
                "order_id" => $order_id
            ];
            $titleNote = __($titleNote) ;
            $notify_fire = new FirebaseNotification();
            $notify_fire->sendNote([$provider->player_id], $dataNote, $message , $titleNote  );
        }
        return ;
    }


    public function updates( Request $request , $id )
    {
        if ( $request->filled('remove_item') ) {
            $orderDetails = OrderDetails::where("id",$request->remove_item)
                ->first() ;
            $orderDetails->is_remove = true ;

            $orderDetails->notes = "remove_status" ;
            $orderDetails->save() ;
            $this->adminApproved( $request->remove_item ) ;
        }

        // تم التعديل علي الحاله
        if ( $request->filled('old_service_id') ) {
            $orderDetails = OrderDetails::where("id", $request->old_service_id )->first() ;
            $orderDetails->price = $request->price ;
            $orderDetails->notes = "modified_status" ;
            $orderDetails->save() ;

            $this->adminApproved( $request->old_service_id  ) ;
        }

        // add new item
        if ( $request->filled('service_id') ) {

            OrderDetails::create([
                "service_id" => $request->service_id,
                "price" => $request->price,
                "type" => "edit",
                "is_edit" => 1,
                "admin_status" => "approved",
                "notes" => "add_service_status" ,
                "order_id" => $id
            ]);

            $this->adminApproved( $orderDetails->id ) ;
        }

        $this->getTotal( $id ) ;


        //=====================================

        $order = Order::where("id",$id)->first();

        $order->status = OrderStatusEnum::SendCostModificationToClient->value ;
        $order->save() ;

        $titleNote =  "app.note_response_edit_request_from_admin";
        $message = "" ;

        if ( $order->provider->player_id ) {
            $titleNote =  __($titleNote)  ;
            $notify_fire = new FirebaseNotification();
            $notify_fire->sendNote([$order->provider->player_id], 'Kafo', $message , $titleNote);
        }

        if ( $order->client->player_id ) {
            $message = "تم الموافقه علي طلب التعديل من الادمن . و تكون تكلفه التعديل "  . $request->price ;
            $dataNote = [
                "status_code" => 2 ,
                "order_id" => $id
            ];
            $titleNote = __($titleNote) ;
            $notify_fire = new FirebaseNotification();
            $notify_fire->sendNote([$order->client->player_id], $dataNote, $message , $titleNote ,$order->client->type_device   );

        }
        flash(__('messages.updated_successfully'))->success();
        return redirect()->to(route("orders.show" , $id ) );
    }


    private function adminApproved( $id )
    {
        $orderDetails = OrderDetails::find($id);
        $orderDetails->admin_status = "approved" ;
        $orderDetails->is_edit = 1 ;
        $orderDetails->save() ;
    }

    private function getTotal( $orderId )
    {
        $order = Order::with('orderDetailsItems')->find($orderId);
        $orderDetailsNews = OrderDetails::where("order_id", $orderId)
            ->where("type", "new")
            ->get();

        $orderDetailsEdit = OrderDetails::where("order_id", $orderId)
            ->where("type", "edit")
            ->get();

        $itemRows = [];

        foreach ($orderDetailsNews as $details_)
        {
            $itemRows[] = $details_->price;
        }
        //todo check coupon

        foreach ($orderDetailsEdit as $orderDetails){
            if ( $orderDetails->is_remove == 0 &&
                ( $orderDetails->admin_status == "approved" || $orderDetails->admin_status == "approved_client" )

            ){
                $itemRows[] = $orderDetails->price;
            }
        }

        $order->total = array_sum($itemRows) ;
        $order->save();
    }

}
