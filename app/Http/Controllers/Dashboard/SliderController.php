<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Slider\SliderStoreRequest;
use App\Http\Requests\Dashboard\Slider\SliderUpdateRequest;
use App\Models\Slider;
use App\Services\SliderService;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.sliders.';

    public function __construct()
    {
        $this->model = new Slider();

        $this->middleware('permission:sliders-list|sliders-create|sliders-edit|sliders-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:sliders-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:sliders-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:sliders-delete', ['only' => ['destroy']]);
        /**/

    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index(Request $request , SliderService $sliderService)
    {
        $items = $sliderService->index($request->all()) ;
        return $this->view('index', compact('items'));
    }

    public function store( SliderStoreRequest $request, SliderService $sliderService )
    {
        $sliderService->store($request->validated()) ;
        flash(__('messages.added_successfully'))->success();
        return back();
    }

    public function update(SliderUpdateRequest $request, $id , SliderService $sliderService )
    {
        $sliderService->update( $id ,$request->validated() ) ;
        flash(__('messages.updated_successfully'))->success();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id , SliderService $sliderService )
    {
        $sliderService->delete($id);
        flash(__('messages.deleted_successfully'))->success();
        return back();
    }

    public function updateAxios( Request $request , $id )
    {
        $item = $this->model->findOrFail($id);
        $data = [] ;
        foreach ( $request->all() as $key => $value )
        {
            $data = [ $key => $value ] ;
        }
        $item->update($data);
        return response()->json(["status" => true , "message" => __('messages.updated_successfully') ]) ;
    }
}
