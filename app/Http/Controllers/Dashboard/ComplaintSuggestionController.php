<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\ComplaintSuggestion\ComplaintReplayRequest;
use App\Http\Requests\Dashboard\SurveyQuestion\SurveyQuestionUpdateRequest;
use App\Http\Requests\Dashboard\SurveyQuestion\SurveyQuestionStoreRequest;
use App\Models\ComplaintSuggestion;
use App\Models\SurveyQuestion;
use App\Enums\StatusesEnum;
use App\Services\ComplaintSuggestionService;
use App\Services\SurveyQuestionService;
use Illuminate\Http\Request;

class ComplaintSuggestionController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.complaintSuggestion.';

    public function __construct()
    {
        $this->model = new ComplaintSuggestion();
        /**/
        //$this->middleware('permission:complaintSuggestion-list|survey-create|survey-edit|survey-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:complaints-suggestions-list', ['only' => [ 'index']]);
        $this->middleware('permission:complaints-suggestions-replay', ['only' => ['replay']]);


    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index( Request $request , ComplaintSuggestionService $complaintSuggestionService)
    {
        $items = $complaintSuggestionService->index( $request->all() ) ;
        return $this->view('index', compact('items'));
    }

    public function replay( Request $request, ComplaintSuggestionService $complaintSuggestionService )
    {
        $complaintSuggestionService->replay($request->all()) ;
        flash(__('messages.added_successfully'))->success();
        return back();
    }
}
