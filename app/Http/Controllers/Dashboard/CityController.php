<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\City\CityStoreRequest;
use App\Http\Requests\Dashboard\City\LoginRequest;
use App\Http\Requests\Dashboard\City\CityUpdateRequest;
use App\Models\City;
use App\Services\CityService;
use Illuminate\Http\Request;

class CityController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.cities.';

    public function __construct()
    {
        $this->model = new City();

        $this->middleware('permission:cities-list|cities-create|cities-edit|cities-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:cities-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:cities-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:cities-delete', ['only' => ['destroy']]);

    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , CityService $cityService)
    {
        $cities = $cityService->index($request->all()) ;
        return $this->view('index', compact('cities'));
    }


    public function store(CityStoreRequest $request, CityService $cityService )
    {
        $cityService->store($request->validated()) ;
        flash(__('messages.added_successfully'))->success();
        return back();
    }

    public function update( CityUpdateRequest $request, $id, CityService $cityService)
    {
        $cityService->update($id , $request->validated()) ;
        flash(__('messages.updated_successfully'))->success();
        return back();
    }

    public function edit( $id , CityService $cityService)
    {
        $item = $cityService->show($id) ;
        $polygonLatLng = [];

        $polygon = $item->zone_polygon;

        //POLYGON((30.811489 30.9902944,30.7923579 30.9630002,))
        if($polygon && $polygon !=null && $polygon !='' && strpos($polygon, 'POLYGON((') !== false){
            $poygonstring = str_replace("POLYGON((","",$polygon);
            $poygonstring = str_replace("))","",$poygonstring);
            $stringArrComma = explode(',', $poygonstring);
            $iObject=0;
            foreach($stringArrComma as $point){
                $point = trim($point);
                $stringArrspace = explode(' ', $point);
                if($stringArrspace && $stringArrspace!=null && count($stringArrspace)>0){
                    $polygonLatLng[$iObject]["lat"]=(float)$stringArrspace[0];
                    $polygonLatLng[$iObject]["lng"]=(float)$stringArrspace[1];
                    $iObject=$iObject +1;
                }

            }
        }
        return $this->view('edit', compact('item' ,'polygonLatLng'));
    }

    public function destroy($id , CityService $cityService)
    {
        $cityService->delete($id);
        flash(__("messages.deleted_successfully"))->success();
        return back();
    }

    public function updateAxios( Request $request , $id )
    {
        $item = $this->model->findOrFail($id);
        $data = [] ;
        foreach ( $request->all() as $key => $value )
        {
            $data = [ $key => $value ] ;
        }
        $item->update($data);
        return response()->json(["status" => true , "message" => __('messages.updated_successfully') ]) ;
    }
}
