<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Provider\ProviderStoreRequest;
use App\Http\Requests\Dashboard\Provider\ProviderUpdateRequest;
use App\Models\City;
use App\Models\Client;
use App\Services\ProviderService;
use App\Traits\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProviderController extends Controller
{
    use UploadFile;
    protected $viewsDomain = 'dashboard.providers.';

    public function __construct()
    {
        $this->model = ( new Client() )->newQuery()->provider();
        /**/
        $this->middleware('permission:providers-list|providers-create|providers-edit|providers-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:providers-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:providers-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:providers-delete', ['only' => ['destroy']]);
        $this->middleware('permission:providers-show', ['only' => ['show']]);
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index(Request $request , ProviderService $providerService )
    {
        $items = $providerService->index($request->all()) ;
        $cities = City::active()->get() ;
        return $this->view('index', compact('items','cities'));
    }

    public function create()
    {
        return $this->view('create');
    }

    public function store( ProviderStoreRequest $request, ProviderService $providerService )
    {
        $data = $request->validated();
        $data['verify'] = 1;
        $data['password'] = Hash::make($data['password']);
        $data['type'] = "provider";

        $file = $request->avatar ;
        $data["avatar"] = $this->upload($file , "avatar"  , 400 , 400);
        $this->model->create($data) ;
        flash(__('messages.added_successfully'))->success();
        return back();
    }

    public function update( ProviderUpdateRequest $request, $id )
    {
        $item = $this->model->find($id) ;
        $data = $request->validated() ;
        if (  ! $request->password ) {
            unset($data['password']); ;
        }else{
            $data['password'] = Hash::make($data['password']);
        }

        if ($request->avatar  ) {
            $file = $request->avatar ;
            $data["avatar"] = $this->upload($file , "avatar"  , 400 , 400);
        }

        $item->update($data) ;
        flash(__('messages.updated_successfully'))->success();
        return back();
    }

    public function edit( $id )
    {
        $item = $this->model->find($id) ;
        $cities = City::active()->get();
        return $this->view('edit' , compact('item' ,'cities'));
    }

    public function show( $id , ProviderService $providerService )
    {
        $item = $providerService->show($id) ;
        return $this->view('show', compact('item'));
    }

    public function destroy($id , ProviderService $providerService )
    {
        $providerService->delete($id);
        flash(__('messages.deleted_successfully'))->success();
        return back();
    }

    public function updateAxios( Request $request , $id )
    {
        $item = $this->model->findOrFail($id);
        $data = [] ;
        foreach ( $request->all() as $key => $value )
        {
            $data = [ $key => $value ] ;
        }

        $item->update($data);

        // To remove token from app
        if ( $item->active == "inactive" ){
            $item->tokens()->delete();
        }
        return response()->json(["status" => true , "message" => __('messages.updated_successfully') ]) ;
    }



    public function updateCityAxios( Request $request , $id )
    {
        $item = $this->model->findOrFail($id);
        $cityId = $request->city_id ;
        $block = $request->block_status ;
        $client = Client::where("id",$id)->first();
        $client->city_id = $cityId ;
        $client->block_status = $block ;
        $client->save();

        // To remove token from app
        if ( $item->active == "inactive" ){
            $item->tokens()->delete();
        }
        return response()->json(["status" => true , "message" => __('messages.updated_successfully') ]) ;
    }

    public function updateAxiosBlock( Request $request , $id )
    {
        $item = $this->model->findOrFail($id);

        $item->block_status =  $request->block_status ;
        $item->save();

        // To remove token from app
        if ( $item->block_status == "block" ){
            $item->tokens()->delete();
        }


        if ( $request->active == 'active' ){
            return response()->json(["status" => true , "message" => __('messages.block_successfully_deactivated') ]) ;
        }else{
            return response()->json(["status" => true , "message" => __('messages.block_successfully_delete') ]) ;
        }

    }

}
