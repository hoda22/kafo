<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Brand\BrandStoreRequest;
use App\Http\Requests\Dashboard\Brand\BrandUpdateRequest;
use App\Models\Brand;
use App\Models\Client;
use App\Models\Order;
use App\Services\BrandService;
use App\Services\OrderService;
use App\Traits\PaymentMethodTrait;
use App\Traits\UploadFile;
use Illuminate\Http\Request;

class PaymentRequestController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.orders.';
    use UploadFile;
    use PaymentMethodTrait ;

    public function __construct()
    {
//        $this->middleware('permission:brands-list|brands-create|brands-edit|brands-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:payment-requests-list', ['only' => ['paymentRequest']]);
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function paymentRequest(Request $request , OrderService $orderService)
    {
        $items = Order::where("status" , 7)->orderBy('id', 'DESC')->paginate();
        $clients = Client::client()->get();
        $providers = Client::provider()->get();
        $paymentMethods = $this->getPaymentMethodsAttribute() ;

        return $this->view('paymentRequest', compact('clients' ,'providers','items' ,'paymentMethods'));
    }
}
