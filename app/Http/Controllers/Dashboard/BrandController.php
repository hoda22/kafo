<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Brand\BrandStoreRequest;
use App\Http\Requests\Dashboard\Brand\BrandUpdateRequest;
use App\Models\Brand;
use App\Services\BrandService;
use App\Traits\UploadFile;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.brands.';
    use UploadFile;

    public function __construct()
    {
        $this->model = new Brand();

        $this->middleware('permission:brands-list|brands-create|brands-edit|brands-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:brands-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:brands-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:brands-delete', ['only' => ['destroy']]);

    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , BrandService $brandService)
    {
        $items = $brandService->index($request->all()) ;
        return $this->view('index', compact('items'));
    }

    public function store( BrandStoreRequest $request, BrandService $brandService )
    {
        $brandService->store($request->validated()) ;
        flash(__('messages.added_successfully'))->success();
        return back();
    }

    public function update( BrandUpdateRequest $request, $id , BrandService $brandService)
    {
        $brandService->update($request->validated() ,$id ) ;
        flash(__('messages.updated_successfully'))->success();
        return back();
    }

    public function destroy($id , BrandService $brandService)
    {
        $isDeleted = $brandService->delete($id);
        if ( $isDeleted ) {
            flash(__('messages.deleted_successfully'))->success();
        }else{
            flash(__('messages.cant_deleted'))->error();
        }
        return back();

    }

    public function updateAxios( Request $request , $id )
    {
        $item = $this->model->findOrFail($id);
        $data = [] ;
        foreach ( $request->all() as $key => $value )
        {
            $data = [ $key => $value ] ;
        }
        $item->update($data);
        return response()->json(["status" => true , "message" => __('messages.updated_successfully') ]) ;
    }
}
