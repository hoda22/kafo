<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Setting\SettingStoreRequest;
use App\Models\Setting;
use App\Services\SettingService;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.settings.';

    public function __construct()
    {
        $this->model = new Setting();
        $this->middleware('permission:settings-list|settings-edit', ['only' => ['index', 'show']]);
        $this->middleware('permission:settings-edit', ['only' => ['edit', 'update']]);

    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index( SettingService $settingService )
    {
        $setting = Setting::firstOrNew() ;
        return $this->view('index', compact('setting'));
    }

    public function store( SettingStoreRequest $request , SettingService $settingService )
    {
        $settingService->update( $request->validated() ) ;
        flash(__('messages.updated_successfully'))->success();
        return back();
    }
}
