<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Notification\NotificationStoreRequest;
use App\Models\Notification;
use App\Services\NotificationService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.notifications.';

    public function __construct()
    {
        $this->model = new Notification();

//        $this->middleware('permission:notifications-list|notifications-create|notifications-edit|notifications-delete', ['only' => ['index', 'show']]);
//        $this->middleware('permission:notifications-create', ['only' => ['create', 'store']]);
//        $this->middleware('permission:notifications-show', ['only' => ['show']]);
//        $this->middleware('permission:notifications-edit', ['only' => ['edit', 'update']]);
//        $this->middleware('permission:notifications-delete', ['only' => ['destroy']]);


        $this->middleware('permission:notifications-list', ['only' => ['index']]);
        $this->middleware('permission:notifications-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:notification-show', ['only' => ['show']]);

    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index(Request $request , NotificationService $notificationService )
    {
        $items = $notificationService->index($request->all()) ;
        return $this->view('index', compact('items'));
    }

    public function store( NotificationStoreRequest $request, NotificationService $notificationService )
    {
        $notificationService->store($request->validated()) ;
        flash(__('messages.added_successfully'))->success();
        return back();
    }

    public function show($id , NotificationService $notificationService)
    {
        $item = $notificationService->show($id);
        return $this->view('show', compact('item'));
    }

//    public function destroy($id , NotificationService $notificationService)
//    {
//        $notificationService->show($id);
//        flash(__("messages.deleted_successfully"))->success();
//        return back();
//    }
}
