<?php

namespace App\Http\Controllers\Dashboard;

use App\FirebaseNotification;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\ChannelChat;
use App\Models\ChannelMessage;
use App\Models\Client;
use App\Traits\UploadFile;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.chat.';
    use UploadFile;

    public function __construct()
    {

        /**/
//        $this->middleware('permission:chat-list|brands-create|brands-edit|brands-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:chat-list', ['only' => ['index']]);


    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index( $id = null )
    {
        $chats = ChannelChat::admin()
            ->with("lastMessage")
//            ->join("channel_messages" ,"channel_messages.channel_id","channels.id")
//            ->select("channel_messages.id")
//            ->orderBy("channel_messages.id" , "DESC")
            ->get() ;
//        ->sortBy(function($item, $key) {
//                return $item->lastMessage;
//            });


        $channelChats = ChannelChat::admin()->get() ;
        $chatsIn = [] ;
        foreach ( $channelChats as $channelChat ){
            $chatsIn[] = $channelChat->client_id ;
        }

        $clients = Client::whereNotIn("id",$chatsIn)->get();


        $userChat="" ;
        $client="" ;
        $sender="" ;
        $userChatMessage =[] ;

        if ( $id ) {

            $chatRoom = ChannelChat::where(function ( $query ) use ( $id ){
                $query->where("sender_id" , $id)
                    ->where("receiver_id" ,0) ;
            })->orWhere(function ( $query2 ) use ( $id ){
                $query2->where("sender_id" , 0)
                    ->where("receiver_id" ,$id ) ;
            })->first() ;


            if ( !$chatRoom ) {
                $chatRoom = ChannelChat::create([
                    "sender_id" => $id,
                    "receiver_id" => 0,
                ]);
            }


            $userChat = ChannelChat::where("id", $chatRoom->id )->first() ;



            if ( $userChat->receiver_id != 0 ) {
                $sender = "admin" ;
                $client = Client::find($userChat->receiver_id);
            }else{
                $sender = "user" ;
                $client = Client::find($userChat->sender_id);
            }

            $userChatMessage = ChannelMessage::where( "channel_id" , $id )->get();
        }

        $clientsData = Client::all();

        $clients = [] ;
        foreach ( $clientsData as $client_ ){
            $client_["avatar"] = asset( $client_->avatar );
            $clients[] = $client_ ;
        }

        $clients = json_encode($clients) ;

        return $this->view('index' , compact('chats','clients' ,'userChat' , 'client' , 'userChatMessage' , 'sender'));
    }


    public function sendMessage( Request $request )
    {
        ChannelMessage::create([
            "channel_id" => $request->id,
            "message" => $request->message ,
            "sender_id" => 0,
        ]);
        return response()->json() ;
    }


    public function getAtherClient( Request $request )
    {
        $clients = Client::WhereNotIn("id", $request->chatRooms )
            ->where("block_status","unblock")
            ->where("active","active")
            ->get();
        $html ="";
        $ids = [] ;
        foreach ( $clients as $client ){


        $html.= '<li class="d-none newItem chat-item read '.$client->type.'" id="chat-'.$client->id.'" data-id="'.$client->id.'">
                    <a href="'. route("chat.index", $client->id ) .'" class="d-flex align-items-center">
                        <figure class="mb-0 ml-2">
                            <img src="'.asset( $client->avatar).'" class="img-xs rounded-circle" alt="user">
                        </figure>
                        <div class="d-flex justify-content-between flex-grow border-bottom">
                            <div>
                                <p class="text-body font-weight-bold"> '.$client->full_name .'   </p>
                                 <p class="text-muted tx-13 message_last">
                                </p>
                            </div>
                            <div class="d-flex flex-column align-items-end">
                                <p class="text-muted tx-13 mb-1">   </p> 
                            </div>
                        </div>
                    </a>
                </li>
                ';


            $ids[]=$client->id ;

        }
        return response()->json(["html" => $html , "ids" => $ids , "status" => true ]) ;


    }



    public function sendChatMessage( Request $request )
    {
        $message = $request->message ;
        $clientId = $request->client_id ;

        $client = Client::find($clientId) ;

        if ( $client->player_id ){

            $titleNote =  __('app.new_chat') ;
            $notify_fire = new FirebaseNotification();
            $test = $notify_fire->sendNote([$client->player_id], ["order_id" => $client->id ], $message ,   $titleNote ,$client->type_device ,"chat");
//            dd($test);
            return true ;
        }
        return true;
    }


}
