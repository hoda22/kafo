<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\SparePart\SparePartUpdateRequest;
use App\Http\Requests\Dashboard\SparePart\SparePartStoreRequest;
use App\Models\Brand;
use App\Models\Service;
use App\Models\SparePart;
use App\Services\SparePartService;
use App\Enums\StatusesEnum;
use Illuminate\Http\Request;

class SparePartController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.spareParts.';

    public function __construct()
    {
        $this->model = new SparePart();
        $this->middleware('permission:spare-parts-list|cities-create|spare-parts-edit|spare-parts-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:spare-parts-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:spare-parts-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:spare-parts-delete', ['only' => ['destroy']]);
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index( Request $request , SparePartService $sparePartService)
    {
        $items = $sparePartService->index( $request->all() ) ;
        $brands = Brand::get() ;
        $services = Service::parent()->active()->get();
        $sup_services = Service::childrenItems()->get();
        return $this->view('index', compact('items','brands','services' ,'sup_services'));
    }


    public function store(SparePartStoreRequest $request, SparePartService $sparePartService )
    {
        $sparePartService->store($request->validated()) ;
        flash(__('messages.added_successfully'))->success();
        return back();
    }

    public function update(SparePartUpdateRequest $request, $id ,SparePartService $sparePartService )
    {
        $sparePartService->update($id , $request->validated()) ;
        flash(__('messages.updated_successfully'))->success();
        return back();
    }

    public function destroy($id , SparePartService $sparePartService)
    {
        $sparePartService->delete($id);
        flash(__('messages.deleted_successfully'))->success();
        return back();
    }

    public function updateAxios( Request $request , $id )
    {
        $item = $this->model->findOrFail($id);
        $data = [] ;
        foreach ( $request->all() as $key => $value )
        {
            $data = [ $key => $value ] ;
        }
        $item->update($data);
        return response()->json(["status" => true , "message" => __('messages.updated_successfully') ]) ;
    }
}
