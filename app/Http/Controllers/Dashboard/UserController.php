<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.users.';

    public function __construct()
    {
        $this->model = new User();

        $this->middleware('permission:users-list|users-create|users-edit|users-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:users-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:users-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:users-delete', ['only' => ['destroy']]);

    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $users = $this->model->when($request->filled('name'), function ($q) use ($request) {
            $q->where('name', $request->name);
        })->where("flag" , null)->latest()->paginate(20);


        $roles = Role::get();


//        return $users[0]->getRoleNames() ;


//        return $users[0]->roles()->pluck('id');


        return $this->view('index', compact('users', 'roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:users,name',
            'last_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => ['confirmed', 'min:8'],
            'role_id' => 'required' ,
        ]);

        $user = $this->model;
        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->save();

        $user->roles()->attach([$request->role_id]);
        flash(__('messages.added_successfully'))->success();
        return back();
    }

    public function update(Request $request, $id)
    {

//        dd( $request->password , $request->password_confirmation ) ;
        $user = $this->model->findOrFail($id);
        $this->validate($request, [
            'name' => 'required|string|unique:users,name,'.$id,
            'last_name' => 'required|string',
            'phone' => 'nullable',
            'email' => 'required|email|unique:users,email,'.$id,
//            'password' => ['nullable', 'confirmed', 'min:8'],
            'role_id' => 'required|exists:roles,id'
        ]);
        $user->update($request->only('name', 'email' ,'last_name','phone'));
        if ($request->filled('password')) {
            $user->password = bcrypt($request->password);
            $user->save();
        }
        $user->roles()->sync([$request->role_id]);
        flash(__('messages.updated_successfully'))->success();
        return back();
    }

    public function destroy($id)
    {
        $user = $this->model->findOrFail($id);
        if ($user->id == 1 ) {
            flash()->error('لا يمكن حذف المستخدم الرئيسى');
            return back();
        }

        if ( count( $user->userMessages ) ){
            flash()->error(__('messages.cannot_deleted_user'));
            return back();
        }

        $user->delete();
        flash(__('messages.deleted_successfully'))->success();
        return back();
    }

    public function updateAxios( Request $request , $id )
    {
        $item = $this->model->findOrFail($id);
        $data = [] ;
        foreach ( $request->all() as $key => $value )
        {
            $data = [ $key => $value ] ;
        }
        $item->update($data);
        return response()->json(["status" => true , "message" => __('messages.updated_successfully') ]) ;
    }
}
