<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\UserMessage\UserMessageStoreRequest;
use App\Models\UserMessage;
use App\Services\UserMessageService;
use Illuminate\Http\Request;

class UserMessageController extends Controller
{
    protected $model;
    protected $viewsDomain = 'dashboard.userMessages.';

    public function __construct()
    {
        $this->model = new UserMessage();

        $this->middleware('permission:messages-list|messages-send|messages-edit|messages-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:messages-send', ['only' => ['create', 'store']]);
        $this->middleware('permission:messages-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:messages-delete', ['only' => ['destroy']]);
        $this->middleware('permission:messages-show', ['only' => ['show']]);
        /**/
    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index(Request $request , UserMessageService $userMessageService )
    {
        $items = $userMessageService->index($request->all()) ;
        return $this->view('index', compact('items'));
    }

    public function store( UserMessageStoreRequest $request, UserMessageService $userMessageService )
    {
        $userMessageService->store($request->validated()) ;
        flash(__('messages.added_successfully'))->success();
        return back();
    }

    public function show($id , UserMessageService $userMessageService)
    {
        $item = $userMessageService->show($id);
        return $this->view('show', compact('item'));
    }
}
