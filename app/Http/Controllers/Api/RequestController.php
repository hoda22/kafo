<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Cart\CreateOrderRequest;
use App\Models\AddressClient;
use App\Models\LogProcess;
use App\Models\LoyaltyPoint;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\PricePoint;
use App\Models\WalletTransaction;
use App\Traits\OrderTrait;
use App\Traits\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as UserRequest;

class RequestController extends BaseController
{
    use UploadFile , OrderTrait;

    public function requestOrder( CreateOrderRequest $request )
    {
        $order = $this->getOrder() ;
        $total = $this->getTotalOrderItems($order->id) ;

        $user = UserRequest::user() ;

        $this->addLoyaltyPoints( $total , $order ) ;

        $data["order_id"]= $order->id ;
        $discount_refund = 0 ;
        $coupon_type = "" ;
        $coupon_discount = 0 ;

        if ( $request->filled("refund") && $request->refund == 1 )
        { // if refund
            $valueRefund = $this->refund() ;
            if ( $valueRefund ) {
                $check = $this->updateUserPoints( $valueRefund->num_point ) ;
                if ( $check ) {
                    $discount_refund = $valueRefund->num_sar ;
                }
            }
        }

        if ( $request->filled("coupon_id") )
        { // To discount coupon
            $coupon = $this->coupon( $request->coupon_id , $total ) ;
            $coupon_discount = $coupon['coupon_discount'] ;
            $coupon_type = $coupon['type'] ;
        }

        if ( $request->payment == 2 || $request->payment == 3 )
        {
            $resultCode = $this->onlinePayment( $total , $order->id , $request->payment );
            if ($resultCode->result->code == "000.200.100"){
                $id = $resultCode->id ;
                $data['checkout_id'] = $id ;
            }else{
                return false; // todo here if processe faild
            }
        } else if ( $request->payment == 5 ) {

            info("balance Before :".$user->balance) ;

            if ($user->balance >= $total ){
                $resultCode = $this->walletPayment( $total , $order->id , $request->payment );
            }else{
                $error = __('app.no_balance') ;
                return $this->sendError($error);
            }
            info("Total : ".$total) ;

            info("balance Before :".$user->balance) ;
        }

        $address = AddressClient::find($request->address_id) ;

        $order->basic_total     = $total ;
        $order->total           = $total - $discount_refund ;
        $order->total_all       = $total  ;
        $order->discount_refund = $discount_refund ;

        $order->coupon_discount = $coupon_discount ;
        $order->coupon_type     = $coupon_type ;

        $order->coupon_id       = $request->coupon_id ;
        $order->address_id      = $request->address_id ;
        $order->service_date    = $request->date ;
        $order->service_time    = $request->time ;
        $order->method_payment  = $request->payment ;
        $order->status          = 1 ; // know is order

        $order->lat             = $address->lat ;
        $order->lon             = $address->lon ;

        $order->save();

        $data['total']          = $order->total ;
        $data['net']            = $order->net ;
        $data['total_all']      = $order->total + $discount_refund  ;

        $data['replaced_point'] = "" ;

        $gift = $this->getLoyaltyGift( $user->points ) ;
        if ( $gift ) {

            $data['gift']['service'] = [
                "id"    => $gift->service->id ,
                "name"  => $gift->service->name ,
                "currency"  => __('app.SAR') ,
                "type"  => "service"
            ];
            $data['gift']['sars'] = [
                "id"    => $gift->service->id ,
                "name"  => $gift->service->price ,
                "currency"  => __('app.SAR') ,
                "type"  => "price"
            ];

            $data['replaced_point'] =  $gift->num_point;
        }

        return $this->sendResponse($data, __("messages.successfully"));
    }

    public function paymentDone( Request $request )
    {
        $order_id = $request->order_id ;
        $id = $request->checkout_id ;
        $entityId = $request->entity_id ;


        if ( $entityId == 5 ) { // wallet

            $order = Order::where("id" , $order_id)->first();
            $total = $this->getTotalSubs( $order ) ;
//            dd( $total  , $order->total , $order->id , $order->is_paid ) ;

            $user = UserRequest::user() ;

            if ( $total >= $user->balance ) {
                $error = "رصيد لا يكفي" ;
                return $this->sendError($error );
            }


            $user = UserRequest::user() ;
            $user->balance = $user->balance - $total;
            $user->save() ;

            WalletTransaction::create([
                "amount"        => $total ,
                "checkout_id"   => $order_id ,
                "status_paid"   => "payment" ,
                "client_id"   => $user->id,
            ]);

        }else{
            if ( $entityId == 2 ){
                $entityId = config('settings.payments.visaEntityID');
            }else{
                $entityId = config('settings.payments.madaEntityID');
            }
            $url = config('settings.payments.url')."/checkouts/".$id."/payment?entityId=".$entityId;


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization:Bearer OGE4Mjk0MTc0ZDA1OTViYjAxNGQwNWQ4MjllNzAxZDF8OVRuSlBjMm45aA=='));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $responseData = curl_exec($ch);
            if(curl_errno($ch)) {
                return curl_error($ch);
            }
            curl_close($ch);
        }

        $total_order = 0 ;
        if ( $order->is_paid == 0 && $order->status != 1 ) {
            $total_order = $order->total ;
        }


        $order = Order::find($order_id);
        $order->is_basic_paid = 1 ;
        $order->is_paid = 1 ;
        $order->save();

        $total_paid = [] ;


        $orderDetailsEdit = $order->orderDetailsEdit()->where("paid_status" , 0 )
            ->where("admin_status" , 'approved_client')
            ->where("payment_method" , 0 )
            ->get() ;



        foreach ( $orderDetailsEdit as $orderDetails ) {
            $orderDetails->paid_status = 1;
            $orderDetails->save();
            $total_paid[]= $orderDetails->price ;
        }

        $order->total_paid = array_sum( $total_paid ) + $order->total_paid + $total_order ;
        $order->save();
        return $this->sendResponse([], __("messages.successfully"));
    }

    public function paymentNow( Request $request )
    {
        $order_id = $request->order_id ;
        $entity_id = $request->entity_id ;

        $order = Order::find($order_id) ;
        $is_basic_paid = 1 ; //default

        $orderDetailsEdit = $order->orderDetailsEdit()->where("paid_status" , 0 )->get() ;

        $orderDetailsPrice = [] ;
        $sub = [] ;

        foreach ( $orderDetailsEdit as $orderDetails ) {
            if ( $orderDetails->payment_method == 1 && $orderDetails->admin_status == 'approved_client' ) {
                $orderDetailsPrice[] = $orderDetails->price ;
                $sub[] = $orderDetails->price ;
            }
        }

        if ( $order->is_basic_paid != 1 ) {
            $orderDetailsPrice[] = $order->basic_total;
            $is_basic_paid = 0 ;
        }



        $data['total']          = array_sum($orderDetailsPrice) ;
        $data['sub_services']   = array_sum($sub) ;
        $data['basic_total']    = (int)$order->basic_total;
        $data['is_basic_paid']  = $is_basic_paid;

//        return $data ;

        $resultCode = $this->onlinePayment( $data['total'] , $order_id , $entity_id );



        if ($resultCode->result->code == "000.200.100") {
            $id = $resultCode->id;
            $data['checkout_id'] = $id;
        }else{
            return false; //todo
        }
        return $this->sendResponse($data, __("messages.successfully"));
    }

    public function getGift()
    {
        $user = UserRequest::user() ;
        $gift = $this->getLoyaltyGift( $user->points ) ;
        $data['replaced_point'] = "" ;

        if ( $gift ) {

            $data['gift']['service'] = [
                "id"    => $gift->service->id ,
                "name"  => $gift->service->name ,
                "currency"  => __('app.SAR') ,
                "type"  => "service"
            ];
            $data['gift']['sars'] = [
                "id"    => $gift->service->id ,
                "name"  => $gift->service->price ,
                "currency"  => __('app.SAR') ,
                "type"  => "price"
            ];

            $data['replaced_point'] =  $gift->num_point;

            return response()->json(["status" => 1 , "data" => $data]) ;
        }

        return response()->json(["status" => 0 ]) ;

    }

    public function changeGift( Request $request )
    {
        $user = UserRequest::user() ;
        $gift = $this->getLoyaltyGift( $user->points ) ;

        if ( $gift ) {

            if ( $request->type == "service" ) {
                $order = Order::where("client_id" , $user->id )
                    ->orderBy("id" , "DESC")->first() ;
                OrderDetails::create([
                    "order_id" => $order->id ,
                    "price" => 0 ,
                    "type" => "new" ,
                    "service_id" => $gift->service_id ,
                    "is_edit" => 1 ,

                ]);
            } else {
                $user->loyalty_balance = $user->loyalty_balance + $gift->num_sar ;
                $user->save() ;
            }

            $user->points = $user->points - $gift->num_point ;
            $user->save();


            return response()->json(["status" => 1  ]) ;
        } else{
            return response()->json(["status" => 0  ]) ;
        }



    }

    private function onlinePayment( $total , $order_id , $entityId = 2 )
    {
        $logProcess = LogProcess::create(["order_id" => $order_id , "total" => $total ]);

        if ( $entityId == 2 ){
            $entityId = config('settings.payments.visaEntityID');
        }else{
            $entityId = config('settings.payments.madaEntityID');
        }

        $url = config('settings.payments.url')."/checkouts";
        $data = "entityId=".$entityId."" .
            "&amount=".$total.
            "&currency=".config('settings.payments.currency')."".
            "&paymentType=DB";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer OGE4Mjk0MTc0ZDA1OTViYjAxNGQwNWQ4MjllNzAxZDF8OVRuSlBjMm45aA=='));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);

        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);

        $responseData_js = json_decode($responseData) ;
        $code = $responseData_js->result->code ;

        $logProcess->code = $code ;
        $logProcess->checkout_id = $responseData_js->id;
        $logProcess->build_number = $responseData_js->buildNumber ;
        $logProcess->save();

        return json_decode($responseData);
    }

    private function walletPayment( $total , $order_id )
    {
        $order = Order::where("id" , $order_id)->first();
        $user = UserRequest::user() ;
        $user->balance = $user->balance - $total;
        $user->save() ;

        $order->total_paid = $total ;
        $order->is_basic_paid = 1 ;
        $order->is_paid = 1 ;
        $order->save();


        WalletTransaction::create([
            "amount"        => $total ,
            "checkout_id"   => $order_id ,
            "status_paid"   => "payment" ,
        ]);
    }

    private function addLoyaltyPoints( $total , $order )
    {
        $pricePoint = PricePoint::where("from","<=", $total )
            ->where("to" , ">=" ,$total)
            ->first();

        if ( $pricePoint  ) {
            $order->points = $pricePoint->points_num ;
            $order->save() ;

            //===================
            $user = UserRequest::user() ;

            $user->points = $user->points + $pricePoint->points_num ;
            $user->save() ;
        }
        return ;
    }

    private function getLoyaltyGift( $total )
    {
        $point = LoyaltyPoint::where("num_point" , "<=" , (int)$total)
            ->orderBy("num_sar" , "DESC" )
            ->where("active" , "active" )
            ->has("service")
            ->first();

        if ( $point ){
            return $point ;
        }
        return ;
    }


    private function getTotalSubs($order)
    {
        $total_order = 0 ;
        $orderDetailsEdit = $order->orderDetailsEdit()->where("paid_status" , 0 )
            ->where("admin_status" , 'approved_client')
            ->where("payment_method" , 1 )
            ->get() ;

        $itemsTotal = [] ;
        foreach ( $orderDetailsEdit as $item ){
            $itemsTotal[] = $item->price ;
        }



        if ( $order->is_paid == 0 && $order->status != 1 ) {
            $total_order = $order->total ;
        }

        return array_sum( $itemsTotal ) + $total_order ;
    }


}
