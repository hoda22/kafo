<?php

namespace App\Http\Controllers\Api;

use App\FirebaseNotification;
use App\Http\Resources\ChannelMessageResource;
use App\Models\ChannelChat;
use App\Models\ChannelMessage;
use App\Models\Client;
use App\Traits\UploadFile;
use Illuminate\Support\Facades\Request;

class ChatController extends BaseController
{
    use UploadFile;
    public function getChat()
    {
        $user = Request::user() ;
        $chatRoom = ChannelChat::where(function ( $query ) use ( $user ){
            $query->where("sender_id" , $user->id)
                ->where("receiver_id" ,0) ;
        })->orWhere(function ( $query2 ) use ( $user ){
            $query2->where("sender_id" , 0)
                ->where("receiver_id" ,$user->id) ;
        })
            ->first() ;

        if (!$chatRoom) {
            $chatRoom = ChannelChat::create([
                "receiver_id" => $user->id,
                "sender_id" => 0
            ]);
        }
//        return $chatRoom ;
        $channelMessages = ChannelMessage::where("channel_id",$chatRoom->id)
            ->with("sender")
            ->orderBy("id","DESC")
            ->paginate(20);


        return ChannelMessageResource::collection($channelMessages)->additional(["status"=> 1 , "message" =>__("messages.successfully")]) ;
    }


    public function sendFileChat( \Illuminate\Http\Request $request )
    {
        $baseUrl = "https://mragaey.com/kafo/" ;
        $user = Request::user() ;

        $file = $request->file ;
        $message = $this->upload($file , "chat" , $width = 700, $height = 1000 );
        $file = $baseUrl.$message;

        return response()->json(["status"=>1 , "path" => $file ]);

    }

    public function sendMessageChat( \Illuminate\Http\Request $request )
    {
        $baseUrl = "https://mragaey.com/kafo/" ;
        $user = Request::user() ;
        $message = $request->message;
        $type = "text";
        $src ="";

        if ( $request->type == "file" ){
            $file = $request->message ;
            $message = $this->upload($file , "chat");
            $type = "file" ;
        }

        $chatRoom = ChannelChat::where(function ( $query ) use ( $user ){
            $query->where("sender_id" , $user->id)
                ->where("receiver_id" ,0) ;
        })->orWhere(function ( $query2 ) use ( $user ){
            $query2->where("sender_id" , 0)
                ->where("receiver_id" ,$user->id) ;
        })->first() ;

//         return $message ;

        $messageContent = ChannelMessage::create([
            "message" => $message ,
            "channel_id" => $chatRoom->id ,
            "sender_id" => $user->id ,
            "type" => $type ,
        ]);

        if ( $type == "file" ){
            $messageContent['message'] = $baseUrl.$message;
        }


        return new ChannelMessageResource($messageContent) ;

    }


    public function sendChatClients( \Illuminate\Http\Request $request )
    {
        $user = Request::user() ;
        $message = $request->message ;
        $type = $request->type ?? "message" ;
        $clientId = $request->client_id ;

        $client = Client::find($user->id) ;
        $baseUrl = "https://mragaey.com/kafo/";
        $avatar = $client->avatar ;
        $chatData = [
            "avatar"        => $baseUrl.$avatar,
            "name"          => $client->FULL_NAME ,
            "message_type"  => $type
        ] ;

//        dd( $clientId ) ;

        $receiver = Client::find($clientId) ;
//        return  $receiver ;

        if ( $receiver->player_id ){
            $titleNote =  __('app.new_chat') ;
            $notify_fire = new FirebaseNotification();
            $notify_fire->sendNote([$receiver->player_id], ["order_id" => $user->id ], $message , $titleNote , $receiver->type_device ,"chat", $chatData);

        }
        return $this->sendResponse([], __("messages.successfully"));
    }

}
