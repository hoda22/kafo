<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\SendEmailRequest;
use App\Http\Resources\SettingsResource;
use App\Mail\ContactEmail;
use App\Models\Setting;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;

class SettingController extends BaseController
{
    public function index()
    {
        $settings = Setting::first() ;
        $settings =  new SettingsResource($settings) ;
        return $this->sendResponse($settings, __("messages.successfully"));
    }

    public function sendEmail( SendEmailRequest $request )
    {

        $user = Request::user() ;
        $name = $user->full_name ;
        $message = $request->message ;
        $settings = Setting::first();
        Mail::to($settings->email)->send(new ContactEmail($message , $name));
        return $this->sendResponse("", __("messages.massege_send_successfully"));
    }
}
