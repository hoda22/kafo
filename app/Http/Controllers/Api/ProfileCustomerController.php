<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Auth\ProfileRequest;
use App\Http\Resources\ClientResource;
use App\Models\Order;
use App\Enums\OrderStatusEnum;
use Illuminate\Support\Facades\Request as UserRequest;


class ProfileCustomerController extends BaseController
{
    public function show()
    {
        $user = UserRequest::user() ;
        $user = new ClientResource($user);
        $data['user']        = $user ;
        $data['orders_count']        = $user ;


        return $this->sendResponse($data, "");
    }

    public function update( ProfileRequest $request )
    {
        $user = UserRequest::user() ;

        $user->first_name = $request->first_name ;
        $user->last_name = $request->last_name ;
        $user->email = $request->email ;
        $user->phone = $request->phone ;

        $user->save();

        return $this->sendResponse([], __("messages.updated_successfully"));
    }

    public function loyaltyPoints()
    {
        $user = UserRequest::user() ;

        $countOrderDone = Order::where(["status" => OrderStatusEnum::Implemented->value , "client_id" => $user->id])->count() ;
        $countPoints = $user->points  ;
        $balance = $user->balance  ;
        $rewardBalance = $user->loyalty_balance ;

        $data = [
            "count_order_done"  => $countOrderDone ,
            "count_points"      => $countPoints ,
            "balance"           => $balance ,
            "reward_balance"    => (int)$rewardBalance ,
            "currency"          => __('app.SAR') ,
        ];
        return $this->sendResponse( $data , __("messages.successfully"));
    }

    public function trackingUser( $order_id )
    {
        $order = Order::find( $order_id );
//        return $order->provider ;
        if ( $order ) {
            if ( $order->provider )
            {
                $data = ["lat" => $order->provider->lat , "lon" => $order->provider->lat] ;
                return $this->sendResponse( $data , __("messages.successfully"));
            }
            return $this->sendError("لا يوجد فني لهذا الطلب");
        }
        return $this->sendError("لا يوجد طلب");
    }
}
