<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\SparePartResource;
use App\Models\SparePart;
use Illuminate\Http\Request;

class SparePartsController extends BaseController
{
    public function index( Request $request )
    {
        $ids = $request->ids ;
        $spareParts = SparePart::active()->whereIn("sub_service_id", $ids )->get() ;

        $spareParts = SparePartResource::collection($spareParts) ;
        return $this->sendResponse($spareParts, __("messages.successfully"));
    }
}
