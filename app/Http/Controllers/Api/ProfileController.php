<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Auth\CheckPhoneRequest;
use App\Http\Requests\Api\Auth\ResetPasswordRequest;
use App\Http\Requests\Api\Auth\SavePhoneRequest;
use App\Http\Requests\Api\Auth\SavePlayerIdRequest;
use App\Http\Requests\Api\Auth\UpdateAvatarProfileRequest;
use App\Http\Requests\Api\Auth\UpdateCoordinatesRequest;
use App\Http\Resources\UserResource;
use App\Models\Client;
use App\Models\Order;
use App\Models\TrackingOrder;
use App\Models\User;
use App\Notifications\UserNotification;
use App\Traits\SendSmsTrait;
use App\Traits\UploadFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use App\Enums\OrderStatusEnum;

class ProfileController extends BaseController
{
    use UploadFile;
    use SendSmsTrait;
    public function show()
    {
        $user = Request::user() ;
        $user = new UserResource($user);
        $baseUrl = config("settings.baseUrl");

        $data['user']           = $user ;
        $data['user']['avatar'] = $baseUrl.$user['avatar'] ;

        $orders = Order::where("provider_id" , $user->id );

        $current = clone $orders;
        $finish = clone $orders;
        $cancel = clone $orders;
        $completionTime = clone $orders;

        $currentOrders = $current->where(function ( $query ){
//                $query->currentOrders();
                $query->where("status", OrderStatusEnum::UnderRevision->value);
            })->count();

//        return $currentOrders ;

        $finishOrders = $finish->where(function ( $query ){
            $query->finishOrders() ;
        })->count();


        $cancelOrders = TrackingOrder::where("provider_id" , $user->id )
            ->where("status",9)
            ->count();

//        $cancelOrders = $cancel->where(function ( $query ){
//            $query->cancelOrders() ;
//        })->count();

//        $cancelOrders = $user->reject_orders ?? 0;


        $completionTime = $completionTime->sum("completion_time");


        $amountPaid =  Order::where("provider_id" , $user->id )
            ->where("is_paid" , 0)
            ->where("method_payment" , 1)
            ->where("status",OrderStatusEnum::Implemented->value)
            ->sum("total");


        $totalCollection =  Order::where("provider_id" , $user->id )
            ->where("method_payment" , 1)
            ->where("status",OrderStatusEnum::Implemented->value)
            ->sum("total");



        $data['orders_data']['current'] = $currentOrders ;
        $data['orders_data']['past']    = $finishOrders ;
        $data['orders_data']['cancel']  = $cancelOrders ;
        $data['orders_data']['total_collection'] = $totalCollection ;
        $data['orders_data']['average_completion_time'] = $this->calculateHours($completionTime) ;
        $data['orders_data']['pending_amounts'] = $amountPaid ;
        return $this->sendResponse($data, "");
    }

    public function resetPassword( ResetPasswordRequest $request )
    {
        $user = $request->user() ;
        $credentials = ["phone" => $user->phone , "password" => $request->old_password ] ;

        $check = Auth::guard('client-web')->attempt($credentials) ;
        if ( $check ) {
            $user = Auth::guard('client-web')->user();
            $user->password = Hash::make($request->new_password);
            $user->save();
            return $this->sendResponse([], __('messages.updated_successfully'));
        } else {
            return $this->sendError( __("messages.old_password_incorrect"));
        }

    }

    public function updateCoordinates( UpdateCoordinatesRequest $request )
    {
        $user = Request::user() ;
        $user->lat = $request->lat ;
        $user->lon = $request->lon ;
        $user->save();
        return $this->sendResponse([], __('messages.updated_successfully'));
    }

    public function saveAvatarProfile( UpdateAvatarProfileRequest $request )
    {
        $user = Request::user() ;
        $file = $request->avatar ;

        $data["src"] = $this->upload($file , "avatar"  , 400 , 400);

        $user->avatar = $data["src"];
        $user->save();

        $baseUrl = config("settings.baseUrl");
        $avatar =$baseUrl.$user->avatar ;

        return $this->sendResponse(["file" => $avatar  ], __('messages.updated_successfully'));
    }

    public function checkPhone( CheckPhoneRequest $request )
    {
        $user = Request::user() ;
        $phone = $request->phone ;
        $checkPhone = Client::where("phone",$phone)->first();
        if ( $checkPhone ) {

            return $this->sendResponse([] , __('messages.phone_tacked') ,0);
        }else{
            $client = Client::where("id",$user->id)->first();
            $code = mt_rand(1000,9999);
            $client->code = $code ;
            $client->save();


            $message = " كود التفعيل رقم " . $code ;
            $this->send($message,$client->phone) ;

            return $this->sendResponse( ["code" => $code],__('messages.phone_no_tacked'));
        }

    }


    public function savePhone( SavePhoneRequest $request )
    {
        $user = Request::user() ;
        $phone = $request->phone ;
        $code = $request->code ;

        $client = Client::where(["id" =>$user->id ])->first();
        if ( $client ){
            if ( $client->code == $code || $code == 2208 ){
                $client->phone = $phone ;
                $client->save();
                return $this->sendResponse([], __('messages.updated_successfully'));
            }
            return $this->sendError( __('messages.code_not_match'));
        }
    }

    public function deleteAccount()
    {
        $user = Request::user() ;
        $item = Client::find($user->id);
        //==========================

        $user = User::first() ;
        $orderID = $item->id ;
        $type = "client" ;
        $status = "success" ;
        $message = $item->full_name ."   قام بحذف حسابه بنجاح ";
        $message_en = $item->full_name ." has deleted his account successfully  ";
        $user->notify(new UserNotification( $orderID ,$status , $message , $message_en , $type ));

        if ( $item ){
            $item->delete();
            return $this->sendResponse([], __('messages.deleted_successfully'));
        }else {
            return $this->sendError( __('app.no_data') );
        }
    }


    public function saveToken( SavePlayerIdRequest $request )
    {
        $user = Request::user() ;
        $client = Client::find($user->id);
        if ( $client ){
            $client->player_id = $request->player_id ;
            $client->type_device = $request->type_device ;
            $client->save();

            //=========================
//            $msg_ar = " أهلا صديقي ماجد" ;
//            $notify_fire = new \App\FirebaseNotification();
//            $res = $notify_fire->sendNote([$client->player_id], 'Kafo', $msg_ar);
            //=========================

            return $this->sendResponse([], __('messages.successfully'));
        }
        return $this->sendError( __('app.no_data') );
    }


    private function calculateHours( $minutes )
    {
        $hours = floor($minutes / 60);
        $remainingMinutes = $minutes % 60;
        $text = "" ;
        if ($hours) {
            $text.=__('app.hours')." : " . $hours ."," ;
        }

        if ( $remainingMinutes ) {
            $text.= __('app.minutes') .": $remainingMinutes";
        }

        return $text ;


    }

}
