<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\InvoiceResource;
use App\Models\Order;

class InvoiceController extends BaseController
{
    public function get( $id )
    {
        $order = Order::where("id",$id)->with('client','provider')
            ->with("orderDetailsAll")->first();

        $invoice = new InvoiceResource($order);
        return $this->sendResponse($invoice, __("messages.successfully"));
    }

}
