<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\PageResource;
use App\Models\Page;

class PageController extends BaseController
{
    public function index( $segment )
    {
        $page = Page::where("segment",$segment)->first() ;

        if ( $page ){
            $page = new PageResource($page);
            return $this->sendResponse($page, __("messages.successfully"));
        }
        return $this->sendError(__("messages.no_page_name")) ;

    }
}
