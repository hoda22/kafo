<?php

namespace App\Http\Controllers\Api;

use App\FirebaseNotification;
use App\Http\Requests\Api\Order\RescheduleOrderRequest;
use App\Http\Resources\CartImageResource;
use App\Http\Resources\DetailsCartResource;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\OrderImage;
use App\Models\Service;
use App\Models\User;
use App\Notifications\ClientNotification;
use App\Notifications\UserNotification;
use App\Traits\OrderTrait;
use App\Traits\UploadFile;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Request as UserRequest;

class CartController extends BaseController
{
    use UploadFile;
    use OrderTrait ;

    protected $user;

    public function __construct()
    {
        $this->user = UserRequest::user() ;
    }

    public function get( Request $request )
    {
        $user = UserRequest::user() ;
        $order = $this->getOrder() ;
        $orderDetails = OrderDetails::where("order_id" , $order->id )->with("service")->get() ;

        $data['order'] = [
            "id" => $order->id,
            "notes" => $order->notes,
            "service_date" => $order->service_date ,
            "service_time" => $order->service_time ,
            "coupon_id" => $order->coupon_id ,
            "address_id" => $order->address_id ,
        ];

        $points = $this->getValuePoints( $user->points ) ;

        $data['user']['points_to_money'] = $points ? $points->num_sar : 0 ;
        $data['user']['loyalty_balance'] = $user->loyalty_balance ;
        $data['user']['balance_of_points'] = $user->points;
        $data['user']['balance_wallet'] = $user->balance;
        $data['user']['currency'] = __('app.SAR');

        $data['order_images'] = CartImageResource::collection( $order->orderImages ) ; ;
        $data['order_details'] = DetailsCartResource::collection( $orderDetails ) ;
        return $this->sendResponse($data, __("messages.successfully"));
    }

    public function add( Request $request )
    {

        $order = $this->getOrder() ;
        $service = Service::find( $request->service_id ) ;

        $details = OrderDetails::updateOrCreate(
            [
                'service_id' =>  $request->service_id ,
                "order_id" => $order->id ,
                "is_edit" => 1 ,
            ],
            [
                'service_id' =>  $request->service_id ,
                "order_id" => $order->id,
                "is_edit" => 1 ,
            ]
        );

        $details->price = $service->price * $request->quantity ;
        $details->unit_price = $service->price ;
        $details->quantity = $request->quantity ;
        $details->save() ;
        return $this->sendResponse([], __("messages.add_to_cart"));
    }

    public function removeItemFromCart( Request $request )
    {
        $order = $this->getOrder() ;
        $orderDetails = OrderDetails::where([
            "order_id"      => $order->id ,
            "service_id"    => $request->service_id
        ])->first();

        $orderDetails->delete();
        return $this->sendResponse([], __("messages.remove_item_from_cart"));
    }

    public function updateItemFromCart( Request $request )
    { // service_id - quantity
       $user = UserRequest::user() ;
        $order = $this->getOrder($user->id) ;

        $orderDetails = OrderDetails::where("order_id",$order->id)
            ->where("service_id",$request->service_id)->first();

        if ( !$orderDetails ) {
            $error = "لا يوجد هذه الخدمه في السله" ;
            return $this->sendError($error);
        }
        $orderDetails->quantity = $request->quantity;
        $orderDetails->save();

        return $this->sendResponse([], __("messages.updated_item_cart_successfully"));

    }

    public function addCartData( Request $request  )
    {
        $user = UserRequest::user() ;
        $order = $this->getOrder($user->id) ;
        $baseUrl = config("settings.baseUrl");
        $data = [] ;
        if ( $request->has("image") ){
            $img = $this->upload($request->image, "orders", 600, 700);

            $orderImages = OrderImage::create(["image" => $img , "order_id" => $order->id]);
            $data['file']['image'] = $baseUrl.$orderImages['image'] ;
            $data['file']['id'] = $orderImages['id'] ;
        }

        if ( $request->filled("notes") ) {
            $order->notes = $request->notes;
            $data['notes'] = $request->notes;
        }

        if ( $request->filled("service_date") ) {
            $order->service_date = $request->service_date;
        }

        if ( $request->filled("service_time") ) {
            $order->service_time = $request->service_time;
        }
        $order->save();

        return $this->sendResponse($data, __("messages.successfully"));
    }

    public function RescheduleOrder( RescheduleOrderRequest $request , $id )
    {
        $user = UserRequest::user() ;
        $order = Order::where(["client_id" => $user->id , "id" => $id ])->first() ;

        $order->service_time = $request->time ;
        $order->service_date = $request->date ;
        $order->save();
        //=============================

        $provider_name = $order->provider->FULL_NAME ;
        $user = User::first() ;
        $orderID = $order->id ;
        $status = "success" ;

        $statusProvider = " تم التعديل الموعد الخاص بطلب رقم ".$orderID  ;
        $statusProvider_en = "The deadline for requesting a number has been modified "  . $orderID ;

        $user->notify(new UserNotification($orderID , $status , $statusProvider , $statusProvider_en));
        //==========================


        $message_en = "A technician has been assigned to your order no". $order->id ;
        $titleNote =  "app.modify_appointment";
        $order->provider->notify(new ClientNotification($statusProvider , $message_en , $titleNote));

        $titleNote= __($titleNote) ;
        $notify_fire = new FirebaseNotification();
        $notify_fire->sendNote([$order->provider->player_id], 'Kafo', $statusProvider , $titleNote , $order->client->type_device ,"order");


        return $this->sendResponse([], __("messages.successfully"));
    }

    public function checkCoupon( Request $request  )
    {
        $checkCoupon = Coupon::where("code", $request->coupon_number)->first() ;

        if ( !$checkCoupon ) {
            $error = __("messages.coupon_is_invalid");
            return $this->sendError($error);
        }
        $data = [
            "discount_value" => $checkCoupon->number ,
            "coupon_type" => $checkCoupon->type ,
            "coupon_id" => $checkCoupon->id ,
        ] ;
        return $this->sendResponse($data, __("messages.successfully"));
    }

    public function deleteImageFromCart($id)
    {
        $image = OrderImage::find($id);
        if ( $image )
        {
            $image->delete();
            return $this->sendResponse([], __("messages.successfully"));
        }
        return $this->sendError("app.no_image", __("messages.successfully"));
    }
}
