<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CartResource;
use App\Http\Resources\ChildrenResource;
use App\Http\Resources\ServiceResource;
use App\Http\Resources\SliderResource;
use App\Models\Service;
use App\Models\Slider;
use Illuminate\Http\Request;



class HomeController extends BaseController
{
    public function index()
    {
        $sliders = Slider::active()->orderBy("ordering" , "ASC")->get();
        $services = Service::active()->parent()
            ->has("activeChildren")
            ->ordering()->get();

        $data['sliders']    = SliderResource::collection($sliders) ;
        $data['services']   = ServiceResource::collection($services) ;

        return $this->sendResponse($data, __("messages.successfully"));
    }

    public function search( Request $request )
    {

        $id = $request->id ;
        if (strpos($id, ',') !== false) {
            $idsArray = explode(',', $id);

            $id = $idsArray ;
        }

        $text = $request->text ;
        $sup_service = Service::childrenItems();
        if ( $request->filled("id") ){
            if ( is_array( $id ) ){
                $sup_service = $sup_service->whereIn("parent_id",$id) ;
            }else{
                $sup_service = $sup_service->where("parent_id",$id) ;
            }
        }

        if ( $request->filled("text") ){
            $sup_service = $sup_service->whereTranslationLike('name' , '%'.$text.'%') ;
        }
        $sup_service = $sup_service->paginate();

        if ($request->user('sanctum')) {
//            dd($request->user('sanctum')) ;
            return CartResource::collection( $sup_service )->additional(["status"=> 1]) ;
        } else {
            return ChildrenResource::collection( $sup_service )->additional(["status"=> 1]) ;
        }

    }
}
