<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ServiceResource;
use App\Models\Service;

class ServiceController extends BaseController
{
    public function index()
    {
        $services = Service::active()->parent()->with(["children" => function( $query ){
            $query->where("active", "active");
        }])
            ->has("activeChildren")->get() ;
        $services =  ServiceResource::collection($services) ;
        return $this->sendResponse($services, __("تمت بنجاح"));
    }
}
