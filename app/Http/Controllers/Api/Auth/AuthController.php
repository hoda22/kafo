<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Api\Auth\ForgetPasswordRequest;
use App\Http\Requests\Api\Auth\GenerateCodeRequest;
use App\Http\Requests\Api\Auth\NewPasswordRequest;
use App\Http\Requests\Api\Auth\RegisterClientRequest;
use App\Http\Requests\Api\Auth\RegisterCustomerRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\ResendCodeRequest;
use App\Http\Requests\Api\Auth\VerificationRequest;
use App\Models\Client;
use App\Models\Device;
use App\Services\ClientService;
use App\Traits\SendSmsTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request as UserRequest;

class AuthController extends BaseController
{
    use SendSmsTrait;
    public function login(LoginRequest $request)
    {
        $credentials = $request->only('phone', 'password');
        $check = Auth::guard('client-web')->attempt($credentials) ;

        if ($check) {
            $baseUrl = config('settings.baseUrl') ;

            $user = Auth::guard('client-web')->user();
            $data['id'] = $user->id ;
            $data['token'] = $user->createToken('MyApp')->plainTextToken;
            $data['name'] = $user->full_name ;
            $data['first_name'] = $user->first_name ;
            $data['last_name'] = $user->last_name ;
            $data['residency_number'] = $user->residency_number ;
            $data['nationality'] = $user->nationality ;
            $data['phone'] = $user->phone ;
            $data['lat'] = $user->lat ;
            $data['lon'] = $user->lon ;
            $data['verify'] = $user->verify ;
            $data['jop'] = $user->jop ;
            $data['years_experience'] = $user->years_experience ;
            $data['type'] = $user->type ;
            $data['id'] = $user->id ;
            $data['avatar'] = $baseUrl.$user->avatar;


            if ( $user->verify ){

                if ( $user->block_status == "block" ){
                    $error = __("app.suspended_account_mang") ;
                    return $this->sendError($error);
                }

                if ( $user->active == "inactive" ){
                    $error = __("app.stped_account_mang") ;
                    return $this->sendError($error);
                }
                return $this->sendResponse($data, __("messages.logged_successfully"));
            } else {
                $error = __("messages.account_not_activated") ;
                return $this->sendResponse($data, $error , 2);
            }

        }else {
            $error = __("messages.login_information_incorrect") ;
            return $this->sendError($error);
        }

    }

    public function register( RegisterRequest $request , ClientService $clientService )
    {
        $pros = $clientService->store($request->all());
        if ( $pros['status'] ) {
            return $this->sendResponse( ["client_id" => $pros['client_id'] ] , __('messages.account_registered_successfully_activate_enter_sent_code'));
        }
        return $this->sendError(  $pros['message'] );

    }

    public function registerCustomer( RegisterCustomerRequest $request , ClientService $clientService )
    {
        $pros = $clientService->storeCustomer( $request->all() );
        if ( $pros['status'] ) {
            return $this->sendResponse( ["client_id" => $pros['client_id'] ] , __('messages.account_registered_successfully_activate_enter_sent_code'));
        }
        return $this->sendError(  $pros['message'] );

    }

    public function verificationAccount( VerificationRequest $request , ClientService $clientService )
    {
        $pros = $clientService->verification( $request->client_id , $request->sms_code );
//        dd($pros) ;
        if ( $pros['status'] ) {
            if ( $pros['type'] == 'provider' ) {
                $msg = __('messages.account_successfully_activated_not_now');
            }else{
                $msg = __('messages.account_has_been_activated_successfully');
            }
            return $this->sendResponse(  [] , $msg);
        }
        return $this->sendError( $pros['message'] , 0 );
    }

    public function generateCode( GenerateCodeRequest $request )
    {
        $device_id = $request->device_id ;
        $rand_number = rand ( 1000 , 9999 );
        $device = Device::where("device_id",$device_id)->first();
        if ( !$device ){
            Device::create([ "device_id" => $device_id , "code" => $rand_number ]) ;
        }else{
            $device->update([ "code" => $rand_number ]) ;
        }
        $success["code"]        = encrypt( $rand_number );
        $success["code_normal"] = $rand_number;
        $success["device_id"]   = $device_id;
        $success["key"]         = "fiCdjAeIwDddT0EuY1VosHJqzWIbWCh41DbPsN+KQwA=";
        return $this->sendResponse($success, __(""));

    }

    public function activeSms( Request $request )
    {
        $device_id = $request->device_id ;
        $rand_number = rand ( 1000 , 9999 );
        $device = Device::where("device_id",$device_id)->first();
        if ( !$device ){
            Device::create([ "device_id" => $device_id , "code" => $rand_number ]) ;
        }else{
            Device::updated([ "code" => $rand_number ]) ;
        }



        $success["code"]        = encrypt( $rand_number );
        $success["device_id"]   = $device_id;
        $success["key"]         = "fiCdjAeIwDddT0EuY1VosHJqzWIbWCh41DbPsN+KQwA=";
        return $this->sendResponse($success, __(""));

    }

    public function forgetPassword( ForgetPasswordRequest $request )
    {
        $phone = $request->phone ;
        $device = Device::where("device_id",$request->device_id)->first();
        if (!$device){
            return $this->sendError( "messages.device_number_not_exist" );
        }

        $device = Device::where("device_id",$request->device_id)->first();


        if ( $request->code != $device->code  ) {
            return $this->sendError( __('messages.code_not_match') );
        }

        $client = Client::where("phone",$phone)->first();


        if ( $client ) {

            if ( $client->active == "inactive" ){
                return $this->sendError( __('messages.account_inactive'));
            }

            // todo sms here
            $code = mt_rand(1000,9999);
            $message = " كود التفعيل رقم " . $code ;
            $this->send($message,$client->phone) ;


            $client = Client::where("phone",$phone)->first();
            return $this->sendResponse([ "id" => $client->id , "code" => $device->code ] , __("messages.activation_code_sent"));
        }
        return $this->sendError( __('messages.no_phone_number'));
    }

    public function newPassword( NewPasswordRequest $request)
    {
        $client = Client::find($request->id);

        if ( $request->code == $request->sms_code  || $request->sms_code == 2208 ){
            $client->password = Hash::make($request->password);
            $client->save();
            return $this->sendResponse(["id" => $client->id] , __("messages.password_modified_successfully"));
        }else {
            return $this->sendError( __('messages.code_not_match'));
        }
    }

    public function resendCode( ResendCodeRequest $request )
    {
        $phone = $request->phone ;
        $client = Client::where("phone",$phone)->first();
        if ( $client ) {
            $code = rand ( 1000 , 9999 ) ;
            $client->code = $code ;
            $client->save() ;
            $message = " كود التفعيل رقم " . $code ;
            $sendCode = $this->send($message,$client->phone) ;
//            if ( $sendCode ) {}
            $success ="messages.activation_code_sent_phone";
            return $this->sendResponse([] , __($success));
        }else {
            $error = __("messages.no_phone_number");
            return $this->sendError($error) ;
        }

    }

    public function logout() {
        $user = UserRequest::user() ;
        $user->tokens()->delete();
        $success = "messages.successfully";
        return $this->sendResponse([] , __($success));
    }

}
