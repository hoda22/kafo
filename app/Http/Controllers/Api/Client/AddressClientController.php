<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\AddressClientResource;
use App\Models\AddressClient;
use App\Enums\OrderStatusEnum;
use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as UserRequest;

class AddressClientController extends BaseController
{

    public function __construct()
    {
        $this->model = new AddressClient();

    }

    public function index( )
    {
        $user = UserRequest::user() ;
        $items = AddressClient::where("client_id",$user->id)->get();
        $items = AddressClientResource::collection($items) ;
        return $this->sendResponse($items, __("messages.successfully"));
    }

    public function store( Request $request  )
    {
        $user = UserRequest::user() ;
        $res = $this->checkCity($request->lat ,  $request->lon) ;

        if ( !$res ) {
            $error = __("app.no_city");
            return $this->sendError($error);
        }


        AddressClient::create([
            "client_id" => $user->id ,
            "lat" => $request->lat ,
            "lon" => $request->lon ,
            "city_id" => $request->city_id ,
            "address" => $request->address ,
            "region" => $request->region
        ]);
        return $this->sendResponse([], __("messages.added_successfully"));
    }

    public function delete( $id )
    {
        $user = UserRequest::user() ;
        $item = AddressClient::where(["client_id"=> $user->id , "id" => $id])->first();
        if ($item){
            $item->delete();
            return $this->sendResponse([], __("messages.deleted_successfully"));
        } else {
            return $this->sendError( __("messages.not_found"));
        }
    }


    public function update( Request $request, $id )
    {
        $user = UserRequest::user() ;
        $item = AddressClient::where(["client_id"=> $user->id , "id" => $id])->first();

        if ( $item ){
            $item->lat = $request->lat ;
            $item->lon = $request->lon ;
            $item->city_id = $request->city_id ;
            $item->address = $request->address ;
            $item->region = $request->region ;
            $item->save();
            return $this->sendResponse([], __("messages.updated_successfully"));
        }
        return $this->sendError( __("messages.not_found"));
    }



    private function checkCity( $lat , $lng )
    {
        if ( $lat && $lng ) {
            $city = City::query()
                ->whereNotNull('zone_polygon')
                ->where('zone_polygon', 'LIKE', 'POLYGON%')
                ->whereRaw("ST_Intersects(ST_GEOMFROMTEXT('POINT(".$lat." ".$lng.")'), ST_GEOMFROMTEXT(zone_polygon))")
                ->first();

            if ($city){
                return true ;
            }
            return false ;
        }

        return false ;
    }

}
