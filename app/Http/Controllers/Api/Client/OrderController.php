<?php

namespace App\Http\Controllers\Api\Client;

use App\FirebaseNotification;
use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Enums\OrderStatusEnum;
use App\Models\OrderDetails;
use App\Models\Setting;
use App\Models\SparePartOrderDetails;
use App\Models\User;
use App\Notifications\UserNotification;
use App\Traits\OrderTrait;
use App\Traits\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as UserRequest;

class OrderController extends BaseController
{
    use UploadFile , OrderTrait;
    public function index( $status = "" )
    {
        $user = UserRequest::user() ;
        $orders = Order::with("city",'addressOrder',"service","provider" ,"client",'orderDetails','orderDetailsEdit')
            ->where("client_id" , $user->id) ;


        if ( $status == 1 ) // 1 -- Current
        {
            $orders = $orders->where(function ( $query ){
                $query->where("status", OrderStatusEnum::RequestIsDone->value )
                    ->orWhere("status", OrderStatusEnum::TechnicianArrived->value )
                    ->orWhere("status", OrderStatusEnum::UnderRevision->value )
                    ->orWhere("status", OrderStatusEnum::TechnicianAssigned->value )
                    ->orWhere("status", OrderStatusEnum::TechnicianApproved->value )
                    ->orWhere("status", OrderStatusEnum::AprovedModificationFromClient->value )
                    ->orWhere("status", OrderStatusEnum::RejectedModificationFromClient->value )
                    ->orWhere("status", OrderStatusEnum::AprovedModificationFromAdmin->value )
                    ->orWhere("status", OrderStatusEnum::RejectedModificationFromAdmin->value )
                    ->orWhere("status", OrderStatusEnum::SendCostModificationToClient->value )
                    ->orWhere("status", OrderStatusEnum::Modification->value )
                    ->orWhere("status", OrderStatusEnum::Cancel->value )
                    ->orWhere("status", OrderStatusEnum::Reject->value )
                    ->orWhere("status", OrderStatusEnum::TechnicianWay->value ) ;
                });

        }else if ( $status == 2 ) // -- Previous
        {
            $orders = $orders->where(function ( $query ){
                $query->where("status", OrderStatusEnum::Implemented->value)
                ->orWhere("status", OrderStatusEnum::RejectFromMangment->value) ;
            });
        }


        $data = $orders->whereNot("status", 0)->orderBy("id","DESC")->paginate(10) ;
        if ( count( $data ) ){

            $data =  OrderResource::collection($data)->additional(["status"=> 1 , "message" =>__("messages.successfully")]) ;
            return $data;
        }else {
            $msg = __('app.not_fond_orders') ;
            return $this->sendError($msg);
        }
    }

    public function changeStatus( Request $request , $id )
    {
        $order = Order::find($id) ;
        if (!$order) {
            return $this->sendError([], __("messages.not_found"));
        }
        $order->status = $request->status ;
        $order->save() ;
        $orderID = $order->id ;
        if ( $request->status == 17 ) { // cancel order from client
            $order->reject_reson =  $request->reason ;
            $order->status = OrderStatusEnum::CancelFromClient->value;




            //===========================
            $user = User::first() ;

            $status = "danger" ;
            $statusMsg = " تم الغاء الطلب من العميل رقم الطلب "  . $orderID ;
            $msg_en = " The customer's order has been cancelled. Order number "  . $orderID ;
            $user->notify(new UserNotification($orderID ,$status , $statusMsg , $msg_en));


        }

        if ( $request->status == 12 ) { // approved new cost

            $status = "danger" ;
            $statusMsg = " تم قبول طلب التعديل المقدم للطلب رقم ". $orderID ." وفي انتظار موافقة العميل "  ;
            $msg_en = "The new cost was approved by the customer. order number " . $orderID ;

            //===========================
            $titleNote =  "app.note_response_edit_request_from_admin"  ;
            if ( optional( $order->provider )->player_id ) {
                $notify_fire = new FirebaseNotification();
                $titleNote = __($titleNote) ;

                $dataNote = [
                    "status_code" => 2 ,
                    "order_id" => $order->id
                ];
                $notify_fire->sendNote([$order->provider->player_id], $dataNote, $statusMsg , $titleNote);
            }

            //===========================
            $user = User::first() ;
            $orderID = $order->id ;
            $user->notify(new UserNotification($orderID ,$status , $statusMsg , $msg_en));


            $order = Order::find( $orderID ) ;
            $order->is_paid = 0 ;
            $order->save();

            $orderDetailsEdit = OrderDetails::find( $request->order_details_id ) ;

            if ($orderDetailsEdit){
                $orderDetailsEdit->payment_method = $request->payment_method ;
                $orderDetailsEdit->admin_status = "approved_client";
                $orderDetailsEdit->save();
            }
        }

        if ( $request->status == 13 ) { // rejected new cost

            //===========================
            $user = User::first() ;
            $orderID = $order->id ;
            $status = "danger" ;
            $statusMsg = " تم رفض التكلفه الجديده من قبل العميل . رقم الطلب " . $orderID ;
            $msg_en = "The new cost was rejected by the customer. order number " . $orderID ;


            $settings = Setting::first() ;
            $user->notify(new UserNotification($orderID ,$status , $statusMsg , $msg_en));
            //=====================
            $orderDetailsEdit = OrderDetails::find( $request->order_details_id ) ;
            $orderDetailsEdit->admin_status = "rejected_client";
            $orderDetailsEdit->visit = $settings->visit;

            $orderDetailsEdit->is_edit = 1 ;
            $orderDetailsEdit->save();

            //=====================


            $total = $this->getTotalOrderItems($order->id) ;

            $order = Order::find( $order->id ) ;
            $order->total = $total ;
            $order->save();

            $titleNote =  "app.note_response_reject_edit_request_from_admin"  ;

            $dataNote = [
                "status_code" => 2 ,
                "order_id" => $order->id
            ];

            if ( optional( $order->provider )->player_id ) {
                $notify_fire = new FirebaseNotification();
                $titleNote = __($titleNote) ;
                $notify_fire->sendNote([$order->provider->player_id], $dataNote, $statusMsg , $titleNote);
            }

        }
        $order->save();

        return $this->sendResponse([], __("messages.updated_successfully"));
    }


    public function technicalRating( Request $request , $id )
    {
        $order = Order::where(["id" => $id ])->first();

        if ( !$order->provider_rate ) {
            $order->provider_comment = $request->provider_comment ;
            $order->provider_rate = $request->provider_rate ;
            $order->save() ;
            return $this->sendResponse([], __("messages.added_rating"));
        } else{
            return $this->sendError(__("messages.already_rating"));
        }

    }

    public function show( $id )
    {
        $order = Order::find($id) ;
        $order = new OrderResource($order);
        if ( $order ){return $this->sendResponse($order, __("messages.successfully"));
        } else{
            return $this->sendError(__("app.no_data"));
        }
    }

    public function edit( Request $request , $id )
    {
        $order = Order::find($id);
        $order->status = OrderStatusEnum::Modification->value;
        $order->save();
        $images = [];
        $sub_service_id="" ;

        if ( $request->has("file") ){
            foreach ($request->file as $file) {
                $images[] = $this->upload($file, "orders", 600, 700);
            }
        }

        if ( $request->sub_service_id ){
            $sub_service_id = implode(",",$request->sub_service_id ) ;
        }

        $images = serialize($images);


        $order_details = $order->orderDetails()->create([
            "service_id" => $sub_service_id,
            "type" => "edit",
            "description" => $request->description,
            "images" => $images,
        ]);


        if ( $request->spare_part_ids ){
            foreach ( $request->spare_part_ids as $spare_part_id ){
                SparePartOrderDetails::create([
                    "spare_part_id" =>$spare_part_id,
                    "order_details_id" =>$order_details->id,
                ]);
            }
        }


        $statusProvider = "هناك طلب تعديل علي الطلب رقم الطلب " . $order->id .  " مرسل من الفني " . optional( $order->provider )->FULL_NAME ;

        $status = "info" ;
        $orderID = $order->id ;
        $user = User::first();

        $user->notify(new UserNotification($orderID ,$status , $statusProvider));




        return $this->sendResponse([], __("messages.edited_successfully"));
    }
}
