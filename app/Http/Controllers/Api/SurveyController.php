<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Auth\SaveSurveyRequest;
use App\Http\Resources\SurveyQuestionResource;
use App\Models\SurveyOrder;
use App\Models\SurveyOrderAnswer;
use App\Models\SurveyQuestion;

class SurveyController extends BaseController
{
    public function index()
    {
        $surveyQuestion = SurveyQuestion::with(["answers" => function( $query ){
            $query->ordering();
        }])->ordering()->get() ;
//
//        return $surveyQuestion ;
        $surveyQuestions =  SurveyQuestionResource::collection($surveyQuestion) ;
        return $this->sendResponse($surveyQuestions, __("messages.successfully"));
    }

    public function saveSurvey( SaveSurveyRequest $request )
    {
        $text = json_encode($request->answer)    ;
        $survey = SurveyOrder::create([ "order_id" => $request->order_id , "answers" => $text ]);

        $arr = json_decode($survey->answers);
        $arr = json_decode($arr)  ;
        foreach ( $arr as $item ){
            SurveyOrderAnswer::create([
                "answer_id" => $item->type == 'text' ? null: $item->answer_id,
                "text" =>  $item->answer_text ?? "",
                "answer_type" => $item->type ,
                "question_id" => $item->question_id,
                "survey_order_id" => $survey->id,
                "type" => $item->type
            ]) ;
        }

        return $this->sendResponse([], __("messages.successfully"));
    }
}
