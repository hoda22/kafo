<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Order\RescheduleOrderRequest;
use App\Http\Resources\CartImageResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\DetailsCartResource;
use App\Models\City;
use App\Models\Coupon;
use App\Models\LoyaltyPoint;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\OrderImage;
use App\Models\Service;
use App\Traits\UploadFile;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Request as UserRequest;

class CityController extends BaseController
{
    public function get()
    {
        $cities = City::active()->ordering()->get();  ;
        $data = CityResource::collection($cities) ;
        return $this->sendResponse($data, __("messages.successfully"));
    }

}
