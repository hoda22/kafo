<?php

namespace App\Http\Controllers\Api;

use App\FirebaseNotification;
use App\Http\Requests\Api\Order\RatingToAgentRequest;
use App\Http\Resources\OrderResource;
use App\Models\Client;
use App\Models\Order;
use App\Enums\OrderStatusEnum;
use App\Models\SparePartOrderDetails;
use App\Models\TrackingOrder;
use App\Models\User;

use App\Models\WalletTransaction;
use App\Notifications\ClientNotification;
use App\Notifications\UserNotification;
use App\Traits\UploadFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as UserRequest;

class OrderController extends BaseController
{
    use UploadFile;
    public function index( $status = "" )
    {
        $user = UserRequest::user() ;
        //        return $user->id ;
        $orders = Order::with("city", "addressOrder" ,"service","provider" ,"client" ,'surveyItem','orderDetails','orderDetailsEdit')
            ;
        $rejectOrdersstatus=array();
        if ( $status == 1 ) // 1 -- Waiting
        {
            $orders = $orders->where(function ( $query ){
                $query->where("status", OrderStatusEnum::UnderRevision->value ) ;
            })->where("provider_id" , $user->id)->orderBy("service_date","DESC")
            ->orderBy("service_time","DESC");
            $data = $orders->paginate(10) ;
        } else if ( $status == 2 ) // 2 -- Current
        {
            $orders = $orders->where(function ( $query ){
                $query->where("status", OrderStatusEnum::TechnicianArrived->value )
                    ->orWhere("status", OrderStatusEnum::TechnicianAssigned->value )
                    ->orWhere("status", OrderStatusEnum::TechnicianApproved->value )
                    ->orWhere("status", OrderStatusEnum::AprovedModificationFromClient->value )
                    ->orWhere("status", OrderStatusEnum::RejectedModificationFromClient->value )
                    ->orWhere("status", OrderStatusEnum::AprovedModificationFromAdmin->value )
                    ->orWhere("status", OrderStatusEnum::RejectedModificationFromAdmin->value )
                    ->orWhere("status", OrderStatusEnum::SendCostModificationToClient->value )
                    ->orWhere("status", OrderStatusEnum::Modification->value )
                    ->orWhere("status", OrderStatusEnum::Modification->value )
                    ->orWhere("status", OrderStatusEnum::TechnicianWay->value ) ;
                })->where("provider_id" , $user->id)
                ->orderBy("service_date","Asc")
                ->orderBy("service_time","Asc")
            ;
            $data = $orders->paginate(10) ;

        }else if ( $status == 3 ) // -- Finish
        {
            $usrId=$user->id;
            $rejectOrders = TrackingOrder::where("provider_id",$user->id)->pluck("order_id")->toArray();
            $rejectOrdersIds =Order::WhereIn("id",$rejectOrders)->where(function ($query) use($usrId) {
                $query->where('provider_id','!=',$usrId)
                      ->orWhereNull('provider_id');
            })->pluck("id")->toArray();
            $orders = $orders->where(function ( $query ) use ($user) {
                $query->where(["status" => OrderStatusEnum::Implemented->value , "provider_id" =>$user->id ]) ;

            })->orWhereIn("id",$rejectOrdersIds)
                ->selectRaw("*, 8 AS basic_status")
                ->orderBy("service_date","DESC")
                ->orderBy("service_time","DESC");

                $data = $orders->paginate(10) ;
                $rejectOrdersstatus = TrackingOrder::where("provider_id",$user->id)->select("order_id","status_id","reason")->get();
                if($rejectOrdersstatus){
                    foreach($rejectOrdersstatus as $rejectedOrder){
                        for($i=0;$i<count( $data );$i++){ 
                            if( $data[$i]->id==$rejectedOrder->order_id){
                               $data[$i]->status= $rejectedOrder->status_id;
                               $data[$i]->RejectReason= $rejectedOrder->reason;
                            }
                        }
                    }
                }
        }
        //$data = $orders->paginate(10) ;


        if ( count( $data ) ){
            $data =  OrderResource::collection($data)->additional(["status"=> 1 , "message" =>__("messages.successfully"),'$rejectOrdersstatus'=>$rejectOrdersstatus]) ;
            return $data;
        }else {
            $msg = __('app.not_fond_orders') ;
            return $this->sendError($msg);
        }
    }


    public function changeStatus( Request $request , $id )
    {
        $order = Order::find($id) ;
        if (!$order) {
            return $this->sendError([], __("messages.not_found"));
        }

        if ( $request->status == 9 || $request->status == 3 ) {

            $orderDateTime = $order->service_date .' '.$order->service_time ;

// Convert the string to a Carbon instance
            $yourCarbonDateTime = Carbon::parse($orderDateTime);
            if ($yourCarbonDateTime < Carbon::now()) {
                return $this->sendError(__("messages.service_time_has_expired"));
            }
        }
        $order->status = $request->status ;
        if ( $request->status == 9 ) { // reject order
            $this->trackingOrder($id, $order->provider_id, 9,$request->status , $request->reject_reson );

            $provider_name = $order->provider->FULL_Name ;
            $order->reject_reson =  $request->reject_reson ;
            $order->provider_id = null ;

            $user = User::first() ;
            $orderID = $order->id ;
            $status = "danger" ;
            $statusProvider = " تم رفض الطلب من قبل الفني "   . $provider_name . " رقم الطلب " . $order->id;
            $statusProvider_en = " The request was rejected by the technician "   . $provider_name . " order number " . $order->id;
            $user->notify(new UserNotification($orderID ,$status , $statusProvider , $statusProvider_en));

        }

        if ( $request->status == 5 ) {
            $titleNote =  __('app.technician_on_way') ;

            $message = "الفني في الطريق الطلب رقم" . $order->id ;

            $message_en = "Request No.". $order->id  ." The technician is on the way " ;
            $titleNote =  "app.technician_on_way"  ;

            $order->client->notify(new ClientNotification($message , $message_en , $titleNote));
            if ( $order->client->player_id ) {
                $titleNote =  __($titleNote)  ;
                $notify_fire = new FirebaseNotification();
                $notify_fire->sendNote([$order->client->player_id], 'Kafo', $message ,$titleNote , $order->client->type_device);
            }
        }

        if ( $request->status == 6 ) {
            $titleNote =  __('app.technician_arrived') ;
            $message = "تم وصول الفني الطلب رقم" . $order->id ;
            $message_en = "Request No.". $order->id  ." technician arrived" ;
            $titleNote =  "app.technician_arrived"  ;

            $order->client->notify(new ClientNotification($message , $message_en , $titleNote));
            if ( $order->client->player_id ) {
                $titleNote =  __($titleNote)  ;
                $notify_fire = new FirebaseNotification();
                $notify_fire->sendNote([$order->client->player_id], 'Kafo', $message ,$titleNote , $order->client->type_device);
            }
        }

        if ( $request->status == 7 ) {
            $time_maintenance = $request->time_maintenance ;

            $order->completion_time = $time_maintenance ;
            $order->what_done = $request->what_done ;
            $order->end_date = date('Y-m-d');
            $order->end_time = date('H:i:s'); // Extract
            $this->implemented( $order->id ) ;
            $order->save() ;

            //===============
            $titleNote =  __('app.implemented') ;
            $message = "تم تنفيذ الطلب رقم" . $order->id ;
            $message_en = "Request No.". $order->id  ." has been executed" ;

            $dataNote = [
                "status_code" => 3 ,
                "order_id" => $order->id
            ];


            $order->client->notify(new ClientNotification($message , $message_en , $titleNote));
            if ( $order->client->player_id ) {
                $titleNote =  __($titleNote)  ;
                $notify_fire = new FirebaseNotification();
                $notify_fire->sendNote([$order->client->player_id], $dataNote, $message ,$titleNote , $order->client->type_device);
            }
        }

        if ( $request->status == 8 ) { // cancel order
            $start_at = Carbon::Parse($order->service_date ." ".$order->service_time) ;
            $end_at = Carbon::Parse($order->service_date ." ".$order->service_time)->addMinutes(30) ;

            $check = Carbon::now()->between($start_at, $end_at, true);
//            return [ $order->id , $start_at , $end_at ,  Carbon::now() ,$check] ;
            if($check){
                $msg = __("messages.order_cannot_canceled_half_hour_before_execution");
                return $this->sendError($msg);
            }

            if($order->provider){
                $provider = Client::find($order->provider_id);
                $provider->reject_order_no = $provider->reject_order_no + 1;
                $provider->save();
            }

            $provider_name ="";
            if ( $order->provider ){
                $provider_name = $order->provider->FULL_Name ;
            }
            $this->trackingOrder($order->id,$order->provider_id, 8, $request->status , $request->reject_reson );

            $order->reject_reson =  $request->reject_reson ;
            //$order->provider_id = null ;

            $user = User::first() ;
            $orderID = $order->id ;
            $status = "danger" ;
            $statusProvider = " تم الغاء الطلب من قبل الفني "   . $provider_name . " رقم الطلب " . $order->id;
            $statusProvider_en = " The order was canceled by the technician "   . $provider_name . " order ID " . $order->id;

//            $statusProvider = "The request was cancel by the technician :: " . $provider_name ." :: order id ::".$orderID ;
            $user->notify(new UserNotification($orderID ,$status , $statusProvider , $statusProvider_en));
        }

        if ( $request->status == 4 || $request->status == 3 ) {
//            dd(1221) ;
            $provider_name = $order->provider->FULL_NAME ;
            $user = User::first() ;
            $orderID = $order->id ;
            $status = "success" ;
            $statusProvider = " تم الموافقه علي الطلب من قبل الفني "  . $provider_name . " رقم الطلب " . $orderID ;
            $statusProvider_en = "The request was approved by the technician "  . $provider_name . " order number " . $orderID ;

            $user->notify(new UserNotification($orderID ,$status , $statusProvider , $statusProvider_en));
            //====================
            //  Send note to client
            //=========================
            $message = "تم تخصيص فني لطلبك رقم" . $order->id ;
            $titleNote =  __('app.note_new_request') ;
            $notify_fire = new FirebaseNotification();
//            dd( $order->client->player_id ) ;
            $notify_fire->sendNote([$order->client->player_id], 'Kafo', $message , $titleNote , $order->client->type_device ,"order");

//            dd( $r ) ;
            $message_en = "A technician has been assigned to your order no". $order->id ;
            $titleNote =  "app.technician_assigned"  ;
            $order->client->notify(new ClientNotification($message , $message_en , $titleNote));

//            $order->client->notify(new ClientNotification($message , $message_en , $titleNote));
//            if ( $order->client->player_id ) {
//                $titleNote =  __($titleNote)  ;
//                $notify_fire = new FirebaseNotification();
//                $notify_fire->sendNote([$order->client->player_id], 'Kafo', $message ,$titleNote , $order->client->type_device);
//            }
        }


        $order->save();

        // Todo notification here
        return $this->sendResponse([], __("messages.updated_successfully"));
    }

    public function technicalRating( Request $request , $id )
    {
        $order = Order::where(["id" => $id ])->first();

        if ( !$order->provider_rate ) {
            $order->provider_comment = $request->provider_comment ;
            $order->provider_rate = $request->provider_rate ;
            $order->what_done = $request->what_done ;
            $order->save() ;
            return $this->sendResponse([], __("messages.added_rating"));
        } else{
            return $this->sendError(__("messages.already_rating"));
        }

    }

    public function show( $id )
    {
        $user = UserRequest::user() ;
        $order = Order::with("provider" ,"client" ,'surveyItem','orderDetails','orderDetailsEdit')->find($id) ;
        $rejectOrdersstatus = TrackingOrder::where("provider_id",$user->id)->where("order_id",$id)->select("order_id","status_id","reason")->get();
        if($rejectOrdersstatus){
            foreach($rejectOrdersstatus as $rejectedOrder){
                if( $order->id==$rejectedOrder->order_id){
                    $order->status= $rejectedOrder->status_id;
                }
            }
        }
        $order = new OrderResource($order);
        if ( $order ){return $this->sendResponse($order, __("messages.successfully"));
        } else{
            return $this->sendError(__("app.no_data"));
        }
    }

    public function edit( Request $request , $id )
    {
        $order = Order::find($id);
        $order->status = OrderStatusEnum::Modification->value;
        $order->save();
        $images = [];
        $sub_service_id="" ;

        if ( $request->has("file") ){
            foreach ($request->file as $file) {
                $images[] = $this->upload($file, "orders", 600, 700);
            }
        }

        if ( $request->sub_service_id ){
            $sub_service_id = implode(",",$request->sub_service_id ) ;
        }

        $images = serialize($images);


        $order_details = $order->orderDetails()->create([
            "service_id" => $sub_service_id,
            "type" => "edit",
            "description" => $request->description,
            "images" => $images,
            "payment_method" => $request->payment_method,
        ]);


        if ( $request->spare_part_ids ){
            foreach ( $request->spare_part_ids as $spare_part_id ){
                SparePartOrderDetails::create([
                    "spare_part_id" =>$spare_part_id,
                    "order_details_id" =>$order_details->id,
                ]);
            }
        }


        $statusProvider = "هناك طلب تعديل علي الطلب رقم الطلب " . $order->id .  " مرسل من الفني " . optional( $order->provider )->FULL_NAME ;
        $statusProvider_en = "There is a request to amend the application number " . $order->id .  " Sent by the technician " . optional( $order->provider )->FULL_NAME ;

        $status = "info" ;
        $orderID = $order->id ;
        $user = User::first();

        $user->notify(new UserNotification($orderID ,$status , $statusProvider , $statusProvider_en));

        return $this->sendResponse([], __("messages.edited_successfully"));
    }

    public function evaluateAgent( RatingToAgentRequest $request , $id )
    {
        $order = Order::where("id" , $id )->first();
        $order->client_rate = $request->rate ;
        $order->client_rate_comment = $request->client_rate_comment ;
        $order->save() ;

        return $this->sendResponse(["order_provider_rate_avg" =>$request->rate], __("messages.add_rating_successfully"));
    }

    private function trackingOrder( $order_id , $provider_id , $status_id , $status , $reson = null)
    {
        TrackingOrder::create([
            "order_id" => $order_id ,
            "provider_id" => $provider_id ,
            "status_id" => $status_id ,
            "reason" => $reson ,
            "status" => $status
        ]);

        return true ;
    }


    public function implemented( $orderId )
    {

        $order = Order::find($orderId) ;

        if ( $order->total < $order->total_paid ) { 
            $total = $order->total_paid - $order->total ;

            WalletTransaction::create([
                "amount"        => $total ,
                "checkout_id"   => $orderId ,
                "status_paid"   => "receipt" ,
                "client_id"     => $order->client_id
            ]);

            $client = Client::find($order->client_id);
            $client->balance = $total + $client->balance  ;
            $client->save();
        }
        return ;
    }
}
