<?php

namespace App\Http\Controllers\Api;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class NotificationController extends BaseController
{
    public function index()
    {
        $user = Request::user() ;
        $lang = request()->header("Accept-Language") ;


        $this->makeAllReaded() ;

        $rows = $user->notifications()->paginate(10)->through(fn ( $row ) => [
            'id'         => $row->id,
            'date'       => Carbon::parse($row->data['set_at'])->format('Y-m-d'),
            'time'       => Carbon::parse($row->data['set_at'])->format('h:i A'),
            'message'    => $lang == "en" ? isset($row->data['message_en'] ) ?$row->data['message_en'] : " " : $row->data['message'] ,
            'created_at' => $row->data['set_at'],
        //'created_at' => $this->getRealTime( $row->data['set_at'] ),
            'site_name'  => isset( $row->data['title']  ) ? __($row->data['title']) : "",
            'logo'       => "https://kafo.mragaey.com/assets/images/logo.png",
        ]);
        return $this->sendResponse($rows, __("messages.successfully"));
    }

    public function notificationRead(Request $request)
    {
        $user = Request::user() ;
        if ($request->filled('notification')) {
            $notification = $user->unreadNotifications()->where('id', $request->notification)->first();
            if ($notification) {
                $notification->markAsRead();
            }
        }
        $data = [
            'status' => true,
            'message' => trans('front.done_successfully')
        ];
        return response()->json($data, 200);
    }

    public function notificationDetails($id)
    {
        $row = DB::table('notifications')->where('id', $id)->update([
            'read_at' => Carbon::now()
        ]);
        return response()->json([
            'status' => true,
            'data' => $row
        ], 200);
    }

    public function count()
    {
        $user = Request::user() ;

        $unreadNotifications = $user->unreadNotifications()->count();


        $data['unread_notifications'] = $unreadNotifications ;
        if ( $user->type == 'client' ){
            $order = $this->getOrder( $user->id ) ;
            $count = $order->orderDetails->count();
            $data['cart_count'] = $count ;
        }
        return $this->sendResponse($data, __("messages.successfully"));
    }

    public function makeAllReaded()
    {
        $user = Request::user() ;
        $notifications = $user->unreadNotifications()->get();
        if (count( $notifications )) {
            foreach ( $notifications as $notification ){
                $notification->markAsRead();
            }
        }
    }

    private function getOrder( $client_id )
    {

        $order = Order::with("orderDetails")->firstOrCreate(
            ['client_id' =>  $client_id , 'status' => 0 ],
            ['client_id' =>  $client_id , 'status' => 0]
        );
        return $order ;
    }

    private function getRealTime( $utcTime ){

        $utcDateTime = Carbon::parse($utcTime);
        $localDateTime = $utcDateTime->setTimezone(config('app.timezone'));
        return $localDateTime;
    }
}
