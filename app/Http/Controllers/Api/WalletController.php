<?php

namespace App\Http\Controllers\Api;

use App\Models\WalletTransaction;
use App\Traits\OrderTrait;
use App\Traits\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as UserRequest;

class WalletController extends BaseController
{
    use UploadFile , OrderTrait;

    public function rechargeDone( Request $request )
    {
        $client = UserRequest::user() ;
//        dd( $client->id ) ;

        $checkoutId = $request->checkout_id ;
        $entityId = $request->entity_id ;



        if ( $entityId == 2 ){
            $entityId = config('settings.payments.visaEntityID');
        }else{
            $entityId = config('settings.payments.madaEntityID');
        }
        $url = config('settings.payments.url')."/checkouts/".$checkoutId."/payment?entityId=".$entityId;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer OGE4Mjk0MTc0ZDA1OTViYjAxNGQwNWQ4MjllNzAxZDF8OVRuSlBjMm45aA=='));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);

        $wallet = WalletTransaction::where("checkout_id" , $checkoutId)->first();

        if ( !$wallet ) {
            $error = __('حدث مشكله في رقم التحقق') ;
            return $this->sendError($error);
        }

        $wallet->status = 1 ;
        $wallet->save();


        $client->balance = $client->balance + $wallet->amount ;
        $client->save();


        return $this->sendResponse([], __("messages.successfully"));
    }

    public function recharge( Request $request )
    {

        $client = UserRequest::user() ;
        $amount = $request->amount ;
        $entity_id = $request->entity_id ;
        $resultCode = $this->onlinePayment( $amount , $entity_id );


        if ($resultCode->result->code == "000.200.100") {
            $id = $resultCode->id;
            $data['checkout_id'] = $id;
            WalletTransaction::create([
                "checkout_id" => $id ,
                "amount" => $request->amount ,
                "entity_id" => $entity_id ,
                "status" => 0 ,
                "client_id" => $client->id
            ]) ;

        }else{
            return false; //todo
        }
        return $this->sendResponse($data, __("messages.successfully"));
    }

    private function onlinePayment( $total , $entityId = 2 )
    {
//        $logProcess = LogProcess::create(["order_id" => $order_id , "total" => $total ]);

        if ( $entityId == 2 ){
            $entityId = config('settings.payments.visaEntityID');
        }else{
            $entityId = config('settings.payments.madaEntityID');
        }

        $url = config('settings.payments.url')."/checkouts";
        $data = "entityId=".$entityId."" .
            "&amount=".$total.
            "&currency=".config('settings.payments.currency')."".
            "&paymentType=DB";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer OGE4Mjk0MTc0ZDA1OTViYjAxNGQwNWQ4MjllNzAxZDF8OVRuSlBjMm45aA=='));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);

        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);

        $responseData_js = json_decode($responseData) ;
        $code = $responseData_js->result->code ;

        return json_decode($responseData);
    }

}
