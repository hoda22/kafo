<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Client;
use App\Models\Order;
use App\Models\Service;
use App\Enums\OrderStatusEnum;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $citiesCount = City::count();
        $servicesCount = Service::parent()->count();
        $clientsCount = Client::client()->count();
        $providersCount = Client::provider()->count();


        $clients = Client::client()->limit(5)->get();
        $providers = Client::provider()->limit(5)->get();

        $orderWaiting = Order::where("status", OrderStatusEnum::RequestIsDone->value )
                    ->orWhere("status", OrderStatusEnum::UnderRevision->value)
                    ->orWhere("status", OrderStatusEnum::TechnicianAssigned->value)
                    ->count() ;


        $orderCurrent = Order::where("status", OrderStatusEnum::TechnicianApproved->value )
                    ->orWhere("status", OrderStatusEnum::TechnicianWay->value)
                    ->orWhere("status", OrderStatusEnum::TechnicianArrived->value)
                    ->count() ;


        $orderDone = Order::where("status", OrderStatusEnum::Implemented->value )->count() ;

        $statisticsClient = [];
        $statisticsProvider = [];
        $month = 1;
        while ($month <= 12) {
            $statisticsClient[] = Client::client()->whereYear('created_at', today()->year)
                ->whereMonth('created_at', $month)->count();

            $statisticsProvider[] = Client::provider()->whereYear('created_at', today()->year)
                ->whereMonth('created_at', $month)->count();

            $month++;
        }
        $statisticsClient = json_encode($statisticsClient);
        $statisticsProvider = json_encode($statisticsProvider);


        return view('home' , compact('citiesCount' , 'statisticsClient','statisticsProvider','servicesCount' ,'clientsCount' ,'providersCount' ,'clients' ,'providers',
            'orderWaiting','orderCurrent','orderDone' ));
    }
}
