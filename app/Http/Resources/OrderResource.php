<?php

namespace App\Http\Resources;

use App\Models\AddressClient;
use App\Models\SurveyOrder;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    public function toArray($request)
    {
        $imgProviderAvatar = "";
        $imgClientAvatar = "";
        $baseUrl = config('settings.baseUrl') ;
        if ( $this->provider && $this->provider->avatar ) {
            $imgProviderAvatar = $baseUrl.$this->provider->avatar ;
        }

        if ( $this->client && $this->client->avatar ) {
            $imgClientAvatar = $baseUrl.$this->client->avatar ;
        }


        $orderDetailsEdit = $this->whenLoaded('orderDetailsEdit') ;

//        dd( $orderDetails ) ;

        $pricesCashRemain = [] ;
        $pricesCashTotal = [] ;

        $pricesOnlineTotal = [] ;
        $pricesOnlineRemain = [] ;


        if ( $this->method_payment == 1 ) { // cash

            $pricesCashTotal[] = $this->basic_total;
            if (!$this->is_basic_paid) {
                $pricesCashRemain[] = $this->basic_total;
            }
        }else { //Online
            $pricesOnlineTotal[] = $this->basic_total;
            if (!$this->is_basic_paid) {
                $pricesOnlineRemain[] = $this->basic_total;
            }
        }

        foreach ( $orderDetailsEdit as $orderDetails_item ) {

            if ( $orderDetails_item->payment_method == 0 && $orderDetails_item->admin_status == "approved_client" ) { // cash
                $pricesCashTotal[] = $orderDetails_item->price;
                                                                // cash
                if (
                    $orderDetails_item->paid_status == 0
                    && $orderDetails_item->payment_method == 0
                    && $orderDetails_item->admin_status == "approved_client"
                    )
                {
                    $pricesCashRemain[] = $orderDetails_item->price;
                }
            } else if ( $orderDetails_item->payment_method == 1 && $orderDetails_item->admin_status == "approved_client" ){ // online
                $pricesOnlineTotal[] = $orderDetails_item->price;

                if ( $orderDetails_item->paid_status == 0 && $orderDetails_item->payment_method == 1 && $orderDetails_item->admin_status == "approved_client" ) {
                    $pricesOnlineRemain[] = $orderDetails_item->price;
                }
            }

        }

        //$pricesCashTotal = $orderDetails_item->price ;
//                    if ( $orderDetails_item->paid_status ) {
//                        $pricesCashRemain[] = $orderDetails_item->price ;
//                    }
//        if (  ) { // online
//
//        }


        $payment_online = [
            "payment_quantity" =>  array_sum($pricesOnlineTotal),
            "payment_quantity_remain" => array_sum($pricesOnlineRemain)
        ] ;


        $payment_cash = [
            "payment_quantity" =>  array_sum($pricesCashTotal),
            "payment_quantity_remain" => array_sum($pricesCashRemain)
        ] ;


        $remainingTime =  $this->remainingTime($this->service_date ." ".$this->service_time ) ;

        $ClientAddress=AddressClient::where("id",$this->address_id)->get();
        // $ClientCity=City::where("id",$this->city_id)->get();
        $ClientAddressString="";
        if($ClientAddress !=null && count($ClientAddress) >0){
            $ClientAddressString=$ClientAddress[0]->address." - ".$ClientAddress[0]->region;
            // if($ClientCity !=null && count($ClientCity)>0){
            //    $ClientAddressString .= " (".$ClientCity[0]->name.")";
            // }
        }



        return [
            'order_id'                => $this->id,
            'order_provider_id'       => $this->provider_id,
            'order_client_id'         => $this->client_id,
            'order_client_name'       => optional($this->client)->full_name,
            'order_provider_name'     => optional($this->provider)->full_name,
            'order_provider_rate_avg' => floatval(optional($this->provider)->clientRateAvg),
            'order_provider_image'    => $imgProviderAvatar,
            'order_client_image'      => $imgClientAvatar,


            'order_client_phone'      => optional($this->client)->phone,

            'order_service_date'      => $this->service_date	,
            'order_service_time'      => $this->service_time ,
            'order_address'           => $ClientAddressString,
            'order_new_cost'          => $this->new_cost,

            'order_currency'          => __('app.SAR'),
            'order_provider_reason'      => $this->reject_reson,
            'order_is_paid'           => $this->is_paid,
            'order_is_basic_paid'     => $this->is_basic_paid,

            'order_management_reject_reason'      => $this->management_reject_reson,

            'provider_comment'        => $this->provider_comment,
            'provider_rate'           => $this->provider_rate,
            'order_description'       => $this->notes,
            'order_provider_status'   => $this->provider_status,
            'order_status'            => $this->status,
            'order_lat'               => $this->lat,
            'order_lon'               => $this->lon,
            'order_basic_total'       => (float)$this->basic_total,

            'order_details'           => OrderDetailsResource::collection($this->whenLoaded('orderDetails') ),

            'order_details_edit'      => OrderDetailsEditResource::collection($this->whenLoaded('orderDetailsEdit') ),


            'order_survey'            => SurveyOrder::where("order_id",$this->id)->first() ? 1 : 0 ,
            'order_rating'            => $this->rating,
            'order_set_at'            => $this->set_at,
            'order_set_at_format'     => date('Y-m-d H:i:s', strtotime($this->set_at)) ,


            'client_rate_comment'     => $this->client_rate_comment,
            'client_rate'             => $this->client_rate,

            'what_done'               => $this->what_done,
            'end_date'                => $this->end_date,
            'end_time'                => $this->end_time,
            'remaining_time'          => $remainingTime,
            'payment_method'          => $this->method_payment,

            'payment_online'          => $payment_online,
            'order_images'            => OrderImagesResource::collection($this->orderImages),
            'payment_cash'            => $payment_cash

        ];
    }

    private function remainingTime( $datTime )
    {
        $startDateTime = Carbon::now();
        $endDateTime = Carbon::parse($datTime); // Replace this with your actual end date and time

        $remainingTime = $endDateTime->diff($startDateTime);

        $years = $remainingTime->y;
        $months = $remainingTime->m;
        $days = $remainingTime->d;
        $hours = $remainingTime->h;
        $minutes = $remainingTime->i;
        $seconds = $remainingTime->s;
//        Remaining time:  {$years} years, , {$seconds} seconds
        $text = "" ;
        if ( $months ) {
            $text.= "{$months} months ,";
        }

        if ( $days ) {
            $text.= "{$days} ". __('app.days') ." ,";
        }

        if ( $hours ) {
            $text.= "{$hours}  ". __('app.hours') .",";
        }

        if ( $minutes ) {
            $text.= "{$minutes}  ". __('app.minutes') ." ";
        }

        if ($startDateTime->gte($endDateTime)) {
            return "تم انتهاء الوقت";
        }
        return $text." .";
    }

}
