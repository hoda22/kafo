<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderImagesResource extends JsonResource
{
    public function toArray($request)
    {
        $baseUrl = config('settings.baseUrl') ;
        return [
            'id'        => $this->id ,
            'images'    => $baseUrl.$this->image ,
        ];
    }
}
