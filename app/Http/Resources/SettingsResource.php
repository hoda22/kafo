<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SettingsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'site_name'     => $this->site_name ,
            'email'         => $this->email,
            'phone'         => $this->phone ,
            'facebook'      => $this->facebook ,
            'twitter'       => $this->twitter ,
            'linkedin'      => $this->linkedin ,
            'instagram'     => $this->instagram ,
            'whats_app'     => $this->phone2
        ];
    }
}
