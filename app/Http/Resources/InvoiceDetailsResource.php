<?php

namespace App\Http\Resources;

use App\Models\SurveyOrder;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceDetailsResource extends JsonResource
{
    public function toArray($request)
    {
        $note = "";
        if ( $this->admin_status == 'rejected_client' ) {
            $price = $this->visit ;
            $note = __('app.settings.visit');
        } else {
            $price = $this->price ;
        }

        return [
            'id'                => optional( $this->service_item )->id,
            'service'           => optional( $this->service_item )->name ,
            'unit_price'        => $this->unit_price ,
            'quantity'          => $this->quantity ,
            'service_parent'    => optional( $this->service_parent )->name ,
            'service_price'     => $price ,
            'note'              => $note
        ];
    }
}
