<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PageResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'page_id'           => $this->id,
            'page_name'         => $this->name,
            'page_description'  => $this->description ,
        ];
    }
}