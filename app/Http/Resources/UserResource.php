<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        $image = $this->attachment ? "http://mragaey.com/new_version/slider/".$this->attachment->src :"" ;
        return [
            'id'            => $this->id,
            'first_name'    => $this->first_name ,
            'last_name'     => $this->last_name ,
            'full_name'     => $this->full_name ,
            'email'         => $this->email ,
            'lat'           => $this->lat ,
            'lon'           => $this->lon ,
            'phone'         => $this->phone ,
            'nationality'   => $this->nationality ,
            'jop'           => $this->jop ,
            'verify'        => $this->verify ,
            'date_birth'    => $this->date_birth ,


            'avatar'        => asset($this->avatar) ,


            'residency_number'          => $this->residency_number ,
            'years_experience'          => $this->years_experience ,

            'rate'          => 0 ,
        ];
    }
}
