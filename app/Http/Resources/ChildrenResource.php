<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChildrenResource extends JsonResource
{
    public function toArray($request)
    {
        $baseUrl = config('settings.baseUrl') ;
        $image = $baseUrl.optional( $this->parentItem )->src ;
        return [
            'id'    => $this->id,
            'name'  => $this->name ,
            'main_service'  => optional( $this->parentItem )->name ,
            'image'  => $image,
            'price' => $this->price ,
            'currency' => __('app.SAR'),
        ];
    }
}
