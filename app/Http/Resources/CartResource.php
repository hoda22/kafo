<?php

namespace App\Http\Resources;
use App\Models\Order;
use App\Models\OrderDetails;
use Illuminate\Support\Facades\Request as UserRequest;
use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    public function toArray($request)
    {

        $user = $request->user('sanctum') ;

        $baseUrl = "https://mragaey.com/kafo/" ;
        $image = $baseUrl.optional( $this->parentItem )->src ;


        $data = [
            'id'    => $this->id,
            'name'  => $this->name ,
            'price' => $this->price ,
            'main_service'  => optional( $this->parentItem )->name ,
            'image'  => $image,
            'currency' => __('app.SAR'),
            'is_in_cart' => false,
        ] ;
        $currentOrder = Order::where(["status" => 0 , "client_id" => $user->id])->first() ;
        if ( $currentOrder ){

//            dd( $currentOrder->id ) ;
            $order_details = OrderDetails::where("order_id" , $currentOrder->id)
                ->where("service_id" , $this->id)
                ->first();
            if ( $order_details ){
                $data['quantity'] = $order_details->quantity;
                $data['is_in_cart'] = true;
            }

        }

        return $data;
    }
}
