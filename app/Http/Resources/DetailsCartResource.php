<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DetailsCartResource extends JsonResource
{
    public function toArray($request)
    {

        $baseUrl = config('settings.baseUrl') ;
        $image = $baseUrl.optional( $this->service_parent )->src ;

        return [
            'id'            => $this->id,
            'service'       => optional( $this->service )->name ,
            'service_id'    => optional( $this->service)->id ,
            'main_service'  => optional( $this->service_parent )->name ,
            'image'         => $image,
            'quantity'      =>  $this->quantity,
            'price'         =>  $this->price,
            "unit_price"    => $this->unit_price ,

            'currency'      => __('app.SAR'),
        ];
    }
}
