<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    public function toArray($request)
    {
        $baseUrl = config('settings.baseUrl') ;
        return [
            'id'                => $this->id,
            'service_name'      => $this->name,
            'service_children'  => ChildrenResource::collection($this->whenLoaded('children') ) ,
            'parent_id'         => $this->parent_id,
            'service_name_en'   => optional( $this->translate('en') )->name,
            'image'             => $baseUrl.$this->src
        ];
    }
}
