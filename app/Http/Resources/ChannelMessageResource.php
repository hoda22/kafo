<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChannelMessageResource extends JsonResource
{
    public function toArray($request)
    {
        $baseUrl = "https://mragaey.com/kafo/" ;
        return [
            'message_id'   => $this->id,
            'message'      => ($this->type == 'file') ? $baseUrl.$this->message : $this->message ,
            'sender'       => ($this->sender) ? $this->sender->full_name : "Kafo",
            'sender_id'    => ($this->sender) ? $this->sender->id : "Kafo",
            'image'        => $this->getAvatar() ,
            'type'         => $this->type ,
            'date'         => date('Y-m-d H:i:s', strtotime($this->created_at)) ,
        ];
    }
}
