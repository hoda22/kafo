<?php

namespace App\Http\Resources;

use App\Models\SurveyOrder;
use Illuminate\Http\Resources\Json\JsonResource;

class AddressClientResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'city'      => optional( $this->city )->name,
            'city_id'   =>  $this->city_id,
            'region'    => $this->region ,
            'lat'       => $this->lat ,
            'lon'       => $this->lon ,
            'address'   => $this->address ,

        ];
    }
}
