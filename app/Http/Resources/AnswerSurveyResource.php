<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AnswerSurveyResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'ordering'          => $this->ordering ,
            'text'              => $this->text ,
            'survey_question_id' => $this->survey_question_id,
        ];
    }
}
