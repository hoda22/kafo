<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'city_id'           => $this->id,
            'city_name'         => $this->name,
        ];
    }
}