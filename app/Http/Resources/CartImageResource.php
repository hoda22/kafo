<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartImageResource extends JsonResource
{
    public function toArray($request)
    {

        $baseUrl = config('settings.baseUrl') ;
        $image = $baseUrl.optional( $this->service_parent )->src ;

        return [
            'id'        => $this->id,
            'image'     => $baseUrl.$this->image,
        ];
    }
}
