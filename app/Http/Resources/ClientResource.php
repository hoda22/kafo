<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $baseUrl = config('settings.baseUrl') ;

        $img = "";
        if ( $this->avatar ){
            $img = $baseUrl.$this->avatar ;
        }

        return [
            'id'                => $this->id,
            'phone'             => $this->phone,
            'email'             => $this->email,
            'first_name'        => $this->first_name,
            'last_name'         => $this->last_name,
            'avatar'            =>  $img,
            'wallet_balance'    => $this->balance ,
            'loyalty_balance'   => $this->loyalty_balance ,
            'currency'          => __('app.SAR') ,
        ];
    }
}
