<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailsEditResource extends JsonResource
{
    public function toArray($request)
    {
        // to get sparePart
        $sparePartItem = [] ;
        foreach ( $this->sparePartsOrderDetials as $sparePart ){
            $sparePartItem[] = optional( $sparePart->sparePart )->name ;
        }
        //=================
        $data = [] ;
        foreach ( $this->getServices() as $service ) {
            $serviceData = [
                "id" => $service->id ,
                'name'  => $service->name
            ];
            $data[] = $serviceData;
        }

        //====================


        //=====================

        $imageLinks = [] ;

        $baseUrl = "https://mragaey.com/kafo/";

        if ( $this->images ) {
            foreach ( $this->images as $image ) {
                $imageLinks[] = $baseUrl.$image;
            }
        }

        return [
            'order_details_id'  => $this->id ,
            'status'            => $this->admin_status ,
            'paid_status'       => $this->paid_status ,
            'payment_method'    => $this->payment_method ,
            'data'              => $data ,
            'spare_parts_order_details' => $sparePartItem ,
            'description'       => $this->description,
            'images'            => $imageLinks ,
            'price'             => $this->price
        ];
    }
}
