<?php

namespace App\Http\Resources;

use App\Models\SurveyOrder;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'total'         => $this->total ,
            'client_name'   => optional( $this->client )->FULL_NAME ,
            'provider_name'   => optional( $this->provider )->FULL_NAME ,
            'coupon'        => $this->coupon_id ,
            'service_date'  => $this->service_date ,
            'service_time'  => $this->service_time ,
            'value_added'   => $this->value_added ,
            'item_details'  => InvoiceDetailsResource::collection($this->whenLoaded('orderDetailsAll') ),
        ];
    }
}
