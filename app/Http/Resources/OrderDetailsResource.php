<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'    => optional( $this->service_item )->id,
            'service'  => optional( $this->service_item )->name ,
            'service_parent'  => optional( $this->service_parent )->name ,
            'service_price'  => $this->price ,
//            'payment_method'  => $this->payment ,
//            'images'  => $this->images ? unserialize($this->images) : "" ,
        ];
    }
}
