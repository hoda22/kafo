<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SliderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $baseUrl = config('settings.baseUrl') ;
        //$image = $this->attachment ? "http://mragaey.com/new_version/slider/".$this->attachment->src :"" ;
        return [
            'id'        => $this->id,
            'image'     => $baseUrl.$this->src
        ];
    }
}
