<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SurveyQuestionResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'type'      => $this->type,
            'text'      => $this->text ,
            'ordering'  => $this->ordering ,
            'answers'   => AnswerSurveyResource::collection($this->whenLoaded('answers') ) ,
        ];
    }
}
