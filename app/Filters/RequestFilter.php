<?php
namespace App\Filters;

class RequestFilter
{
    protected $query;

    public function filter ($query)
    {
        $this->query = $query;
        foreach ( request()->all() as $filter => $value) {
            if (method_exists($this, $filter) && !is_null($value)) {
                $this->$filter($value);
            }
        }
    }

    protected function from($value)
    {
        $this->query->whereDate("service_date",">=" , $value);
    }

    protected function to($value)
    {
        $this->query->whereDate("service_date","<=" , $value );
    }

    protected function service_id($value)
    {
        $this->query->where("service_id", $value );
    }

//    protected function text($value)
//    {
//        $this->query->where("name", 'LIKE', '%'.$value.'%') ;
//    }
}