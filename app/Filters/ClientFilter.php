<?php
namespace App\Filters;

class ClientFilter
{
    protected $query;

    public function filter ($query)
    {
        $this->query = $query;
        foreach ( request()->all() as $filter => $value) {
            if (method_exists($this, $filter) && !is_null($value)) {
                $this->$filter($value);
            }
        }
    }

    protected function from($value)
    {
        $this->query->whereDate("created_at",">=" , $value);
    }

    protected function to($value)
    {
        $this->query->whereDate("created_at","<=" , $value );
    }

}