<?php

namespace App\Exports;

use App\Models\Order;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ExportRequest implements FromQuery ,WithHeadings , WithMapping
{
    private $i = 1;
    protected $filter;
    public function __construct( $filterData )
    {
        $this->filter = $filterData ;
    }

    public function query()
    {
        $query = Order::latest();

        if ( isset( $this->filter['from'] ) ){
            $query = $query->whereDate("service_date",">=" , $this->filter['from']);
        }
        if (isset( $this->filter['to'] ) ){
            $query = $query->whereDate("service_date","<=" , $this->filter['to']);
        }

        return $query;
    }

    public function map($data): array
    {
        return [
            $this->i++,
            optional( $data->client )->full_name,
            optional( $data->provider )->full_name,
            optional( $data->service )->name,
            $data->price,
            $data->service_date,
        ];
    }


    public function headings(): array
    {
        return [
            '#',
            __('app.client_name') ,
            __('app.provider_name'),
            __('app.service_name'),
            __('app.price') ,
            __('app.service_date'),
        ];
    }
}
