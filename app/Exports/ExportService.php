<?php

namespace App\Exports;

use App\Models\Service;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ExportService implements FromQuery ,WithHeadings , WithMapping
{
    private $i = 1;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Service::all();
    }

    public function query()
    {
        return Service::parent()->latest();
    }

    public function map($data): array
    {
        return [
            $this->i++,
            $data->name,
            $data->translate('en')->name,
            count($data->children),
            $data->price,
            $data->ordering,
            $data->active,

        ];
    }


    public function headings(): array
    {
        return [
            '#',
            'Service Name Arabic',
            'Service Name English',
            'How Many Services',
            'Price',
            'Ordering',
            'Status',
        ];
    }
}
