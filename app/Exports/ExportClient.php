<?php

namespace App\Exports;

use App\Models\Client;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ExportClient implements FromQuery ,WithHeadings , WithMapping
{
    private $i = 1;
    protected $filter;
    public function __construct( $filterData )
    {
        $this->filter = $filterData ;
    }



    public function query()
    {
        $query = Client::client();
        if ( isset( $this->filter['from'] ) ){
            $query = $query->whereDate("created_at",">=" , $this->filter['from']);
         }
        if (isset( $this->filter['to'] ) ){
            $query = $query->whereDate("created_at","<=" , $this->filter['to']);
        }
        $query = $query->latest();

        return $query ;
    }

    public function map($data): array
    {
        return [
            $this->i++,
            $data->full_name,
            $data->email,
            $data->phone,
            $data->created_at,
        ];
    }


    public function headings(): array
    {
        return [
            '#',
            'Name',
            'Email',
            'Phone',
            'Date of join',
        ];
    }
}
