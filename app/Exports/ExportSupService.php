<?php

namespace App\Exports;

use App\Models\Service;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ExportSupService implements FromQuery ,WithHeadings , WithMapping
{
    private $i = 1;

    public function __construct( $id )
    {
        $this->id = $id;
    }

    public function query()
    {
        return Service::where("parent_id" , $this->id )->latest();
    }

    public function map($data): array
    {
        return [
            $this->i++,
            $data->name,
            $data->translate('en')->name,
            $data->price,
            $data->ordering,
            $data->active == "active" ?"نشط": "غير نشط",

        ];
    }


    public function headings(): array
    {
        return [
            '#',
            'Service Name Arabic',
            'Service Name English',
            'Price',
            'Ordering',
            'Status',
        ];
    }
}
