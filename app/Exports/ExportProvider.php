<?php

namespace App\Exports;

use App\Models\Client;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ExportProvider implements FromQuery ,WithHeadings , WithMapping
{
    private $i = 1;
    protected $filter;
    public function __construct( $filterData )
    {
        $this->filter = $filterData ;
    }

    public function query()
    {
        $query = Client::provider();
        if ( isset( $this->filter['from'] ) ){
            $query = $query->whereDate("created_at",">=" , $this->filter['from']);
         }
        if (isset( $this->filter['to'] ) ){
            $query = $query->whereDate("created_at","<=" , $this->filter['to']);
        }
        $query = $query->latest();

        return $query ;
    }

    public function map($data): array
    {
        return [
            $this->i++,
            $data->full_name,
            $data->email,
            $data->providerHoursOrders,
            count($data->providerOrders),
            $data->created_at,
        ];
    }


    public function headings(): array
    {
        return [
            '#',
            __('app.full_name'),
            __('app.email'),
            __('app.hoursNum'),
            __('app.ordersNum'),
            __('app.create_at')
        ];
    }
}
