<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Mail;

use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/sas', function () {
    $total = 48 ;
    return \App\Models\LoyaltyPoint::where("num_sar" , "<=" , $total)
        ->orderBy("num_sar" , "DESC")
        ->where("active","active")
        ->first();
});

Route::get('/curl', function () {

    $firebaseDatabaseURL = 'https://kafo-8f75c-default-rtdb.firebaseio.com';
    $firebaseSecret = 'kBU5uUbdIGz8AXaHu19EVTR3N1EFcLOOtaE57M2w	';

    $client = new Client();

    // Example data to be pushed to Firebase
    $data = [
        'event_name' => 'Example Event',
        'timestamp' => now()->toDateTimeString(),
    ];

    // Make a PUT request to push data to Firebase
    $response = $client->put($firebaseDatabaseURL . 'events.json?auth=' . $firebaseSecret, [
        'json' => $data,
    ]);

    // Check the response for success or handle errors
    if ($response->getStatusCode() === 200) {
        return response()->json(['message' => 'Event pushed to Firebase']);
    } else {
        return response()->json(['error' => 'Failed to push event to Firebase'], $response->getStatusCode());
    }
});

Route::get('/change-order/{id}/{status}', function ( \Illuminate\Http\Request $request ) {

    $id =  $request->id ;
    $status =  $request->status ;

    $order = \App\Models\Order::where("id",$id)->first();
    $order->status = $status ;
    $order->save();
    return "Done";
});


Route::get('/test-query', function () {
//    $lat =  "75.2343743294477";
//    $lng = "-78.37305343098593";

    $lat =  "30.956465";
    $lng = "31.2503834";

    $results = \Illuminate\Support\Facades\DB::table('cities')
        ->whereNotNull('zone_polygon')
        ->where('zone_polygon', 'LIKE', 'POLYGON%')
        ->whereRaw("ST_Intersects(ST_GEOMFROMTEXT('POINT(".$lat." ".$lng.")'), ST_GEOMFROMTEXT(zone_polygon))")
        ->get();

    return $results ;

    /*
     * SELECT * FROM cities
WHERE (zone_polygon IS NOT NULL)
AND (zone_polygon LIKE 'POLYGON%')
AND ST_Intersects(ST_GEOMFROMTEXT('POINT(75.2343743294477 -78.37305343098593)'), ST_GEOMFROMTEXT(zone_polygon));
     */

   // $providers = \Illuminate\Support\Facades\DB::select( \Illuminate\Support\Facades\DB::raw(
  //      "SELECT * FROM `citis` WHERE (`zone_polygon` IS NOT NULL) AND (`zone_polygon` LIKE  'POLYGON%')
//AND ST_Intersects(ST_GEOMFROMTEXT('POINT('+$lng -78.37305343098593)'), ST_GEOMFROMTEXT(zone_polygon));
//ST_Intersects(GeomFromText(extra), GeomFromText('POINT(".$lat." ".$lng.")'))") );
    $providers = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("SELECT * FROM cities
WHERE (zone_polygon IS NOT NULL)
AND (zone_polygon LIKE 'POLYGON%')
AND ST_Intersects(ST_GEOMFROMTEXT('POINT(30.0333 30.0333)'), ST_GEOMFROMTEXT(zone_polygon));"));
    return $providers ;
});


Route::get('/ddd', function () {
    $user = \App\Models\User::first() ;
    $orderID = 3 ;
    $status = "danger" ;
    $statusProvider = "تم رفض الطلب من قبل الفني ' محمد عزت راشد '" ;
    $user->notify(new \App\Notifications\UserNotification($orderID ,$status , $statusProvider));
    return "Sdsds" ;
});


Route::get('/dd1111d', function () {
//    $user = \App\Models\User::first() ;
//    $orderID = 3 ;
//    $status = "danger" ;
//    $statusProvider = "تم رفض الطلب من قبل الفني ' محمد عزت راشد '" ;
//    $user->notify(new \App\Notifications\UserNotification($orderID ,$status , $statusProvider));
//    return "Sdsds" ;

    $notes = \App\Models\Notification::where("notifiable_type" , "App\Models\User")->get() ;
     $data = $notes[1] ;
    return json_decode($data)->data ;
});



Route::get('/qqq', function () {
    $dateTime = date('Y-m-d H:i:s') ;
    return \Carbon\Carbon::parse(\Carbon\Carbon::parse($dateTime)->setTimezone('UTC')->format('Y-m-d H:i:s'), 'UTC');
});

Route::get('language/{locale}', function ($locale) {
    app()->setLocale($locale);
    session()->put('locale', $locale);
    return redirect()->back();
})->name("change-lang");
//
//Route::get('/send-notification', function () {
//    $data = date('Y-m-d H:i:s') ;
////    return $data ;
//    $da = \Carbon\Carbon::parse(Carbon\Carbon::parse($data)->setTimezone('UTC')->format('Y-m-d H:i:s'), 'UTC');
//
//
//    return $da ;
//    $users = \App\Models\Client::get() ;
//    \Illuminate\Support\Facades\Notification::send($users , new \App\Notifications\AdminNotification());
//});
//
Route::get('/send-firebaseNotification', function () {
    $player_id = "e8a4M26HXkzEl23wgXy9vq:APA91bHySKTfxYgVyl9sKQ3TMmORJzk3K-K0VVNIow001uFakn4fhnMpMvW1ZxXi_hO4mpdrR0bVn58uySvCQ9IIxD1gH6fY0SMt7P_JEG5XpbETCos-KIIY4E4_ooUwnTBMRb-v0Bdq";
    $notify_fire = new \App\FirebaseNotification();

    $msg_ar = " أهلا صديقي سعيد" ;
    try {
        $res = $notify_fire->sendNote([$player_id], 'Kafo', $msg_ar,"تجريب عنوان",1);
    } catch (\Exception $e) {
        // Handle the exception (log it, display an error message, etc.)
        \Log::error('Email sending failed: ' . $e->getMessage());
        dd(">>",$e->getMessage()) ;
    }
    return $res;
});

Route::get('/send-email-121', function () {

    return Mail::raw('This is a test email', function($message) {
        $message->to('mahmoud.ragaey.99@gmail.com');
        $message->subject('Test Email');
    });

    return $tt ;

//$to = "melzaiady@yahoo.co.uk";
//$to = "mahmoud.ragaey.99@gmail.com";
//$subject = "HTML email";
//
//$message = "
//<html>
//<head>
//<title>HTML email</title>
//</head>
//<body>
//<p>This email contains HTML Tags!</p>
//<table>
//<tr>
//<th>Firstname</th>
//<th>Lastname</th>
//</tr>
//<tr>
//<td>John</td>
//<td>Doe</td>
//</tr>
//</table>
//</body>
//</html>
//";

// Always set content-type when sending HTML email
//$headers = "MIME-Version: 1.0" . "\r\n";
//$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
//
//// More headers
//$headers .= 'From: <info@mragaey.com>' . "\r\n";
//
//mail($to,$subject,$message,$headers);

    return "Done"  ;
});

Route::get('/send-email-21', function () {
    $email = "mahmoud.ragaey.99@gmail.com" ;
    $subject ="subject" ;
    $text = "text" ;
    $phone = "090090909" ;
    $data = array('email' => $email, 'subject' => $subject, 'body' => $text, 'mobile'=>$phone);

    //********* end send email
    $aa = Mail::send('emails_items.contact', $data, function($message) use ($data) {
        $message->from($data['email']);
        $message->to('mahmoud.ragaey.99@gmail.com');
        $message->subject($data['subject']);
    });

    return $aa ;



    $em = "mahmoud.ragaey.99@gmail.com" ;
    $name  = " name" ;
    return \Illuminate\Support\Facades\Mail::to($em)->send(new \App\Mail\ContactEmail($em , $name));

    return "sasa";

    $toAdminMail= "your_admin_email@gmail.com";
    $message  = " Check Contact Form" ;


    \Illuminate\Support\Facades\Mail::to($toAdminMail)->send(new \App\Notifications\ContactMessage($message , $name));

    return "DONE" ;


    $provider = \App\Models\Client::where("id" ,54)->first();
    $name ="Yaaarb" ;
    $order_id =11 ;
    $message = 'new request been sent to you and awaiting your approval of the request :: ' .$name .' :: order id ::'.$order_id;
    $titleNote =  'app.note_new_request' ;
    $provider->notify(new \App\Notifications\ClientNotification($message , $titleNote));
    return "Done" ;
});

Route::get('/hyperPaypayment-23', function () {
    $id = "B5EAB7D8FD0D9BEEDC738820C043833A.uat01-vm-tx01" ;

    $url = "https://test.oppwa.com/v1/checkouts/".$id."/payment?entityId=8a8294174d0595bb014d05d82e5b01d2";
//    $url .= "";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization:Bearer OGE4Mjk0MTc0ZDA1OTViYjAxNGQwNWQ4MjllNzAxZDF8OVRuSlBjMm45aA=='));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $responseData = curl_exec($ch);
    if(curl_errno($ch)) {
        return curl_error($ch);
    }
    curl_close($ch);
    return $responseData;
});

Route::get('/hyperPaypayment-22', function () {

    //visa 8ac7a4c78694e687018697314f3e02f4
    // 8ac7a4c78694e68701869731bae102f9

//    return config("settings.payments.visaEntityID") ;

    $price = 92.00;

    $url = config('settings.payments.url')."/checkouts";
    $data = "entityId=".config('settings.payments.visaEntityID')."" .
        "&amount=".$price.
        "&currency=".config('settings.payments.currency')."".
        "&paymentType=DB";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization:Bearer OGE4Mjk0MTc0ZDA1OTViYjAxNGQwNWQ4MjllNzAxZDF8OVRuSlBjMm45aA=='));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // this should be set to true in production
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $responseData = curl_exec($ch);

    if(curl_errno($ch)) {
        return curl_error($ch);
    }
    curl_close($ch);

    return  json_decode($responseData);

    return $responseData;

});


Route::get('/hyperPaypayment-21', function () {
    $user = \App\Models\Client::first();

    $price = 200;
    $amount = $price;// $request->amount ;
    //
    $package_id = 2 ;
    $merchantTransactionId = $user->id.strtotime(date("Y-m-d H:m:s"));
    $userID = $user->id;
    //
    $UserName= $user->name;
    $email = $user->email;
    $street1= "street address of customer";
    $city= "SA";
    $state= "state";
    $country= "SA";
    $postcode="12345";
    $customerGivenName=$user->first_name;
    $customerSurName=$user->first_name.$user->mobile;
    $PaymentBrandId = 3 ;
    if($PaymentBrandId == 0 || $PaymentBrandId == 2){ // visa , stc
        $entityId=config('constants.Payment.VisaEntityID'); //test -- VisaEntityID
        $entityId="8ac7a4c78694e687018697314f3e02f4"; //test -- VisaEntityID
    }
    else if($PaymentBrandId == 1){ //mada
        $entityId=config('constants.Payment.MadaEntityID'); //test -- MadaEntityID
        $entityId="8ac7a4c78694e68701869731bae102f9"; //test -- MadaEntityID
    }
    else if($PaymentBrandId == 3){ //  apple pay
        $entityId=config('constants.Payment.ApplePayEntityID'); //test -- apple pay   "8ac7a4c87edd1a80017ede0f92940431";//
    }
    $url = "https://oppwa.com/v1/checkouts";
    //$url = "https://test.oppwa.com/v1/checkouts"; //test
    $data = "entityId=".$entityId.
        "&amount=". $amount .
        "&currency=SAR&paymentType=DB&merchantTransactionId=".$merchantTransactionId.
        "&customer.email=".$email."&billing.street1=".$street1."&billing.city=".$city."&billing.state=".$state.
        "&billing.country=".$country."&billing.postcode=".$postcode;
    if($PaymentBrandId != 2 && $PaymentBrandId != 3){
        $data .="&createRegistration=true";
    }
    if($PaymentBrandId == 3){
        $data = "entityId=".$entityId.
            "&amount=". $amount .
            "&currency=SAR&paymentType=DB&merchantTransactionId=".$merchantTransactionId.
            "&customer.email=".$email."&billing.street1=".$street1."&billing.city=".$city."&billing.state=".$state.
            "&billing.country=".$country."&billing.postcode=".$postcode;
    }


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization:Bearer OGFjZGE0Y2U3YTgxMzUwOTAxN2E4MTY2MzM3MjAxZDR8ejkzOEdFN2o1Tg=='));
    // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    //     'Authorization:Bearer OGFjN2E0Yzc3NjQxYjEzMDAxNzY0MjliMDkyODA1ZmJ8SFl3NlB3V3pzSw=='));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $responseData = curl_exec($ch);


    if(curl_errno($ch)) {
        return response()->json(['status' => false, 'message' => __('Please try again'),'$responseData'=>$responseData], 200);
    }
    curl_close($ch);
    $Vat = 14;
    $checkoutId = json_decode($responseData);

    return $checkoutId ;
    if($checkoutId->result->code == "000.200.100"){
        $checkoutId = $checkoutId->id;
        $ReturnedURL = config('global.WebsiteURL')."public/en/hyperpay?checkoutId=".$checkoutId."&user_id=".$userID.
            "&package_id=".$package_id."&paymentBrand=".$PaymentBrandId."&merchantTransactionId=".$merchantTransactionId
            ."&price=".$price."&Vat=".$Vat;
    }
    else{
        return response()->json(['status' => false, 'message' => "error,please try again",'$responseData'=>$responseData], 200);
    }
    return response()->json(['status' => true, 'message' => $ReturnedURL,'repomse'=>json_decode($responseData),"EE"=>$entityId,'$data'=>$data ], 200);
});


Auth::routes();
Route::group(['middleware' => ['localization_web'], ['as' => 'dashboard.']], function () {



    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


    Route::get('/reload-captcha', [\App\Http\Controllers\CaptchaServiceController::class, 'reloadCaptcha']);


    Route::group(['prefix' => 'admin_ltc', 'middleware' => ['auth']], function () {

        Route::get('/test', function () {
            return view("dashboard.test.index");
        })->name('home');



        Route::get('/test', [App\Http\Controllers\Dashboard\TestController::class, 'update']);
        Route::get('/send-sms', [App\Http\Controllers\Dashboard\TestController::class, 'sendSms']);


        $except = ['edit', 'create', 'show'];
        $exceptE_C = ['show'];
        Route::resource('user', App\Http\Controllers\Dashboard\UserController::class)->except($except);

        Route::resource('cities', App\Http\Controllers\Dashboard\CityController::class);
        Route::resource('clients', App\Http\Controllers\Dashboard\ClientController::class);
        Route::resource('pages', App\Http\Controllers\Dashboard\PageController::class);
        Route::resource('providers', App\Http\Controllers\Dashboard\ProviderController::class);
        Route::resource('sliders', App\Http\Controllers\Dashboard\SliderController::class)->except($except);
        Route::resource('brands', App\Http\Controllers\Dashboard\BrandController::class)->except($except);
        Route::resource('settings', App\Http\Controllers\Dashboard\SettingController::class)->except($except);
        Route::resource('users', App\Http\Controllers\Dashboard\UserController::class)->except($except);
        Route::resource('orders', App\Http\Controllers\Dashboard\OrderController::class);
        Route::resource('services', App\Http\Controllers\Dashboard\ServiceController::class)->except($except);
        Route::resource('spare-parts', App\Http\Controllers\Dashboard\SparePartController::class);
        Route::resource('survey-questions', App\Http\Controllers\Dashboard\SurveyQuestionController::class);
        Route::resource('complaints-suggestions', App\Http\Controllers\Dashboard\ComplaintSuggestionController::class);
        Route::resource('loyalty-points', App\Http\Controllers\Dashboard\LoyaltyPointController::class);
        Route::resource('prices-points', App\Http\Controllers\Dashboard\PricePointController::class);
        Route::resource('coupons', App\Http\Controllers\Dashboard\CouponController::class);

        Route::resource('roles', App\Http\Controllers\Dashboard\RoleController::class);
        Route::resource('notifications', App\Http\Controllers\Dashboard\NotificationController::class);
        Route::resource('messages', App\Http\Controllers\Dashboard\UserMessageController::class);

        Route::group(['prefix' => 'cities'], function () {
            Route::post('/update-city/{id}', [App\Http\Controllers\Dashboard\CityController::class, 'updateAxios'])->name('city.updateAxios');
        });


        Route::group(['prefix' => 'coupons'], function () {
            Route::post('/update-coupon/{id}', [App\Http\Controllers\Dashboard\CouponController::class, 'updateAxios'])->name('coupon.updateAxios');
        });

        Route::group(['prefix' => 'loyalty-points'], function () {
            Route::post('/update-loyalty-point/{id}', [App\Http\Controllers\Dashboard\LoyaltyPointController::class, 'updateAxios'])->name('loyalty-point.updateAxios');
        });

        Route::group(['prefix' => 'clients'], function () {

            Route::post('/update-client/{id}', [App\Http\Controllers\Dashboard\ClientController::class, 'updateAxios'])->name('client.updateAxios');
//            Route::get('/getClient', [App\Http\Controllers\Dashboard\ClientController::class, 'getClient'])->name('client.getClient');
            Route::post('/getClient', [App\Http\Controllers\Dashboard\ClientController::class, 'getClient'])->name('client.getClient');
            Route::post('/update-client-block/{id}', [App\Http\Controllers\Dashboard\ClientController::class, 'updateAxiosBlock'])->name('client.updateAxiosBlock');
        });


        Route::group(['prefix' => 'complaints-suggestions'], function () {
            Route::post('/replay/{id}', [App\Http\Controllers\Dashboard\ComplaintSuggestionController::class, 'replay'])->name('complaintsSuggestion.replay');
        });

        Route::group(['prefix' => 'orders'], function () {
            Route::post('/order-assign', [App\Http\Controllers\Dashboard\OrderController::class, 'assign'])->name('order.assign');
            Route::post('/order-reject', [App\Http\Controllers\Dashboard\OrderController::class, 'reject'])->name('order.reject');
            Route::post('/order-updates/{id}', [App\Http\Controllers\Dashboard\OrderController::class, 'updates'])->name('order.updates');
            Route::post('/update-collection/{id}', [App\Http\Controllers\Dashboard\OrderController::class, 'updateCollection'])->name('order.updateCollection');
            Route::post('/reject-canceled/{id}', [App\Http\Controllers\Dashboard\OrderController::class, 'rejectCanceled'])->name('order.rejectCanceled');
            Route::post('/new-cost', [App\Http\Controllers\Dashboard\OrderController::class, 'newCost'])->name('order.newCost');
            Route::post('/modification-status', [App\Http\Controllers\Dashboard\OrderController::class, 'modificationStatus'])->name('order.modification-status');
            Route::post('/modification-status-reject-admin', [App\Http\Controllers\Dashboard\OrderController::class, 'modificationStatusRejectAdmin'])->name('order.modificationStatusRejectAdmin');
        });

        Route::post('/order-getProviders', [App\Http\Controllers\Dashboard\OrderController::class, 'getProviders'])->name('order.getProviders');

        Route::get('/order-subServices', [App\Http\Controllers\Dashboard\OrderController::class, 'subServices'])->name('order.subServices');

        Route::group(['prefix' => 'providers'], function () {
            Route::post('/update-provider/{id}', [App\Http\Controllers\Dashboard\ProviderController::class, 'updateAxios'])->name('provider.updateAxios');
            Route::post('/update-city-provider/{id}', [App\Http\Controllers\Dashboard\ProviderController::class, 'updateCityAxios'])->name('provider.updateCityAxios');

            Route::post('/update-provider-block/{id}', [App\Http\Controllers\Dashboard\ProviderController::class, 'updateAxiosBlock'])->name('provider.updateAxiosBlock');
        });


        Route::group(['prefix' => 'users'], function () {
            Route::post('/update-provider/{id}', [App\Http\Controllers\Dashboard\UserController::class, 'updateAxios'])->name('user.updateAxios');
        });

        Route::group(['prefix' => 'slider'], function () {
            Route::post('/update-slider/{id}', [App\Http\Controllers\Dashboard\SliderController::class, 'updateAxios'])->name('slider.updateAxios');
        });

        Route::group(['prefix' => 'reports'], function () {
            Route::get('/', [App\Http\Controllers\Dashboard\ReportController::class, 'index'])->name('reports.index');
            Route::get('/report-clients', [App\Http\Controllers\Dashboard\ReportController::class, 'clients'])->name("report-clients");
            Route::get('/report-requests', [App\Http\Controllers\Dashboard\ReportController::class, 'requests'])->name("report-requests");
            Route::get('/report-services', [App\Http\Controllers\Dashboard\ReportController::class, 'services'])->name("report-services");
            Route::get('/report-finance', [App\Http\Controllers\Dashboard\ReportController::class, 'finance'])->name("report-finance");
            Route::get('/report-providers', [App\Http\Controllers\Dashboard\ReportController::class, 'provider'])->name("report-provider");
            Route::get('/report-spare-parts', [App\Http\Controllers\Dashboard\ReportController::class, 'sparePart'])->name("report-sparePart");

            Route::get('/report-clients-export', [App\Http\Controllers\Dashboard\ReportController::class, 'clientsExport'])->name("clients-export");
            Route::get('/report-providers-export', [App\Http\Controllers\Dashboard\ReportController::class, 'ProvidersExport'])->name("providers-export");
            Route::get('/report-services-export', [App\Http\Controllers\Dashboard\ReportController::class, 'ServicesExport'])->name("services-export-reports");
            Route::get('/report-requests-export', [App\Http\Controllers\Dashboard\ReportController::class, 'requestsExport'])->name("requests-export");
            Route::get('/report-finance-export', [App\Http\Controllers\Dashboard\ReportController::class, 'financeExport'])->name("finance-export");

            Route::get('/report-spareParts-export', [App\Http\Controllers\Dashboard\ReportController::class, 'sparePartsExport'])->name("spareParts-export-reports");
        });


        Route::group(['prefix' => 'services'], function () {
            Route::post('/update-service/{id}', [App\Http\Controllers\Dashboard\ServiceController::class, 'updateAxios'])->name('service.updateAxios');

            Route::get('services/{type}', [\App\Http\Controllers\Dashboard\ServiceController::class, 'export'])->name('services-export');
            Route::get('sup-services/{type}', [\App\Http\Controllers\Dashboard\ServiceController::class, 'supServicesExport'])->name('sup-services-export');
            Route::get("sup_services/{id?}", [App\Http\Controllers\Dashboard\ServiceController::class, 'supServices'])->name("sup_services.index");

        });


        Route::group(['prefix' => 'spare-parts'], function () {
            Route::post('/update-spare-parts/{id}', [App\Http\Controllers\Dashboard\SparePartController::class, 'updateAxios'])->name('spare-parts.updateAxios');
        });

        Route::group(['prefix' => 'brands'], function () {
            Route::post('/update-brand/{id}', [App\Http\Controllers\Dashboard\BrandController::class, 'updateAxios'])->name('brand.updateAxios');

        });

        Route::get('/payment-request', [App\Http\Controllers\Dashboard\PaymentRequestController::class, 'paymentRequest'])->name('payment-request.index');


        Route::get('/chat/{id?}', [App\Http\Controllers\Dashboard\ChatController::class, 'index'])->name('chat.index');


        Route::post('/chat-message', [App\Http\Controllers\Dashboard\ChatController::class, 'sendChatMessage'])->name('chat.sendChatMessage');


        Route::post('/send_message', [App\Http\Controllers\Dashboard\ChatController::class, 'sendMessage'])->name('chat.send_message');
        Route::post('/getAtherClient', [App\Http\Controllers\Dashboard\ChatController::class, 'getAtherClient'])->name('chat.getAtherClient');
    });

    Route::get('/setup1', function () {
        \Illuminate\Support\Facades\Artisan::call('db:seed');

        return redirect('/');
    });


    Route::get('/setup', function () {
        \Illuminate\Support\Facades\Artisan::call('migrate:fresh --seed');

        return redirect('/');
    });

    Route::get('/perms', function () {
        $permissions = [


//        'clients-list',
//        'clients-create',
//        'clients-edit',
//        'clients-delete',
//
//        'users-list',
//        'users-create',
//        'users-edit',
//        'users-delete',
//
//        'cities-list',
//        'cities-create',
//        'cities-edit',
//        'cities-delete',
//
//        'categories-list',
//        'categories-create',
//        'categories-edit',
//        'categories-delete',
//
//        'providers-list' ,
//        'providers-show' ,
//        'providers-create' ,
//        'providers-edit' ,
//        'providers-delete' ,
//
//
//        'sliders-list' ,
//        'sliders-create' ,
//        'sliders-edit' ,
//        'sliders-delete' ,
//
//
//        'roles-list',
//        'roles-create',
//        'roles-edit',
//        'roles-delete',

//        'settings-list',

//        'settings-edit',
//
//        'notifications-list',
//        'notifications-send',

//        'spare-parts-list',
//        'spare-parts-create',
//        'spare-parts-edit',
//        'spare-parts-delete',
//        'services-list' ,
//        'services-create' ,
//        'services-edit' ,
//        'services-delete' ,


//            'messages',
//            'messages-list',
//            'messages-send',
//
//            'reports',
//            'reports-list',
//            'reports-export',



//            'sup-services-list',
//            'sup-services-create',
//            'sup-services-edit',
//            'sup-services-delete',
//
//
//            'coupons-list',
//            'coupons-create',
//            'coupons-edit',
//            'coupons-delete',
//
//
//            'points-list',
//            'points-create',
//            'points-edit',
//            'points-delete',
//
//
//            'orders-actions',


//            "coupons" ,
//            "points" ,
//            "sup-services" ,


//            "performance-panel",
//            "performance-panel-settings" ,
//            "complaints-suggestions",
//            "complaints-suggestions-list" ,
//            "complaints-suggestions-replay"


            "report-clients",
            "report-services",
            "report-request",
            "report-finance",
            "report-provider",
            "report-spare-pats",

        ];

//    \DB::table('permissions')->delete();
        foreach ($permissions as $permission) {
            $p = new \App\Models\Permission();
            $p->name = $permission;
            $p->guard_name = "web";
            $p->save();
        }
    });

});