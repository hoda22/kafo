<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\Auth\AuthController ;
use App\Http\Controllers\Api\HomeController ;
use \App\Http\Controllers\Api\ServiceController ;
use \App\Http\Controllers\Api\OrderController ;
use \App\Http\Controllers\Api\Client\OrderController as ClientOrder ;
use \App\Http\Controllers\Api\PageController ;
use \App\Http\Controllers\Api\ProfileController ;
use \App\Http\Controllers\Api\SurveyController ;
use \App\Http\Controllers\Api\SettingController ;
use \App\Http\Controllers\Api\NotificationController ;
use \App\Http\Controllers\Api\ChatController ;
use \App\Http\Controllers\Api\SparePartsController ;
use \App\Http\Controllers\Api\CartController ;
use \App\Http\Controllers\Api\RequestController ;
use \App\Http\Controllers\Api\InvoiceController ;
use \App\Http\Controllers\Api\CityController ;
use \App\Http\Controllers\Api\ProfileCustomerController ;
use \App\Http\Controllers\Api\Client\AddressClientController ;

Route::post('/save-token', [\App\Http\Controllers\FCMController::class,'index']);

Route::middleware("localization")->group(function () {

    Route::group(['prefix' => 'v1'], function () {
        Route::post('generate-code', [AuthController::class, 'generateCode']);
        Route::post('verification-account', [AuthController::class, 'verificationAccount']);
        Route::post('login', [AuthController::class, 'login']);



        Route::post('forget-password', [AuthController::class, 'forgetPassword']);
        Route::post('new-password', [AuthController::class, 'newPassword']);

        Route::post('resend-code', [AuthController::class, 'resendCode']);

        Route::post('register', [AuthController::class, 'register']);
        Route::post('register-client', [AuthController::class, 'registerCustomer']);

        Route::get('home', [HomeController::class, 'index']);

        Route::get('price-list', [ServiceController::class, 'index']);
        Route::get('page/{status?}', [PageController::class, 'index']);

        Route::get('settings', [SettingController::class, 'index']);
        Route::get('cities', [CityController::class, 'get']);
        Route::get('get-spare-parts-by-service', [SparePartsController::class, 'index']);


        Route::middleware(['auth:sanctum'])->group(function () {
            Route::post('reset-password', [ProfileController::class, 'resetPassword']);
            Route::get('show-profile', [ProfileController::class, 'show']);

            Route::post('logout', [AuthController::class, 'logout']);

            Route::post('save-player-id', [ProfileController::class, 'saveToken']);

            Route::post('check-phone', [ProfileController::class, 'checkPhone']);
            Route::post('save-phone', [ProfileController::class, 'savePhone']);

            Route::post('save-avatar-profile', [ProfileController::class, 'saveAvatarProfile']);
            Route::post('update-coordinates', [ProfileController::class, 'updateCoordinates']);
            // orders
            Route::get('orders/{status?}', [OrderController::class, 'index']);
            Route::post('order-change-status/{id}', [OrderController::class, 'changeStatus']);
            // survey-questions
            Route::get('survey-questions', [SurveyController::class, 'index']);
            //Technical comment
            Route::post('technical-rating/{id}', [OrderController::class, 'technicalRating']);
            Route::post('save-survey', [SurveyController::class, 'saveSurvey']);


            Route::group(['prefix' => 'notifications'], function () {
                Route::get('/', [NotificationController::class, 'index']);
                Route::get('/count', [NotificationController::class, 'count']);
            });

            Route::post('delete-account', [ProfileController::class, 'deleteAccount']);
            Route::get('order_details/{id}', [OrderController::class, 'show']);
            Route::post('send-email', [SettingController::class, 'sendEmail']);
            Route::post('edit-order/{id}', [OrderController::class, 'edit']);

            Route::get('chat', [ChatController::class, 'getChat']);
            Route::post('send-message-chat', [ChatController::class, 'sendMessageChat']);
            Route::post('send-file-chat', [ChatController::class, 'sendFileChat']);
            Route::post('send-message-clients', [ChatController::class, 'sendChatClients']);

            //=============

            Route::middleware("is_client")->group(function () {

                Route::post('add-to-cart', [CartController::class, 'add']);
                Route::get('get-cart', [CartController::class, 'get']);

                Route::post('payment-done', [RequestController::class, 'paymentDone']);
                Route::post('payment-now', [RequestController::class, 'paymentNow']);

                Route::post('delete-image-from-cart/{id}', [CartController::class, 'deleteImageFromCart']);

                Route::post('add-data-to-cart', [CartController::class, 'addCartData']);
                Route::post('reschedule-order/{id}', [CartController::class, 'RescheduleOrder']);


                Route::post('update-cart', [CartController::class, 'updateItemFromCart']);
                Route::post('remove-from-cart', [CartController::class, 'removeItemFromCart']);


                Route::get('client-orders/{id}', [ClientOrder::class, 'index']);

                Route::group(['prefix' => 'addresses'], function () {
                    Route::get('/', [AddressClientController::class, 'index']);
                    Route::post('store', [AddressClientController::class, 'store']);
                    Route::post('update/{id}', [AddressClientController::class, 'update']);
                    Route::post('delete/{id}', [AddressClientController::class, 'delete']);
                    Route::get('index', [AddressClientController::class, 'index']);
                });

                Route::get('show-profile-customer', [ProfileCustomerController::class, 'show']);
                Route::get('loyalty-points', [ProfileCustomerController::class, 'loyaltyPoints']);

                Route::post('update-customer-profile', [ProfileCustomerController::class, 'update']);

                Route::get('tracking-user/{id}', [ProfileCustomerController::class, 'trackingUser']);



                Route::post('evaluate-agent/{id}', [OrderController::class, 'evaluateAgent']);
                Route::post('check-coupon', [CartController::class, 'checkCoupon']);

                Route::post('create-request-order', [RequestController::class, 'requestOrder']);
                Route::post('client-change-order-status/{id}', [\App\Http\Controllers\Api\Client\OrderController::class, 'changeStatus']);

                Route::get('get-gift', [RequestController::class, 'getGift']);
                Route::post('change-gift', [RequestController::class, 'changeGift']);

                Route::post('recharge-wallet', [\App\Http\Controllers\Api\WalletController::class, 'recharge']);
                Route::post('recharge-wallet-done', [\App\Http\Controllers\Api\WalletController::class, 'rechargeDone']);


                Route::post('recharge-wallet-done', [\App\Http\Controllers\Api\WalletController::class, 'rechargeDone']);
                Route::post('invoice/{id}', [InvoiceController::class, 'get']);
            });

        });
        Route::get('search', [HomeController::class, 'search']);//->middleware('auth:sanctum');

    });

    Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
        return $request->user();
    });
});


