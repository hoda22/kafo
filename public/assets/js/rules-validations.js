$(function() {
  jQuery.validator.addMethod("isEmail", function(sEmail) {

      var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
      if (filter.test(sEmail)) {
        return true;
      }
      else {
        return false;
      }
  })

    //======================phoneStartingWithPlus

  jQuery.validator.addMethod("phoneStartingWithPlus", function(phone_number, element) {

      if ( phone_number.charAt(0) == "+" ) {
        return true
      }else{
        return false
      }
  });
//======================

    function isNumeric(value) {
        return /^-?\d+$/.test(value);
    }
  //  ==================================greaterThanToDay

  jQuery.validator.addMethod( "greaterThanToDay", function(value, element) {
      var currentDate = new Date();
      var selectedDate = new Date(value);

      return (currentDate.setDate(currentDate.getDate() - 1) > selectedDate);
  });

//  ==================================phoneStartingNumber
    jQuery.validator.addMethod("phoneStartingNumber", function(value, element) {
        let phoneNumber = value.substring(1);

        console.log("phoneNumber >>> ", phoneNumber)
        if ( isNumeric(phoneNumber) ) {
            return true
        }else{
            return false
        }

    });

//  ==================================greaterThan

    $.validator.addMethod("greaterThan", function(value, element, params) {
        var fromValue = parseFloat(value);
        var toValue = parseFloat($(params).val());
        return fromValue > toValue;
    }, "From date must be greater than To date.");
});