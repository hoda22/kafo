<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> {{__('app.login')}} </title>
    @include("auth.includs.head")
</head>

<body class="{{ app()->getLocale() == "en" ? "":"rtl"  }}">
<div class="main-wrapper">
    <div class="page-wrapper full-page">
        <div class="page-content d-flex align-items-center justify-content-center">
            <div class="row w-100 mx-0 auth-page">
                <div class="col-md-8 col-xl-6 mx-auto">
                    @include('partials.messages')
                    <div class="card">
                        <div class="row">
                            <div class="col-md-4 pr-md-0">
                                <div class="d-flex align-items-center justify-content-center login-logo text-center">
                                    <img src="{{ asset('assets/images/logo.png') }}" alt="{{__('app.title_site')}}" title="{{__('app.title_site')}}">
                                </div>
                            </div>
                            <div class="col-md-8 pl-md-0">
                                <div class="auth-form-wrapper px-4 py-5">
                                    <a href="#" class="noble-ui-logo d-block mb-2"> Kafo <span> </span></a>
                                    <h5 class="text-muted font-weight-normal mb-4">
                                        {{__('app.welcome_back')}}
                                    </h5>
                                    <form class="forms-sample" method="post" action="{{route('login')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">{{__('app.email')}} <span class="required_star">*</span> </label>
                                            <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                                                   placeholder="{{__('app.email')}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">{{__('app.password')}} <span class="required_star">*</span> </label>
                                            <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                                                   autocomplete="current-password" placeholder="{{__('app.password')}}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputPassword1">{{__('app.code')}} <span class="required_star">*</span> </label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="captcha" class="form-control" id="exampleInputPassword1"
                                                            placeholder="{{__('app.enter_code')}}">
                                                </div>
                                                <div class="col-md-6 captcha">
                                                    <span>{!! captcha_img() !!}</span>
                                                    <button type="button" class="btn btn-success" class="reload" id="reload"
                                                        style="margin-right: 5px ;"
                                                    >
                                                        &#x21bb;
                                                    </button>
                                                </div>


                                            </div>

                                        </div>

                                        <button type="submit" class="btn btn-primary mb-2 mb-md-0 text-white">
                                            {{__('app.enter')}}
                                        </button>
                                        <div class="reset_password_link">
                                            <a href="{{route("password.request")}}"> {{__('app.forget_password')}} </a>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include("auth.includs.foot")
</body>
</html>
