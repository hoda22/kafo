<link rel="stylesheet" href="{{ asset('assets/vendors/core/core.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/fonts/feather-font/css/iconfont.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/demo_1/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/demo_1/custome-style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/demo_1/custome-style-filter.css') }}">
<link rel="stylesheet" href="{{ asset('style.css') }}">
<link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}" />
<style type="text/css">
    .login-logo {
        background: #CCCCCC;
        width: 100%;
        height: 100%;
        align-items: center;
    }
</style>