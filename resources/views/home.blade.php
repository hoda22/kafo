@extends('dashboard.layouts.app', ['title' => __('app.welcome_dashboard')])
@section('content')

    @can('performance-panel-settings')
    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">
                <div class="col-md-3 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body position-relative">
                            <a href="{{route("cities.index")}}" class="link_abs"></a>
                            <div class="d-flex justify-content-between align-items-baseline">
                                <h5 class="card-title mb-0"> {{__('app.titles.cities')}} </h5>
                                <div class="dropdown mb-2">
                                    <i class="link-icon" data-feather="map-pin"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-12 col-xl-5">
                                    <h3 class="mb-2 stat-number">{{$citiesCount}}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-md-3 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <a href="{{route("services.index")}}" class="link_abs"></a>
                            <div class="d-flex justify-content-between align-items-baseline">
                                <h5 class="card-title mb-0"> {{__('app.titles.services')}} </h5>
                                <div class="dropdown mb-2">
                                    <i class="link-icon" data-feather="server"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-12 col-xl-5">
                                    <h3 class="mb-2 stat-number">{{$servicesCount}}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-3 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <a href="{{route("providers.index")}}" class="link_abs"></a>
                            <div class="d-flex justify-content-between align-items-baseline">
                                <h5 class="card-title mb-0"> {{__('app.titles.providers')}} </h5>
                                <div class="dropdown mb-2">
                                    <i class="link-icon" data-feather="user-check"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-12 col-xl-5">
                                    <h3 class="mb-2 stat-number">
                                        {{$providersCount}}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <a href="{{route("clients.index")}}" class="link_abs"></a>
                            <div class="d-flex justify-content-between align-items-baseline">
                                <h5 class="card-title mb-0"> {{__('app.titles.clients')}} </h5>
                                <div class="dropdown mb-2">
                                    <i class="link-icon" data-feather="user"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-12 col-xl-5">
                                    <h3 class="mb-2 stat-number">{{$clientsCount}}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4">
                    <div class="card">
                        <div class="card-body pb-1">
                            <div class="d-flex justify-content-between align-items-baseline mb-3">
                                <h4 class="card-title mb-2"> {{__('app.recently_registered_customers')}}  </h4>
                            </div>
                            <div class="d-flex flex-column">
                                @foreach( $clients as $client )
                                <a href="{{route("clients.edit", $client->id)}}" class="d-flex align-items-center border-bottom pb-2 pt-2">
                                    <div class="w-100">
                                        <div class="d-flex justify-content-between">
                                            <h6 class="text-body mb-2">{{$client->FullName}}</h6>
                                            <p class="text-muted tx-12">{{$client->created_at}}</p>
                                        </div>
                                        <p class="text-muted tx-13">
                                            <i class="link-icon feather-12" data-feather="map-pin"></i>
                                            {{$client->email}}
                                        </p>
                                    </div>
                                </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4">
                    <div class="card">
                        <div class="card-body pb-1">
                            <div class="d-flex justify-content-between align-items-baseline mb-2">
                                <h6 class="card-title mb-2"> {{__('app.recently_enrolled_technicians')}} </h6>
                            </div>
                            <div class="d-flex flex-column">
                                <div class="d-flex flex-column">
                                    @foreach( $providers as $provider )
                                        <a href="{{route("providers.edit", $provider->id)}}" class="d-flex align-items-center border-bottom pb-2 pt-2">
                                            <div class="w-100">
                                                <div class="d-flex justify-content-between">
                                                    <h6 class="text-body mb-2">{{$provider->FullName}}</h6>
                                                    <p class="text-muted tx-12">{{$provider->created_at}}</p>
                                                </div>
                                                <p class="text-muted tx-13">
                                                    <i class="link-icon feather-12" data-feather="map-pin"></i>
                                                    {{$provider->email}}
                                                </p>
                                            </div>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body pb-1">
                            <h6 class="card-title mb-3"> {{__('app.titles.orders')}} </h6>
                            <div id="apexPie"></div>
                        </div>
                    </div>
                </div>


                <div class="col-xl-12 stretch-card mt-3">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title"> {{__('app.providers&clients')}}  </h6>
                            <div id="apexMixed"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
        <div class="text-center mt-5">
            <img class="no-permissions" src="{{ asset('assets/images/ass.png') }}"  />
        </div>
    @endcan
@endsection

<style>
    #apexMixed > div  svg{
        height: 430px;
    }
    .no-permissions{
        max-width: 550px;
    }
</style>
@push('scripts')
    <script src="{{ asset('assets/vendors/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/morris.js/morris.min.js') }}"></script>
    <script src="{{ asset('assets/js/morris.js') }}"></script>



    {{--<script src="{{asset("assets/js/apexcharts.js")}}"></script>--}}

    <script>

        // Apex Pie chart end
        var options = {
            chart: {
                height: 300,
                type: "pie"
            },
            colors: ["#f77eb9", "#7ee5e5","#fbbc06"],
            legend: {
                position: 'top',
                horizontalAlign: 'center'
            },
            labels:["{{__('app.finished_orders')}}" ,"{{__('app.requests_are_progress')}}" ,"{{__('app.canceled_orders')}}"] ,
            stroke: {
                colors: ['rgba(0,0,0,0)']
            },
            dataLabels: {
                enabled: false
            },
            series: [{!! $orderWaiting !!}, {!! $orderCurrent !!}, {!! $orderDone !!}]
        };

        var chart = new ApexCharts(document.querySelector("#apexPie"), options);

        chart.render();
        //$orderWaiting



        // Apex Mixed chart start
        var options = {
            chart: {
                height: 400,
                type: 'line',
                stacked: false,
                parentHeightOffset: 0
            },
            grid: {
                borderColor: "rgba(77, 138, 240, .1)",
                padding: {
                    bottom: -15
                }
            },
            stroke: {
                width: [0, 2, 5],
                curve: 'smooth'
            },
            plotOptions: {
                bar: {
                    columnWidth: '50%'
                }
            },
            series: [{
                name: '{{__('app.titles.providers')}}',
                type: 'area',
                data:   {!! $statisticsClient !!}
            }, {
                name: '{{__('app.titles.clients')}}',
                type: 'area',
                data:  {!! $statisticsProvider !!}
            }],
            legend: {
                position: 'top',
                horizontalAlign: 'left'
            },
            fill: {
                opacity: [0.85,0.25,1],
                gradient: {
                    inverseColors: false,
                    shade: 'light',
                    type: "vertical",
                    opacityFrom: 0.85,
                    opacityTo: 0.55,
                    stops: [0, 100, 100, 100]
                }
            },
            labels: ['01/01/2023', '02/01/2023','03/01/2023','04/01/2023','05/01/2023','06/01/2023','07/01/2023','08/01/2023','09/01/2023','10/01/2023','11/01/2023','11/01/2023'],
            markers: {
                size: 0
            },
            xaxis: {
                type:'datetime'
            },
            yaxis: {
                title: {
                    text: 'Points',
                },
            },
            tooltip: {
                shared: true,
                intersect: false,
                y: [{
                    formatter: function (y) {
                        if(typeof y !== "undefined") {
                            return  y.toFixed(0) + " points";
                        }
                        return y;
                    }
                }, {
                    formatter: function (y) {
                        if(typeof y !== "undefined") {
                            return  y.toFixed(2) + " $";
                        }
                        return y;
                    }
                }]
            }
        }
        var chart = new ApexCharts(
            document.querySelector("#apexMixed"),
            options
        );
        chart.render();
        // Apex Mixed chart end

        //

        $('.stat-number').each(function () {
            var size = $(this).text().split(".")[1] ? $(this).text().split(".")[1].length : 0;
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 1000,
                step: function (func) {
                    $(this).text(parseFloat(func).toFixed(size));
                }
            });
        });
    </script>

    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js"></script>

    <!-- TODO: Add SDKs for Firebase products that you want to use
        https://firebase.google.com/docs/web/setup#available-libraries -->
    <script>

        // Your web app's Firebase configuration
        const firebaseConfig = {
            apiKey: "AIzaSyDeSXrYzSm5V-Eqezrw3s-LzMzqHo75fOw",
            authDomain: "chatsi-realtime-laravel-e5469.firebaseapp.com",
            projectId: "chatsi-realtime-laravel-e5469",
            storageBucket: "chatsi-realtime-laravel-e5469.appspot.com",
            messagingSenderId: "469331204362",
            appId: "1:469331204362:web:13726161d6bfd6e872c2f3"
        };
        firebase.initializeApp(firebaseConfig);
        // Initialize Firebase
        const messaging = firebase.messaging();
        // const db = firebase.database();
        // console.log("db",db)
        console.log("firebase >>",firebase);


        function initFirebaseMessagingRegistration() {
            console.log("initFirebaseMessagingRegistration")

            messaging.requestPermission().then(function () {

                console.log("messaging.getToken()",messaging.getToken())
                return messaging.getToken()
            }).then(function(token) {
                alert(2222)
                const user_id = '{{auth()->user()->id}}';
                console.log("user_id" , user_id);
                // axios.post('/api/save-token', {
                //     fcm_token, user_id
                // })
                // .then(res => {
                //     console.log(res);
                // });


            }).catch(function (err) {
                console.log(`Token Error :: ${err}`);
            });
        }

        // alert(12212) ;
        initFirebaseMessagingRegistration();

        messaging.onMessage(function({data:{body,title}}){
            new Notification(title, {body});
        });




        // Retrieve Firebase Messaging object.
        messaging.usePublicVapidKey("1:469331204362:web:13726161d6bfd6e872c2f3");

        function sendTokenToServer(fcm_token) {
            const user_id = '{{auth()->user()->id}}';
            //console.log($user_id);
            axios.post('/api/save-token', {
                fcm_token, user_id
            })
                .then(res => {
                    console.log(res);
                })

        }

        function retreiveToken(){
            messaging.getToken().then((currentToken) => {
                if (currentToken) {
                    sendTokenToServer(currentToken);
                    // updateUIForPushEnabled(currentToken);
                } else {
                    // Show permission request.
                    //console.log('No Instance ID token available. Request permission to generate one.');
                    // Show permission UI.
                    //updateUIForPushPermissionRequired();
                    //etTokenSentToServer(false);
                    alert('You should allow notification!');
                }
            }).catch((err) => {
                console.log(err.message);
                // showToken('Error retrieving Instance ID token. ', err);
                // setTokenSentToServer(false);
            });
        }
        retreiveToken();
        messaging.onTokenRefresh(()=>{
            retreiveToken();


        });

        messaging.onMessage((payload)=>{
            console.log('Message received');
            console.log(payload);

            location.reload();
        });










        //===============


        var database = firebase.database();
        var chatRef = database.ref('chat_messages/conversation_id');

        // Sending a message
        function sendMessage(message) {
            chatRef.push({
                sender_id: 'user_id',
                message: message,
                timestamp: firebase.database.ServerValue.TIMESTAMP
            });
        }

        // Receiving messages in real-time
        chatRef.on('child_added', function(snapshot) {
            var message = snapshot.val();
            // Handle the received message
        });

    </script>
@endpush
