<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{__('app.title_site')}}  | {{ $title ?? '' }}</title>
    <link rel="stylesheet" href="{{ asset('assets/vendors/core/core.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/feather-font/css/iconfont.css') }}">
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/demo_1/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/demo_1/custome-style.css') }}">
    <style>
        .card-body table{
            margin-bottom: 20px;
        }

        .rtl .main-wrapper .page-wrapper {
            margin-right: 0;
        }

        .main-wrapper .page-wrapper {
            width: 100% ;
        }

        @media (max-width: 767px) {
            .submit{
                margin-top: 15px;
            }
        }

        .offset-md-4 {
            margin-right: 33.33333%;
        }

        .reset_password_link {
            float: left;
        }
    </style>
    @stack('styles')
    <link rel="shortcut icon" href="{{ asset('/public/assets/images/favicon.ico') }}"/>
</head>
<body class="rtl sidebar-dark">
<div class="main-wrapper">



    <div class="page-wrapper">

        <div class="page-content">

            @include('flash::message')
            @include('partials.messages')
            @yield('content')
        </div>

    </div>
</div>
<script src="{{ asset('assets/vendors/core/core.js') }}"></script>
<script src="{{ asset('assets/vendors/chartjs/Chart.min.js') }}"></script>
<script src="{{ asset('assets/vendors/jquery.flot/jquery.flot.js') }}"></script>
<script src="{{ asset('assets/vendors/jquery.flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/vendors/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('assets/vendors/progressbar.js/progressbar.min.js') }}"></script>
<script src="{{ asset('assets/vendors/feather-icons/feather.min.js') }}"></script>
<script src="{{ asset('assets/js/template.js') }}"></script>
<script src="{{ asset('assets/js/dashboard.js') }}"></script>
<script src="{{ asset('assets/js/datepicker.js') }}"></script>

@stack('scripts')
</body>
</html>
