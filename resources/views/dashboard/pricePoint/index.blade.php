@extends('dashboard.layouts.app', ['title' => __('app.titles.pricePoint')])
@section('content')
    @push('button')
        @can("loyalty-points-create")
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                {{__('app.points.add')}}
            </button>
        </div>
        @endcan
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.pricesPoints.from')}} </th>
                                <th class="pt-0"> {{__('app.pricesPoints.to')}} </th>
                                <th class="pt-0"> {{__('app.pricesPoints.points_num')}} </th>
                                <th class="pt-0"> {{__('app.processes')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $items as $item )
                                <tr>
                                    <td> {{$loop->iteration }} </td>
                                    <td> {{$item->from }} {{__('app.SAR')}} </td>
                                    <td> {{$item->to }} {{__('app.SAR')}}</td>
                                    <td> {{$item->points_num }} </td>

                                    <td>
                                        @can("loyalty-points-edit")
                                        <a href="#" data-id="{{$item->id}}"
                                           data-from="{{ $item->from }}"
                                           data-to="{{ $item->to }}"
                                           data-num_point="{{ $item->points_num }}"
                                           data-ordering="{{ $item->ordering }}"
                                           class="btn btn-primary btn-icon aligner edit">
                                            <i data-feather="edit"></i>
                                        </a>
                                        @endcan

                                        @can("loyalty-points-delete")
                                            <button type="submit" class="btn btn-danger btn-icon aligner"
                                                    onclick="showSwal('passing-parameter-execute-cancel' , '{{route("prices-points.destroy", $item->id)}}' )" >
                                                <i data-feather="trash"></i>
                                            </button>
                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("styles")

@endpush
@push('scripts')

    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                to = el.data('to'),
                from = el.data('from'),
                points_num = el.data('num_point'),

                active = el.data('active'),
                ordering = el.data('ordering'),
                route = "{{ route('prices-points.update', ':id') }}";
            route = route.replace(':id', id);


            $(document).find("#edit_to").val(to);
            $(document).find("#edit_from").val(from);
            $(document).find("#edit_points_num").val(points_num);

            if ( active == 'active' ){
                $(document).find("#edit_active").prop('checked',true);
            }else{
                $(document).find("#edit_active").prop('checked',false);
            }

            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);

        });

    </script>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel"> {{__('app.points.add')}}  </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route("prices-points.store")}}" id="add-form">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add-from" class="col-form-label">{{__('app.pricesPoints.from')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="from" id="add-from">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_to" class="col-form-label">{{__('app.pricesPoints.to')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="to" id="add_to">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_points_num" class="col-form-label">{{__('app.pricesPoints.points_num')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="points_num" id="add_points_num">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel"> {{__('app.points.edit')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="edit-form" action="">
                        @method('put')
                        @csrf

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_from" class="col-form-label"> {{__('app.pricesPoints.from')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="from" id="edit_from">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_num_rus" class="col-form-label">{{__('app.pricesPoints.to')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="to" id="edit_to">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_points_num" class="col-form-label">{{__('app.pricesPoints.points_num')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="points_num" id="edit_points_num">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active" id="edit_active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('dashboard.pricePoint.script')


@endpush
