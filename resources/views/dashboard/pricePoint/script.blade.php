<script>

    $.validator.addMethod("greaterThanOtherInput", function(value, element, params) {
        var otherInputValue = $(params).val();
        return parseFloat(value) > parseFloat(otherInputValue);
    }, "Please enter a value greater than the other input");



    $("#add-form").validate({
        rules: {
            from :{
                required: true,
            },
            to :{
                required: true,
                greaterThanOtherInput: "#add-from"
            },
            points_num :{
                required: true,
            },

            action: "required"
        },
        messages: {
            from: {
                required: "{{__('app.validation.field_from_required')}}",
            },
            to: {
                required: "{{__('app.validation.field_to_required')}}" ,
                greaterThanOtherInput : "{{__('app.validation.field_greater_than_required')}}"
            },
            points_num: {
                required: "{{__('app.validation.field_points_num_required')}}"
            },


            action: "Please provide some data"
        }
    });


    $("#edit-form").validate({
        rules: {
            from :{
                required: true,

            },
            to :{
                required: true,
                greaterThanOtherInput: "#edit_from"
            },
            points_num :{
                required: true,
            },
            action: "required"
        },
        messages: {
            from: {
                required: "{{__('app.validation.field_from_required')}}",
            },
            to: {
                required: "{{__('app.validation.field_to_required')}}" ,
                greaterThanOtherInput : "{{__('app.validation.field_greater_than_required')}}"
            },
            points_num: {
                required: "{{__('app.validation.field_points_num_required')}}"
            },
            action: "Please provide some data"
        }
    });
</script>