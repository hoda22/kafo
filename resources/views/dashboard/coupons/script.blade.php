<script>
    $("#add-form").validate({
        rules: {
            code: {
                required: true,
                maxlength:4,
            },
            number :{
                required: true,
            },
            expire_date :{
                required: true,
            },

            action: "required"
        },
        messages: {
            code: {
                required: "{{__('app.validation.field_code_required')}}",
                maxlength: "{{__('app.validation.field_code_maxlength')}}",
            },
            number: {
                required: "{{__('app.validation.field_number_required')}}"
            },

            expire_date: {
                required: "{{__('app.validation.field_expire_date_required')}}"
            },

            action: "Please provide some data"
        }
    });


    $("#edit-form").validate({
        rules: {
            code: {
                required: true,
                maxlength:4,
            },
            number :{
                required: true,
            },
            expire_date :{
                required: true,
            },
            action: "required"
        },
        messages: {
            code: {
                required: "{{__('app.validation.field_code_required')}}",
                maxlength: "{{__('app.validation.field_code_maxlength')}}",
            },
            number: {
                required: "{{__('app.validation.field_number_required')}}"
            },

            expire_date: {
                required: "{{__('app.validation.field_expire_date_required')}}"
            },
            action: "Please provide some data"
        }
    });
</script>