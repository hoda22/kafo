@extends('dashboard.layouts.app', ['title' => __('app.titles.coupons')])
@section('content')
    @push('button')
        @can("coupons-create")
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                {{__('app.coupons.add')}}
            </button>
        </div>
        @endcan
    @endpush
    <div class="row">
        {{--<div class="col-lg-12 col-xl-12 stretch-card">--}}
            {{--<div class="card mb-3">--}}
                {{--<div class="card-body filter">--}}
                    {{--<form class="filter_form">--}}
                        {{--<div class="form-group row">--}}
                            {{--<label for="place" class="col-sm-2 col-form-label"> {{__('app.city_name')}} </label>--}}
                            {{--<div class="col-sm-3">--}}
                                {{--<input type="text" class="form-control" name="name" value="{{ request('name') }}"/>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-3">--}}
                                {{--<button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>--}}
                                {{--<button type="button" class="btn clear_search btn-danger btn-icon aligner">--}}
                                    {{--<i class="link-icon" data-feather="refresh-cw"></i>--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.coupons.code')}} </th>
                                <th class="pt-0"> {{__('app.coupons.expire_date')}} </th>
                                <th class="pt-0"> {{__('app.coupons.type')}} </th>
                                <th class="pt-0"> {{__('app.coupons.discount')}} </th>
                                <th class="pt-0"> {{__('app.status')}} </th>

                                <th class="pt-0"> {{__('app.processes')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td> {{$item->code}} </td>
                                    <td> {{$item->expire_date }} </td>

                                    <td>
                                        @if( $item->type == "percent" )
                                            {{__('app.coupons.percent')}}
                                        @else
                                            {{__('app.coupons.amount')}}
                                        @endif
                                    </td>
                                    <td> {{ $item->number }} </td>
                                    <td>
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="active" value="active" class="change_status"
                                                    data-route="{{route("coupon.updateAxios" , $item->id )}}"
                                                    data-id="{{$item->id}}"
                                                   @if($item->active == 'active')
                                                       checked
                                                   @endif
                                                >
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        @can("coupons-edit")
                                        <a href="#" data-id="{{$item->id}}"
                                           data-code="{{ $item->code }}"
                                           data-expire_date="{{ $item->expire_date }}"
                                           data-type="{{ $item->type }}"
                                           data-clients_id="{{ json_encode($item->clients) }}"
                                           data-number="{{ $item->number }}"
                                           data-active="{{ $item->active }}"
                                           class="btn btn-primary btn-icon aligner edit">
                                            <i data-feather="edit"></i>
                                        </a>
                                        @endcan

                                        @can("coupons-delete")
                                            <button type="submit" class="btn btn-danger btn-icon aligner"
                                                    onclick="showSwal('passing-parameter-execute-cancel' , '{{route("coupons.destroy", $item->id)}}' )" >
                                                <i data-feather="trash"></i>
                                            </button>
                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("styles")

@endpush
@push('scripts')

    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                code = el.data('code'),
                expire_date = el.data('expire_date'),
                number = el.data('number'),
                type = el.data('type'),
                active = el.data('active'),
                clients_id = el.data('clients_id'),

                route = "{{ route('coupons.update', ':id') }}";
            route = route.replace(':id', id);
            $(document).find("#edit_code").val(code);
            $(document).find("#edit_expire_date").val(expire_date);
            $(document).find("#edit_type").val(type);
            $(document).find("#edit_number").val(number);

            $(document).find("#edit_number").val(number);

            if ( active == 'active' ){
                $(document).find("#edit_active").prop('checked',true);
            }else{
                $(document).find("#edit_active").prop('checked',false);
            }

            if ( type == 'percent' ){
                $(document).find("#edit_percent").prop('checked',true);
                $("#coupons_number_edit").text("{{__('app.coupons.percent')}}");
            }else{
                $(document).find("#edit_amount").prop('checked',true);
                $("#coupons_number_edit").text("{{__('app.coupons.amount')}}");
            }


            $(document).find("#editModal").modal('show');

            $(document).find('#edit_clients_items').val(clients_id)
            $(document).find("#editModal form").attr('action', route);


            $('#edit_clients_items').select2({
                dropdownParent: $('#editModal')
            });
        });

    </script>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel"> {{__('app.coupons.add')}}  </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route("coupons.store")}}" id="add-form">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="code" class="col-form-label">{{__('app.coupons.code')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="code" id="code">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_start_date" class="col-form-label">{{__('app.coupons.start_date')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control date" name="expire_date"  id="add_start_date">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_expire_date" class="col-form-label">{{__('app.coupons.expire_date')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control date" name="expire_date"  id="add_expire_date">
                            </div>
                        </div>




                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_type" class="col-form-label">{{__('app.coupons.type')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" checked class="form-check-input coupon_type" data-type="amount" value="amount" name="type"> {{__('app.coupons.amount')}}
                                    </label>
                                </div>

                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input coupon_type" data-type="percent" value="percent" name="type"> {{__('app.coupons.percent')}}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="number" class="col-form-label"><span id="coupons_number">{{__('app.coupons.amount')}}</span> <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="number" id="number">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="number" class="col-form-label"> {{__('app.clients_')}}   :</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control select2" name="clients_id[]" id="clients_items" multiple="multiple">
                                    <option value="0"> -- {{__('app.all')}} -- </option>
                                    @foreach( $clients as $client )
                                    <option value="{{$client->id}}"> {{$client->FULL_NAME}}  </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="active">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active" checked>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel"> {{__('app.coupons.edit')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="edit-form" action="">
                        @method('put')
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_code" class="col-form-label">{{__('app.coupons.code')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="code" id="edit_code">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_start_date" class="col-form-label">{{__('app.coupons.start_date')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control date" name="expire_date"  id="edit_start_date">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_expire_date" class="col-form-label">{{__('app.coupons.expire_date')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="expire_date" id="edit_expire_date">
                            </div>
                        </div>




                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_type" class="col-form-label">{{__('app.coupons.type')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input coupon_type_edit" id="edit_amount" data-type="amount" value="amount" name="type"> {{__('app.coupons.amount')}}
                                    </label>
                                </div>

                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input coupon_type_edit" id="edit_percent" data-type="percent" value="percent" name="type"> {{__('app.coupons.percent')}}
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="number" class="col-form-label"><span id="coupons_number_edit">{{__('app.coupons.amount')}}</span> <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="number" id="edit_number">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active" id="edit_active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_clients_items" class="col-form-label"> {{__('app.clients_')}}   :</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control select2" name="clients_id[]" id="edit_clients_items" multiple="multiple">
                                    <option value=""> -- {{__('app.choose')}} -- </option>
                                    @foreach( $clients as $client )
                                        <option value="{{$client->id}}"> {{$client->FULL_NAME}}  </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('dashboard.coupons.script')


    <script>
        $(".coupon_type").click(function (e) {
            let type = $(this).data("type");

            if ( type == "amount" ) {
                $("#coupons_number").text("{{__('app.coupons.amount')}}");
            }else{
                $("#coupons_number").text("{{__('app.coupons.percent')}}");
            }
        })

        $(".coupon_type_edit").click(function (e) {
            let type = $(this).data("type");

            if ( type == "amount" ) {
                $("#coupons_number_edit").text("{{__('app.coupons.amount')}}");
            }else{
                $("#coupons_number_edit").text("{{__('app.coupons.percent')}}");
            }
        })



        $('#clients_items').select2({
            dropdownParent: $('#createModal')
        });


        $('#edit_clients_items').select2({
            dropdownParent: $('#editModal')
        });



        $("#createModal").on("hidden.bs.modal", function () {
            $(".form-control").val("");
            $("#clients_items").val("").trigger("change");
        });


    </script>
@endpush
