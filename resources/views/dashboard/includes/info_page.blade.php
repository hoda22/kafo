<div class="card-header">
    <div class="row">
        <div class="col-md-6">
            <div class="info_page">
                {{__('app.number_elements')}} :
                <span> {{ $items->count() }} </span>
                {{--|--}}
                {{--{{__('app.number_elements_page')}} :--}}
                {{--<span> {{ $items->count() }} </span>--}}
            </div>
        </div>
        <div class="col-md-6">
            @if( isset($excelRoute) )
            <div class="text-end text-left">
                <a href="{{route($excelRoute,$filter)}}" class="btn btn-outline-success"> {{__('app.export_data')}} </a>
            </div>
            @endif
        </div>
    </div>
</div>