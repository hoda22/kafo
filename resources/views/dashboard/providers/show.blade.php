@extends('dashboard.layouts.app', ['title' => __('app.providers.show')])
@section('content')
    @push('button')
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <a href="{{route('providers.index')}}" class="btn btn-primary">
                <i data-feather="skip-back"></i>
            </a>
        </div>
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 ">
            <div class="card">
                <div class="card-body resize_section">
                    <div class="row">
                        <div class="col-md-12 text-center " style="margin-bottom: 20px">
                            <img src="{{asset($item->avatar)}}" class="avatar" />
                        </div>
                        <div class="col-md-2 mb-3">
                            {{__('app.name')}} :
                        </div>
                        <div class="col-md-4 mb-3">
                            {{$item->full_name}}
                        </div>

                        <div class="col-md-2 mb-3">
                            {{__('app.status')}} :
                        </div>
                        <div class="col-md-4 mb-2">
                            {{$item->status}}

                            <label class="switch">
                                <input type="checkbox" name="block_status" value="block" class="change_block_status"
                                       data-route="{{route("provider.updateAxios" , $item->id )}}"
                                       data-id="{{$item->id}}"
                                       data-city-id="{{$item->id}}"
                                       @if($item->block_status == 'block')
                                       checked
                                        @endif
                                >
                                <span class="slider round"></span>
                            </label>
                        </div>

                        <div class="col-md-2 mb-3">
                            {{__('app.email')}} :
                        </div>
                        <div class="col-md-4 mb-3">
                            {{$item->email}}
                        </div>

                        <div class="col-md-2 mb-3">
                            {{__('app.phone')}} :
                        </div>
                        <div class="col-md-4 mb-3 phone">
                            {{$item->phone}}
                        </div>
                        <div class="col-md-2 mb-3">
                            {{__('app.date_joining')}} :
                        </div>
                        <div class="col-md-4 mb-3">
                            {{$item->created_at}}
                        </div>

                        <div class="col-md-2 mb-3">
                            {{__('app.city_work')}} :
                        </div>
                        <div class="col-md-4 mb-3">
                            {{optional( $item->city )->name}}
                        </div>

                        {{--<div class="col-md-2 mb-3">--}}
                            {{--{{__('app.balance')}} :--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 mb-3">--}}
                            {{--{{$item->balance}}--}}
                        {{--</div>--}}

                        {{--<div class="col-md-2 mb-3">--}}
                            {{--{{__('app.block_statue')}} :--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 mb-3">--}}
                            {{--@if( $item->block_status == "block" )--}}
                                {{--{{__('app.block')}}--}}
                            {{--@else--}}
                                {{--{{__('app.unblock')}}--}}
                            {{--@endif--}}
                        {{--</div>--}}


                        {{--@if( $item->block_status == "block" )--}}
                        {{--<div class="col-md-2 mb-3">--}}
                            {{--{{__('app.reason')}} :--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 mb-3">--}}
                            {{--{{$item->block_reason}}--}}
                        {{--</div>--}}
                        {{--@endif--}}


                        {{--<div class="col-md-2 mb-3">--}}
                            {{--{{__('app.block_statue')}} :--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 mb-3">--}}
                            {{--{{$item->block_status}}--}}

                        {{--</div>--}}
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@push("styles")
    <style>
            /*.resize_section .col-md-2 {*/
                /*max-width: 11%;!**!*/
            /*}*/

            /*.resize_section .col-md-4 {*/
                /*max-width: 46%;*/
            /*}*/
    </style>
@endpush
@push("scripts")
<script type="text/javascript">

    $(".change_block_status").change(function () {
        let statusAdmin = 'unblock' ,
            id = $(this).data('id') ,
            route = $(this).data('route') ;
        let msg = "";

        if ( !$(this).is(":checked") ) {
            statusAdmin = 'unblock'
            id = $(this).data('id');
            $("#route_id").val(route)
            $("#client_id").val(id)
            msg = "{{__("app.active_successfully")}}"

        }else{
            // alert(1212121) ;
            statusAdmin = 'block'


            msg = "{{__("app.unactive_successfully")}}"

        }

        axios.post(route, {
            block_status : statusAdmin
        }).then((response) => {
            if (response.data.status == true) {
                $("#messages_").empty().append("<p class='alert alert-success'> "+ msg + " </p>")
            }
        })
    });

    </script>
@endpush