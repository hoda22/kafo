@extends('dashboard.layouts.app', ['title' => __('app.titles.providers')])
@section('content')
    @push('button')
        @can("providers-create")
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <a href="{{route('providers.create')}}" class="btn btn-primary">
                {{__('app.providers.add')}}
            </a>
        </div>
        @endcan
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3 ">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.name')}} </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="name" value="{{ request('name') }}"/>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items ])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.name')}} </th>
                                <th class="pt-0"> {{__('app.email')}} </th>
                                <th class="pt-0"> {{__('app.phone')}} </th>
                                <th class="pt-0"> {{__('app.status')}} </th>

                                @can("provider-approve-reject")
                                <th class="pt-0"> {{__('app.status_active')}} </th>
                                @endcan
                                <th class="pt-0"> {{__('app.processes')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $items as $item )
                                <tr>
                                    <td> {{ $loop->iteration }}</td>
                                    <td> {{ $item->full_name }} </td>
                                    <td> {{ $item->email }} </td>
                                    <td class="td-phone"> <span class="text-right" style="direction: ltr">{{ $item->phone }}</span> </td>


                                    <td>
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="active" value="active" class="change_status"
                                                       data-route="{{route("provider.updateAxios" , $item->id )}}"
                                                       data-id="{{$item->id}}"
                                                       @if($item->active == 'active')
                                                       checked
                                                        @endif
                                                >
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </td>
                                    @can("provider-approve-reject")
                                    <td>
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="block_status" value="block" class="change_block_status"
                                                       data-route="{{route("provider.updateCityAxios" , $item->id )}}"
                                                       data-id="{{$item->id}}"
                                                       data-city-id="{{$item->id}}"
                                                       @if($item->block_status == 'block')
                                                       checked
                                                        @endif
                                                >
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </td>
                                    @endcan

                                    <td>

                                        @can("providers-show")
                                            <a href="{{route("providers.show", $item->id)}}"
                                               class="btn btn-success btn-icon aligner ">
                                                <i data-feather="eye"></i>
                                            </a>
                                        @endcan



                                        @can("providers-edit")
                                        <a href="{{route("providers.edit", $item->id)}}"
                                           class="btn btn-primary btn-icon aligner ">
                                            <i data-feather="edit"></i>
                                        </a>
                                        @endcan

                                        @can("providers-delete")

                                            <button type="submit" class="btn btn-danger btn-icon aligner"
                                                    onclick="showSwal('passing-parameter-execute-cancel' , '{{route("providers.destroy", $item->id)}}' )" >
                                                <i data-feather="trash"></i>
                                            </button>

                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <div class="modal fade" id="createModal" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createSubServiceModalLabel">{{__('app.select_work_city')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add-block" >
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name" class="col-form-label">{{__('app.city')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control select2" name="city_id" id="cities">
                                    <option> -- {{__("app.choose")}} -- </option>
                                    @foreach( $cities as $city )
                                        <option value="{{$city->id}}"> {{$city->name}} </option>
                                    @endforeach
                                </select>

                                <p class="city_error error"></p>

                                <input type="hidden" id="route_id">
                                <input type="hidden" id="client_id">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" id="btn-add-city" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    @include('dashboard.providers.script')

    <script type="text/javascript">

        $(".change_block_status").change(function () {
            $("#cities").val(null).trigger("change");

            let statusAdmin = 'unblock' ,
                id = $(this).data('id') ,
                route = $(this).data('route') ;

            let msg = "";

            if ( !$(this).is(":checked") ) {
                statusAdmin = 'unblock'

                id = $(this).data('id') ;

                $(this).prop('checked',true)

                $("#createModal").modal("show") ;
                $("#route_id").val(route)
                $("#client_id").val(id)

                 return false

            }else{
                // alert(1212121) ;
                statusAdmin = 'block'
                msg = "{{__("app.block_successfully_agent_deactivated")}}"

            }

            // alert( statusAdmin ) ;
            //
            // return false

            axios.post(route, {
                block_status : statusAdmin
            }).then((response) => {
                if (response.data.status == true) {
                    $("#messages_").empty().append("<p class='alert alert-success'> "+ msg + " </p>")
                }
                $("#createModal").modal("hide") ;
            })
        });


        $('#cities').select2({
            dropdownParent: $('#createModal')
        });



        $(document).on("submit" , "#add-block" ,function(e){
            e.preventDefault()
            $(".city_error").empty() ;

            let route = $("#route_id").val(),
                city_id = $("#cities").val(),
                client_id =  $("#client_id").val()

            if ( !city_id ) {
                $(".city_error").append("{{__('app.validation.field_city_id_required')}}")
                return false
            }

            axios.post(route, {
                city_id: city_id ,
                block_status:"unblock"
            }).then((response) => {
                if (response.data.status == true) {

                    $("#messages_").empty().append("<p class='alert alert-success'> {{__("app.select_work_city_successfully")}} </p>")

                    $(`[data-city-id="${client_id}"]`).prop('checked',false);
                    setTimeout(function() {
                        $("#messages_").empty()
                    }, 2000);
                }

                $("#createModal").modal("hide") ;
            })
        })

    </script>



@endpush
