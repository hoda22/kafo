@extends('dashboard.layouts.app', ['title' => __('app.providers.add')])
@section('content')
    @push('button')
        <div class="d-flex align-items-center flex-wrap text-nowrap btn-back">
            <a href="{{route('providers.index')}}" class="btn btn-primary">
                <i data-feather="skip-back"></i>
            </a>
        </div>
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <form method="post" id="add-form" action="{{route("providers.store")}}" enctype="multipart/form-data">
                        @csrf

                        <div class="col-md-12">
                            <div class="text-center">
                                <div id="imagePreview" src="{{asset('avater.png')}}" alt="placeholder image goes here"></div>
                                <input id="uploadFile" type="file" name="avatar" class="img" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_name" class="col-form-label"> {{__('app.first_name')}} <span class="required_star">*</span> : </label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{old('first_name')}}" name="first_name"  id="add_name">
                            </div>

                            <div class="col-lg-2">
                                <label for="add_name" class="col-form-label">{{__('app.last_name')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{old('last_name')}}" name="last_name"  id="last_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_phone" class="col-form-label">{{__('app.phone')}}  <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control phone" value="{{old('phone')}}" name="phone" id="add_phone">
                            </div>


                            <div class="col-lg-2">
                                <label for="add_email" class="col-form-label">  {{__('app.email')}}  <span class="required_star">*</span>  :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="email" class="form-control" value="{{old('email')}}" name="email"  id="add_email">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active">
                                    <span class="slider round"></span>
                                </label>
                            </div>


                            <div class="col-lg-2">
                                <label for="add_jop" class="col-form-label">  {{__('app.jop')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{old('jop')}}" name="jop"  id="add_jop">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_active" class="col-form-label">  {{__('app.nationality')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{old('nationality')}}" name="nationality"  id="add_nationality">
                            </div>

                            <div class="col-lg-2">
                                <label for="add_residency_number" class="col-form-label"> {{__('app.residency_number')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{old('residency_number')}}" name="residency_number"  id="add_residency_number">
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_active" class="col-form-label">  {{__('app.years_experience')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{old('years_experience')}}" name="years_experience" id="years_experience">
                            </div>

                            <div class="col-lg-2">
                                <label for="date_birth" class="col-form-label">  {{__('app.date_birth')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control date" value="{{old('date_birth')}}" name="date_birth" id="date_birth">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="password" class="col-form-label">  {{__('app.password')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="password" class="form-control"  name="password" id="password">
                            </div>

                            <div class="col-lg-2">
                                <label for="re_password" class="col-form-label">  {{__('app.re_password')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="password" class="form-control" name="password_confirmation" id="re_password">
                            </div>
                        </div>







                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script type="text/javascript">
        $(function() {
            $("#uploadFile").on("change", function()
            {
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

                if (/^image/.test( files[0].type)){ // only image file
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file

                    reader.onloadend = function(){ // set image data as background of div
                        $("#imagePreview").css("background-image", "url("+this.result+")");
                    }
                }
            });
        });

        $('#imagePreview').click(function(){
            $('#uploadFile').click();
        });
    </script>

    @include('dashboard.providers.script')
@endpush