
<script>


    // jQuery.validator.addMethod("phoneStartingWithPlus", function(phone_number, element) {
    //
    //     if ( phone_number.charAt(0) == "+" ) {
    //         return true
    //     }else{
    //         return false
    //     }
    // });
    //
    //
    // function isNumeric(value) {
    //     return /^-?\d+$/.test(value);
    // }
    //
    //
    // jQuery.validator.addMethod("phoneStartingNumber", function(value, element) {
    //     let phoneNumber = value.substring(1);
    //
    //     console.log("phoneNumber >>> ", phoneNumber)
    //     if ( isNumeric(phoneNumber) ) {
    //         return true
    //     }else{
    //         return false
    //     }
    //add-block
    // } );


    $("#add-form").validate({
        rules: {
            first_name: {
                required: true,
            },
            last_name :{
                required: true,
            },

            jop :{
                required: true,
            },

            nationality:{
                required: true,
            },

            years_experience :{
                required: true,
                maxlength:3,
                number:true
            },

            date_birth :{
                required: true,
                greaterThanToDay:true
            },

            phone :{
                phoneStartingWithPlus:true,
                phoneStartingNumber:true,
                required: true,
                minlength: 10,
                maxlength: 14
            },

            residency_number:{
                required: true,
                number:true,
                maxlength: 10,
            },

            password :{
                required: true,
                minlength: 8
            },

            password_confirmation :{
                required: true,
                equalTo: "#password"
            },

            email :{
                required: true,
                email: true,
            },

            role_id :{
                required: true,
            },



            action: "required"
        },
        messages: {
            first_name: {
                required: "{{__('app.validation.field_first_name_required')}}"
            },
            last_name: {
                required: "{{__('app.validation.field_last_name_required')}}"
            },

            phone: {
                required: "{{__('app.validation.field_phone_required')}}",
                minlength: "{{__('app.validation.field_phone_minlength')}}",
                maxlength: "{{__('app.validation.field_phone_maxlength')}}",
                phoneStartingNumber:"{{__('app.validation.phoneNumberIsNumber')}}",
                phoneStartingWithPlus:"{{__('app.validation.phoneNumberShouldStartWithPlus')}}",
            },

            jop: {
                required: "{{__('app.validation.field_jop_required')}}"
            },

            nationality: {
                required: "{{__('app.validation.field_nationality_required')}}"
            },

            residency_number: {
                required: "{{__('app.validation.field_residency_number_required')}}",
                maxlength: "{{__('app.validation.field_residency_number_maxlength')}}",
                number: "{{__('app.validation.field_residency_number_number')}}",
            },

            date_birth: {
                required: "{{__('app.validation.field_date_birth_required')}}",
                greaterThanToDay:"{{__('app.validation.field_date_birth_less_than_today')}}"
            },
            years_experience: {
                required: "{{__('app.validation.field_years_experience_required')}}",
                maxlength: "{{__('app.validation.field_years_experience_maxlength')}}",
                number: "{{__('app.validation.field_years_experience_number')}}",
            } ,

            email: {
                required: "{{__('app.validation.field_email_required')}}",
                email: "{{__('app.validation.field_email_email')}}",
            },

            password: {
                required: "{{__('app.validation.field_password_required')}}" ,
                minlength: "{{__('app.validation.field_password_min')}}" ,
            },
            password_confirmation: {
                required: "{{__('app.validation.field_password_confirmation_required')}}",
                equalTo: "{{__('app.validation.field_equalTo')}}" ,
            },

            role_id: {
                required: "{{__('app.validation.field_role_id_required')}}"
            },



            action: "Please provide some data"
        }
    });


    $("#edit-form").validate({
        rules: {
            first_name: {
                required: true,
            },
            last_name :{
                required: true,
            },
            phone :{
                required: true,
                phoneStartingNumber:true,
                phoneStartingWithPlus:true,
                minlength: 10,
                maxlength: 14,

            },

            password :{
                minlength: 8
            },

            jop :{
                required: true,
            },
            nationality:{
                required: true,
            },

            years_experience :{
                required: true,
                maxlength:3
            },

            residency_number:{
                required: true,
                number:true,
                maxlength: 10,
            },

            date_birth :{
                required: true,
                greaterThanToDay:true
            },

            password_confirmation :{
                equalTo: "#password"
            },

            email :{
                required: true,
                email: true,
            },

            city_id :{
                required: true,
            },

            action: "required"
        },
        messages: {
            first_name: {
                required: "{{__('app.validation.field_first_name_required')}}"
            },
            last_name: {
                required: "{{__('app.validation.field_last_name_required')}}"
            },

            phone: {
                required: "{{__('app.validation.field_phone_required')}}",
                minlength: "{{__('app.validation.field_phone_minlength')}}",
                maxlength: "{{__('app.validation.field_phone_maxlength')}}",
                phoneStartingNumber:"{{__('app.validation.phoneNumberIsNumber')}}",
                phoneStartingWithPlus:"{{__('app.validation.phoneNumberShouldStartWithPlus')}}",
            },

            jop: {
                required: "{{__('app.validation.field_jop_required')}}"
            },

            nationality: {
                required: "{{__('app.validation.field_nationality_required')}}"
            },

            city_id: {
                required: "{{__('app.validation.field_city_id_required')}}"
            },

            residency_number: {
                required: "{{__('app.validation.field_residency_number_required')}}",
                maxlength: "{{__('app.validation.field_residency_number_maxlength')}}",
                number: "{{__('app.validation.field_residency_number_number')}}",
            },

            date_birth: {
                required: "{{__('app.validation.field_date_birth_required')}}",
                greaterThanToDay :"{{__('app.validation.field_date_birth_less_than_today')}}"
            },
            years_experience: {
                required: "{{__('app.validation.field_years_experience_required')}}",
                maxlength: "{{__('app.validation.field_years_experience_maxlength')}}",
                number: "{{__('app.validation.field_years_experience_number')}}",
            } ,

            email: {
                required: "{{__('app.validation.field_email_required')}}",
                email: "{{__('app.validation.field_email_email')}}",
            },

            password: {
                required: "{{__('app.validation.field_password_required')}}" ,
                minlength: "{{__('app.validation.field_password_min')}}" ,
            },
            password_confirmation: {
                required: "{{__('app.validation.field_password_confirmation_required')}}",
                equalTo: "{{__('app.validation.field_equalTo')}}" ,
            },
            action: "Please provide some data"
        } ,

        errorPlacement: function (error, element) {
            // Customize the placement of error messages
            error.appendTo("#" + element.attr("name") + "-error");
        },
    });



    $("#add-block").validate({
        rules: {
            city_id: {
                required: true,
            },
            cities: {
                required: true,
            }
        } , messages: {
            city_id: {
                required: "{{__('app.validation.field_city_id_required')}}"
            },


    });



</script>