@extends('dashboard.layouts.app', ['title' => __('app.titles.survey')])
@section('content')
    @push('button')
        @can("survey-create")
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                {{__('app.survey.add')}}
            </button>
        </div>
        @endcan
    @endpush
    <div class="row">

        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items ])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.survey.question_ar')}} </th>
                                <th class="pt-0"> {{__('app.survey.question_en')}} </th>
                                <th class="pt-0"> {{__('app.survey.question_type')}} </th>
                                <th class="pt-0"> {{__('app.ordering')}} </th>
                                <th class="pt-0"> {{__('app.processes')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $items as $item )
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td> {{ optional($item->translate('ar'))->text }} </td>
                                    <td>
                                        {{ optional( $item->translate('en') )->text  }}
                                    </td>

                                    <td>
                                        @if( $item->type == 'text' )
                                            {{--{{$item->type}}--}}
                                            {{__('app.survey.ans_text')}}
                                        @else
                                            {{__('app.survey.select_choose')}}
                                        @endif
                                    </td>

                                    <td>
                                        {{ $item->ordering }}
                                    </td>

                                    <td>
                                        @can("survey-edit")
                                        <a href="#" data-id="{{$item->id}}"
                                           data-text="{{ optional($item->translate('ar'))->text }}"
                                           data-text_en="{{ optional($item->translate('en'))->text }}"
                                           data-ordering="{{ $item->ordering }}"
                                           data-answer_type="{{ $item->type }}"
                                           data-answer_items="{{json_encode($item->answers)}}"
                                           class="btn btn-primary btn-icon aligner edit">
                                            <i data-feather="edit"></i>
                                        </a>
                                        @endcan

                                        {{--@can("cities-delete")--}}
                                        {{--<form style="display: inline-block" method="post"--}}
                                              {{--action="{{route("survey-questions.destroy", $item->id)}}"--}}
                                              {{--onsubmit="return confirm('هل أنت متأكد من الحذف؟')">--}}
                                            {{--@method('delete')--}}
                                            {{--@csrf--}}
                                            {{--<button type="submit" class="btn btn-danger btn-icon aligner"><i--}}
                                                    {{--data-feather="trash"></i></button>--}}
                                        {{--</form>--}}

                                        @can("survey-delete")
                                        <button type="submit" class="btn btn-danger btn-icon aligner"
                                                onclick="showSwal('passing-parameter-execute-cancel' , '{{route("survey-questions.destroy", $item->id)}}' )" >
                                            <i data-feather="trash"></i>
                                        </button>
                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="10" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('styles')
<style>
    .close_item_content{
        margin-top: 25px;
    }
    #add_new_btn {
        margin-top: 25px;
    }

    .item.row{
        width: 100%;
        margin-right: 1px;
    }

    button#edit_new_btn{
        margin-top: 26px;
    }
</style>
@endpush
@push('scripts')
    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                text = el.data('text'),
                text_en = el.data('text_en'),
                ordering = el.data('ordering'),
                answer_type = el.data('answer_type'),
                answer_items = el.data('answer_items'),

                route = "{{ route('survey-questions.update', ':id') }}";
            route = route.replace(':id', id);


            if( answer_type == 'choose' ) {

                $("#edit_append_answers").empty();
                $("#edit_append_answers").append("<hr class='w-100'>")

                var html = "";
                html+= `<div class="row"><div class="col-lg-4">
                                <label for="edit_answer_ar"> {{__('app.survey.ans_ar')}} </label>
                                <input type="text" class="form-control mb-2" id="edit_answer_ar">
                              </div>
                              <div class="col-lg-4">
                                <label for="editanswer_en"> {{__('app.survey.ans_en')}} </label>
                                <input type="text" class="form-control mb-2" id="edit_answer_en">
                              </div>
                              <div class="col-lg-2">
                                <label for="edit_answer_ordering"> {{__('app.ordering')}} </label>
                                <input type="number" class="form-control mb-2" id="edit_answer_ordering">
                              </div>
                              <div class="col-lg-1">
                                  <button class="btn btn-primary" type="button" id="edit_new_btn">
                                    +
                                  </button>
                              </div>
                          </div>
                            `;
                $("#edit_append_answers").append(html);

                $("#edit_append_answers").append("<h5> {{__('app.survey.answers')}} </h5>")
                $("#edit_append_answers").append("<p class='text-error item_error'>  </p>")
                $("#edit_append_answers").append("<p id='delete_elements'>  </p>")

                for(let i = 0; i < answer_items.length; i++){
                    console.log("length",answer_items[i].translations.length);
                    console.log("answer_items >> ",answer_items[i].id);

                    var itemContent = `<div class='row form-group item-answer-content' data-id='${answer_items[i].id}'>` ;
                        // itemContent+= "<input type='hidden' name='item_id[]' value='"+ answer_items[i].id +"'> "
                    for(let o = 0; o < answer_items[i].translations.length ; o++){
                        let y = answer_items[i].translations[o] ;
                            console.log(">>>>>>>>>",answer_items[i].translations[o])
                            console.log(">>y>>>",y)
                            var langAns = "" ;
                            var langItem = "" ;
                            if (y.locale == "ar"){
                                langAns = "{{__("app.ar")}}"
                                langItem = "ar"
                            }else{
                                langAns = "{{__("app.en")}}"
                                langItem = "en"
                            }
                            itemContent+= `
                                        <div class="col-lg-4">
                                            <label>  {{__('app.answer')}} ${langAns} </label>
                                            <input type="text" value="${y.text}" class="form-control mb-2 answer_${langItem}" name="item_id_s[${y.id}]" >
                                        </div>
                                        `;
                    }
                    itemContent+= `<div class="col-lg-2">
                                        <label> {{__('app.ordering')}} </label>
                                        <input type="number" value="${answer_items[i].ordering}" class="form-control mb-2 answers_ordering" name="ordering_ans[${answer_items[i].id}]"  >
                                    </div>` ;
                    itemContent+= `<div class="col-lg-1">
                                        <button class="btn btn-danger btn-icon aligner close_item_content"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg> </button>
                                    </div>` ;
                    itemContent+="</div>";

                    $("#edit_append_answers").append(itemContent)
                }

                $("#edit_append_answers").append()
            } else {
                $("#edit_append_answers").empty()
            }


            $(document).find("#edit_text").val(text);
            $(document).find("#edit_text_en").val(text_en);
            $(document).find("#edit_ordering").val(ordering);
            $(document).find("#edit_answer_type").val(answer_type);

            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);


            $(".close_item_content").click(function (e) {
                e.preventDefault();
                let itemAns = $(this).closest(".item-answer-content")
                let itemId = itemAns.data("id");

                $("#delete_elements").append(`<input type='hidden' name='delete_ans[]' value='${itemId}' >`)
                itemAns.remove()
            })
        });

        $(document).on( "change","#edit_answer_type" ,function(){
            let val = $(this).val()

            if( val == "text" ){
                $("#edit_append_answers").hide()
            }else{
                $("#edit_append_answers").show()
            }
        })

    </script>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel"> {{__('app.survey.add')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add-form" method="post" action="{{route("survey-questions.store")}}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_text" class="col-form-label"> {{__('app.survey.question_ar')}} :</label>
                            </div>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="text"  id="add_text">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_text_en" class="col-form-label">{{__('app.survey.question_en')}} :</label>
                            </div>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="text_en"  id="add_text_en">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_ordering" class="col-form-label">{{__('app.ordering')}} :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="number" class="form-control" name="ordering"  id="add_ordering">
                            </div>

                            <div class="col-lg-2">
                                <label for="add_answer" class="col-form-label">{{__('app.survey.question_type')}} :</label>
                            </div>
                            <div class="col-lg-4">
                                <select class="form-control" name="type" id="answer_type">
                                    <option value="text"> {{__('app.survey.ans_text')}} </option>
                                    <option value="choose"> {{__('app.survey.select_choose')}} </option>
                                </select>
                            </div>
                        </div>
                        <div class="item_error text-danger">

                        </div>

                        <div class="form-group row" id="append_answers"></div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel"> {{__("app.survey.edit")}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="" id="edit-form" >
                        @method('put')
                        @csrf

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="edit_text" class="col-form-label"> {{__('app.survey.question_ar')}} :</label>
                            </div>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="text" id="edit_text">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="edit_text_en" class="col-form-label">{{__('app.survey.question_en')}} :</label>
                            </div>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="text_en" id="edit_text_en">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="edit_ordering" class="col-form-label">{{__('app.ordering')}} :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="number" class="form-control" name="ordering1" id="edit_ordering">
                            </div>

                            <div class="col-lg-2">
                                <label for="edit_answer_type" class="col-form-label">{{__('app.survey.ans_type')}} :</label>
                            </div>
                            <div class="col-lg-4">
                                <select class="form-control" name="type" id="edit_answer_type">
                                    <option value="text"> {{__('app.survey.ans_text')}} </option>
                                    <option value="choose">{{__('app.survey.select_choose')}} </option>
                                </select>
                            </div>
                        </div>

                        <div class="item_error text-danger">

                        </div>

                        <div class="item_errors text-danger">

                        </div>

                        <div id="edit_append_answers"></div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endpush

@push("scripts")

    <script>
        $("#answer_type").on("change",function ( e ) {

            if ( $(this).val() == "choose" ) {
                var html = "";
                    html+= `<div class="col-lg-4">
                                <label for="add_answer_ar"> {{__('app.survey.ans_ar')}} </label>
                                <input type="text" class="form-control mb-2" id="add_answer_ar">
                              </div>
                              <div class="col-lg-4">
                                <label for="add_answer_en"> {{__('app.survey.ans_en')}} </label>
                                <input type="text" class="form-control mb-2" id="add_answer_en">
                              </div>
                              <div class="col-lg-2">
                                <label for="add_answer_ordering"> {{__('app.ordering')}} </label>
                                <input type="number" class="form-control mb-2" id="add_answer_ordering">
                              </div>
                              <div class="col-lg-1">
                                  <button class="btn btn-primary" type="button" id="add_new_btn">
                                    +
                                  </button>
                              </div>
                            `;
                $("#append_answers").append(html);
            } else {
                $("#append_answers").empty();
            }
        })



        $( "#editModal" ).on('show.bs.modal', function(){
            $(".item_error").empty() ;
        });

        $(document).on("click","#add_new_btn" , function() {
            let answer_ar   = $("#add_answer_ar").val(),
                answer_en   = $("#add_answer_en").val(),
                ordering    = $("#add_answer_ordering").val(),
                itemsCount  = $("#append_answers").find(".item").length


            $(".item_error").empty();


            if ( itemsCount >= 5 ) {
                $(".item_error").append("{{__('app.survey.cannot_add_more')}}")
                return false ;
            }



            $("#add_answer_ar ,#add_answer_en , #add_answer_ordering").val("")
            let html = $(`
                        <div class="item row answer__items">
                            <div class="col-lg-4">
                                <label> {{__('app.survey.ans_ar')}} </label>
                                <input type="text " value="${answer_ar}" class="form-control mb-2 answer_ar" name="answers[ar][]"   >
                              </div>
                              <div class="col-lg-4">
                                <label> {{__('app.survey.ans_en')}} </label>
                                <input type="text" value="${answer_en}" class="form-control mb-2 answer_en" name="answers[en][]"  >
                              </div>
                              <div class="col-lg-2">
                                <label> {{__('app.ordering')}} </label>
                                <input type="number" value="${ordering}" class="form-control mb-2 answer_item" name="answers_ordering[]" >
                              </div>
                              <div class="col-lg-1">
                                  <button typeof="button" class="btn btn-danger btn-icon aligner remove_btn mt-4" type="button">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
                                  </button>
                              </div>
                           </div>
                            `);
            $("#append_answers").append(html);
        })


        //-==============================

        $(document).on("click","#edit_new_btn" , function() {
            let answer_ar   = $("#edit_answer_ar").val(),
                answer_en   = $("#edit_answer_en").val(),
                ordering    = $("#edit_answer_ordering").val(),
                itemsCount  = $("#edit_append_answers").find(".item-answer-content").length;

            $(".item_error").empty();

            if ( itemsCount >= 4 ) {
                $(".item_error").append("{{__('app.survey.cannot_add_more')}}")
                return false ;
            }

            console.log("?>?>?>?", answer_en ,answer_ar ,ordering , itemsCount)
            console.log("itemsCount", itemsCount)

            $("#edit_answer_ar ,#edit_answer_en , #edit_answer_ordering").val("")
            let html = $(`
                        <div class="row form-group item-answer-content">
                            <div class="col-lg-4">
                                <label> {{__('app.survey.ans_ar')}} </label>
                                <input type="text" value="${answer_ar}" class="form-control mb-2 answer_ar" name="answers[ar][]"    >
                              </div>
                              <div class="col-lg-4">
                                <label> {{__('app.survey.ans_en')}} </label>
                                <input type="text" value="${answer_en}" class="form-control mb-2 answer_en" name="answers[en][]"   >
                              </div>
                              <div class="col-lg-2">
                                <label> {{__('app.ordering')}} </label>
                                <input type="number" value="${ordering}" class="form-control mb-2 answers_ordering" name="answers_ordering[]" >
                              </div>
                              <div class="col-lg-1">
                                  <button typeof="button" class="btn btn-danger btn-icon aligner remove_btn mt-4" type="button">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
                                  </button>
                              </div>
                           </div>
                            `);
            $("#edit_append_answers").append(html);
        })

        $(document).on("click",".remove_btn",function() {
            $(this).closest(".item").remove()
        })

        //========================================


        $(document).on( "change","#edit_answer_type" ,function(){
            //
            // alert(  2121)

            if ( $(this).val() == "choose" ) {
                var html = "<div class='form-group row'>";
                html+= `<div class="col-lg-4">
                                <label for="edit_answer_ar"> {{__('app.survey.ans_ar')}} </label>
                                <input type="text" class="form-control mb-2 answer_ar" id="edit_answer_ar">
                              </div>
                              <div class="col-lg-4">
                                <label for="add_answer_en"> {{__('app.survey.ans_en')}} </label>
                                <input type="text" class="form-control mb-2 answer_en" id="add_answer_en">
                              </div>
                              <div class="col-lg-2">
                                <label for="edit_answer_ordering"> {{__('app.ordering')}} </label>
                                <input type="number" class="form-control mb-2 answer_item" id="edit_answer_ordering">
                              </div>
                              <div class="col-lg-1">
                                  <button class="btn btn-primary" type="button" id="edit_new_btn">
                                    +
                                  </button>
                              </div>
                            `;

                html+= "</div>"
                $("#edit_append_answers").append(html);
            } else {
                $("#edit_append_answers").empty();
            }
        })




        $("#add-form").on("submit" , function (e) {
            $(".item_error").empty()
            let countLi = $("#append_answers").find(".answer__items").length
            let answerType = $("#answer_type").val()


            if ( answerType == 'choose' ) {
                if ( countLi < 2 ) {
                    $(".item_error").empty().append("{{__('app.number_answers_must_2_or_more')}}")
                    console.log("countLi >> ", countLi) ;
                    return false ;
                }
            }

            $(".item_error").empty()
        })



        $("#edit-form").on("submit" , function (e) {
            $(".item_error").empty()
            let countLi = $("#edit_append_answers").find(".item-answer-content").length
            let answerType = $("#edit_answer_type").val()


            if ( answerType == 'choose' ) {
                if ( countLi < 2 ) {
                    $(".item_errors").empty().append("{{__('app.number_answers_must_2_or_more')}}")
                    console.log("countLi >> ", countLi) ;
                    return false ;
                }
            }

            $(".item_error").empty()
        })
    </script>


    @include('dashboard.surveyQuestion.script')



    <script>

        function myFunction() {
            let ansEns = $(".answer_en")
            let ansArs = $(".answer_ar")
            let answerItem = $(".answer_item")
            let isAns = true ;

            ansEns.each(function() {
                if ( $(this).val() == "") {
                    isAns = false
                    $(this).closest("div").append("<label class='error'>{{ __('app.validation.field_answer_en_required') }} </label>")
                }
            });


            answerItem.each(function() {
                if ( $(this).val() == "") {
                    isAns = false
                    $(this).closest("div").append("<label class='error'> {{ __('app.validation.field_ordering_required') }}  </label>")
                }
            });


            ansArs.each(function() {
                if ($(this).val() === "") {
                    isAns = false
                    $(this).closest("div").append("<label class='error'> {{ __('app.validation.field_answer_ar_required') }}  </label>")
                }
            });


            // ansArs.each(function() {
            //     if ($(this).val() === "") {
            //         isAns = false
            //         $(this).closest("div").append("<label class='error'> حقل االترتيب مطلوبه  </label>")
            //     }
            // });

            if ( !isAns ) {
                return false ;
            }else{
                return true ;
            }
        }



        $("#add-form").on("submit", function ( e ) {


            let dd = myFunction()
            return dd ;
            //return false;
        })



        $("#edit-form").on("submit", function ( e ) {


            let dd = myFunction()
            return dd ;
            //return false;
        })
    </script>
@endpush
