<script>
    $("#add-form").validate({
        rules: {
            text: {
                required: true,
            },
            text_en :{
                required: true,
            },

            ordering :{
                required: true,
            },
            // 'answers[en][]' :{
            //     required: true,
            // },
            //
            // 'answers[ar][]' :{
            //     required: true,
            // },
            action: "required"
        },
        messages: {
            text: {
                required: "{{__('app.validation.field_text_q_required')}}"
            },
            text_en: {
                required: "{{__('app.validation.field_name_en_required')}}"
            },

            ordering: {
                required: "{{__('app.validation.field_ordering_required')}}"
            },
            {{--'answers[en][]': {--}}
                {{--required: "{{__('app.validation.field_answer_en_required')}}" ,--}}
                {{--required: "{{__('app.validation.field_answer_en_required')}}" ,--}}
                {{--// Add more custom messages for specific rules as needed--}}
            {{--} ,--}}

            {{--'answers[ar][]': {--}}
                {{--required: "{{__('app.validation.field_answer_en_required')}}" ,--}}
                {{--required: "{{__('app.validation.field_answer_ar_required')}}" ,--}}
                {{--// Add more custom messages for specific rules as needed--}}
            {{--} ,--}}
            {{--'answers[ar][]': {--}}
                {{--required: "{{__('app.validation.field_answer_ar_required')}}" ,--}}
                {{--// Add more custom messages for specific rules as needed--}}
            {{--} ,--}}


            action: "Please provide some data"
        }
    });


    $("#edit-form").validate({
        rules: {
            name: {
                required: true,
            },
            name_en :{
                required: true,
            },

            // 'item_id_s[]' :{
            //     required: true,
            // },
            //
            // ordering :{
            //     required: true,
            // },
            action: "required"
        },
        messages: {
            name: {
                required: "{{__('app.validation.field_name_required')}}"
            },
            name_en: {
                required: "{{__('app.validation.field_name_en_required')}}"
            },

            {{--'answers[en][]': {--}}
                {{--required: "{{__('app.validation.field_answer_en_required')}}" ,--}}
                {{--required: "Custom error message for the answers field in English." ,--}}
                {{--// Add more custom messages for specific rules as needed--}}
            {{--} ,--}}

            ordering: {
                required: "{{__('app.validation.field_ordering_required')}}"
            },


            action: "Please provide some data"
        }
    });
</script>