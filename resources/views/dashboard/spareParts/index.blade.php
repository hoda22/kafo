@extends('dashboard.layouts.app', ['title' => __('app.titles.spare-parts')])
@section('content')
    @push('button')
        @can("spare-parts-create")
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                {{__('app.spare_part_add')}}
            </button>
        </div>
        @endcan
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.spare_part_name')}} </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="name" value="{{ request('name') }}"/>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items ])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.name_ar')}} </th>
                                <th class="pt-0"> {{__('app.name_en')}} </th>
                                <th class="pt-0"> {{__('app.basic_service')}} </th>
                                <th class="pt-0"> {{__('app.sub_service')}} </th>

                                <th class="pt-0"> {{__('app.ordering')}} </th>
                                <th class="pt-0"> {{__('app.status')}} </th>
                                <th class="pt-0"> {{__('app.processes')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item )
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td> {{ optional($item->translate('ar'))->name }} </td>
                                    <td>
                                        {{ optional( $item->translate('en') )->name  }}
                                    </td>
                                    <td> {{ optional( $item->service )->name}} </td>
                                    <td> {{ optional( $item->subService )->name}} </td>

                                    {{--<td>--}}
                                        {{--{{ optional($item->brand)->name  }}--}}
                                    {{--</td>--}}
                                    <td> {{$item->ordering}} </td>
                                    <td>
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="active" value="active" class="change_status"
                                                       data-route="{{route("spare-parts.updateAxios" , $item->id )}}"
                                                       data-id="{{$item->id}}"
                                                       @if($item->active == 'active')
                                                       checked
                                                        @endif
                                                >
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </td>

                                    <td>
                                        @can("spare-parts-edit")
                                        <a href="#" data-id="{{$item->id}}"
                                           data-name="{{ optional($item->translate('ar'))->name }}"
                                           data-name_en="{{ optional($item->translate('en'))->name }}"
                                           data-active="{{ $item->active }}"
                                           data-department_id="{{ $item->id }}"
                                           data-service_id="{{ $item->service_id }}"
                                           data-sub_service_id="{{ $item->sub_service_id }}"
                                           data-ordering="{{ $item->ordering }}"
                                           class="btn btn-primary btn-icon aligner edit">
                                            <i data-feather="edit"></i>
                                        </a>
                                        @endcan

                                        @can("spare-parts-delete")
                                        <button type="submit" class="btn btn-danger btn-icon aligner"
                                                onclick="showSwal('passing-parameter-execute-cancel' , '{{route("spare-parts.destroy", $item->id)}}' )" >
                                            <i data-feather="trash"></i>
                                        </button>
                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="danger text-center"> {{__('app.no_data')}} </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                name = el.data('name'),
                name_en = el.data('name_en'),
                active = el.data('active'),
                department = el.data('department_id'),
                service_id = el.data('service_id'),
                ordering = el.data('ordering'),
                sub_service_id = el.data('sub_service_id'),
                route = "{{ route('spare-parts.update', ':id') }}";
            route = route.replace(':id', id);
            $(document).find("#edit_name").val(name);
            $(document).find("#edit_name_en").val(name_en);
            $(document).find("#edit_service_id").val(service_id);
            $(document).find("#edit_ordering").val(ordering);
            $(document).find("#edit_sub_service_id").val(sub_service_id);


            $('#edit_sub_service_id option').each(function() {
                if ($(this).data("id") !== service_id) {
                    $(this).addClass('d-none');
                }else{
                    $(this).removeClass('d-none');
                }
            });


            getChildren( service_id , sub_service_id )



            if ( active == 'active' ){
                $(document).find("#edit_active").prop('checked',true);
            }else{
                $(document).find("#edit_active").prop('checked',false);
            }

            $(document).find("#edit_department").val(department);
            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);
        });
    </script>
    
    
    
    
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel">{{__('app.spare_parts.add')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="add-form" action="{{route("spare-parts.store")}}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name" class="col-form-label"> {{__('app.name_ar')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name" id="add_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name_en" class="col-form-label"> {{__('app.name_en')}}  <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name_en" id="add_name_en">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_ac_type" class="col-form-label"> {{__('app.basic_service')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <select name="service_id" id="basic_service" class="form-control select2">
                                    <option value=""> -- {{__('app.choose')}} --</option>
                                    @foreach( $services as $service )
                                    <option value="{{$service->id}}"> {{$service->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_sub_service" class="col-form-label"> {{__('app.sub_service')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-9">
                                <select name="sub_service_id" id="add_sub_service" class="form-control select2">
                                    <option value=""> -- {{__('app.choose')}} --</option>
                                    {{--@foreach($brands as $brand)--}}
                                    {{--<option value="{{ $brand->id }}">{{ $brand->name }}</option>--}}
                                    {{--@endforeach--}}
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name_en" class="col-form-label">{{__('app.ordering')}} <span class="required_star">*</span>  :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="ordering" id="add_ordering">
                            </div>
                        </div>




                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3 text-end">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel"> {{__('app.spare_parts.edit')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="edit-form" action="">
                        @method('put')
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_name" class="col-form-label">{{__('app.name_ar')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name" id="edit_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_name" class="col-form-label"> {{__('app.name_en')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name_en" id="edit_name_en">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_ac_type" class="col-form-label"> {{__('app.basic_service')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <select name="service_id" id="edit_service_id" class="form-control select2">
                                    <option value=""> -- {{__('app.choose')}} --</option>

                                    @foreach( $services as $service )
                                    <option value="{{$service->id}}"> {{$service->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        {{--basic_service
sub_service--}}

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_brand_id" class="col-form-label"> {{__('app.sub_service')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-9">
                                <select name="sub_service_id" id="edit_sub_service_id" class="form-control select2">
                                    <option value=""> -- {{__('app.choose')}} --</option>
                                    @foreach( $sup_services as $sup_service )
                                        <option value="{{ $sup_service->id }}" data-id="{{$sup_service->id}}">{{ $sup_service->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_ordering" class="col-form-label">{{__('app.ordering')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="ordering" id="edit_ordering">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active" id="edit_active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary">{{__('app.save')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('dashboard.spareParts.script')
    
    
    <script>
        
        $("#basic_service").on("change", function () {
            let basicService = $(this).val();

            let route   = "{{route('order.subServices')}}";
            axios.get(route, {
                params: {
                    id: basicService
                }
            }).then((response) => {
                console.log("response >> " , response )
                if (response.data.status == true) {
                    $("#add_sub_service").empty().append(response.data.data)
                }
            })
        } )




        $("#edit_service_id").on("change", function () {
            let basicService = $(this).val();


            let route   = "{{route('order.subServices')}}";
            axios.get(route, {
                params: {
                    id: basicService
                }
            }).then((response) => {
                console.log("response >> " , response )
                if (response.data.status == true) {
                    $("#edit_sub_service_id").empty().append(response.data.data)
                }
            })
        } )


        function getChildren( id , sub_service_id ){
            let route   = "{{route('order.subServices')}}";
            axios.get(route, {
                params: {
                    id: id
                }
            }).then((response) => {
                console.log("response >> " , response )
                if (response.data.status == true) {
                    $("#edit_sub_service_id").empty().append(response.data.data)
                }
            }).finally( () =>{
                $("#edit_sub_service_id").val(sub_service_id)
            } )
        }




    </script>
@endpush
