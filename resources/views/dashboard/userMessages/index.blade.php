@extends('dashboard.layouts.app', ['title' => __('app.titles.userMessages')])
@section('content')
    @push('button')
        @can("messages-send")
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                {{__('app.userMessagesAdd')}}
            </button>
        </div>
        @endcan
    @endpush
    <div class="row">

        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items ])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.notification')}} </th>
                                <th class="pt-0"> {{__('app.posting_time')}} </th>
                                @can("messages-show")
                                <th class="pt-0"> {{__('app.processes')}}</th>
                                @endcan
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item )
                                <tr>
                                    <td> {{ $loop->iteration }}</td>
                                    <td> {{ $item->text }} </td>


                                    <td> {{ $item->created_at }} </td>
                                    <td>
                                        @can("messages-show")
                                        <a  class="btn btn-success btn-icon aligner" href="{{route("messages.show", $item->id)}}">
                                            <i data-feather="eye"></i>
                                        </a>
                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="danger text-center"> {{__('app.no_data')}} </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                name = el.data('name'),
                name_en = el.data('name_en'),
                ordering = el.data('ordering'),
                route = "{{ route('cities.update', ':id') }}";
            route = route.replace(':id', id);
            $(document).find("#edit_name").val(name);
            $(document).find("#edit_name_en").val(name_en);
            $(document).find("#edit_ordering").val(ordering);
            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);
        });
    </script>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel"> {{__('app.notifications.add')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route("messages.store")}}" id="add-form">
                        @csrf

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_category" class="col-form-label"> {{__('app.how_send')}}  :</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control" name="how_send" id="how_send">
                                    <option value="email"> {{__('app.email')}} </option>
                                    <option value="sms"> sms </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_text" class="col-form-label">{{__('app.text')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <textarea class="form-control" rows="3" name="text" id="add_text"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.notifications.script')
@endpush
