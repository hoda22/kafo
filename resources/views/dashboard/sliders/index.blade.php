@extends('dashboard.layouts.app', ['title' => __('app.titles.sliders')])
@section('content')
    @push('button')
        @can("sliders-create")
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                {{__('app.sliders.add')}}
            </button>
        </div>
        @endcan
    @endpush
    <div class="row">

        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items ])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.image')}} </th>
                                <th class="pt-0"> {{__('app.ordering')}} </th>
                                <th class="pt-0"> {{__('app.status')}} </th>
                                <th class="pt-0"> {{__('app.processes')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td> <img src="{{asset($item->src)}}" /> </td>

                                    <td>
                                        {{$item->ordering}}
                                    </td>


                                    <td>
                                        {{--<span class="badge badge-lg badge-{{$item->active_status['className']}}"> {{$item->active_status['text']}}</span>--}}
                                        <label class="switch">
                                            <input type="checkbox" name="active" value="active" class="change_status"
                                                   data-route="{{route("slider.updateAxios" , $item->id )}}"
                                                   data-id="{{$item->id}}"
                                                   @if($item->active == 'active')
                                                   checked
                                                    @endif
                                            >
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td>
                                        @can("sliders-edit")
                                        <a href="#" data-id="{{$item->id}}"
                                           data-ordering="{{ $item->ordering }}"
                                           data-img_src="{{ asset($item->src) }}"
                                           data-active="{{ $item->active }}"
                                           class="btn btn-primary btn-icon aligner edit">
                                            <i data-feather="edit"></i>
                                        </a>
                                        @endcan

                                        @can("sliders-delete")

                                        <button type="submit" class="btn btn-danger btn-icon aligner"
                                                onclick="showSwal('passing-parameter-execute-cancel' , '{{route("sliders.destroy", $item->id)}}' )" >
                                            <i data-feather="trash"></i>
                                        </button>
                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="danger text-center">  {{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('/') }}assets/vendors/tinymce/tinymce.min.js"></script>
    <script src="{{ asset('/') }}assets/js/tinymce.js"></script>
    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                active = el.data('active'),
                ordering = el.data('ordering'),
                img_src = el.data('img_src'),
                route = "{{ route('sliders.update', ':id') }}";
            route = route.replace(':id', id);
            $(document).find("#edit_ordering").val(ordering);
            $(document).find("#img_src").attr("src",img_src);

            if ( active == 'active' ){
                $(document).find("#edit_active").prop('checked',true);
            }else{
                $(document).find("#edit_active").prop('checked',false);
            }

            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);
        });
    </script>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel">{{__('app.sliders.add')}}  </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="add-form" action="{{route("sliders.store")}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_file" class="col-form-label">{{__('app.image')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="file" class="form-control" name="src" accept="image/png, image/gif, image/jpeg" id="add_file">
                                {{----}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_ordering" class="col-form-label">{{__('app.ordering')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="ordering" id="add_ordering">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active" >
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">{{__('app.sliders.edit_image')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="edit-form" action="" enctype="multipart/form-data">
                        @method('put')
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_file" class="col-form-label">{{__('app.image')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="file" class="form-control" accept="image/png, image/gif, image/jpeg" name="src" id="edit_file">
                                {{-- --}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_file" class="col-form-label"> {{__('app.old_image')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <img src="" id="img_src" width="50" height="50" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_ordering" class="col-form-label"> {{__('app.ordering')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="ordering" id="edit_ordering">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active" id="edit_active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary">{{__('app.save')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.sliders.script')
@endpush
