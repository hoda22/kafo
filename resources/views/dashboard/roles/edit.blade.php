@extends('dashboard.layouts.app', ['title' => __('app.titles.roles')])
@section('content')


    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-header">
                    <h4>{{__('app.edit_role')}} - <strong>{{ $role->name }}</strong></h4>
                </div>
                <div class="card-body">
                    <div class="col-xs-12">

                        {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-xs-6" style="padding:10px;"> {{__('app.name_ar')}} :</label>
                                    <div class="col-md-6">
                                        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-xs-6" style="padding:10px;"> {{__('app.name_en')}} :</label>
                                    <div class="col-md-6">
                                        {!! Form::text('name_en', null , ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>

                            <hr class="w-100">

                            <div class="col-md-12">
                                <h3>{{__('app.role')}}:</h3>
                            </div>
                            <div class="col-md-12">
                                <div class="row form-group">
                                    @foreach($permissions as $permission)
                                        <div class="col-md-3">
                                            <div class="card card-role mb-4">
                                                <div class="card-header ">
                                                    <h5>{{ __('app.titles.' . $permission->name)}} </h5>
                                                </div>
                                                <div class="card-body">
                                                    <ul class="list-unstyled roles-list">
                                                        <li class="mb-2 ">
                                                            <label>
                                                                <input name="item" class="select_all" type="checkbox" value="">
                                                                {{ __('roles.all') }}
                                                            </label>
                                                        </li>
                                                        @foreach($permission->children as $value)
                                                            <li class="mb-2 {{($value->head_list) ? "head_list-js":"body_list-js"}}">
                                                                <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name items_checks')) }}
                                                                    {{ __('roles.' . $value->name) }}</label>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        {{--<label class="col-sm-3 col-xs-6" style="padding:10px;">{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}--}}
                                            {{--{{ __('roles.' . $value->name) }}--}}
                                        {{--</label>--}}

                                    @endforeach
                                </div>
                            </div>


                            <div class="clearfix"></div>
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                                </div>
                            {{--@can('roles-edit')--}}
                            {{--@else--}}
                                {{--<div class="col-xs-12 col-sm-12 col-md-12 text-center">--}}
                                    {{--<a href="javascript:void(0)" class="text-danger">NOT AUTHORIZED TO UPDATE ROLES</a>--}}
                                {{--</div>--}}
                            {{--@endcan--}}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        .roles-list{
            padding-right: 0;
        }

        .roles-list label input {
            margin-left: 7px;
        }

        .roles-list{
            margin-bottom: 0 !important;
        }

        .card-role .card-header{
            background: #001A5C;
            color: #fff;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(".select_all").click(function () {
            if($(this).is(':checked')) {
                $(this).closest("ul").find(".items_checks").prop('checked',true)
            }else{
                $(this).closest("ul").find(".items_checks").prop('checked',false)
            }
        })

        let rolesList = $(".roles-list");
        rolesList.each(function( item ){
            let itemsCheked = $(this).find("li:not(.select_all)").find(".items_checks:checked").length  ;
            let itemsNotCheked = $(this).find("li:not(.select_all)").find(".items_checks").length  ;
            
            if ( itemsCheked == itemsNotCheked ) {
                $(this).find(".select_all").prop('checked',true)
            }else{
                $(this).find(".select_all").prop('checked',false)
            }
            console.log("itemsCheked",itemsCheked)
            console.log("itemsNotCheked",itemsNotCheked)
            console.log("====")

        })


        $(".items_checks").click(function (e) {
            // e.preventDefault();
            let lengthCheckItems = $(this).closest(".roles-list").find(".items_checks:checked").length
            let itemsNotCheked = $(this).closest(".roles-list").find(".items_checks").length  ;
            if ( lengthCheckItems == itemsNotCheked ) {
                $(this).closest(".roles-list").find(".select_all").prop('checked',true)
            }else{
                $(this).closest(".roles-list").find(".select_all").prop('checked',false)
            }
        })



        $(".body_list-js .items_checks").click(function () {
            if($(this).is(':checked')) {
                $(this).closest(".roles-list").find(".head_list-js").find(".items_checks").prop('checked',true)




                let lengthCheckItems = $(this).closest(".roles-list").find(".items_checks:checked").length
                let itemsNotCheked = $(this).closest(".roles-list").find(".items_checks").length  ;
                if ( lengthCheckItems == itemsNotCheked ) {
                    $(this).closest(".roles-list").find(".select_all").prop('checked',true)
                }

            }
        })

        $(".head_list-js .items_checks").click(function () {
            let lengthItems = $(this).closest(".roles-list").find(".body_list-js").find(".items_checks:checked").length
            console.log( "lengthItems",lengthItems )
            if ( lengthItems ) {
                return false ;
            }
        })
    </script>
@endpush
