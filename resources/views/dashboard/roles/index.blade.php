@extends('dashboard.layouts.app', ['title' => __('app.titles.roles')])
@section('content')
    @push('button')
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <a href="{{route("roles.create")}}" class="btn btn-primary">
                {{__('app.roles.add')}}
            </a>
        </div>
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label"> {{__('app.roles.name')}} </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="name" value="{{ request('name') }}"/>
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0">{{__('app.name_ar')}} </th>
                                <th class="pt-0">{{__('app.name_en')}} </th>
                                <th class="pt-0">{{__('app.processes')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($roles as $key => $role)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->name_en }}</td>
                                    <td>
                                        @can('roles-edit')
                                            
                                        <a href="{{ route('roles.edit',$role->id) }}"
                                           class="btn btn-primary btn-icon aligner ">
                                            <i data-feather="edit"></i>
                                        </a>
                                        @endcan
                                        @can('roles-delete')
                                        {!! Form::open([
                                            'method' => 'DELETE',
                                            'onsubmit' => 'return ConfirmDelete()',
                                            'route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                        {{--{!! Form::submit('مسح', ['class' => 'btn btn-danger']) !!}--}}
                                        <button type="submit" class="btn btn-danger btn-icon aligner"><i
                                                    data-feather="trash"></i></button>
                                        {!! Form::close() !!}
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $roles->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection