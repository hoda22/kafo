<script>
    $("#add-form").validate({
        rules: {
            name: {
                required: true,
            },
            last_name :{
                required: true,
            },

            password :{
                required: true,
                minlength: 8
            },

            password_confirmation :{
                required: true,
                equalTo: "#add_password"
            },

            email :{
                required: true,
                email: true,
            },

            role_id :{
                required: true,
            },

            action: "required"
        },
        messages: {
            name: {
                required: "{{__('app.validation.field_name_required')}}"
            },
            last_name: {
                required: "{{__('app.validation.field_name_en_required')}}"
            },

            email: {
                required: "{{__('app.validation.field_email_required')}}",
                email: "{{__('app.validation.field_email_email')}}",
                regex:"aaaaaaaaaa"
            },

            password: {
                required: "{{__('app.validation.field_password_required')}}" ,
                minlength: "{{__('app.validation.field_password_min')}}" ,
            },
            password_confirmation: {
                required: "{{__('app.validation.field_password_confirmation_required')}}",
                equalTo: "{{__('app.validation.field_equalTo')}}" ,
            },

            role_id: {
                required: "{{__('app.validation.field_role_id_required')}}"
            },

            action: "Please provide some data"
        }
    });


    $("#edit-form").validate({
        rules: {
            name: {
                required: true,
            },
            last_name :{
                required: true,
            },

            password :{
                minlength: 8
            },

            password_confirmation :{
                equalTo: "#edit_confirmation_password"
            },

            email :{
                required: true,
                email: true,
                accept:"[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}" ,
            },

            role_id :{
                required: true,
            },
            action: "required"
        },
        messages: {
            name: {
                required: "{{__('app.validation.field_name_required')}}"
            },
            last_name: {
                required: "{{__('app.validation.field_name_en_required')}}"
            },

            email: {
                required: "{{__('app.validation.field_email_required')}}",
                email: "{{__('app.validation.field_email_email')}}",
                accept: "{{__('app.validation.field_email_email')}}",
            },

            password: {
                required: "{{__('app.validation.field_password_required')}}" ,
                minlength: "{{__('app.validation.field_password_min')}}" ,
            },
            password_confirmation: {
                required: "{{__('app.validation.field_password_confirmation_required')}}",
                equalTo: "{{__('app.validation.field_equalTo')}}" ,
            },

            role_id: {
                required: "{{__('app.validation.field_role_id_required')}}"
            },

            action: "Please provide some data"
        }
    });
</script>