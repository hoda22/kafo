@extends('dashboard.layouts.app', ['title' => __('app.titles.chat')])
@section('content')
    {{--@push('button')--}}
        {{--@can("users-create")--}}
        {{--<div class="d-flex align-items-center flex-wrap text-nowrap">--}}
            {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">--}}
                {{--{{__('app.users.add')}}--}}
            {{--</button>--}}
        {{--</div>--}}
        {{--@endcan--}}
    {{--@endpush--}}
    <div class="page-content1 h-99">
        <div class="row chat-wrapper h-99">
            <div class="col-md-12 h-99">
                <div class="card h-99">
                    <div class="card-body h-99">
                        <div class="row position-relative h-100">
                            <div class="col-lg-4 chat-aside border-lg-left">
                                <div class="aside-content">

                                    @if( request("id") )
                                    <div class="aside-header">
                                        <div class="d-flex justify-content-between align-items-center pb-2 mb-2">
                                            <div class="d-flex align-items-center">
                                                <figure class="ml-2 mb-0">
                                                    <img src="{{asset($client->avatar)}}" class="img-sm rounded-circle" alt="profile">
                                                </figure>
                                                <div>
                                                    <h6> {{$client->full_name}} </h6>
                                                    <p class="text-muted tx-13">{{$client->email}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="aside-body">
                                        <div class="search-box">
                                            <div class="topnav">
                                                <div class="input-text-search">
                                                    <input type="text" id="searchInput" placeholder="{{__('app.Search')}} ..">
                                                </div>
                                                <div>
                                                    <div class="form-check-inline disabled">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" name="filter" value="all" checked >
                                                            {{__("app.all")}}
                                                        </label>
                                                    </div>

                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" name="filter"
                                                                   id="clientFilter"
                                                                   value="client"
                                                            >
                                                            {{__("app.clients_")}}
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" name="filter"
                                                                   id="providerFilter"
                                                                   value="provider">
                                                            {{__("app.providers_")}}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-content mt-3">
                                            <div class="tab-pane fade show active" id="chats" role="tabpanel" aria-labelledby="chats-tab">
                                                <div>
                                                    <p class="text-muted mb-1" id="conversations__title"> {{__('app.no_conversations')}} </p>
                                                    <ul class="list-unstyled chat-list px-1" id="listOfChats">

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 chat-content">
                                @if( request('id') )
                                    {{--<div class="chat-header border-bottom pb-2">--}}
                                        {{--<div class="d-flex justify-content-between">--}}
                                            {{--<div class="d-flex align-items-center">--}}
                                                {{--<i data-feather="corner-up-left" id="backToChatList" class="icon-lg mr-2 ml-n2 text-muted d-lg-none"></i>--}}
                                                {{--<figure class="mb-0 ml-2">--}}
                                                    {{--<img src="{{asset($client->avatar)}}" class="img-sm rounded-circle" alt="image">--}}
                                                {{--</figure>--}}
                                                {{--<div>--}}
                                                    {{--<p> {{$client->full_name}} </p>--}}
                                                    {{--<p class="text-muted tx-13"> {{$client->email}} </p>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="chat-body">
                                        <ul class="messages" id="chat-box">

                                        </ul>
                                    </div>

                                    <div class="chat-footer d-flex">
                                    <form class="search-form flex-grow mr-2">
                                        <div class="input-group">
                                            <input type="text" class="form-control rounded-pill" id="chatForm" placeholder="{{__('app.send_message')}} ....">
                                        </div>
                                    </form>
                                    <div>
                                        <button type="button" class="btn btn-primary btn-icon rounded-circle" id="send__message">
                                            <i data-feather="send"></i>
                                        </button>
                                    </div>
                                </div>
                                @else
                                    <p class="text-center ">

                                    </p>

                                    <div class="parent">
                                        <div class="child">
                                            {{--{{__('app.send_conversations')}}--}}
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- The Modal -->
    <div id="myModal" class="modal">

        <!-- The Close Button -->
        <span class="close">&times;</span>

        <!-- Modal Content (The Image) -->
        <img class="modal-content" id="img01">

        <!-- Modal Caption (Image Text) -->
        <div id="caption"></div>
    </div>
@endsection

@push('styles')
    <style>
        .message-item.friend .content{
            top: -25px;
            position: relative;
            margin-right: 45px;
        }
        .message-item.me {
            margin-right: -14px;
        }
        .chat-wrapper .chat-aside .aside-body .tab-content{
            overflow-y: visible;
            max-height: calc(100vh - 380px);
        }


        /* Add a black background color to the top navigation bar */
        .topnav {
            overflow: hidden;
            background-color: #ebebeb;
            padding-bottom: 10px;
            padding-right: 15px;
            padding-left: 15px;
        }

        /* Style the links inside the navigation bar */
        .topnav a {
            float: left;
            display: block;
            color: black;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }

        /* Change the color of links on hover */
        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }

        /* Style the "active" element to highlight the current page */
        .topnav a.active {
            background-color: #2196F3;
            color: white;
        }

        /* Style the search box inside the navigation bar */
        .topnav input[type=text] {
            padding: 6px;
            border: none;
            margin-top: 8px;
            margin-right: 0;
            font-size: 17px;
            width: 100%;
        }

        .input-text-search{
            width: 100%;
            height: 50px;
        }

        .h-99{
            height: 99% !important;
        }

        .chat-body{
            min-height: 450px;
        }
        .rtl .chat-wrapper .chat-content .chat-body .messages .message-item.friend img{
            margin-top:0 ;
        }

        .chat-body{
            max-height: 500px;
        }
        /* When the screen is less than 600px wide, stack the links and the search field vertically instead of horizontally */
        @media screen and (max-width: 600px) {
            .topnav a, .topnav input[type=text] {
                float: none;
                display: block;
                text-align: left;
                width: 100%;
                margin: 0;
                padding: 14px;
            }
            .topnav input[type=text] {
                border: 1px solid #ccc;
            }
        }

        .font-weight-bold{
            font-size:14px !important;
        }







        /*=========================*/

        .myImg {
            border-radius: 5px;
            cursor: pointer;
            transition: 0.3s;
        }

        .myImg:hover {opacity: 0.7;}

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
        }

        /* Modal Content (Image) */
        .modal-content {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
        }

        /* Caption of Modal Image (Image Text) - Same Width as the Image */
        #caption {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
            text-align: center;
            color: #ccc;
            padding: 10px 0;
            height: 150px;
        }

        /* Add Animation - Zoom in the Modal */
        .modal-content, #caption {
            animation-name: zoom;
            animation-duration: 0.6s;
        }

        @keyframes zoom {
            from {transform:scale(0)}
            to {transform:scale(1)}
        }

        /* The Close Button */
        .close {
            position: absolute;
            top: 65px;
            right: 20%;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }

        .close:hover,
        .close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }

        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px){
            .modal-content {
                width: 100%;
            }
        }
    </style>
@endpush
@push('scripts')
    <script type="text/javascript">

        function scrollingToTop (){
            $(".chat-body").scrollTop(10000000)
        }

        scrollingToTop()

    </script>

    {{--//==============================================--}}



    <script type="text/javascript">
        //======================================================


        const chatItems = [];
        // ChatRoomListener
        const database  = firebase.database();

        markLastMessageAsRead(  {!! request('id')  !!} )


        let currentChatRoomId = '{!! request("id") !!}'
        // Function to set up a listener for a specific chat room
        function setupChatRoomListener(chatRoomId) {

            if ( currentChatRoomId != chatRoomId ) {
                let chat1 = "chats/chat-" + chatRoomId;

                const chatRef1 = database.ref(chat1);
                chatRef1.on('child_added', function (snapshot) {
                    let message = snapshot.val().message;


                    $("#chat-"+chatRoomId).find(".message_last").empty().append(message)

                    $("#chat-"+chatRoomId).removeClass("d-none")


                    $("#chat-"+chatRoomId).addClass("un-read")
                    let newItem = $("#chat-"+chatRoomId)
                    $("#listOfChats").prepend( newItem )

                    console.log("Listener sasas >>> a" )
                    console.log("#chat-"+chatRoomId )
                })

                console.log("Listener " + chatRoomId )
            }  else {
                console.log("nnnnooooooeeeeewwww")
                markLastMessageAsRead(currentChatRoomId)


            }
        }

        // Example usage when switching chat rooms




        // chatItems.forEach(function (item) {
        //     setupChatRoomListener(item);
        // });
        // Initial setup for the first chat room


        // When user switches to another chat room, call the function with the new chat room ID




        //==============================================================


        // Function to mark the last message as read
        function markLastMessageAsRead(chatRoomId = null ) {
            // alert(chatRoomId)
            if ( chatRoomId ) {
                const chatRoomRef = database.ref('chats').child("chat-"+chatRoomId);

                chatRoomRef.limitToLast(1).once('value', (snapshot) => {
                    snapshot.forEach((messageSnapshot) => {

                        const messageId = messageSnapshot.key;

                        chatRoomRef.child(messageId).update({
                            admin_read: true
                        });

                        console.log(">?>?>",  $("#chat-"+chatRoomId) )
                        $("#chat-"+chatRoomId).removeClass("un-read").addClass("qqqqqqqqq")

                        console.log('Last message marked as read in chat room ' + chatRoomId);
                    });
                });
            }

        }


        //======================================================

        var chatRef ="";

        if ( "{!! request('id') !!}" ) {
            let id = "{!! request('id') !!}"
            let chat = "chats/chat-"+id;
            chatRef = database.ref(chat);
            getChatData(id)
        }



        function getChatData(id){

            chatRef.on('child_added', function(snapshot) {
                var text = snapshot.val().message;
                const snapshot1 = snapshot.val();


                console.log("type >>>" , snapshot.val().type)
                if ( snapshot.val().type == 'file' ){
                    text = `<img class='myImg' src='${snapshot.val().message}' width="100" height="100" />`
                }

                var timestamp = firebase.database.ServerValue.TIMESTAMP;
                let adminImage = "https://mragaey.com/kafo/assets/images/logo-sm.png"



                let userImage = @if( $client )
                                    "{{ asset($client->avatar)  }}"
                                @endif

                console.log("message snapshot1" , snapshot1)


                $("#chat-"+id).find(".message_last").empty().append(text)

                let newItem = $("#chat-"+id)
                $("#listOfChats").prepend( newItem )

                var msgContent = "" ;
                if ( snapshot1.sender_id ) {
                    msgContent =  $(`<li class="message-item friend">
                        <img src="${userImage}" class="img-xs rounded-circle sdsd" alt="avatar">
                        <div class="content">
                            <div class="message">
                                <div class="bubble">
                                    <p>
                                        ${text}
                                    </p>
                                </div>
                                <span>${getTimeStamp(snapshot1.timestamp)}</span>
                            </div>
                        </div>
                    </li>`) ;
                } else {
                    msgContent =  $(`<li class="message-item me">
                        <img src="${adminImage}" class="img-xs rounded-circle" alt="avatar">
                        <div class="content">
                            <div class="message">
                                <div class="bubble">
                                    <p>
                                        ${text}
                                    </p>
                                </div>
                                <span>${getTimeStamp(snapshot1.timestamp)}</span>
                            </div>
                        </div>
                    </li>`) ;
                }

                $("ul.messages").append(msgContent) ;


                scrollingToTop()
                //chatBox.scrollTop = chatBox.scrollHeight; // Scroll to the bottom of the chat box
            });
        }


        if (Notification.permission === 'granted') {
            // You have permission to show notifications, proceed to get the FCM token.
            messaging.getToken().then((token) => {
                // Handle the token (store it, send it to your server, etc.).
                console.log('FCM Token:', token);
            }).catch((err) => {
                console.error('Error getting FCM token:', err);
            });
        } else if (Notification.permission === 'blocked') {
            // Notifications are blocked, inform the user to unblock them.
            console.error('Notifications are blocked. Please unblock them to receive messages.');
        } else {
            // Ask the user for notification permission.
            Notification.requestPermission().then((permission) => {
                if (permission === 'granted') {
                    // User granted permission, proceed to get the FCM token.
                    messaging.getToken().then((token) => {
                        // Handle the token.
                        console.log('FCM Token:', token);
                    }).catch((err) => {
                        console.error('Error getting FCM token:', err);
                    });
                } else {
                    // User denied permission, handle accordingly.
                    console.error('Notification permission denied.');
                }
            });
        }

        //===============================

        $("#send__message").click(function () {
            let chatForm = $("#chatForm").val();
            sendMessage( chatForm )

            // =======================
            // = send in firebase
            //=====================

            let route = "{{ route("chat.sendChatMessage") }}";



            axios.post(route, {
                "message" : chatForm,
                "client_id" : "{!! request('id') !!}"
            }).then((response) => {
                console.log("response >> " , response )
                if ( response.data.status == true) {
                    console.log("response.data >> ",response.data)
                    console.log("add Listener to 1")
                }
            })

        })

        function sendMessage( chatForm )
        {
            var timestamp = firebase.database.ServerValue.TIMESTAMP;
            var date = new Date(timestamp * 1000)

            if ( chatForm ) {
                let msgContent =  $(`<li class="message-item friend aaaa">
                        <img src="https://via.placeholder.com/43x43" class="img-xs rounded-circle" alt="avatar">
                        <div class="content">
                            <div class="message">
                                <div class="bubble">
                                    <p>
                                        ${chatForm}
                                    </p>
                                </div>
                                <span> ${date.toLocaleString()} </span>
                            </div>
                        </div>
                    </li>`) ;

                // $("ul.messages").append(chatForm) ;


                chatRef.push({
                    message: chatForm,
                    sender_id: 0,
                    read: true,
                    admin_read:false ,
                    type: "text",
                    timestamp: firebase.database.ServerValue.TIMESTAMP
                });

                $("#chatForm").val('');
                scrollingToTop()

            }
        }

        //=============================

        messaging.getToken().then((token) => {
            if (token) {
                // Send the token to your server to associate it with the user.
                // You can store this token in your database and use it to send messages to this specific user.
                console.log("token",token);
            } else {
                // Show permission request UI if required.
                console.log('No Instance ID token available.');
            }
        }).catch((err) => {
            console.log('An error occurred while retrieving token. ', err);
        });

        // Set up a message listener to handle incoming FCM messages
        messaging.onMessage((payload) => {
            console.log('Message received:', payload);
            // Handle the incoming message and update your UI as needed.
        });

        //=================================


        async function fetchData( chatItems ) {

            console.log(">>?chatItems" , chatItems)
            const chats = [];

            for (const item of chatItems ) {
                const query = firebase.database().ref("chats").child("chat-"+item).orderByChild("timestamp").limitToLast(1);

                try {
                    const snapshot = await query.once("value");
                    snapshot.forEach((message) => {
                        let messageContent = message.val()
                        messageContent.roomChat = item
                        let clientData = findClient( item )
                        messageContent.clientData = clientData
                        chats.push(messageContent); // Assuming 'message' is the key in your chat object
                    });
                } catch (error) {
                    console.error("Error fetching data:", error);
                }
            }

            chats.sort((a, b) => b.timestamp - a.timestamp);

            if ( chats.length > 0 ){
                $("#conversations__title").hide()
            }

            for (const itemChat of chats) {
                let statusClass = "un-read"
                if ( itemChat.admin_read ) {
                    statusClass = "read"
                }
                // else if( itemChat.sender_id == 0 ){
                //     statusClass = "read"
                // }
                let routeChatRoom =  itemChat.roomChat ;
                let message =  sliceText( itemChat.message );
                let titleName =  sliceText( itemChat.clientData.first_name+" "+itemChat.clientData.last_name , 16 );

                let route = "{{ route('chat.index', ':id') }}";
                route = route.replace(':id', routeChatRoom);

                let itemLastMessage = `
                <li class="chat-item ${statusClass} client" id="chat-${itemChat.roomChat}" data-id="${itemChat.roomChat}">
                    <a href="${route}" class="d-flex align-items-center">
                        <figure class="mb-0 ml-2">
                            <img  src="${itemChat.clientData.avatar}" class="img-xs rounded-circle" alt="user">
                        </figure>
                        <div class="d-flex justify-content-between flex-grow border-bottom">
                            <div>
                                <p class="text-body font-weight-bold">${titleName} </p>
                                <p class="text-muted tx-13 message_last">
                                    ${message}
                                </p>
                            </div>
                            <div class="d-flex flex-column align-items-end">
                                <p class="text-muted tx-13 mb-1"> ${getTimeStamp(itemChat.timestamp)}  </p>
                            {{--<div class="badge badge-pill badge-primary ml-auto">5</div>--}}
                            </div>
                        </div>
                    </a>
                </li>`

                console.log(">>>?> clientData :", itemChat.clientData )

                if ( itemChat.clientData.block_status == "unblock" && itemChat.clientData.active == "active" ){
                    $("#listOfChats").append(itemLastMessage);
                }
            }

            return chats ;
        }

        function sliceText(text , countLetter = 20){
            if (text.length > countLetter) {
                text = text.slice(0, countLetter ) + '...';
            }
            return text;

        }


        async function getDataClient( id ) {
            let route = "{{ route("client.getClient") }}";


            let response = await axios.post(route, {  id: id  } ).then((response) => {
                if (response.data.status == true) {
                    messageContent.image = response.data.avater
                    messageContent.name = response.data.name

                    data = response.data
                    console.log("response.data >> ",response.data)
                }
            })

            return response ;
        }


        function created() {
            let p = getRoomChat()

            console.log("pp", p )

        }




        created();

        async function getRoomChat()
        {
            var chatsRef = database.ref("chats");
            try {
                const snapshot = await chatsRef.once("value");
                const chatRooms = [];

                snapshot.forEach((chatSnapshot) => {
                    const chatRoomId = chatSnapshot.key;
                    const chatRoomData = chatSnapshot.val();
                    const chatId = chatRoomId.split("-");
                    // chatItems.push(chatId[1])
                    // chatRooms.push({
                    //     id: chatRoomId,
                    //     data: chatRoomData,
                    //     idd: chatRoomId.split("-")[1]
                    // });


                    chatRooms.push(chatRoomId.split("-")[1] );

                    var chatItems = chatRooms

                });

                console.log("chatRooms >>", chatRooms)


                fetchData(chatRooms)



                chatRooms.forEach((chatRoom) => {
                    console.log("Listener >?>?>", chatRoom)
                    setupChatRoomListener(chatRoom)
                })


                getClientAppendInChats(chatRooms)


                // console.log("Chat rooms:", chatRooms);
                return chatRooms;
            } catch (error) {
                console.error("Error fetching chat rooms:", error);
                throw error; // Rethrow the error for handling in the calling function, if necessary
            }


        }



        function findClient( id ) {

            // let clients = [{id:2, name:"Doe", image:46},{id:15, name:"Fatma", image:89}]
            let clients = {!! $clients !!}

            let client = clients.find(element => element.id == id);
            return client ;
        }


        function getTimeStamp(timestamp) {
            const date = new Date(timestamp);

            const year = date.getFullYear();
            const month = date.getMonth() + 1; // Months are zero-based
            const day = date.getDate();
            const hours = date.getHours();
            const minutes = date.getMinutes();
            const seconds = date.getSeconds();

            const formattedDateTime = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;


            console.log("formattedDateTime >>",formattedDateTime); // Output: 2021-10-14 10:17:38


            return formattedDateTime
        }



        markLastMessageAsRead(  {!! request('id')  !!} )


    // ===========================================

        $(document).ready(function(){
            // Add an input event listener to the search input field
            $("#searchInput").on("input", function() {
                let searchInputLength = $(this).val().length;
                if ( searchInputLength > 0 ) {


                    $("#listOfChats li").each(function() {
                        $(this).removeClass("d-none")
                    })

                    filterSearch()
                }else{
                    $("#listOfChats li.newItem").each(function() {
                        $(this).addClass("d-none")
                    })
                    filterSearch()
                }

            });

            $("input[name='filter']").on("change", function() {
                filterSearch()
            })

            function filterSearch() {
                var searchText = $("#searchInput").val().toLowerCase();
                var filterType = $("input[name='filter']:checked").val();

                console.log( "filterType >>", filterType )

                // Filter the list items based on the search text
                $("#listOfChats li").each(function() {
                    var listItemText = $(this).find('.text-body').text().toLowerCase();
                    var listItemType = ($(this).hasClass(filterType) || filterType === "all");


                    console.log("listItemType>>", listItemType )




                    // If the search text is found in the list item, show it, otherwise hide it
                    if (listItemText.indexOf(searchText) !== -1 && listItemType ) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            }
        });


        function getClientAppendInChats( chatRooms ) {

            let  route = "{{ route("chat.getAtherClient") }}";

            axios.post(route, {
                chatRooms
            }).then((response) => {
                console.log("response >> " , response )
                if ( response.data.status == true) {
                    console.log("response.data >> ",response.data)

                    $("#listOfChats").append(response.data.html)


                    let ids = response.data.ids

                    ids.forEach(function (id) {

                        console.log("LLLLLLos" ,id)
                        setupChatRoomListener(id)
                    });

                    ids.each(function () {
                        setupChatRoomListener($(this))
                    })



                    console.log("add Listener to 1")
                }
            })
        }


        $("#chatForm").on("keydown", function(event) {
            // Check if the pressed key is Enter (key code 13)
            if (event.keyCode === 13) {
                // Prevent the default behavior of the Enter key (preventing a new line)
                event.preventDefault();
                var textareaValue = $(this).val();

                sendMessage( textareaValue )

            }
        });

    </script>


    <script>

        $(document).ready(function(){
            // Get the modal
            var modal = $("#myModal");

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = $(".myImg");
            var modalImg = $("#img01");
            var captionText = $("#caption");

            // img.on("click", function(){
            $(document).on("click",".myImg",function () {

                console.log("?>? class >",  $(this).attr("class") )

                console.log("?>? src >",  $(this).attr("src") )

                // alert(12121) ;
                modal.css("display", "block");
                modalImg.attr("src", $(this).attr("src"));
                captionText.html($(this).attr("alt"));
            });

            // Get the <span> element that closes the modal
            var span = $(".close");

            // When the user clicks on <span> (x), close the modal
            span.on("click", function() {
                modal.css("display", "none");
            });
        });


    </script>


    {{--@include('dashboard.users.script')--}}
@endpush
