@extends('dashboard.layouts.app', ['title' => __('app.titles.users')])
@section('content')

    @push('button')
        @can("users-create")
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                {{__('app.users.add')}}
            </button>
        </div>
        @endcan
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label"> {{__('app.users.name')}} </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="name" value="{{ request('name') }}"/>
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $users ])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.users.first_name')}}</th>
                                <th class="pt-0"> {{__('app.users.last_name')}}</th>
                                <th class="pt-0"> {{__('app.email')}} </th>
                                <th class="pt-0"> {{__('app.phone')}} </th>
                                <th class="pt-0"> {{__('app.role')}}  </th>
                                <th class="pt-0"> {{__('app.status')}}</th>
                                <th class="pt-0"> {{__('app.processes')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($users as $user)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->last_name }}</td>
                                    <td> {{ $user->email }} </td>
                                    <td> {{ $user->phone }} </td>
                                    <td>
                                        @if(!empty($user->getRoleNames()))
                                            @foreach($user->getRoleNames() as $v)
                                                <label class="badge badge-success">{{ $v }}</label>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="active" value="active" class="change_status"
                                                       data-route="{{route("user.updateAxios" , $user->id )}}"
                                                       data-id="{{$user->id}}"
                                                       @if($user->active == 'active')
                                                       checked
                                                       @endif
                                                >
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        @can('users-edit')
                                            <a href="#" data-id="{{$user->id}}" data-name="{{ $user->name }}"
                                               data-email="{{ $user->email }}"
                                               data-last_name="{{ $user->last_name }}"
                                               data-phone="{{ $user->phone }}"
                                               data-roles="{{count( $user->roles()->pluck('id') ) ? $user->roles()->pluck('id')[0] : ""}}"

                                               class="btn btn-primary btn-icon aligner edit">
                                                <i data-feather="edit"></i>
                                            </a>
                                        @endcan

                                        @can("users-delete")
                                        @if( $user->id != auth()->user()->id )
                                            <button type="submit" class="btn btn-danger btn-icon aligner"
                                                    onclick="showSwal('passing-parameter-execute-cancel' , '{{route("user.destroy", $user->id)}}' )" >
                                                <i data-feather="trash"></i>
                                            </button>
                                        @endif
                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="danger text-center"> {{__('app.no_data')}} </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $users->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                name = el.data('name'),
                last_name = el.data('last_name'),
                email = el.data('email'),
                phone = el.data('phone'),
                role = el.data('role'),
                roles = el.data('roles'),
                route = "{{ route('user.update', ':id') }}";
            route = route.replace(':id', id)


            console.log("roles >>" , roles) ;

            $(document).find("#edit_name").val(name);
            $(document).find("#edit_last_name").val(last_name);
            $(document).find("#edit_email").val(email);
            $(document).find("#edit_phone").val(phone);
            $(document).find("#edit_role").val(roles);
            // $(document).find("#role_option-"+roles).attr("selected" ,true );

            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);
        });

    </script>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel">اضف مستخدم</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="add-form" action="{{route('user.store')}}" autocomplete="off">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name" class="col-form-label"> {{__('app.users.first_name')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name" id="add_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_last_name" class="col-form-label"> {{__('app.users.last_name')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="last_name" id="add_last_name">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_last_name" class="col-form-label"> {{__('app.phone')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="phone" id="add_phone">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_password" class="col-form-label"> {{__('app.password')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="password" class="form-control" name="password" autocomplete="false" required id="add_password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_password_confirmation" class="col-form-label">
                                    {{__('app.re_password')}} <span class="required_star">*</span>
                                    :
                                </label>
                            </div>
                            <div class="col-lg-9">
                                <input type="password" class="form-control" autocomplete="false" name="password_confirmation" required
                                       id="add_password_confirmation">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_email" class="col-form-label"> {{__('app.email')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="email" class="form-control" name="email" id="add_email">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_phone" class="col-form-label"> {{__('app.phone')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="phone" id="add_phone">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="create_role" class="col-form-label"> {{__('app.role')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control" id="create_role" name="role_id">
                                    <option value=""> -- {{__('app.choose')}} --</option>
                                    @foreach( $roles as $role )
                                        <option value="{{$role->id}}" > {{$role->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel"> {{__('app.users.update')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="edit-form" action="" autocomplete="off">
                        @method('put')
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_name" class="col-form-label"> {{__('app.users.name')}}  <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name" id="edit_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_last_name" class="col-form-label"> {{__('app.users.last_name')}}  <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="last_name" id="edit_last_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_phone" class="col-form-label"> {{__('app.phone')}}  <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="phone" id="edit_phone">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_password" class="col-form-label"> {{__('app.password')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="password" class="form-control" autocomplete="false" name="password" id="edit_password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_confirmation_password" class="col-form-label">
                                    {{__('app.re_password')}} <span class="required_star">*</span> :
                                </label>
                            </div>
                            <div class="col-lg-9">
                                <input type="password" class="form-control" autocomplete="false" name="password_confirmation"
                                       id="edit_confirmation_password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_email" class="col-form-label"> {{__('app.email')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="email" class="form-control" name="email" id="edit_email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_role" class="col-form-label">{{__('app.role')}}</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control" id="edit_role" name="role_id">
                                    <option style="display: none" value=""> -- {{__('app.choose')}} --</option>
                                    @foreach( $roles as $role )
                                        <option value="{{$role->id}}"
                                                id="role_option-{{$role->id}}"
                                        >
                                            {{$role->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('dashboard.users.script')
@endpush
