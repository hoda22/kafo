<script>

    jQuery.validator.addMethod("isEmail", function(sEmail) {

        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            return false;
        }
    })

    $("#add-form").validate({
        rules: {
            name: {
                required: true,
            },
            last_name :{
                required: true,
            },

            password :{
                required: true,
                minlength: 8
            },


            phone :{
                required: true,
            },

            password_confirmation :{
                required: true,
                equalTo: "#add_password"
            },

            email :{
                required: true,
                email: true,
                isEmail:true
            },

            role_id :{
                required: true,
            },

            action: "required"
        },
        messages: {
            name: {
                required: "{{__('app.validation.field_name_required')}}"
            },

            phone: {
                required: "{{__('app.validation.field_phone_required')}}"
            },
            last_name: {
                required: "{{__('app.validation.field_name_en_required')}}"
            },

            email: {
                required: "{{__('app.validation.field_email_required')}}",
                email: "{{__('app.validation.field_email_email')}}",
                isEmail:"{{__('app.validation.field_email_email')}}",
            },

            password: {
                required: "{{__('app.validation.field_password_required')}}" ,
                minlength: "{{__('app.validation.field_password_min')}}" ,
            },
            password_confirmation: {
                required: "{{__('app.validation.field_password_confirmation_required')}}",
                equalTo: "{{__('app.validation.field_equalTo')}}" ,
            },

            role_id: {
                required: "{{__('app.validation.field_role_id_required')}}"
            },

            action: "Please provide some data"
        }
    });


    $("#edit-form").validate({
        rules: {
            name: {
                required: true,
            },
            last_name :{
                required: true,
            },

            password :{
                minlength: 8
            },
            phone :{
                required: true,
            },

            password_confirmation :{
                equalTo: "#edit_password"
            },

            email :{
                required: true,
                email: true,
                isEmail:true
            },

            role_id :{
                required: true,
            },
            action: "required"
        },
        messages: {
            name: {
                required: "{{__('app.validation.field_name_required')}}"
            },
            last_name: {
                required: "{{__('app.validation.field_name_en_required')}}"
            },
            phone: {
                required: "{{__('app.validation.field_phone_required')}}"
            },
            email: {
                required: "{{__('app.validation.field_email_required')}}",
                email: "{{__('app.validation.field_email_email')}}",
                isEmail:"{{__('app.validation.field_email_email')}}",
            },

            password: {
                required: "{{__('app.validation.field_password_required')}}" ,
                minlength: "{{__('app.validation.field_password_min')}}" ,
            },
            password_confirmation: {
                required: "{{__('app.validation.field_password_confirmation_required')}}",
                equalTo: "{{__('app.validation.field_equalTo')}}" ,
            },

            role_id: {
                required: "{{__('app.validation.field_role_id_required')}}"
            },

            action: "Please provide some data"
        }
    });
</script>