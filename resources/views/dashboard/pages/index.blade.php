@extends('dashboard.layouts.app', ['title' => __('app.titles.pages')])
@section('content')
    {{--@push('button')--}}
        {{--@can("notifications-create")--}}
        {{--<div class="d-flex align-items-center flex-wrap text-nowrap">--}}
            {{--<a href="{{route("pages.create")}}" class="btn btn-primary" >--}}
                {{--{{__('app.pages.add')}}--}}
            {{--</a>--}}
        {{--</div>--}}
        {{--@endcan--}}
    {{--@endpush--}}
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.pages.name')}} </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="name" value="{{ request('name') }}"/>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.pages.page_title')}} </th>
                                {{--<th class="pt-0"> {{__('app.name_en')}} </th>--}}
                                {{--<th class="pt-0"> {{__('app.status')}} </th>--}}
                                {{--<th class="pt-0"> {{__('app.ordering')}} </th>--}}
                                <th class="pt-0"> {{__('app.processes')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item )
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td> {{optional( $item->translate('ar') )->name }} </td>
                                    {{--<td> {{ optional( $item->translate('en') )->name  }} </td>--}}

                                    {{--<td>--}}
                                        {{--<span class="badge badge-lg badge-{{$item->active_status['className']}}">--}}
                                            {{--{{$item->active_status['text']}}--}}
                                        {{--</span>--}}
                                    {{--</td>--}}
                                    {{--<td> {{ $item->ordering  }} </td>--}}
                                    <td>
                                        @can("pages-edit")
                                        <a href="{{route("pages.edit", $item->id)}}" data-id="{{$item->id}}"
                                           class="btn btn-primary btn-icon aligner ">
                                            <i data-feather="edit"></i>
                                        </a>
                                        @endcan
                                        {{--@can("pages-delete")--}}
                                        {{--<button type="submit" class="btn btn-danger btn-icon aligner"--}}
                                                {{--onclick="showSwal('passing-parameter-execute-cancel' , '{{route("pages.destroy", $item->id)}}' )" >--}}
                                            {{--<i data-feather="trash"></i>--}}
                                        {{--</button>--}}
                                        {{--@endcan--}}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
@endpush
