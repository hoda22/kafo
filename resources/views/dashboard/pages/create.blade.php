@extends('dashboard.layouts.app', ['title' => 'أضف صفحه تعريفيه'])
@section('content')
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="{{route("pages.store")}}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_name" class="col-form-label">اسم الصفحه :</label>
                            </div>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" name="name" required id="add_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="name_en" class="col-form-label">اسم الصفحه - En :</label>
                            </div>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" name="name_en" required id="name_en">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="ordering" class="col-form-label">الترتيب :</label>
                            </div>
                            <div class="col-lg-3">
                                <input type="number" class="form-control" name="ordering" required id="ordering">
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="description" class="col-form-label">محتوي الصفحه :</label>
                            </div>
                            <div class="col-lg-10">
                                <textarea class="form-control tinymceExample" id="description" rows="6" name="description"></textarea>
                            </div>
                        </div>




                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_page" class="col-form-label">محتوي الصفحه En :</label>
                            </div>
                            <div class="col-lg-10">
                                <textarea class="form-control tinymceExample" rows="6" name="description_en"></textarea>
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_active" class="col-form-label">  حاله الظهور :</label>
                            </div>
                            <div class="col-lg-10">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> حفظ </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('/') }}assets/vendors/tinymce/tinymce.min.js"></script>
    <script src="{{ asset('/') }}assets/js/tinymce.js"></script>
    <script>
        if ($(".tinymceExample").length) {
            tinymce.init({
                selector: '.tinymceExample',
                height: 300,
                theme: 'silver',
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                ],
                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
                image_advtab: true,
                templates: [{
                    title: 'Test template 1',
                    content: 'Test 1'
                },
                    {
                        title: 'Test template 2',
                        content: 'Test 2'
                    }
                ],
                content_css: []
            });
        }
    </script>

@endpush
