@extends('dashboard.layouts.app', ['title' => __('app.welcome_dashboard')])
@section('content')
asdasdadas

    @can('performance-panel-settings')
    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">
                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-baseline">
                                <h5 class="card-title mb-0"> {{__('app.titles.cities')}} </h5>
                                <div class="dropdown mb-2">
                                    <i class="link-icon" data-feather="map"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-12 col-xl-5">
                                    <h3 class="mb-2">{{$citiesCount}}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-baseline">
                                <h5 class="card-title mb-0"> sdasdsad </h5>
                                <div class="dropdown mb-2">
                                    <i class="link-icon" data-feather="command"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-12 col-xl-5">
                                    <h3 class="mb-2">{{$categoriesCount}}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-baseline">
                                <h5 class="card-title mb-0"> التجار </h5>
                                <div class="dropdown mb-2">
                                    <i class="link-icon" data-feather="user-check"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-12 col-xl-5">
                                    <h3 class="mb-2">
                                        {{$providersCount}}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>







                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-baseline">
                                <h5 class="card-title mb-0"> العملاء </h5>
                                <div class="dropdown mb-2">
                                    <span class="link-title"> العملاء </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-12 col-xl-5">
                                    <h3 class="mb-2">{{$clientsCount}}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-xl-12 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title"> العملاء و التجار </h6>
                            <div id="morrisArea"></div>
                        </div>
                    </div>
                </div>


                <div class="page-content">


                    <div class="row" hidden>
                        <div class="col-xl-6 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h6 class="card-title">Line chart</h6>
                                    <div id="morrisLine"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row" hidden>
                        <div class="col-xl-6 stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h6 class="card-title">Area chart</h6>
                                    <div id="morrisArea"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h6 class="card-title">Donut chart</h6>
                                    <div id="morrisDonut"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    @endcan

@endsection
@push('scripts')
    <script src="{{ asset('assets/vendors/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/morris.js/morris.min.js') }}"></script>
    <script src="{{ asset('assets/js/morris.js') }}"></script>

@endpush
