@extends('dashboard.layouts.app', ['title' => __('app.titles.reportProviders')])
@section('content')
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3 ">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="place" class="col-sm-1 col-form-label"> {{__('app.from')}} </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control date" name="from" value="{{ request('from') }}"/>
                            </div>

                            <label for="place" class="col-sm-1 col-form-label"> {{__('app.to')}} </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control date" name="to" value="{{ request('to') }}"/>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <?php
                $dataExcel=[];
                ?>
                @can("export-report-provider")
                    <?php
                    $dataExcel = [
                        "excelRoute" => "providers-export" ,
                        "filter" => [
                            "from" => request("from") ,
                            "to" => request("to") ,
                        ]
                    ]
                    ?>
                @endcan

                @include("dashboard.includes.info_page",['items' => $items] , $dataExcel )
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.full_name')}} </th>
                                <th class="pt-0"> {{__('app.email')}} </th>
                                <th class="pt-0"> {{__('app.status')}} </th>
                                <th class="pt-0"> {{__('app.hoursNum')}} </th>
                                <th class="pt-0"> {{__('app.ordersNum')}} </th>
                                <th class="pt-0"> {{__('app.create_at')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $items as $item )
                                <tr>
                                    <td> {{ $loop->iteration }}</td>
                                    <td> {{ $item->full_name }} </td>
                                    <td> {{ $item->email }} </td>
                                    <td
                                            @if( $item->block_status == "block" )
                                            class="alert alert-danger"
                                            @else
                                            class="alert alert-success"
                                            @endif
                                    >
                                        @if( $item->block_status == "block" )
                                            {{ __('app.block')}}
                                        @else
                                            {{ __('app.unblock')}}
                                        @endif
                                    </td>

                                    <td> {{ $item->providerHoursOrders }} </td>
                                    <td> {{ count($item->providerOrders)  }} </td>
                                    <td> {{ $item->created_at }} </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-12 stretch-card mt-3">
            <div class="card">
                <div class="card-body">
                    {{--<h6 class="card-title"> {{__('app.providers&clients')}}  </h6>--}}
                    <div id="chart"></div>
                </div>
            </div>
        </div>

        <div class="col-xl-6 grid-margin stretch-card mt-4">
            <div class="card">
                <div class="card-body pb-1">
                    {{--<h6 class="card-title mb-3"> {{__('app.titles.orders')}} </h6>--}}
                    <div id="apexPie"></div>
                </div>
            </div>
        </div>


    </div>
@endsection
@push('scripts')

    <script>
                {{--Chart--}}
        var chartData = {
                series: [{
                    name: 'Column Chart',
                    data: {!! $statisticsClient !!}
                }],
                chart: {
                    height: 350,
                    type: 'bar', // Use 'bar' for column chart
                },
                plotOptions: {
                    bar: {
                        horizontal: false,
                        columnWidth: '55%',
                        endingShape: 'rounded' // You can use 'flat' or 'rounded'
                    },
                },
                dataLabels: {
                    enabled: false
                },
                title: {
                    // text: 'Column Chart Example',
                    align: 'center'
                },
                xaxis: {
                    categories: ['01/01/2023', '02/01/2023','03/01/2023','04/01/2023','05/01/2023','06/01/2023','07/01/2023','08/01/2023','09/01/2023','10/01/2023','11/01/2023','11/01/2023','12/01/2023'],
                }
            };

        // Create the chart
        var chart = new ApexCharts(document.getElementById('chart'), chartData);
        chart.render();

    </script>


    <script>

        // Apex Pie chart end
        var options = {
            chart: {
                height: 300,
                type: "pie"
            },
            colors: [ "#FFD6E0" , "#CFF1DE" ],
            legend: {
                position: 'top',
                horizontalAlign: 'center'
            },
            labels:["{{__('app.block')}}" ,"{{__('app.unblock')}}"] ,
            stroke: {
                colors: ['rgba(0,0,0,0)']
            },
            dataLabels: {
                enabled: false
            },
            series: [{!! $blockProviders !!}, {!! $unBlockProviders !!} ]
        };

        var chart = new ApexCharts(document.querySelector("#apexPie"), options);

        chart.render();

    </script>

    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                name = el.data('name'),
                name_en = el.data('name_en'),
                department = el.data('department_id'),
                active = el.data('active'),
                ac_type = el.data('ac_type'),
                route = "{{ route('clients.update', ':id') }}";
            route = route.replace(':id', id);

            $(document).find("#edit_name").val(name);
            $(document).find("#edit_name_en").val(name_en);
            $(document).find("#edit_ac_type").val(ac_type);

            if ( active == 'active' ){
                $(document).find("#edit_active").prop('checked',true);
            }else{
                $(document).find("#edit_active").prop('checked',false);
            }
            $(document).find("#edit_department").val(department);
            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);
        });
    </script>

@endpush


