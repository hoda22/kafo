@extends('dashboard.layouts.app', ['title' => __('app.titles.reportServices')])
@section('content')
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3 ">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="place" class="col-sm-1 col-form-label"> {{__('app.from')}} </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control date" name="from" value="{{ request('from') }}"/>
                            </div>

                            <label for="place" class="col-sm-1 col-form-label"> {{__('app.to')}} </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control date" name="to" value="{{ request('to') }}"/>
                            </div>

                            <label for="place" class="col-sm-1 col-form-label"> {{__('app.service')}} </label>
                            <div class="col-sm-3">
                                <select class="form-control select2" name="service_id">
                                    <option value=""> -- {{__('app.choose')}} --  </option>
                                    @foreach( $sup_services as $service )
                                    <option value="{{$service->id}}"
                                        {{request('service_id') == $service->id ? "selected" :""}}
                                    > {{$service->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                                <div class="col-sm-3 offset-md-2">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items] , [
                        "excelRoute" => "services-export-reports" ,
                        "filter" => [
                            "from" => request("from") ,
                            "to" => request("to") ,
                        ]
                    ])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0"> {{__('app.order_id')}} </th>
                                <th class="pt-0"> {{__('app.status')}} </th>
                                <th class="pt-0"> {{__('app.category_')}} </th>
                                <th class="pt-0"> {{__('app.sup_category')}} </th>
                                <th class="pt-0"> {{__('app.invoiced_amount')}} </th>
                                <th class="pt-0"> {{__('app.assigned_agent')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $items as $item )
                                <tr>
                                    <td> {{ $item->id }}</td>
                                    <td
                                        @if( $item->status == 7 || $item->status == 10 )
                                            class="alert alert-success"
                                        @else
                                            class="alert alert-danger"
                                        @endif
                                    >
                                        @if( $item->status == 7 || $item->status == 10 )
                                            {{ __('app.close')}}
                                        @else
                                            {{ __('app.open')}}
                                        @endif
                                    </td>
                                    <td>
                                        @foreach( $item->orderDetails as $order_details )
                                            {{optional($order_details->service_parent)->name}}
                                            @if(!$loop->last)
                                                -
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach( $item->orderDetails as $order_details )
                                            {{optional($order_details->service_item)->name}}
                                            @if(!$loop->last)
                                            -
                                            @endif
                                        @endforeach
                                    </td>

                                    <td> {{ $item->basic_total }} {{__('app.SAR')}} </td>
                                    <td> {{ optional( $item->provider )->full_name }} </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-12 stretch-card mt-3">
            <div class="card">
                <div class="card-body">
                    {{--<h6 class="card-title"> {{__('app.providers&clients')}}  </h6>--}}
                    <div id="chart"></div>
                </div>
            </div>
        </div>

        <div class="col-xl-6 grid-margin stretch-card mt-4">
            <div class="card">
                <div class="card-body pb-1">
                    {{--<h6 class="card-title mb-3"> {{__('app.titles.orders')}} </h6>--}}
                    <div id="apexPie"></div>
                </div>
            </div>
        </div>

    </div>
@endsection
@push('scripts')

    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                name = el.data('name'),
                name_en = el.data('name_en'),
                department = el.data('department_id'),
                active = el.data('active'),
                ac_type = el.data('ac_type'),
                route = "{{ route('clients.update', ':id') }}";
            route = route.replace(':id', id);

            $(document).find("#edit_name").val(name);
            $(document).find("#edit_name_en").val(name_en);
            $(document).find("#edit_ac_type").val(ac_type);

            if ( active == 'active' ){
                $(document).find("#edit_active").prop('checked',true);
            }else{
                $(document).find("#edit_active").prop('checked',false);
            }
            $(document).find("#edit_department").val(department);
            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);
        });
    </script>

    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

    <script>

        // Apex Pie chart end
        var options = {
            chart: {
                height: 300,
                type: "pie"
            },
            colors: ["#f77eb9", "#7ee5e5","#fbbc06"],
            legend: {
                position: 'top',
                horizontalAlign: 'center'
            },
            labels:["{{__('app.finished_orders')}}" ,"{{__('app.requests_are_progress')}}" ,"{{__('app.canceled_orders')}}"] ,
            stroke: {
                colors: ['rgba(0,0,0,0)']
            },
            dataLabels: {
                enabled: false
            },
            series: [{!! $orderWaiting !!}, {!! $orderCurrent !!}, {!! $orderDone !!}]
        };

        var chart = new ApexCharts(document.querySelector("#apexPie"), options);

        chart.render();

    </script>

    {{--Chart--}}
    <script>
        var chartData = {
            series: [{
                name: 'Column Chart',
                data: {!! $serviceCount !!}
            }],
            chart: {
                height: 350,
                type: 'bar', // Use 'bar' for column chart
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded' // You can use 'flat' or 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            title: {
                // text: 'Column Chart Example',
                align: 'center'
            },
            xaxis: {
                categories: {!! $sup_services_names !!},
            }
        };

        // Create the chart
        var chart = new ApexCharts(document.getElementById('chart'), chartData);
        chart.render();
    </script>

@endpush


