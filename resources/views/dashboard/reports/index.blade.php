@extends('dashboard.layouts.app', ['title' =>__('app.titles.reports')])
@section('content')

    <div class="row reports-mb">

        @can("report-clients")
        <div class="col-lg-3 col-3 stretch-card">
            <div class="card card-report text-center">
                <a href="{{route('report-clients')}}">
                    <i class="link-icon" data-feather="user" style="margin:auto"></i>
                    <h4> {{__('app.titles.reportClients')}} </h4>
                </a>
            </div>
        </div>
        @endcan

        @can("report-services")
        <div class="col-lg-3 col-3 stretch-card">
            <div class="card card-report text-center">
                <a href="{{route('report-services')}}">
                    <i class="link-icon" data-feather="server" style="margin:auto"></i>
                    <h4> {{__('app.titles.reportServices')}} </h4>
                </a>
            </div>
        </div>
        @endcan

        @can("report-request")
        <div class="col-lg-3 col-3 stretch-card">
            <div class="card card-report text-center">
                <a href="{{route('report-requests')}}">
                    <i class="link-icon" data-feather="shopping-bag" style="margin:auto"></i>
                    <h4> {{__('app.titles.reportRequests')}} </h4>
                </a>
            </div>
        </div>
        @endcan

        @can("report-finance")
        <div class="col-lg-3 col-3 stretch-card">
            <div class="card card-report text-center">
                <a href="{{route('report-finance')}}">
                    <i class="link-icon" data-feather="dollar-sign" style="margin:auto"></i>
                    <h4> {{__('app.titles.reportFinance')}} </h4>
                </a>
            </div>
        </div>
        @endcan

        @can("report-provider")
        <div class="col-lg-3 col-3 stretch-card">
            <div class="card card-report text-center">
                <a href="{{route('report-provider')}}">
                    <i class="link-icon" data-feather="user-check" style="margin:auto"></i>
                    <h4> {{__('app.titles.reportProviders')}} </h4>
                </a>
            </div>
        </div>
        @endcan
        @can("report-spare-pats")
        <div class="col-lg-3 col-3 stretch-card">
            <div class="card card-report text-center">
                <a href="{{route('report-sparePart')}}">
                    <i class="link-icon" data-feather="tool" style="margin:auto"></i>
                    <h4> {{__('app.titles.reportSpareParts')}} </h4>
                </a>
            </div>
        </div>
        @endcan
    </div>
@endsection
@push("styles")
    <style>
        .card-report > a{
            color: #000;
        }

        .card-report > a svg{
            margin-bottom: 20px;
        }

        .card-report > a svg {
            margin-bottom: 20px !important;
            font-size: 35px;
            width: 44px;
            height: 41px;
        }
    </style>
@endpush