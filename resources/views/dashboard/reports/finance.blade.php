@extends('dashboard.layouts.app', ['title' => __('app.titles.reportFinance')])
@section('content')
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3 ">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.from')}} </label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control date" name="from" value="{{ request('from') }}"/>
                            </div>

                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.to')}} </label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control date" name="to" value="{{ request('to') }}"/>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <?php
                $dataExcel=[];
                ?>
                @can("export-report-finance")
                    dddddddddddd
                    <?php
                    $dataExcel = [
                        "excelRoute" => "finance-export" ,
                        "filter" => [
                            "from" => request("from") ,
                            "to" => request("to") ,
                        ]
                    ]
                    ?>
                @endcan
                @include("dashboard.includes.info_page",['items' => $items] , $dataExcel)
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.service_name')}} </th>
                                <th class="pt-0"> {{__('app.client_name')}} </th>
                                <th class="pt-0"> {{__('app.provider_name')}} </th>
                                <th class="pt-0"> {{__('app.price')}} </th>
                                <th class="pt-0"> {{__('app.service_date')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $items as $item )
                                <tr>
                                    <td> {{ $loop->iteration }}</td>
                                    <td> {{ optional( $item->service )->name }} </td>
                                    <td> {{ optional( $item->client )->full_name }} </td>
                                    <td> {{ optional( $item->provider )->full_name }} </td>
                                    <td> {{ $item->price }} </td>
                                    <td> {{ $item->service_date }} </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                name = el.data('name'),
                name_en = el.data('name_en'),
                department = el.data('department_id'),
                active = el.data('active'),
                ac_type = el.data('ac_type'),
                route = "{{ route('clients.update', ':id') }}";
            route = route.replace(':id', id);

            $(document).find("#edit_name").val(name);
            $(document).find("#edit_name_en").val(name_en);
            $(document).find("#edit_ac_type").val(ac_type);

            if ( active == 'active' ){
                $(document).find("#edit_active").prop('checked',true);
            }else{
                $(document).find("#edit_active").prop('checked',false);
            }
            $(document).find("#edit_department").val(department);
            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);
        });
    </script>

@endpush


