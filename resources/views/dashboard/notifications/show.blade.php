@extends('dashboard.layouts.app', ['title' => __("app.titles.showNotifications")])
@section('content')

    @push('button')
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <a href="{{route('notifications.index')}}" class="btn btn-primary">
                <i data-feather="skip-back"></i>
            </a>
        </div>
    @endpush


    <div class="row">
        <div class="col-lg-12 col-xl-12 ">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.notification')}} </th>
                                <th class="pt-0"> المرسل له </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($item->notificationPostItems as $item_ )
                                <tr>
                                    <td> {{ $loop->iteration }}</td>
                                    <td> {{ $item->text }} </td>
                                    <td>
                                        {{optional($item_->client)->full_name}}

                                    </td>


                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="danger text-center"> {{__('app.no_data')}} </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {{--{!! $items->appends(request()->query())->render() !!}--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push("styles")
    <style>
            /*.resize_section .col-md-2 {*/
                /*max-width: 11%;!**!*/
            /*}*/

            /*.resize_section .col-md-4 {*/
                /*max-width: 46%;*/
            /*}*/
    </style>
@endpush
