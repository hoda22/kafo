@extends('dashboard.layouts.app', ['title' => __('app.titles.notifications')])
@section('content')
    @push('button')
        @can("notifications-create")
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                {{__('app.notifications.add')}}
            </button>
        </div>
        @endcan
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.category_type')}} </label>
                            <div class="col-sm-3">
                                <select class="form-control" >
                                    <option value="all"> {{__('app.all')}} </option>
                                    <option value="clients"> {{__('app.clients_')}} </option>
                                    <option value="providers">الفنيين</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items ])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.notification')}} </th>
                                <th class="pt-0"> {{__('app.category_type')}} </th>
                                <th class="pt-0"> {{__('app.posting_time')}} </th>
                                @can("notification-show")
                                <th class="pt-0"> {{__('app.processes')}}</th>
                                @endcan
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item )
                                <tr>
                                    <td> {{ $loop->iteration }}</td>
                                    <td> {{ $item->text }} </td>
                                    <td>
                                        @if( $item->category == 'all' )
                                            {{__('app.all')}}
                                        @elseif( $item->category == 'clients' )
                                            {{__('app.clients_')}}
                                        @else
                                            {{__('app.providers_')}}
                                        @endif
                                    </td>
                                    <td >

                                        @if( $item->set_at )

                                        <span id="time-{{$item->id}}"> </span>

                                        <script>
                                            var utcDate = new Date('{{ $item->set_at }}' + " UTC");
                                            var localDate = new Date(utcDate.toLocaleString());

                                           // var dateString = "Sat Nov 25 2023 13:28:38 GMT+0200 (Eastern European Standard Time)";

                                            // Create a Date object from the string
                                            var dateObject = new Date(localDate);
                                            // Extract date components
                                            var day = dateObject.getDate();
                                            var month = dateObject.getMonth() + 1; // Months are zero-based
                                            var year = dateObject.getFullYear();
                                            var hours = dateObject.getHours();
                                            var minutes = dateObject.getMinutes();
                                            var seconds = dateObject.getSeconds();

                                            // Format the date
                                            var formattedDate = `${day}-${month}-${year} ${hours}:${minutes}:${seconds}`;

                                            console.log("formattedDate >> " ,formattedDate);
                                            console.log("LLLL" , "{{ $item->set_at }}" , "--", localDate)

                                            {{--let doc = document.getElementById("time-"+{!! $item->id !!})--}}



                                            let d = document.getElementById('time-{{$item->id}}')
                                            d.innerText = formattedDate;
                                        </script>
                                        @endif
                                    </td>
                                    <td>
                                        @can("notification-show")
                                        <a  class="btn btn-success btn-icon aligner" href="{{route("notifications.show", $item->id)}}">
                                            <i data-feather="eye"></i>
                                        </a>
                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="danger text-center"> {{__('app.no_data')}} </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                name = el.data('name'),
                name_en = el.data('name_en'),
                ordering = el.data('ordering'),
                route = "{{ route('cities.update', ':id') }}";
            route = route.replace(':id', id);
            $(document).find("#edit_name").val(name);
            $(document).find("#edit_name_en").val(name_en);
            $(document).find("#edit_ordering").val(ordering);
            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);
        });
    </script>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel"> {{__('app.notifications.add')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route("notifications.store")}}" id="add-form">
                        @csrf

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_category" class="col-form-label"> {{__('app.select_category')}}  :</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control" name="category" id="category">
                                    <option value="all"> {{__('app.all')}} </option>
                                    <option value="clients"> العملاء </option>
                                    <option value="providers"> الفنيين </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_category" class="col-form-label"> {{__('app.how_send')}}  :</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control" name="how_send" id="how_send">
                                    <option value="notice"> إشعار </option>
                                    <option value="email"> {{__('app.email')}} </option>
                                    <option value="sms"> رسالة sms </option>
                                </select>
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_text" class="col-form-label">{{__('app.text')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <textarea class="form-control" rows="4" name="text" id="add_text"></textarea>
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.notifications.script')


    <script>

        const utcDate = new Date('2023-11-25T14:00:00');
        const browserTimezoneOffset = new Date().getTimezoneOffset();
        const browserTimezoneTimestamp = utcDate.getTime() + (browserTimezoneOffset * 60 * 1000);
        const browserTimezoneDate = new Date(browserTimezoneTimestamp);
        console.log( ">?>?>?"+browserTimezoneDate ) ;

    </script>
@endpush
