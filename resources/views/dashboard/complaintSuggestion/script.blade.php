<script>

    jQuery.validator.addMethod("extension", function (value, element, param) {
        param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif";
        return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
    });


    $("#add-form").validate({
        rules: {
            ordering :{
                required: true,
            },
            src :{
                required: true,
                extension: "png|jpeg|jpg|gif"
            },
            action: "required"
        },
        messages: {
            ordering: {
                required: "{{__('app.validation.field_ordering_required')}}"
            },

            src: {
                required: "{{__('app.validation.field_src_required')}}",
                extension:"{{__('app.validation.field_src_extension')}}"
            },

            action: "Please provide some data"
        }
    });


    $("#edit-form").validate({
        rules: {
            ordering :{
                required: true,
            },

            src :{
                extension: "png|jpeg|jpg|gif"
            },
            action: "required"
        },
        messages: {
            ordering: {
                required: "{{__('app.validation.field_ordering_required')}}"
            },

            src: {
                required: "{{__('app.validation.field_src_required')}}",
                extension:"{{__('app.validation.field_src_extension')}}"
            },


            action: "Please provide some data"
        }
    });
</script>