@extends('dashboard.layouts.app', ['title' => __('app.titles.complaints-suggestions')])
@section('content')

    <div class="row">

        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items ])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table-custome table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0 text-center" style="width: 15px">#</th>
                                <th class="pt-0 text-center">{{__('app.client_name')}}</th>
                                <th class="pt-0" style="width: 60%"> {{__('app.message')}} </th>
                                <th class="pt-0 text-center" style="width: 15%"> {{__('app.type')}} </th>
                                <th class="pt-0" style="width: 10%"> {{__('app.replay')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item)
                                <tr class="
                                    @if( $item->type == "complaint" )
                                        table-danger
                                    @endif
                                        "
                                >
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td class="text-center">{{ optional($item->client)->full_name }}</td>

                                    <td>
                                        {{$item->content}}
                                    </td>

                                    <td class="text-center">
                                        @if( $item->type == 'suggestion' )
                                            {{__('app.suggestion')}}
                                        @else
                                            {{__('app.complaint')}}
                                        @endif
                                    </td>
                                    <td>
                                        @if( $item->type == "complaint" )
                                            @if( !$item->replay_id )
                                                <a href="#"
                                                   class="btn btn-primary btn-icon aligner replay"
                                                   data-id="{{$item->id}}"
                                                   data-route="{{route("complaintsSuggestion.replay",$item->id)}}"
                                                >
                                                    <i data-feather="message-square"></i>
                                                </a>
                                            @else
                                                <a href="#" data-id="{{$item->id}}" data-answer="{{optional($item->answer)->content}}"
                                                   data-toggle="modal"

                                                   data-route="{{route("complaintsSuggestion.replay",$item->id)}}"
                                                   class="btn btn-success btn-icon aligner edit">
                                                    <i data-feather="eye"></i>
                                                </a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="danger text-center">  {{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

    <div class="modal fade" id="createModal" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-12" style="font-size: 16px; margin-bottom: 10px"> {{__('app.replay_management')}} : </label>
                        <div class="col-lg-12">

                            <textarea class="form-control" rows="7" readonly id="answer_replay"
                                style="line-height: 1.5"
                            ></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="replayModal" tabindex="-1" role="dialog" aria-labelledby="replayModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">{{__('app.complaints_suggestions.title_replay')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="edit-form" action="" enctype="multipart/form-data">
                        @csrf

                        <input type="hidden" id="replay_id" name="replay_id">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <textarea class="form-control" rows="4" name="content" id="answer"
                                          style="line-height: 1.5"
                                ></textarea>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 ">
                                <button type="submit" class="btn btn-primary">{{__('app.save')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('dashboard.sliders.script')


    <script>
        $(".edit").click(function (e) {
            e.preventDefault();
            let id = $(this).data("id")
            let answer = $(this).data("answer")
            $("#answer_replay").text(answer)

            $("#createModal").modal("show");
        });

        $(".replay").click(function (e) {
            e.preventDefault();
            $("#answer").text("");
            $("#answer").val("");


            let id = $(this).data("id"),
                route = $(this).data("route");

            $("#replay_id").val(id)

            console.log("route >> ",route)
            $("#edit-form").attr("action",route) ;
            $("#replayModal").modal("show");
        });


    </script>
@endpush
