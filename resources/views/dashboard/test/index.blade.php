@extends('dashboard.layouts.app', ['title' => ""])
@section('content')

    <div class="row">


        <div class="form-group">
            <input type="hidden" value=""  name="zone_polygon" id="zone_polygon"/>
            <label  class="col-sm-2 control-label">{{__('translation.map')}}</label>
            <div class="col-sm-10" id="map_" style="height:500px;width:900px;">
            </div>we
        </div>
        <div class="box-footer">
            <button type="button" onclick="RemoveZoneFromMap();" class="btn btn-info pull-right">{{__('translation.RemovwZoneFromMap')}}</button>
        </div>
        <script>

            var drawingManager;
            var CurrentShape;
            var bermudaTriangle;
            var map ;
            var tri ;
            function initMap() {
                // map
                var polygonArray = [];
                map = new google.maps.Map(document.getElementById('map_'), {
                    zoom: 11,
                    center: {lat: 31.120676, lng: 31.356298},
                    map: map
                });
                //drawingManager
                drawingManager = new google.maps.drawing.DrawingManager({
                    drawingMode: google.maps.drawing.OverlayType.MARKER,
                    drawingControl: true,
                    drawingControlOptions: {
                        position: google.maps.ControlPosition.TOP_CENTER,
                        drawingModes: ['polygon']
                    },
                    markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
                    circleOptions: {
                        fillColor: '#ffff00',
                        fillOpacity: 1,
                        strokeWeight: 5,
                        clickable: false,
                        editable: true,
                        zIndex: 1
                    }
                });
                drawingManager.setMap(map);
                //action on drawing
                google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
                    drawingManager.setMap(null);
                    CurrentShape = event.overlay;

                    Bounds = event.overlay.getPath().getArray();
                    newpoly = "";
                    MyList = [];
                    BoundsList = null;
                    Radius = 0;
                    LatCenter = 0;
                    LngCenter = 0;
                    BoundsList = Bounds;
                    for (var i = 0; i < BoundsList.length; i++) {
                        debugger;
                        var l = BoundsList[i].lat();
                        var g = BoundsList[i].lng();
                        var myObj = l + " " + g;
                        MyList.push(myObj);
                        newpoly = 'POLYGON((' + MyList.toString() + '))';
                    }
                    //Added
                    var NewmyObj = MyList[0];
                    MyList.push(NewmyObj);
                    newpoly = 'POLYGON((' + MyList.toString() + '))';
                    document.getElementById("zone_polygon").value=newpoly;
                });

            }
            function RemoveZoneFromMap(){
                drawingManager.setMap(map);
                if(CurrentShape!=null){
                    CurrentShape.setMap(null);
                }
                document.getElementById("zone_polygon").value="";
            }
        </script>


    </div>
@endsection

@push("styles")

@endpush
@push('scripts')
    <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBk6k6N7Djg5N0JMg5o__suGw9dwO-B9V8&libraries=drawing&callback=initMap"></script>
@endpush
