<script src="{{ asset('assets/vendors/core/core.js') }}"></script>

<script src="{{ asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/vendors/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('assets/vendors/progressbar.js/progressbar.min.js') }}"></script>
<script src="{{ asset('assets/vendors/feather-icons/feather.min.js') }}"></script>
<script src="{{ asset('assets/js/template.js') }}"></script>
<script src="{{ asset('assets/js/dashboard.js') }}"></script>
<script src="{{ asset('assets/js/datepicker.js') }}"></script>

<script src="{{ asset('assets/js/rules-validations.js') }}"></script>



<script src="{{ asset('/assets/vendors/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.26.0/axios.min.js"></script>

<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>

<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>


<script>
    $(".select2").select2()

    $( function() {
        $( ".date" ).datepicker({ dateFormat: 'yy-mm-dd' });
    } );

</script>

<!-- scripts -->
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-database.js"></script>

<script type="text/javascript">
    const firebaseConfig = {
        apiKey: "AIzaSyDbf4EZIdJB9fTgIGh9axnwt6u2B0RbuRw",
        authDomain: "kafo-8f75c.firebaseapp.com",
        databaseURL: "https://kafo-8f75c-default-rtdb.firebaseio.com",
        projectId: "kafo-8f75c",
        storageBucket: "kafo-8f75c.appspot.com",
        messagingSenderId: "84462226458",
        appId: "1:84462226458:web:b1461d7737a6b01ed555a3",
        measurementId: "G-R19VP9H3NT"
    };

    firebase.initializeApp(firebaseConfig);
    // Initialize Firebase
    const messaging = firebase.messaging();
    messaging.usePublicVapidKey("1:84462226458:android:cc3ddc46029bee3fd555a3");

</script>


@stack('scripts')


@if( !request()->is('chat*') )

    <script>
        const database  = firebase.database();
        function setupChatRoomListener(chatRoomId) {

            let chat1 = "chats/chat-" + chatRoomId;

            const chatRef1 = database.ref(chat1);
            chatRef1.on('child_added', function (snapshot) {



                $("#point-chat").remove();
                $("#have_chat").append("<span id='point-chat'>.</span>")
                console.log("#chat-"+chatRoomId )
            })

            console.log("Listener " + chatRoomId )

        }







        $(document).ready(function() {
            let ids = {!! $clientIds !!}

            console.log("ids>?>?>?",ids)

            ids.forEach(function (id) {

                console.log("LLLLLLossasasasasaasasas" ,id)
                setupChatRoomListener(id)
            });
            // setupChatRoomListener(1)



            setTimeout(function() {
                $("#point-chat").remove();
            }, 3500); // 2000 milliseconds (2 seconds) delay

        });


    </script>

@endif


<script>
    $(function() {

        showSwal = function (type , route ) {
            'use strict';

            console.log("route >> ",route)
            $("#form_delete").attr("action" , route)
            // alert(id) ;

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {


                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger mr-2' ,
                },
                buttonsStyling: false,
            })

            swalWithBootstrapButtons.fire({
                title: '{{__('app.really_deleted')}}',
                // text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'mr-2',
                confirmButtonText: '{{__('app.yes_deleted')}}',
                cancelButtonText: '{{__('app.cancel')}}',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $("#form_delete").submit()
                    // swalWithBootstrapButtons.fire(
                    //     'Deleted!',
                    //     'Your file has been deleted.',
                    //     'success'
                    // )
                } else {
                }
            })
        }


        let lengthItems = $(".sidebar-body nav").find(".active");
        console.log(lengthItems) ;
    });



    $('.change_status').change(function(){

        let statusAdmin = 'inactive' ,
            id = $(this).data('id') ,
            route = $(this).data('route') ;

        console.log("id",id)
        if ( $(this).is(":checked") ){
            statusAdmin = 'active'
        }

        axios.post(route, {
            active  : statusAdmin
        }).then((response) => {
            console.log("response >> " , response )
            if (response.data.status == true) {
                if ( statusAdmin == 'active' ) {
                    $("#messages_").empty().append("<p class='alert alert-success'> {{__("app.activation_successfully")}} </p>")
                }else {
                    $("#messages_").empty().append("<p class='alert alert-success'> {{__("app.activation_successfully_deactivated")}} </p>")
                }

                setTimeout(function() {
                    $("#messages_").empty()
                }, 2000);
            }
        })
    });


    $(".clear_search").on("click",function () {
        $(this).closest("form").find(".form-control").val("")
        $(this).closest("form").submit()
    })


    function play() {
        var audio = document.getElementById("audio");
        audio.play();
    }
    //
    // play()
    //
    // alert("Asas")


    function setupListenerEvent() {

        let notes = "notes";

        const notesRef = database.ref(notes);
        notesRef.on('child_added', function (snapshot) {

            var audio = document.getElementById("audio");
            audio.play();
            // alert(12121)
        })
    }
    $(document).ready(function() {
        // play()
        setupListenerEvent()

    });



    // $(document).ready(function() {
    //     var audio = $('#audio')[0]; // Note: Access the raw DOM element with [0]
    //
    //     // Play the audio when the document is ready
    //     audio.play()
    //         .then(function() {
    //             console.log('Audio playback started successfully.');
    //         })
    //         .catch(function(error) {
    //             console.error('Error playing audio:', error.message);
    //         });
    // });


</script>

