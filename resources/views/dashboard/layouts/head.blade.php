<link rel="stylesheet" href="{{ asset('assets/vendors/core/core.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/fonts/feather-font/css/iconfont.css') }}">
<link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/demo_1/style.css') }}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/css/demo_1/custome-style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/flag-icon-css/css/flag-icon.min.css') }}">

<link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">

@if(app()->getLocale() == "en")
    <link rel="stylesheet" href="{{ asset('assets/css/demo_1/custome-style-ltr.css') }}">
@else
    <link rel="stylesheet" href="{{ asset('assets/css/demo_1/custome-style.css') }}">
@endif
<link rel="stylesheet" href="{{ asset('assets/css/demo_1/custome-style-filter.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/sweetalert2/sweetalert2.min.css') }}">

<style>
    #imagePreview {
        width: 180px;
        height: 180px;
        background-position: center center;
        background-size: cover;
        display: inline-block;
        background-image: url({{asset('assets/avater.png')}});
    }

    #point-chat{
        background: #fff;
        width: 15px;
        height: 15px;
        border-radius: 50%;
        display: inline-block;
        position: absolute;
        top: 11px;
        left: 0;
    }
</style>