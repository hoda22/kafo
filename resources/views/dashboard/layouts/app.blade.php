<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{__('app.title_site')}}  | {{ $title ?? '' }}</title>
    @include('dashboard.layouts.head')




    <style>
        #SvgjsSvg1582{
            height: 520px;
        }
        #apexMixed > div  svg{
            height: 430px;
        }

        .apexcharts-toolbar{
            display: none !important;
        }
        .error{
            color: red;
        }

        .swal2-actions .swal2-cancel{
            order: 1;
        }

        .swal2-actions .swal2-cancel{
            order: 2;
        }
        .phone{
            direction: ltr;
        }

        .td-phone span{
            direction: ltr;
            unicode-bidi: embed;
        }

        .pagination{
            margin-top: 20px;
        }


        {{--#imagePreview {--}}
            {{--width: 180px;--}}
            {{--height: 180px;--}}
            {{--background-position: center center;--}}
            {{--background-size: cover;--}}
            {{--display: inline-block;--}}
            {{--background-image: url({{asset('assets/avater.png')}});--}}
        {{--}--}}

        #uploadFile{
            display: none
        }

        .title-page , label , .table thead th  , .btn {
            text-transform: capitalize;
        }

        .card-report{
            padding: 30px;
        }

        .avatar{
            width:90px;
            height: 90px;
            border-radius: 50%;
            border: 1px solid #000;
        }

        .change_block_status:checked + .slider{
            background-color: #FF3366 !important;
        }

        .phone{
            direction: ltr;
            text-align: right;
        }

        .rtl .chat-wrapper .chat-content .chat-body .messages .message-item.me .content .bubble{
            border-radius: 0 5px 5px 5px;
            margin-right: auto;
            margin-left: 46px;
        }

        html, body { height: 100%; }

        .parent {
            height: 200px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .child {
            font-size: 20px;
            color: red;
        }
        .chat-body {
            margin-top: 20px;
        }
        .rtl .chat-wrapper .chat-content .chat-body .messages .message-item.me .content span {
            text-align: left;
        }

        .chat-body {
            margin-top: 20px;
            max-height: 600px;
            overflow-y: scroll;
        }

        .search-form{
            margin-left: 10px;
            margin-right: 0 !important;
        }

        .reports-mb .stretch-card{
            margin-bottom: 24px;
        }
        .chat-body {
            min-height: 80%;
        }

        .table-custome{
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
        }
        .title-page, label, .table thead th, .btn{
            text-transform: capitalize;
        }
        .table-custome thead th {
            font-family: 'luma-Semibold';
            font-size: 14px;
        }
        .table-custome thead th {
            border-top: 0;
            border-bottom-width: 1px;
            font-weight: bold;
            text-transform: uppercase;
            color: #686868;
        }

        .table-custome thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #e8ebf1;
            padding-bottom: 15px;
            font-size: 14px;
        }

        .table-custome td {
            border-bottom: 1px solid #E8EBF1;
            padding-bottom: 25px;
            padding-top: 25px;
            padding-right: 7px;
            padding-left: 7px;
        }

        .chat-item.un-read {
            background: #f7f7f7;
        }

        .form-check-inline{
            margin-left: .8rem;
            margin-right: 0;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #001A5C;
            border: 1px solid #aaa;
            border-radius: 4px;
            cursor: default;
            float: right;
            margin-right: 5px;
            margin-top: 5px;
            padding: 0 5px;
            font-size: 16px;
        }

        .link_abs{
            position: absolute;
            top: 0;
            right: 0;
            z-index: 9;
            width: 100%;
            height: 100%;
        }

        /*.card.card-report {*/
            /*margin-bottom: 20px;*/
        /*}*/

    </style>
    @stack('styles')
    <link rel="shortcut icon" href="{{ asset('/assets/images/favicon.ico') }}"/>
</head>
<body class="{{ app()->getLocale() == "en" ? "":"rtl"  }} sidebar-dark">
<div class="main-wrapper">
    {{--{{helper_asset()}}--}}
    <nav class="sidebar">
        <div class="sidebar-header">
            <a href="{{route("home")}}" class="sidebar-brand">
                <img src="{{ asset('assets/images/logo-sm.png') }}" alt="" >
                {{--Texa Hub--}}
                {{--<a href="{{route("home")}}" class="sidebar-brand">--}}
                <strong> KAFO </strong>

                {{--Ka //<span> Fo </span>--}}
                {{--</a>--}}
            </a>
            <div class="sidebar-toggler not-active">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="sidebar-body">
            <ul class="nav">

                @can('performance-panel-settings')
                <li class="nav-item">
                    <a href="{{route('home')}}" class="nav-link">
                        <i class="link-icon" data-feather="bar-chart"></i>
                        <span class="link-title"> {{__('app.titles.statistics')}} </span>
                        {{--@if( $applyClientCount && $applyClientCount > 0 )--}}
                        {{--<small class="icon-count"> {{$applyClientCount}} </small>--}}
                        {{--@endif--}}
                    </a>
                </li>
                @endcan


                @can('clients-list')
                    <li class="nav-item">
                        <a href="{{route('clients.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="user"></i>
                            <span class="link-title"> {{__('app.titles.clients')}} </span>
                            {{--@if( $applyClientCount && $applyClientCount > 0 )--}}
                            {{--<small class="icon-count"> {{$applyClientCount}} </small>--}}
                            {{--@endif--}}
                        </a>
                    </li>
                @endcan

                @can('providers-list')
                    <li class="nav-item">
                        <a href="{{route('providers.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="user-check"></i>
                            <span class="link-title"> {{__('app.titles.providers')}} </span>
                            {{--@if( $applyClientCount && $applyClientCount > 0 )--}}
                            {{--<small class="icon-count"> {{$applyClientCount}} </small>--}}
                            {{--@endif--}}
                        </a>
                    </li>
                @endcan


                @can('orders-list')
                    <li class="nav-item">
                        <a href="{{route('orders.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="shopping-bag"></i>
                            <span class="link-title"> {{__('app.titles.orders')}} </span>
                            {{--@if( $applyClientCount && $applyClientCount > 0 )--}}
                            {{--<small class="icon-count"> {{$applyClientCount}} </small>--}}
                            {{--@endif--}}
                        </a>
                    </li>
                @endcan

                @can('services-list')
                    <li class="nav-item">
                        <a href="{{route('services.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="server"></i>
                            <span class="link-title"> {{__('app.titles.services')}} </span>
                            {{--@if( $applyClientCount && $applyClientCount > 0 )--}}
                            {{--<small class="icon-count"> {{$applyClientCount}} </small>--}}
                            {{--@endif--}}
                        </a>
                    </li>
                @endcan

                @can('spare-parts-list')
                    <li class="nav-item">
                        <a href="{{route('spare-parts.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="tool"></i>
                            <span class="link-title"> {{__('app.titles.spare-parts')}} </span>
                            {{--@if( $applyClientCount && $applyClientCount > 0 )--}}
                            {{--<small class="icon-count"> {{$applyClientCount}} </small>--}}
                            {{--@endif--}}
                        </a>
                    </li>
                @endcan

                @can('brands-list')
                    <li class="nav-item">
                        <a href="{{route('brands.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="award"></i>
                            <span class="link-title">{{__('app.titles.air_conditioner_brands')}} </span>
                            {{--@if( $applyClientCount && $applyClientCount > 0 )--}}
                            {{--<small class="icon-count"> {{$applyClientCount}} </small>--}}
                            {{--@endif--}}
                        </a>
                    </li>
                @endcan
                @can('notifications-list')
                    <li class="nav-item">
                        <a href="{{route('notifications.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="bell"></i>
                            <span class="link-title">  {{__('app.titles.notifications')}} </span>
                        </a>
                    </li>
                @endcan

                @can('cities-list')
                    <li class="nav-item">
                        <a href="{{route('cities.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="map-pin"></i>
                            <span class="link-title"> {{__('app.titles.cities')}} </span>
                        </a>
                    </li>
                @endcan

                @can('pages-list')
                    <li class="nav-item">
                        <a href="{{route('pages.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="file-text"></i>
                            <span class="link-title">  {{__('app.titles.pages')}}   </span>
                            {{--@if( $applyClientCount && $applyClientCount > 0 )--}}
                            {{--<small class="icon-count"> {{$applyClientCount}} </small>--}}
                            {{--@endif--}}
                        </a>
                    </li>
                @endcan

                @can('settings-edit')
                    <li class="nav-item">
                        <a href="{{route('settings.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="settings"></i>
                            <span class="link-title"> {{__('app.titles.settings')}} </span>
                        </a>
                    </li>
                @endcan


                @can('survey-list')
                    <li class="nav-item">
                        <a href="{{route('survey-questions.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="book-open"></i>
                            <span class="link-title"> {{__('app.titles.survey')}}  </span>
                            {{--@if( $applyClientCount && $applyClientCount > 0 )--}}
                            {{--<small class="icon-count"> {{$applyClientCount}} </small>--}}
                            {{--@endif--}}
                        </a>
                    </li>
                @endcan

                @can('payment-requests-list')
                    <li class="nav-item">
                        <a href="{{route('payment-request.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="dollar-sign"></i>
                            <span class="link-title"> {{__('app.titles.payment_request')}}  </span>
                        </a>
                    </li>
                @endcan

                @can('sliders-list')
                    <li class="nav-item">
                        <a href="{{route('sliders.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="image"></i>
                            <span class="link-title"> {{__('app.titles.sliders')}} </span>
                        </a>
                    </li>
                @endcan


                @can('messages-list')
                    <li class="nav-item">
                        <a href="{{route('messages.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="mail"></i>
                            <span class="link-title"> {{__('app.titles.userMessages')}} </span>
                        </a>
                    </li>
                @endcan


                @if( auth()->user()->can('report-clients') ||
                     auth()->user()->can('report-services') ||
                     auth()->user()->can('report-request') ||
                     auth()->user()->can('report-finance') ||
                     auth()->user()->can('report-provider') ||
                     auth()->user()->can('report-spare-pats')
                     )
                    <li class="nav-item">
                        <a href="{{route('reports.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="activity"></i>
                            <span class="link-title"> {{__('app.titles.reports')}} </span>
                        </a>
                    </li>
                @endif

                @can('complaints-suggestions-list')
                    <li class="nav-item">
                        <a href="{{route('complaints-suggestions.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="archive"></i>
                            <span class="link-title"> {{__('app.titles.complaints-suggestions')}} </span>
                        </a>
                    </li>
                @endcan

                @can('loyalty-points-list')
                <li class="nav-item">
                    <a href="{{route('loyalty-points.index')}}" class="nav-link">
                        <i class="link-icon" data-feather="aperture"></i>
                        <span class="link-title"> {{__('app.titles.points_')}} </span>
                    </a>
                </li>
                @endcan
                @can('coupons-list')
                <li class="nav-item">
                    <a href="{{route('coupons.index')}}" class="nav-link">
                        <i class="link-icon" data-feather="percent"></i>
                        <span class="link-title"> {{__('app.titles.coupons')}} </span>
                    </a>
                </li>
                @endcan

                <li class="nav-item">
                    <a href="{{route('prices-points.index')}}" class="nav-link">
                        <i class="link-icon" data-feather="percent"></i>
                        <span class="link-title"> اسعار النقاط </span>
                    </a>
                </li>
                @can('chat-list')
                    <li class="nav-item" id="have_chat">
                        <a href="{{route('chat.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="message-circle"></i>
                            <span class="link-title"> {{__('app.titles.chat')}} </span>

                        </a>
                    </li>
                @endcan

                @if( auth()->user()->can('users-list') || auth()->user()->can('roles-list')  )
                <li class="nav-item nav-category">  {{__('app.titles.users_permissions')}}  </li>
                @endif

                @can('users-list')
                    <li class="nav-item">
                        <a href="{{route('user.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="users"></i>
                            <span class="link-title"> {{__('app.titles.users')}} </span>
                        </a>
                    </li>
                @endcan

                @can('roles-list')
                    <li class="nav-item">
                        <a href="{{route('roles.index')}}" class="nav-link">
                            <i class="link-icon" data-feather="user-x"></i>
                            <span class="link-title"> {{__('app.titles.roles')}} </span>
                        </a>
                    </li>
                @endcan
                {{--<li class="nav-item nav-category">  {{__('app.titles.settings')}}  </li>--}}
            </ul>
        </div>
    </nav>
    <div class="page-wrapper">
        <nav class="navbar">
            <a href="#" class="sidebar-toggler">
                <i data-feather="menu"></i>
            </a>
            <div class="navbar-content">
                <ul class="navbar-nav">
                    @if( app()->getLocale() == 'en' )
                        <li class="nav-item dropdown nav-profile">
                            <a href="{{route('change-lang','ar')}}">
                                <i class="flag-icon flag-icon-sa" title="sa" id="sa"></i>
                            </a>
                        </li>
                    @else
                        <li class="nav-item dropdown nav-profile">
                            <a href="{{route('change-lang','en')}}">
                                <i class="flag-icon flag-icon-us" title="us" id="us"></i>
                            </a>
                        </li>
                    @endif

                    <li class="nav-item dropdown nav-notifications">
                        <a class="nav-link dropdown-toggle" href="#" id="notificationDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i data-feather="bell"></i>
                            <div class="indicator">
                                <div class="circle"></div>
                            </div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="notificationDropdown">
                            <div class="dropdown-header d-flex align-items-center justify-content-between">
                                {{--<p class="mb-0 font-weight-medium">6 New Notifications</p>--}}
                                {{--<a href="javascript:;" class="text-muted">Clear all</a>--}}
                            </div>
                            <div class="dropdown-body" style="    overflow-y: scroll;">

                                @foreach( $adminNotifications as $adminNotification )
                                    {{--{{$adminNotification->data}}--}}
                                <?php
                                    $dataAdminNotification = json_decode($adminNotification->data);
                                ?>

                                @if(isset($dataAdminNotification->type) )

                                    @if( $dataAdminNotification->type == "client" )
                                        <a href="{{route( "clients.show",$dataAdminNotification->order_id )}}" class="dropdown-item">
                                    @elseif($dataAdminNotification->type == "provider")
                                        <a href="{{route( "providers.show",$dataAdminNotification->order_id )}}" class="dropdown-item">
                                    @else
                                        <a href="{{route( "orders.show",$dataAdminNotification->order_id )}}" class="dropdown-item">
                                    @endif
                                        <div class="icon">
                                            <i data-feather="user-plus"></i>
                                        </div>
                                        <div class="content">
                                            @if( app()->getLocale() == 'en' )
                                                <p>
                                                    @if( isset($dataAdminNotification->msg_en ))
                                                        {{$dataAdminNotification->msg_en}}
                                                    @endif
                                                </p>
                                            @else
                                                <p>{{$dataAdminNotification->status_provider}}</p>
                                            @endif
                                            <p class="sub-text text-muted">{{$adminNotification->created_at}}</p>
                                        </div>
                                    </a>

                                @endif

                                @endforeach

                            </div>
                            {{--<div class="dropdown-footer d-flex align-items-center justify-content-center">--}}
                                {{--<a href="javascript:;">View all</a>--}}
                            {{--</div>--}}
                        </div>
                    </li>

                    <li class="nav-item dropdown nav-profile">
                        <a class="nav-link dropdown-toggle" href="#" id="profileDropdown" role="button"
                           data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <i class="icon feather icon-chevron-down"></i>
                            {{ auth()->user()->name }}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="profileDropdown">
                            <div class="dropdown-header d-flex flex-column align-items-center">
                                <div class="info text-center">
                                    <p class="email text-muted mb-3">
                                        {{ auth()->user()->email }}
                                    </p>
                                </div>
                            </div>
                            <div class="dropdown-body">
                                <ul class="profile-nav p-0 pt-3">

                                    <li class="nav-item">
                                        <script>
                                            function submitSignout() {
                                                document.getElementById('signoutForm').submit();
                                            }
                                        </script>
                                        {!! Form::open(['method' => 'post', 'url' => url('logout'),'id'=>'signoutForm']) !!}

                                        {!! Form::close() !!}
                                        <a href="#" onclick="submitSignout()" class="nav-link">
                                            <i data-feather="log-out"></i>
                                            <span>  {{__('app.logout')}}  </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="page-content">
            <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
                <div class="col-md-6 col-7">
                    <h4 class="mb-3 mb-md-0 title-page">{{ $title??'' }}</h4>
                </div>
                {{--<div class="col-6">--}}
                @stack('button')
                {{--</div>--}}
            </div>
            <div id="messages_"></div>
            @include('flash::message')
            @include('partials.messages')
            @yield('content')

        </div>
        <form style="display: inline-block" method="post" id="form_delete" >
            @method('delete')
            @csrf
        </form>
        <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
            <p class="text-muted text-center text-md-left">{{__('app.copyright')}} © {{ date('Y') }}
                <a href="#" target="_blank">

                </a>.
            </p>
            {{--<p class="text-muted text-center text-md-left mb-0 d-none d-md-block">--}}
            {{--Developed with ❤️ by <a href="https://appzone-technology.com/" style="color: #686868 "> AppZone Technology </a>--}}
            {{--</p>--}}
        </footer>
    </div>
</div>
{{--<audio id="audio" src="https://interactive-examples.mdn.mozilla.net/media/cc0-audio/t-rex-roar.mp3"></audio>--}}
<audio id="audio" src="{{asset('public/sounds/sound-.mp3')}}"></audio>
@include('dashboard.layouts.foot')



</body>
</html>
