<script>
    $("#add-form").validate({
        rules: {
            name: {
                required: true,
            },
            name_en :{
                required: true,
            },

            ordering :{
                required: true,
            },

            src :{
                required: true,
                accept: "jpg|jpeg|png|JPG|JPEG|PNG",
            },
            action: "required"
        },
        messages: {
            name: {
                required: "{{__('app.validation.field_name_required')}}"
            },
            name_en: {
                required: "{{__('app.validation.field_name_en_required')}}"
            },

            ordering: {
                required: "{{__('app.validation.field_ordering_required')}}"
            },
            src :{
                required: "{{__('app.validation.field_src_required')}}",
                accept: "المسار غير صحيح"
            },


            action: "Please provide some data"
        }
    });


    $("#edit-form").validate({
        rules: {
            name: {
                required: true,
            },
            name_en :{
                required: true,
            },

            ordering :{
                required: true,
            },
            action: "required"
        },
        messages: {
            name: {
                required: "{{__('app.validation.field_name_required')}}"
            },
            name_en: {
                required: "{{__('app.validation.field_name_en_required')}}"
            },

            ordering: {
                required: "{{__('app.validation.field_ordering_required')}}"
            },


            action: "Please provide some data"
        }
    });
</script>