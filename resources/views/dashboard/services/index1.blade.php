@extends('dashboard.layouts.app', ['title' => __('app.services')])
@section('content')
    @push('button')
        @can("services-create")
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                {{__('app.service_add')}}
            </button>
        </div>
        @endcan
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.service_name')}}  </label>
                            <div class="col-sm-3">
                                <select class="form-control select2" name="service_id">
                                    <option value=""> {{__('app.choose')}}  </option>
                                @foreach( $services as $item__ )
                                {{--<input type="text" class="form-control" name="name" value="{{ request('name') }}"/>--}}

                                    <option value="{{$item__->id}}" {{ ($item__->id == request('service_id') ) ? "selected":"" }}> {{$item__->name}}  </option>
                                @endforeach
                                </select>
                            </div>

                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.status')}}  </label>
                            <div class="col-sm-3">
                                <select class="form-control" name="active">
                                    <option value=""> {{__('app.choose')}}  </option>

                                    @foreach( App\Enums\ActiveEnum::cases() as $case )
                                    <option value="{{$case->value}}" {{ ($case->value == request('active') ) ? "selected":"" }}> {{__('app.'.$case->value)}}  </option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="info_page">
                                {{__('app.number_elements')}} :
                                <span> {{ $items->count() }} </span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="text-end float-left">
                                <a href="{{route('services-export','xlsx')}}" class="btn btn-outline-success"> {{__('app.export_data')}} </a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.name_ar')}} </th>
                                <th class="pt-0"> {{__('app.name_en')}} </th>
                                <th class="pt-0"> {{__('app.image')}} </th>
                                <th class="pt-0"> {{__('app.how_many_services')}} </th>
                                <th class="pt-0"> {{__('app.ordering')}} </th>
                                <th class="pt-0"> {{__('app.status')}} </th>
                                <th class="pt-0"> {{__('app.processes')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $items as $item )
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td> {{ optional($item->translate('ar'))->name }} </td>
                                    <td> {{ optional( $item->translate('en') )->name  }} </td>
                                    <td>
                                        @if( !$item->parent_id )
                                        <img src="{{asset($item->src)}}" />
                                        @else
                                            --
                                        @endif
                                    </td>

                                    <td> {{ $item->children_count }} </td>
                                    <td> {{ $item->ordering }} </td>
                                    <td>
                                        {{--<span class="badge badge-lg badge-{{$item->active_status['className']}}"> {{$item->active_status['text']}}</span></td>--}}
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="active" value="active" class="change_status"
                                                       data-route="{{route("service.updateAxios" , $item->id )}}"
                                                       data-id="{{$item->id}}"
                                                       @if($item->active == 'active')
                                                       checked
                                                       @endif
                                                >
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        @if( !$item->parent_id )
                                            @can("sup-services-list")
                                            <a class="btn btn-success btn-icon aligner parent-js"
                                               href="{{route("sup_services.index",$item->id)}}"
                                                    {{--data-target="#createSubServiceModal"--}}
                                            >
                                                <i data-feather="plus"></i>
                                            </a>
                                            @endcan
                                        @endif

                                        @can("services-edit")
                                        <a href="#" data-id="{{$item->id}}" data-name="{{ optional($item->translate('ar'))->name }}" data-name_en="{{ optional($item->translate('en'))->name }}"
                                           data-active="{{ $item->active }}"
                                           data-ordering="{{ $item->ordering }}"
                                           data-ac_type="{{ $item->price }}"
                                           data-img_src="{{ asset($item->src) }}"
                                           data-parent_id="{{ $item->parent_id }}"
                                           data-price="{{ $item->price }}"
                                           class="btn btn-primary btn-icon aligner edit">
                                            <i data-feather="edit"></i>
                                        </a>
                                        @endcan

                                        @can("services-delete")
                                        <button type="submit" class="btn btn-danger btn-icon aligner"
                                                onclick="showSwal('passing-parameter-execute-cancel' , '{{route("services.destroy", $item->id)}}' )" >
                                            <i data-feather="trash"></i>
                                        </button>
                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                name = el.data('name'),
                name_en = el.data('name_en'),
                ordering = el.data('ordering'),
                department = el.data('service_id'),
                img_src = el.data('img_src'),
                active = el.data('active'),
                ac_type = el.data('ac_type'),
                price = el.data('price'),
                parent_id = el.data('parent_id'),
                route = "{{ route('services.update', ':id') }}";
            route = route.replace(':id', id);
            $(document).find("#edit_name").val(name);
            $(document).find("#edit_name_en").val(name_en);
            $(document).find("#edit_ordering").val(ordering);
            $(document).find("#edit_ac_type").val(ac_type);
            $(document).find("#edit_price").val(price);

            if ( parent_id ){
                $(document).find("#edit_parent_id").val(parent_id);
                $("#edit_price__content").removeClass("d-none")
            } else{
                $(document).find("#edit_parent_id").val("");
                $("#edit_price__content").addClass("d-none")
            }

            let isParent = !parent_id ;

            if ( isParent ) {
                $(".parent-content-js").removeClass("d-none") ;
                $(".child-content-js").addClass("d-none") ;
            }else{
                $(".parent-content-js").addClass("d-none") ;
                $(".child-content-js").removeClass("d-none") ;
            }


            $(document).find("#img_src").attr("src",img_src);

            if ( active == 'active' ){
                $(document).find("#edit_active").prop('checked',true);
            }else{
                $(document).find("#edit_active").prop('checked',false);
            }

            $(document).find("#edit_department").val(department);
            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);
        });

        // $('#createModal').select2({
        //     dropdownParent: $('#add_parent_id')
        // });
        $(document).on("change", "#add_parent_id",function (e) {
            // if ( $(this).val() ) {
            //     $("#add_price__content").removeClass("d-none")
            //     $("#add_image").attr("required" , false)
            //
            //     alert(12211111);
            //     $("#add_price").attr("required",true)
            // } else {
            //     $("#add_price__content").addClass("d-none")
            //     $("#add_image").attr("required",true)
            //     $("#add_price").removeAttribute("required")
            // }
        });

        $(document).on("click" , ".parent-js" ,function (e) {
            let parentId = $(this).data("id") ;
            $("#parent_id").val(parentId)
        })

        $(document).on("change", "#edit_parent_id",function (e) {
            if ( $(this).val() ) {
                $("#edit_price__content").removeClass("d-none")
            } else {
                $("#edit_price__content").addClass("d-none")
            }
        });

    </script>

    <div class="modal fade" id="createModal" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createSubServiceModalLabel">{{__('app.service_add')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="add-form" action="{{route("services.store")}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name" class="col-form-label">{{__('app.name_ar')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name" id="add_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name_en" class="col-form-label">{{__('app.name_en')}} <span class="required_star">*</span>  :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name_en" id="add_name_en">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_image" class="col-form-label"> {{__('app.image')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="file" class="form-control" name="src" id="add_image" accept="image/png, image/gif, image/jpeg">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name_en" class="col-form-label">{{__('app.ordering')}} <span class="required_star">*</span>  :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="ordering" id="add_ordering">
                            </div>
                        </div>
                        <input type="hidden" id="parent_id" name="parent_id" >
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="editModal"  role="dialog" aria-labelledby="editModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel"> {{__('app.service_update')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="edit-form" action="" enctype="multipart/form-data">
                        @method('put')
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_name" class="col-form-label"> {{__('app.name_ar')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name" id="edit_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_name" class="col-form-label">{{__('app.name_en')}} <span class="required_star">*</span>   :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name_en" id="edit_name_en">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_ordering" class="col-form-label">{{__('app.ordering')}} <span class="required_star">*</span>   :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="ordering" id="edit_ordering">
                            </div>
                        </div>



                        <div class="form-group row parent-content-js">
                            <div class="col-lg-3">
                                <label class="col-form-label"> {{__('app.old_image')}}  :</label>
                            </div>
                            <div class="col-lg-9">
                                <div class="old-img">
                                    <span> x </span>
                                    <img src="" id="img_src"  height="50" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group row parent-content-js">
                            <div class="col-lg-3">
                                <label for="edit_image" class="col-form-label">{{__('app.image')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="file" class="form-control" name="src" id="edit_image" accept="image/png, image/gif, image/jpeg">
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active" id="edit_active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('dashboard.services.script')
@endpush
