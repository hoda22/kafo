<script>
    $("#add-form").validate({
        rules: {
            site_name: {
                required: true,
            },
            email :{
                required: true,
            },

            phone :{
                required: true,
            },
            phone2 :{
                required: true,
            },

            facebook :{
                url: true
            },
            twitter :{
                url: true
            },
            linkedin :{
                url: true
            },
            instagram :{
                url: true
            },
            action: "required"
        },
        messages: {
            site_name: {
                required: "{{__('app.validation.field_site_name_required')}}"
            },
            email: {
                required: "{{__('app.validation.field_email_required')}}"
            },

            phone: {
                required: "{{__('app.validation.field_phone_required')}}"
            },
            phone2 :{
                required: "{{__('app.validation.field_phone2_required')}}"
            },

            facebook :{
                url: "{{__('app.validation.field_url_required')}}"
            },
            twitter :{
                url: "{{__('app.validation.field_url_required')}}"
            },
            linkedin :{
                url: "{{__('app.validation.field_url_required')}}"
            },
            instagram :{
                url: "{{__('app.validation.field_url_required')}}"
            },




            action: "Please provide some data"
        }
    });
</script>