@extends('dashboard.layouts.app', ['title' => __('app.titles.settings')])
@section('content')
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <form method="post" id="add-form" action="{{route("settings.store")}}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_name" class="col-form-label">{{__('app.settings.visit')}} :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="number" min="1" value="{{$setting->visit}}" class="form-control" name="visit" id="add_name">
                            </div>

                        </div>

                        <hr>
                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_name" class="col-form-label">{{__('app.settings.site_name')}} :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" value="{{$setting->site_name}}" class="form-control" name="site_name" id="add_name">
                            </div>

                            <div class="col-lg-2">
                                <label for="add_email" class="col-form-label"> {{__('app.email')}} :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="email" class="form-control" name="email" value="{{$setting->email}}" id="add_email">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_phone" class="col-form-label">{{__('app.phone')}}  :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="phone" value="{{$setting->phone}}" id="add_phone">
                            </div>

                            <div class="col-lg-2">
                                <label for="add_phone2" class="col-form-label">{{__('app.phone')}}  :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="phone2" value="{{$setting->phone2}}" id="add_phone2">
                            </div>
                        </div>


                        <div class="form-group row mb-3">
                            <div class="col-lg-2">
                                <label for="meta_description" class="col-form-label"> {{__('app.settings.meta_description')}} :</label>
                            </div>
                            <div class="col-lg-10">
                                <textarea class="form-control" name="meta_description" rows="7" >{{$setting->meta_description}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <div class="col-lg-2">
                                <label for="meta_keywords" class="col-form-label">{{__('app.settings.meta_keywords')}} :</label>
                            </div>
                            <div class="col-lg-10">
                                <textarea class="form-control" name="meta_keywords" rows="7" >{{$setting->meta_keywords}}</textarea>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_facebook" class="col-form-label">{{__('app.facebook')}}  :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="facebook" value="{{$setting->facebook}}" id="add_facebook">
                            </div>

                            <div class="col-lg-2">
                                <label for="add_twitter" class="col-form-label">{{__('app.twitter')}}  :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="twitter" value="{{$setting->twitter}}" id="add_twitter">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_linkedin" class="col-form-label">{{__('app.linkedin')}}  :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="linkedin" value="{{$setting->linkedin}}" id="add_linkedin">
                            </div>

                            <div class="col-lg-2">
                                <label for="add_instagram" class="col-form-label">{{__('app.instagram')}}  :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="instagram" value="{{$setting->instagram}}" id="add_instagram">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
    @include('dashboard.settings.script')
@endpush