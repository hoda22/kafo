@extends('dashboard.layouts.app', ['title' => __('app.titles.cities')])
@section('content')
    @push('button')
        @can("cities-create")
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                    {{__('app.city_add')}}
                </button>
            </div>
        @endcan
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.city_name')}} </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="name" value="{{ request('name') }}"/>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $cities])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.name_ar')}} </th>
                                <th class="pt-0"> {{__('app.name_en')}} </th>
                                <th class="pt-0"> {{__('app.ordering')}}</th>
                                <th class="pt-0"> {{__('app.status')}}</th>
                                <th class="pt-0"> {{__('app.processes')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($cities as $city)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td> {{ optional($city->translate('ar'))->name }} </td>
                                    <td>
                                        {{ optional( $city->translate('en') )->name  }}
                                    </td>
                                    <td>
                                        {{ $city->ordering }}
                                    </td>
                                    <td>
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="active" value="active" class="change_status"
                                                       data-route="{{route("city.updateAxios" , $city->id )}}"
                                                       data-id="{{$city->id}}"
                                                       @if($city->active == 'active')
                                                       checked
                                                        @endif
                                                >
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        @can("cities-edit")
                                            <a href="{{route("cities.edit",$city->id )}}"
                                               {{--data-id="{{$city->id}}"--}}
                                               {{--data-active="{{ $city->active }}"--}}
                                               {{--data-name="{{ optional($city->translate('ar'))->name }}"--}}
                                               {{--data-name_en="{{ optional($city->translate('en'))->name }}"--}}
                                               {{--data-ordering="{{ $city->ordering }}"--}}
                                               class="btn btn-primary btn-icon aligner">
                                                <i data-feather="edit"></i>
                                            </a>
                                        @endcan

                                        @can("cities-delete")
                                            <button type="submit" class="btn btn-danger btn-icon aligner"
                                                    onclick="showSwal('passing-parameter-execute-cancel' , '{{route("cities.destroy", $city->id)}}' )" >
                                                <i data-feather="trash"></i>
                                            </button>
                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $cities->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("styles")

@endpush
@push('scripts')

    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                name = el.data('name'),
                name_en = el.data('name_en'),
                active = el.data('active'),
                ordering = el.data('ordering'),
                route = "{{ route('cities.update', ':id') }}";
            route = route.replace(':id', id);
            $(document).find("#edit_name").val(name);
            $(document).find("#edit_name_en").val(name_en);
            $(document).find("#edit_ordering").val(ordering);

            if ( active == 'active' ){
                $(document).find("#edit_active").prop('checked',true);
            }else{
                $(document).find("#edit_active").prop('checked',false);
            }

            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);
        });

    </script>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel"> {{__('app.city_add')}}  </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route("cities.store")}}" id="add-form">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name" class="col-form-label">{{__('app.name_ar')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name" id="add_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name_en" class="col-form-label">{{__('app.name_en')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name_en" id="add_name_en">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name" class="col-form-label">{{__('app.ordering')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="ordering"  id="add_ordering">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <div class="text-left">
                            <button type="button" onclick="RemoveZoneFromMap();" class="btn btn-danger remove-pin mb-2">
                                <i data-feather="trash"></i>
                            </button>
                        </div>
                        <div class="form-group">
                            <input type="hidden" value="" name="zone_polygon" id="zone_polygon"/>
                            <div class="col-sm-12" id="map_" style="height:400px;width:100%;"> </div>
                        </div>



                        <script>

                            var drawingManager;
                            var CurrentShape;
                            var bermudaTriangle;
                            var map ;
                            var tri ;
                            function initMap() {
                                // map
                                var polygonArray = [];
                                map = new google.maps.Map(document.getElementById('map_'), {
                                    zoom: 11,
                                    center: {lat: 31.120676, lng: 31.356298},
                                    map: map
                                });
                                //drawingManager
                                drawingManager = new google.maps.drawing.DrawingManager({
                                    drawingMode: google.maps.drawing.OverlayType.MARKER,
                                    drawingControl: true,
                                    drawingControlOptions: {
                                        position: google.maps.ControlPosition.TOP_CENTER,
                                        drawingModes: ['polygon']
                                    },
                                    markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
                                    circleOptions: {
                                        fillColor: '#ffff00',
                                        fillOpacity: 1,
                                        strokeWeight: 5,
                                        clickable: false,
                                        editable: true,
                                        zIndex: 1
                                    }
                                });
                                drawingManager.setMap(map);
                                //action on drawing
                                google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
                                    drawingManager.setMap(null);
                                    CurrentShape = event.overlay;

                                    Bounds = event.overlay.getPath().getArray();
                                    newpoly = "";
                                    MyList = [];
                                    BoundsList = null;
                                    Radius = 0;
                                    LatCenter = 0;
                                    LngCenter = 0;
                                    BoundsList = Bounds;
                                    for (var i = 0; i < BoundsList.length; i++) {
                                        debugger;
                                        var l = BoundsList[i].lat();
                                        var g = BoundsList[i].lng();
                                        var myObj = l + " " + g;
                                        MyList.push(myObj);
                                        newpoly = 'POLYGON((' + MyList.toString() + '))';
                                    }
                                    //Added
                                    var NewmyObj = MyList[0];
                                    MyList.push(NewmyObj);
                                    newpoly = 'POLYGON((' + MyList.toString() + '))';
                                    document.getElementById("zone_polygon").value=newpoly;
                                });

                            }
                            function RemoveZoneFromMap(){
                                drawingManager.setMap(map);
                                if(CurrentShape!=null){
                                    CurrentShape.setMap(null);
                                }
                                document.getElementById("zone_polygon").value="";
                            }
                        </script>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel"> {{__('app.city_edit')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="edit-form" action="">
                        @method('put')
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_name" class="col-form-label">{{__('app.name_ar')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" required name="name" id="edit_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_name" class="col-form-label">{{__('app.name_en')}}   <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" required name="name_en" id="edit_name_en">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_ordering" class="col-form-label">{{__('app.ordering')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control"  name="ordering" id="edit_ordering">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active" id="edit_active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>


                        {{--<div class="form-group row">--}}
                        {{--<div class="col-lg-3">--}}
                        {{--<label for="edit_ordering" class="col-form-label">{{__('app.ordering')}} :</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-9">--}}
                        {{--<input type="number" class="form-control" name="ordering" required id="edit_ordering">--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('dashboard.cities.script')
@endpush


@push('scripts')
    <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBr9CvUU6j7HbG5jfhTG4euA94LRwtb7iM&libraries=drawing&callback=initMap"></script>
@endpush
