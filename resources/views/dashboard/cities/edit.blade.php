@extends('dashboard.layouts.app', ['title' => __('app.city_edit')])
@section('content')
    @push('button')
        <div class="d-flex align-items-center flex-wrap text-nowrap btn-back">
            <a href="{{route('cities.index')}}" class="btn btn-primary">
                <i data-feather="skip-back"></i>
            </a>
        </div>
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <form method="post" id="edit-form" action="{{route("cities.update", $item->id)}}">
                        @method('put')
                        @csrf

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-lg-3">
                                        <label for="edit_name" class="col-form-label">{{__('app.name_ar')}} <span class="required_star">*</span>:</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control"  name="name" id="edit_name" value="{{ optional($item->translate('ar'))->name }}" >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-3">
                                        <label for="edit_name" class="col-form-label">{{__('app.name_en')}}   <span class="required_star">*</span> :</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control"  name="name_en" id="edit_name_en" value="{{ optional($item->translate('en'))->name }}">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-lg-3">
                                        <label for="edit_ordering" class="col-form-label">{{__('app.ordering')}} <span class="required_star">*</span> :</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control"  name="ordering" id="edit_ordering" value="{{ $item->ordering }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-3">
                                        <label for="add_active" class="col-form-label">  {{__('app.status')}} <span class="required_star">*</span> :</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <input type="hidden" name="active" value="inactive">
                                        <label class="switch">
                                            <input type="checkbox" name="active" value="active" id="edit_active"  {{ $item->active == 'active' ?  "checked":"" }}>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <button type="button" onclick="RemoveZoneFromMap();" class="btn btn-danger remove-pin mb-2">
                                    <i data-feather="trash"></i>
                                </button>
                                <input type="hidden" value="" name="zone_polygon" id="zone_polygon"/>
                                <div id="map_" style="width: 100%; height: 350px;">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>

    var drawingManager;
    var CurrentShape;
    var bermudaTriangle;
    var map ;
    var tri ;
    function initMap() {
        // map
        var polygonArray = [];
        map = new google.maps.Map(document.getElementById('map_'), {
            zoom: 11,
            center: {lat: 31.120676, lng: 31.356298},
            map: map
        });
        //drawingManager
        drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.MARKER,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: ['polygon']
            },
            markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
            circleOptions: {
                fillColor: '#ffff00',
                fillOpacity: 1,
                strokeWeight: 5,
                clickable: false,
                editable: true,
                zIndex: 1
            }
        });
        drawingManager.setMap(map);
        //action on drawing
        google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
            drawingManager.setMap(null);
            CurrentShape = event.overlay;

            Bounds = event.overlay.getPath().getArray();
            newpoly = "";
            MyList = [];
            BoundsList = null;
            Radius = 0;
            LatCenter = 0;
            LngCenter = 0;
            BoundsList = Bounds;

            for (var i = 0; i < BoundsList.length; i++) {
                debugger;
                var l = BoundsList[i].lat();
                var g = BoundsList[i].lng();
                var myObj = l + " " + g;
                MyList.push(myObj);
                newpoly = 'POLYGON((' + MyList.toString() + '))';
            }
            //Added
            var NewmyObj = MyList[0];
            MyList.push(NewmyObj);
            newpoly = 'POLYGON((' + MyList.toString() + '))';
            document.getElementById("zone_polygon").value=newpoly;
        });
        //draw polygon if found on system
        // Define the LatLng coordinates for the polygon's path.
        tri = {!! json_encode($polygonLatLng) !!};
        triangleCoords=tri;
        // Construct the polygon.
        bermudaTriangle = new google.maps.Polygon({
            paths: triangleCoords,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35
        });
        bermudaTriangle.setMap(map);
        //
        var triangleCoordsLS12 = [];
        if (triangleCoords != null && triangleCoords !="") {
            for (var i = 0; i < triangleCoords.length; i++) {
                point=triangleCoords[i];
                triangleCoordsLS12[i] = new google.maps.LatLng(parseFloat(point["lat"]), parseFloat(point["lng"]));
            }
        }
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < bermudaTriangle.getPath().length; i++) {

            var point = new google.maps.LatLng(triangleCoordsLS12[i].lat(), triangleCoordsLS12[i].lng());
            bounds.extend(point);
        }
        map.fitBounds(bounds);
    }
    function RemoveZoneFromMap(){
        drawingManager.setMap(map);
        bermudaTriangle.setMap(null);
        if(CurrentShape!=null){
            CurrentShape.setMap(null);
        }
        document.getElementById("zone_polygon").value="";
    }
</script>

@push('scripts')
    @include('dashboard.cities.script')
    <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBr9CvUU6j7HbG5jfhTG4euA94LRwtb7iM&libraries=drawing&callback=initMap"></script>
@endpush

@push('styles')
    <style>
        .remove-pin{
            float: left;
        }
    </style>
@endpush
