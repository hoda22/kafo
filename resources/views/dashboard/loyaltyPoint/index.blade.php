@extends('dashboard.layouts.app', ['title' => __('app.titles.points_')])
@section('content')
    @push('button')
        @can("loyalty-points-create")
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                {{__('app.points.add')}}
            </button>
        </div>
        @endcan
    @endpush
    <div class="row">
        {{--<div class="col-lg-12 col-xl-12 stretch-card">--}}
            {{--<div class="card mb-3">--}}
                {{--<div class="card-body filter">--}}
                    {{--<form class="filter_form">--}}
                        {{--<div class="form-group row">--}}
                            {{--<label for="place" class="col-sm-2 col-form-label"> {{__('app.city_name')}} </label>--}}
                            {{--<div class="col-sm-3">--}}
                                {{--<input type="text" class="form-control" name="name" value="{{ request('name') }}"/>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-3">--}}
                                {{--<button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>--}}
                                {{--<button type="button" class="btn clear_search btn-danger btn-icon aligner">--}}
                                    {{--<i class="link-icon" data-feather="refresh-cw"></i>--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.points.num_points')}} </th>
                                <th class="pt-0"> {{__('app.points.num_rus')}} </th>
                                <th class="pt-0"> {{__('app.ordering')}} </th>
                                <th class="pt-0"> الخدمه الهديه </th>
                                <th class="pt-0"> {{__('app.status')}} </th>


                                <th class="pt-0"> {{__('app.processes')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td> {{$item->num_point}} </td>
                                    <td> {{$item->num_sar }} </td>

                                    <td>
                                        {{ $item->ordering }}
                                    </td>

                                    <td>
                                        {{ optional( $item->service )->name }}
                                    </td>
                                    <td>
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="active" value="active" class="change_status"
                                                    data-route="{{route("loyalty-point.updateAxios" , $item->id )}}"
                                                    data-id="{{$item->id}}"
                                                   @if($item->active == 'active')
                                                       checked
                                                   @endif
                                                >
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        @can("loyalty-points-edit")
                                        <a href="#" data-id="{{$item->id}}"
                                           data-num_sar="{{ $item->num_sar }}"
                                           data-num_point="{{ $item->num_point }}"
                                           data-ordering="{{ $item->ordering }}"
                                           data-service-parent-id="{{ optional($item->service)->parent_id }}"
                                           data-service-id="{{ optional($item->service)->id }}"
                                           data-service-sups="{{ optional($item->service)->parent_id }}"
                                           data-active="{{ $item->active }}"
                                           class="btn btn-primary btn-icon aligner edit">
                                            <i data-feather="edit"></i>
                                        </a>
                                        @endcan

                                        @can("loyalty-points-delete")
                                            <button type="submit" class="btn btn-danger btn-icon aligner"
                                                    onclick="showSwal('passing-parameter-execute-cancel' , '{{route("loyalty-points.destroy", $item->id)}}' )" >
                                                <i data-feather="trash"></i>
                                            </button>
                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("styles")

@endpush
@push('scripts')

    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                num_sar = el.data('num_sar'),
                num_point = el.data('num_point'),
                parentId = el.data('service-parent-id'),
                serviceId = el.data('service-id'),
                active = el.data('active'),
                ordering = el.data('ordering'),
                route = "{{ route('loyalty-points.update', ':id') }}";
            route = route.replace(':id', id);


            $(document).find("#edit_num_sar").val(num_sar);
            $(document).find("#edit_num_point").val(num_point);
            $(document).find("#edit_services").val(parentId);
            $(document).find("#edit_ordering").val(ordering);

            if ( active == 'active' ){
                $(document).find("#edit_active").prop('checked',true);
            }else{
                $(document).find("#edit_active").prop('checked',false);
            }

            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);


            getItems(parentId , serviceId )
        });

    </script>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel"> {{__('app.points.add')}}  </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route("loyalty-points.store")}}" id="add-form">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="num_point" class="col-form-label">{{__('app.points.num_points')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="num_point"  id="num_point">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_num_rus" class="col-form-label">{{__('app.points.num_rus')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="num_sar" id="add_num_sar">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name" class="col-form-label">{{__('app.ordering')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="ordering" id="add_ordering">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <hr />
                        <p class="text-center">
                            اختار هديه لعدد النقاط
                        </p>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_services" class="col-form-label">{{__('app.services')}}  :</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control select2" id="add_services">
                                    <option> -- {{__("app.choose")}} --  </option>
                                    @foreach( $services as $service )
                                        <option value="{{$service->id}}"> {{$service->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_sup_services" class="col-form-label">{{__('app.titles.sup_services')}}  :</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control" name="service_id" id="add_sup_services" disabled >
                                    <option></option>
                                </select>
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel"> {{__('app.points.edit')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="edit-form" action="">
                        @method('put')
                        @csrf

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="num_point" class="col-form-label">{{__('app.points.num_points')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="num_point" id="edit_num_point">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_num_rus" class="col-form-label">{{__('app.points.num_rus')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="num_sar"  id="edit_num_sar">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_ordering" class="col-form-label">{{__('app.ordering')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control"  name="ordering" id="edit_ordering">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active" id="edit_active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <hr />
                        <p class="text-center">
                            اختار هديه لعدد النقاط
                        </p>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_services" class="col-form-label">{{__('app.services')}}  :</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control select2" id="edit_services">
                                    <option> -- {{__("app.choose")}} --  </option>
                                    @foreach( $services as $service )
                                        <option value="{{$service->id}}"> {{$service->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_sup_services" class="col-form-label">{{__('app.titles.sup_services')}}  :</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control" name="service_id" id="edit_sup_services" disabled >
                                    <option></option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('dashboard.loyaltyPoint.script')


    <script>
        $("#add_services").on("change", function () {
            let basicService = $(this).val();

            let route   = "{{route('order.subServices')}}";
            axios.get(route, {
                params: {
                    id: basicService
                }
            }).then((response) => {
                console.log("response >> " , response )
                if ( response.data.status == true) {
                    if ( response.data.count )  {
                        $("#add_sup_services").removeAttr("disabled","disabled")
                        $("#add_sup_services").empty().append(response.data.data)
                    }else{
                        $("#add_sup_services").attr("disabled","disabled")
                        $("#add_sup_services").empty()
                    }
                }else{
                    $("#add_sup_services").attr("disabled","disabled")
                }
            })
        } )



        $("#edit_services").on("change", function () {
            let basicService = $(this).val();

            let route   = "{{route('order.subServices')}}";
            axios.get(route, {
                params: {
                    id: basicService
                }
            }).then((response) => {
                console.log("response >> " , response )
                if ( response.data.status == true) {
                    if ( response.data.count )  {
                        $("#edit_sup_services").removeAttr("disabled","disabled")
                        $("#edit_sup_services").empty().append(response.data.data)
                    }else{
                        $("#edit_sup_services").attr("disabled","disabled")
                        $("#edit_sup_services").empty()
                    }
                }else{
                    $("#edit_sup_services").attr("disabled","disabled")
                }
            })
        } )






        function getItems(basicService , subItemId ) {

            let route   = "{{route('order.subServices')}}";
            axios.get(route, {
                params: {
                    id: basicService ,
                    subItemId:subItemId
                }
            }).then((response) => {
                console.log("response Edit >> " , response )
                if ( response.data.status == true) {
                    if ( response.data.count )  {
                        $("#edit_sup_services").removeAttr("disabled","disabled")
                        $("#edit_sup_services").empty().append(response.data.data)
                    }else{
                        $("#edit_sup_services").attr("disabled","disabled")
                        $("#edit_sup_services").empty()
                    }
                }else{
                    $("#edit_sup_services").attr("disabled","disabled")
                }
            })
        }
    </script>
@endpush
