<script>
    $("#add-form").validate({
        rules: {
            num_sar :{
                required: true,
            },
            num_point :{
                required: true,
            },

            ordering :{
                required: true,
            },

            action: "required"
        },
        messages: {
            num_sar: {
                required: "{{__('app.validation.field_num_sar_required')}}",
            },
            num_point: {
                required: "{{__('app.validation.field_num_point_required')}}"
            },
            ordering: {
                required: "{{__('app.validation.field_ordering_required')}}"
            },


            action: "Please provide some data"
        }
    });


    $("#edit-form").validate({
        rules: {
            num_sar :{
                required: true,
            },
            num_point :{
                required: true,
            },

            ordering :{
                required: true,
            },
            action: "required"
        },
        messages: {
            num_sar: {
                required: "{{__('app.validation.field_num_sar_required')}}",
            },
            num_point: {
                required: "{{__('app.validation.field_num_point_required')}}"
            },
            ordering: {
                required: "{{__('app.validation.field_ordering_required')}}"
            },
            action: "Please provide some data"
        }
    });
</script>