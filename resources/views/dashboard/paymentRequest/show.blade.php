@extends('dashboard.layouts.app', ['title' => 'عرض الطلب'])
@section('content')
    <div class="col-lg-12 col-xl-12 stretch-card">
        <div class="card">
            <div class="card-body ">
                <div class="row">
                    <div class="col-md-12">
                        <div id="messages_">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-2 col-sm-4 col-4">
                                <strong>
                                    الخدمه الفرعيه  :
                                </strong>
                            </div>
                            <div class="col-md-2 col-sm-8 col-8"> {{optional($data->service)->name}} </div>
                            <div class="col-md-2 col-sm-4 col-4">
                                <strong>
                                    الخدمه الاساسيه  :
                                </strong>
                            </div>
                            <div class="col-md-2 col-sm-8 col-8"> {{optional( optional($data->service)->parentItem )->name}} </div>

                            <div class="col-md-1 col-sm-4 col-4 status_item">
                                <strong>  حاله الطلب :  </strong>
                            </div>
                            <div class="col-md-3 col-sm-8 col-8 text-left">
                                {{optional($data->status)->name}}
                            </div>

                            <div class="col-md-12">
                                <hr class="w-100">
                            </div>
                            <div class="col-md-2  col-sm-6 col-6"><strong> السعر :  </strong></div>
                            <div class="col-md-2 col-sm-6 col-6">  {{ $data->price}} </div>

                            <div class="col-md-2  col-sm-6 col-6"><strong> السعر المدفوع :  </strong></div>
                            <div class="col-md-2 col-sm-6 col-6">  {{ $data->amount_paid}} </div>

                            <div class="col-md-2  col-sm-6 col-6"><strong> تاريخ الخدمه :  </strong></div>
                            <div class="col-md-2 col-sm-6 col-6">  {{ $data->service_date}}  -- {{$data->service_time}}</div>


                            <div class="col-md-12">
                                <hr class="w-100">
                            </div>

                            <div class="col-md-2 col-sm-6 col-6"><strong> تاريخ التسجيل :    </strong></div>
                            <div class="col-md-2 col-sm-6 col-6">  {{ $data->created_at }} </div>

                            <div class="col-md-12">
                                <hr class="w-100">
                            </div>
                            <div class="col-md-2 col-sm-6 col-6"><strong> تعليق طالب الخدمه : </strong></div>
                            <div class="col-md-4 col-sm-6 col-6">  {{ $data->client_comment }} </div>

                            <div class="col-md-2 col-sm-6 col-6"><strong> تعليق الفني : </strong></div>
                            <div class="col-md-4 col-sm-6 col-6">  {{ $data->provider_comment }} </div>

                            <div class="col-md-12">
                                <hr class="w-100">
                            </div>

                            <div class="col-md-2 col-sm-6 col-6"><strong> تعليق الاداره : </strong></div>
                            <div class="col-md-4 col-sm-6 col-6">  {{ $data->management_comment }} </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12 mt-4 ">
        <div class="row">
            <div class="col-md-6">
            <div class="card ">
            <h4 class="mb-3 mt-3" style="padding: 0 20px 0 0;margin-bottom: 0 !important;"> معلومات طالب الخدمه </h4>
            <hr style="margin-bottom: 0 !important;">
            @if( $data->client )
            <div class="card-body row">
                <div class="col-sm-3 col-4"><strong> اسم طالب الخدمه :  </strong></div>
                <div class="col-sm-8 col-8">  {{ optional($data->client)->full_name}}  </div>

                <div class="col-md-12">
                    <hr class="w-100">
                </div>

                <div class="col-md-3 col-sm-6 col-6"><strong>  التقييم  :  </strong></div>
                <div class="col-md-3 col-sm-6 col-6">  3 </div>



                <div class="col-md-12">
                    <hr class="w-100">
                </div>

                <div class="col-md-3 col-sm-6 col-6"><strong> الوصف :  </strong></div>
                <div class="col-md-9 col-sm-6 col-6">  {{ optional($data->providerInfo)->description  ?? "--"  }} </div>
            </div>
            @else
                <div class="card">
                    <div class="card-body row">
                        لا يوجد عميل
                    </div>
                </div>
            @endif
        </div>
            </div>
            <div class="col-md-6">
                <div class="card ">
                    <h4 class="mb-3 mt-3" style="padding: 0 20px 0 0;margin-bottom: 0 !important;"> معلومات الفني </h4>
                    <hr style="margin-bottom: 0 !important;">
                    @if( $data->client )
                        <div class="card-body row">
                            <div class="col-sm-3 col-4"><strong> اسم طالب الخدمه :  </strong></div>
                            <div class="col-sm-8 col-8">  {{ optional($data->client)->full_name}}  </div>

                            <div class="col-md-12">
                                <hr class="w-100">
                            </div>

                            <div class="col-md-3 col-sm-6 col-6"><strong>  التقييم  :  </strong></div>
                            <div class="col-md-3 col-sm-6 col-6">  3 </div>



                            <div class="col-md-12">
                                <hr class="w-100">
                            </div>

                            <div class="col-md-3 col-sm-6 col-6"><strong> الوصف :  </strong></div>
                            <div class="col-md-9 col-sm-6 col-6">  {{ optional($data->providerInfo)->description  ?? "--"  }} </div>
                        </div>
                    @else
                        <div class="card">
                            <div class="card-body row">
                                لا يوجد عميل
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>

    </div>






@endsection
@push('scripts')


    {{--<form action="{{route("provider.update" , request('provider'))}}" id="statusProvider" method="post">--}}
        {{--@method('put')--}}
        {{--@csrf--}}
        {{--<input type="hidden" id="status_provider" name="provider_status" />--}}
    {{--</form>--}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.26.0/axios.min.js"></script>
    <script type="text/javascript">
        {{--$('#store_type').change(function(){--}}
            {{--let storeType = $(this).val() ,--}}
                {{--id = $(this).data('id')--}}

            {{--let route = "{{ route('updateStoreType') }}";--}}
            {{--axios.post(route, {--}}
                {{--storeType  : storeType ,--}}
                {{--id  :  id ,--}}
            {{--}).then((response) => {--}}
                {{--if (response.data.status == true) {--}}
                    {{--$("#messages_").empty().append("<p class='alert alert-success'> تم تعديل تصنيف التاجر بنجاح </p>")--}}
                {{--}--}}
            {{--})--}}
        {{--});--}}


        {{--$('#store_type').change(function(){--}}
            {{--let storeType = $(this).val() ,--}}
                {{--id = $(this).data('id')--}}

            {{--let route = "{{ route('updateStoreType') }}";--}}
            {{--axios.post(route, {--}}
                {{--storeType  : storeType ,--}}
                {{--id  :  id ,--}}
            {{--}).then((response) => {--}}
                {{--if (response.data.status == true) {--}}
                    {{--$("#messages_").empty().append("<p class='alert alert-success'> تم تعديل تصنيف التاجر بنجاح </p>")--}}
                {{--}--}}
            {{--})--}}
        {{--});--}}

        {{--$('.auto_approve').change(function(){--}}

            {{--let autoApprove = 0 ,--}}
                {{--id = $(this).data('id') ,--}}
                {{--route = "{{ route('updateData', ':id') }}";--}}
            {{--route = route.replace(':id', id);--}}

            {{--if ( $(this).is(":checked") ){--}}
                {{--autoApprove = 1--}}
            {{--}--}}

            {{--axios.post(route, {--}}
                {{--auto_approve  : autoApprove--}}
            {{--}).then((response) => {--}}
                {{--if (response.data.status == true) {--}}
                    {{--$("#messages_").empty().append("<p class='alert alert-success'> تم التعديل بنجاح </p>")--}}
                {{--}--}}
            {{--})--}}
        {{--});--}}

        {{--$('#status').change(function(){--}}
            {{--let status = 'inactive' ,--}}
                {{--id = $(this).data('id') ,--}}
                {{--route = "{{ route('updateDataClient', ':id') }}";--}}
            {{--route = route.replace(':id', id);--}}

            {{--if ( $(this).is(":checked") ){--}}
                {{--status = 'active'--}}
            {{--}--}}

            {{--axios.post(route, {--}}
                {{--status_admin : status--}}
            {{--}).then((response) => {--}}
                {{--if (response.data.status == true) {--}}
                    {{--$("#messages_").empty().append("<p class='alert alert-success'> تم تعديل تصنيف التاجر بنجاح </p>")--}}
                {{--}--}}
            {{--})--}}
        {{--});--}}

        {{--$('#customCheck12').change(function(){--}}

            {{--let status = 0 ,--}}
                {{--id = $(this).data('id') ,--}}
                {{--route = "{{ route('updateData', ':id') }}";--}}
            {{--route = route.replace(':id', id);--}}



            {{--if ( $(this).is(":checked") ){--}}
                {{--status = 1--}}
            {{--}--}}


            {{--axios.post(route, {--}}
                {{--is_vip : status--}}
            {{--}).then((response) => {--}}
                {{--if (response.data.status == true) {--}}
                    {{--$("#messages_").empty().append("<p class='alert alert-success'> تم التعديل بنجاح </p>")--}}
                {{--}--}}
            {{--})--}}
        {{--});--}}


        {{--$(".provider_status > button").click(function ( e ) {--}}
            {{--e.preventDefault();--}}

            {{--let status = $(this).data("status");--}}
            {{--$(this).data("status");--}}

            {{--$("#status_provider").val(status) ;--}}

            {{--$("#statusProvider").submit();--}}
        {{--})--}}

        $(function() {
            let lengthNavItems = $(".sidebar-body").find("li.nav-item").hasClass("active")
            // alert(lengthNavItems)

            if (lengthNavItems )  {
                $(".sidebar-body").find(".nav-item").removeClass("active")
            }
        });

    </script>
@endpush
@push('styles')
    <style>
        .logo img{
            width: 100% ;
            height: 200px;
        }

        .cover{
            width: 100%;
        }

        .cover img{
            width: 100%;
            height: 200px    ;
        }

        .status_item ,.float-left-desc{
            float: left;
        }

        @media (max-width: 768px) {
            .status_item {
                display: none;
            }
            .logo{
                margin-top: 30px;
            }
        }
    </style>
@endpush