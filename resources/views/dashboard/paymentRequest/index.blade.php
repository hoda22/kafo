@extends('dashboard.layouts.app', ['title' => __('app.titles.orders')])
@section('content')
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            {{--<label for="filter_type" class="col-sm-2 col-form-label"> {{__('app.order_type')}}  : </label>--}}
                            {{--<div class="col-sm-2">--}}
                                {{--<select class="form-control select2" name="type" id="filter_type">--}}
                                    {{--<option value=""> -- {{__("app.choose")}} -- </option>--}}

                                    {{--<option value="1"> ملغي </option>--}}
                                    {{--<option value="2"> جاري </option>--}}
                                    {{--<option value="3"> انتهي </option>--}}
                                {{--</select>--}}
                            {{--</div>--}}


                            <label for="period" class="col-sm-2 col-form-label"> {{__('app.duration')}} : </label>
                            <div class="col-sm-3">
                                <select class="form-control select2" name="type" id="period">
                                    <option value=""> -- {{__("app.choose")}} -- </option>
                                    <option value="1"> شهر </option>
                                    <option value="2"> 2 شهر </option>
                                    <option value="2"> 3 شهور </option>
                                    <option value="3"> سنه </option>
                                </select>
                            </div>

                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.client')}} :  </label>
                            <div class="col-sm-2">
                                <select class="form-control select2" name="client_id">
                                    <option value=""> -- {{__("app.choose")}} -- </option>
                                    @foreach( $clients as $client )
                                        <option value="{{$client->id}}"
                                            @if($client->id == request("client_id"))
                                                selected
                                            @endif
                                        > {{$client->FULLNAME}} </option>
                                    @endforeach
                                </select>
                            </div>

                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.provider')}} </label>
                            <div class="col-sm-2">
                                <select class="form-control select2" name="provider_id">
                                    <option value=""> -- {{__("app.choose")}} -- </option>
                                    @foreach( $providers as $provider )
                                        <option value="{{$provider->id}}"
                                                @if($provider->id == request("provider_id"))
                                                selected
                                                @endif
                                        > {{$provider->FULLNAME}} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items ])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.order_id')}} </th>
                                <th class="pt-0"> {{__('app.service')}} </th>
                                <th class="pt-0"> {{__('app.service_technician')}} </th>
                                <th class="pt-0"> {{__('app.service_requester')}} </th>
                                <th class="pt-0"> {{__('app.price')}} </th>
                                <th class="pt-0"> {{__('app.processes')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $items as $item )
                                <tr>
                                    <td> {{ $loop->iteration }} </td>
                                    <td> {{ $item->id }} </td>

                                    <td>
                                        <strong>
                                            {{ optional( optional($item->service)->parentItem )->name }}
                                        </strong>
                                            /
                                            {{ optional($item->service)->name }}
                                    </td>

                                    <td>
                                        <div style="width: 200px">
                                            <select class="form-control select2 technician_item" data-route="{{route("order.assign")}}" data-id="{{$item->id}}" name="client_id" >
                                                <option value=""> -- {{__("app.choose")}} -- </option>
                                                @foreach( $providers as $provider )
                                                    <option value="{{$provider->id}}"
                                                         @if( $provider->id == $item->provider_id )
                                                             selected
                                                         @endif
                                                    > {{$provider->full_name}} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>

                                    <td> {{ optional($item->client)->full_name }} </td>
                                    <td> {{ $item->price }} </td>
                                    <td>
                                        {{--@can("orders-show")--}}
                                        <a href="{{route("orders.show" , $item->id)}}"
                                           class="btn btn-success btn-icon aligner">
                                            <i data-feather="eye"></i>
                                        </a>

                                        {{--<form style="display: inline-block" method="post"--}}
                                              {{--action="{{route("orders.destroy", $item->id)}}"--}}
                                              {{--onsubmit="return confirm('هل أنت متأكد من الحذف؟')">--}}
                                            {{--@method('delete')--}}
                                            {{--@csrf--}}
                                            {{--<button type="submit" class="btn btn-danger btn-icon aligner"><i--}}
                                                    {{--data-feather="trash"></i></button>--}}
                                        {{--</form> --}}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script>
        $('.technician_item').change(function(){

            let id = $(this).data('id') ,
                val = $(this).val() ,
                route = $(this).data('route') ;

            axios.post(route, {
                val  : val ,
                id   : id
            }).then((response) => {
                console.log("response >> " , response )
                if (response.data.status == true) {
                    $("#messages_").empty().append("<p class='alert alert-success'> {{ __('app.assigned_to_order') }} </p>")
                    setTimeout(function() {
                        $("#messages_").empty()
                    }, 2000);
                }
            })
        });
    </script>

@endpush

