@extends('dashboard.layouts.app', ['title' => __('app.titles.air_conditioner_brands')])
@section('content')
    @push('button')
        @can("brands-create")
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                {{__('app.air_conditioner_brands.add')}}
            </button>
        </div>
        @endcan
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.air_conditioner_brands.name')}} </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="name" value="{{ request('name') }}"/>
                            </div>


                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items ])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.name_ar')}} </th>
                                <th class="pt-0"> {{__('app.name_en')}} </th>
                                <th class="pt-0"> {{__('app.image')}} </th>
                                <th class="pt-0"> {{__('app.AC_type')}} </th>
                                <th class="pt-0"> {{__('app.ordering')}} </th>
                                <th class="pt-0"> {{__('app.status')}} </th>
                                <th class="pt-0"> {{__('app.processes')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $items as $item )
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td> {{ optional($item->translate('ar'))->name }} </td>
                                    <td>
                                        {{ optional( $item->translate('en') )->name  }}
                                    </td>

                                    <td>
                                        <img src="{{asset($item->src)}}" />
                                    </td>

                                    <td> {{ $item->actype_text}} </td>
                                    <td> {{ $item->ordering}} </td>
                                    {{--<td> --}}
                                        {{--<span class="badge badge-lg badge-{{$item->active_status['className']}}"> {{$item->active_status['text']}}</span>--}}
                                        {{----}}
                                    {{--</td>--}}

                                    <td>
                                        {{--<span class="badge badge-lg badge-{{$item->active_status['className']}}"> {{$item->active_status['text']}}</span></td>--}}
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="active" value="active" class="change_status"
                                                       data-route="{{route("brand.updateAxios" , $item->id )}}"
                                                       data-id="{{$item->id}}"
                                                       @if($item->active == 'active')
                                                       checked
                                                        @endif
                                                >
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </td>

                                    <td>
                                        @can("brands-edit")
                                        <a href="#" data-id="{{$item->id}}" data-name="{{ optional($item->translate('ar'))->name }}" data-name_en="{{ optional($item->translate('en'))->name }}"
                                           data-active="{{ $item->active }}"
                                           data-ac_type="{{ $item->ac_type }}"
                                           data-img_src="{{ asset($item->src) }}"
                                           data-department_id="{{ $item->id }}"
                                           class="btn btn-primary btn-icon aligner edit">
                                            <i data-feather="edit"></i>
                                        </a>
                                        @endcan

                                        @can("brands-delete")
                                        <button type="submit" class="btn btn-danger btn-icon aligner"
                                                onclick="showSwal('passing-parameter-execute-cancel' , '{{route("brands.destroy", $item->id)}}' )" >
                                            <i data-feather="trash"></i>
                                        </button>
                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="danger text-center"> {{__('app.no_data')}} </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

    <script type="text/javascript">
        $(".edit").click(function (e) {
            e.preventDefault();
            let el = $(this),
                id = el.data('id'),
                name = el.data('name'),
                name_en = el.data('name_en'),
                ordering = el.data('ordering'),
                department = el.data('department_id'),
                img_src = el.data('img_src'),
                active = el.data('active'),
                ac_type = el.data('ac_type'),
                route = "{{ route('brands.update', ':id') }}";
            route = route.replace(':id', id);


            $(document).find("#edit_name").val(name);
            $(document).find("#edit_ordering").val(ordering);
            $(document).find("#edit_name_en").val(name_en);
            $(document).find("#edit_ac_type").val(ac_type);
            $(document).find("#img_src").attr("src",img_src);

            if ( active == 'active' ){
                $(document).find("#edit_active").prop('checked',true);
            }else{
                $(document).find("#edit_active").prop('checked',false);
            }

            $(document).find("#edit_department").val(department);
            $(document).find("#editModal").modal('show');
            $(document).find("#editModal form").attr('action', route);
        });
    </script>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel">{{__("app.air_conditioner_brands.add")}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route("brands.store")}}" id="add-form" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name" class="col-form-label"> {{__('app.name_ar')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name" id="add_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name_en" class="col-form-label">{{__('app.name_en')}}  <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name_en" id="add_name_en">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_image" class="col-form-label"> {{__('app.image')}}   <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="file" class="form-control" name="src" id="add_image" accept="image/png, image/gif, image/jpeg">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_type" class="col-form-label"> {{__('app.AC_type')}}  <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control" name="ac_type" id="add_type">
                                    <option value=""> -- {{__('app.choose')}} --</option>
                                    @foreach( \App\Enums\AcTypeEnum::cases() as $acType )
                                    <option value="{{$acType->name}}"> {{$acType->value}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name" class="col-form-label">{{__('app.ordering')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="ordering" id="add_ordering">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">تعديل ماركه </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="" enctype="multipart/form-data" id="edit-form">
                        @method('put')
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_name" class="col-form-label">{{__('app.name_ar')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name" id="edit_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_name" class="col-form-label">{{__('app.name_en')}}   <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name_en" id="edit_name_en">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_image" class="col-form-label"> {{__('app.old_image')}}  :</label>
                            </div>
                            <div class="col-lg-9">
                                <div class="old-img">
                                    <span> x </span>
                                    <img src="" id="img_src" width="50" height="50" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_image" class="col-form-label"> {{__('app.image')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="file" class="form-control" name="src" id="add_image" accept="image/png, image/gif, image/jpeg">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_ac_type" class="col-form-label"> {{__('app.AC_type')}}  <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control" name="ac_type" id="edit_ac_type">
                                    @foreach( \App\Enums\AcTypeEnum::cases() as $acType )
                                        <option value="{{$acType->name}}" > {{$acType->value}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="edit_ordering" class="col-form-label">{{__('app.ordering')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="ordering" required id="edit_ordering">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active" id="edit_active">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('dashboard.brands.script')
@endpush
