<script>
    $("#add-form").validate({
        rules: {
            name: {
                required: true,
            },
            name_en :{
                required: true,
            },

            ordering :{
                required: true,
            },

            brand_id :{
                required: true,
            },

            ac_type :{
                required: true,
            },

            src :{
                required: true,
            },
            action: "required"
        },
        messages: {
            name: {
                required: "{{__('app.validation.field_name_required')}}"
            },
            name_en: {
                required: "{{__('app.validation.field_name_en_required')}}"
            },

            brand_id: {
                required: "{{__('app.validation.field_air_conditioner_type_required')}}"
            },

            ac_type: {
                required: "{{__('app.validation.field_ac_type_required')}}"
            },

            ordering: {
                required: "{{__('app.validation.field_ordering_required')}}"
            },
            src :{
                required: "{{__('app.validation.field_src_required')}}"
            },


            action: "Please provide some data"
        }
    });


    $("#edit-form").validate({
        rules: {
            name: {
                required: true,
            },
            name_en :{
                required: true,
            },

            ordering :{
                required: true,
            },

            brand_id :{
                required: true,
            },

            ac_type :{
                required: true,
            },
            action: "required"
        },
        messages: {
            name: {
                required: "{{__('app.validation.field_name_required')}}"
            },
            name_en: {
                required: "{{__('app.validation.field_name_en_required')}}"
            },

            brand_id: {
                required: "{{__('app.validation.field_air_conditioner_type_required')}}"
            },

            ac_type: {
                required: "{{__('app.validation.field_ac_type_required')}}"
            },

            ordering: {
                required: "{{__('app.validation.field_ordering_required')}}"
            },
            src :{
                required: "{{__('app.validation.field_src_required')}}"
            },


            action: "Please provide some data"
        }
    });
</script>