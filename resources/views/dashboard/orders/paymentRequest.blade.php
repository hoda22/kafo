@extends('dashboard.layouts.app', ['title' => __('app.titles.paymentRequest')])
@section('content')
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.client')}} :  </label>
                            <div class="col-sm-4">
                                <select class="form-control select2" name="client_id">
                                    <option value=""> -- {{__("app.choose")}} -- </option>
                                    @foreach( $clients as $client )
                                        <option value="{{$client->id}}"
                                                @if($client->id == request("client_id"))
                                                selected
                                                @endif
                                        > {{$client->FULLNAME}} </option>
                                    @endforeach
                                </select>

                            </div>


                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.provider')}} :  </label>
                            <div class="col-sm-4">
                                <select class="form-control select2" name="provider_id">
                                    <option value=""> -- {{__("app.choose")}} -- </option>
                                    @foreach( $providers as $provider )
                                        <option value="{{$provider->id}}"
                                                @if($provider->id == request("provider_id"))
                                                selected
                                                @endif
                                        > {{$provider->FULLNAME}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.payment_method')}} :  </label>
                            <div class="col-sm-4">
                                <select class="form-control select2" name="paymentMethod">
                                    <option value=""> -- {{__("app.choose")}} -- </option>
                                    @foreach( $paymentMethods as $key =>$paymentMethod )
                                        <option value="{{$key}}" {{$key == request("paymentMethod") ? "selected":""}}>
                                            {{$paymentMethod }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>
                            </div>
                            <div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items ])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0"> {{__('app.order_id')}} </th>
                                {{--<th class="pt-0"> {{__('app.order_status')}} </th>--}}
                                <th class="pt-0"> {{__('app.service_technician')}} </th>
                                <th class="pt-0"> {{__('app.service_requester')}} </th>
                                <th class="pt-0"> {{__('app.payment_method')}} </th>
                                <th class="pt-0"> {{__('app.is_paid')}} </th>
                                <th class="pt-0"> {{__('app.price')}} </th>
                                @can('payment-requests-collection')
                                <th class="pt-0"> {{__('app.processes')}} </th>
                                @endcan
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $items as $item )
                                <tr class="tr_row_{{$item->id}}">
                                    <td> {{ $item->id }} </td>
                                    <td>
                                        @if( $item->provider )
                                            {{$item->provider->full_name}}
                                        @else
                                            <p>
                                                {{__('app.not_yet')}}
                                            </p>
                                        @endif
                                    </td>
                                    <td>
                                        {{ optional( $item->client )->full_name }}
                                    </td>
                                    <td> {{ $item->payment_method }}  </td>
                                    <td class="is_paid"> {{ $item->paid_done }} </td>

                                    <td> {{ $item->total }}  {{ $item->total ? __("app.SAR") :"" }}</td>
                                    @can('payment-requests-collection')
                                    <td class="collection_btn_js">
                                        {{--{{$item->method_payment ." -- ". $item->is_paid }}--}}
                                        {{--$item->payment_method == 1 &&--}}
                                        @if( $item->method_payment == 1 )
                                            @if( $item->is_paid == 0 )
                                            <button type="button"
                                            class="btn btn-success aligner collection_js"
                                               data-order-id="{{$item->id}}"
                                               data-route="{{route("order.updateCollection" , $item->id )}}"
                                            >
                                                تحصيل من الفني
                                            </button>
                                            @endif
                                        @endif


                                    </td>
                                    @endcan
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script>
        $('.collection_js').click(function(){
            let route = $(this).data('route'),
                id    = $(this).data('order-id') ;

            $(this).text()
            axios.post(route).then((response) => {
                console.log("response >> " , response )

                if (response.data.status == true) {


                    $(".tr_row_"+id).find(".is_paid").text("{{__('app.paid')}}")
                    $(".tr_row_"+id).find(".collection_btn_js").text("")

                    $("#messages_").empty().append("<p class='alert alert-success'> {{ __('app.collection_done') }} </p>")
                    setTimeout(function() {
                        $("#messages_").empty()
                    }, 2000);
                }
            })
        });
    </script>

@endpush

