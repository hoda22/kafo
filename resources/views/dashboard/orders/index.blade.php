@extends('dashboard.layouts.app', ['title' => __('app.titles.orders')])
@section('content')
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            {{--<label for="filter_type" class="col-sm-2 col-form-label"> {{__('app.order_type')}}  : </label>--}}
                            {{--<div class="col-sm-2">--}}
                                {{--<select class="form-control select2" name="type" id="filter_type">--}}
                                    {{--<option value=""> -- {{__("app.choose")}} -- </option>--}}

                                    {{--<option value="1"> ملغي </option>--}}
                                    {{--<option value="2"> جاري </option>--}}
                                    {{--<option value="3"> انتهي </option>--}}
                                {{--</select>--}}
                            {{--</div>--}}


                            <label for="status" class="col-sm-2 col-form-label">  {{__('app.status')}} : </label>
                            <div class="col-sm-4">
                                <select class="form-control" name="status" id="status">
                                    <option value=""> -- {{__("app.choose")}} -- </option>

                                    @foreach( $orderStatusItems as $key => $orderStatusItem )
                                    <option value="{{$key}}"> {{$orderStatusItem}} </option>
                                    @endforeach
                                </select>
                            </div>

                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.client')}} :  </label>
                            <div class="col-sm-4">
                                <select class="form-control select2" name="client_id">
                                    <option value=""> -- {{__("app.choose")}} -- </option>
                                    @foreach( $clients as $client )
                                        <option value="{{$client->id}}"
                                            @if($client->id == request("client_id"))
                                                selected
                                            @endif
                                        > {{$client->FULLNAME}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.provider')}} :  </label>
                            <div class="col-sm-4">
                                <select class="form-control select2" name="provider_id">
                                    <option value=""> -- {{__("app.choose")}} -- </option>
                                    @foreach( $providers as $provider )
                                        <option value="{{$provider->id}}"
                                            @if($provider->id == request("provider_id"))
                                            selected
                                            @endif
                                        > {{$provider->FULLNAME}} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>

                            </div>

                            <input type="button" value="PLAY" onclick="play()">
                            {{--<audio id="audio" src="https://interactive-examples.mdn.mozilla.net/media/cc0-audio/t-rex-roar.mp3"></audio>--}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items ])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.order_id')}} </th>
                                <th class="pt-0"> {{__('app.service_technician')}} </th>
                                <th class="pt-0"> {{__('app.service_requester')}} </th>
                                <th class="pt-0"> {{__('app.price')}} </th>
                                <th class="pt-0"> {{__('app.status')}} </th>
                                <th class="pt-0"> {{__('app.processes')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $items as $item )
                                <tr class="order-id-{{$item->id}}
                                        @if( $item->status == App\Enums\OrderStatusEnum::RejectFromMangment->value )
                                        table-danger
                                        @elseif( $item->status == 9 )
                                        table-warning

                                        @elseif( $item->status == 8 )
                                        table-danger
                                        @elseif( $item->status == 11 || $item->status == 12 || $item->status == 13 )
                                        table-warning
                                        @endif

                                        ">
                                    <td> {{ $loop->iteration }} </td>
                                    <td> {{ $item->id }} </td>

                                    <td>
                                        @if( $item->provider )
                                            {{$item->provider->full_name}}
                                        @else
                                            <span class="no-provider">
                                                {{__('app.it_is_not_determined_yet')}}
                                            </span>
                                        @endif
                                    </td>

                                    <td> {{ optional($item->client)->full_name }} </td>
                                    <td> {{ $item->total }} </td>
                                    <td class="status"> {{ $item->status_order }} </td>
                                    <td>
                                        {{--@can("orders-show")--}}
                                        @if ( $item->status == 1 || $item->status == "" || $item->status == 9 )
                                        <button data-id="{{$item->id}}" class="btn btn-success btn-icon aligner approve-status"
                                            {{--data-date="{{date('Y-m-d', strtotime($item->set_at))}}"--}}
                                            {{--data-time="{{date('H:i:s', strtotime($item->set_at))}}"--}}

                                            data-date="{{$item->service_date}}"
                                            data-time="{{$item->service_time}}"
                                            data-lat="{{$item->lat }}"
                                            data-lon="{{$item->lon}}"
                                        >
                                            <i data-feather="check"></i>
                                        </button>

                                        <button data-id="{{$item->id}}" class="btn btn-danger btn-icon aligner reject-status" >
                                            <i data-feather="x"></i>
                                        </button>
                                        @endif


                                        @if ( $item->status == 8 )
                                            <p class="mb-1 order_canceled">
                                                {{__('app.order_canceled_by_technician')}}
                                            </p>
                                            <button data-id="{{$item->id}}" class="btn btn-success aligner approve-status"
                                                data-reasign="1"
                                                data-date="{{$item->service_date}}"
                                                data-time="{{$item->service_time}}"
                                                data-lat="{{$item->lat }}"
                                                data-lon="{{$item->lon}}"
                                            >
                                                {{__('app.approve')}}
                                            </button>

                                            <button data-id="{{$item->id}}" data-route="{{route("order.rejectCanceled" , $item->id )}}" class="btn btn-danger aligner reject-canceled" >
                                                {{__('app.reject')}}
                                            </button>
                                        @endif


                                        @if ( $item->status == 11  )
                                            <p class="mb-1 order_canceled">
                                                {{__('app.order_modification_technician')}}
                                            </p>

                                        @endif

                                        @if( $item->status == 12 )
                                            <p class="mb-1 order_canceled">
                                                {{__('app.client_approved_new_cost')}}
                                            </p>
                                            {{--<button data-id="{{$item->id}}" data-status="1" data-total="{{$item->price}}" class="btn btn-success aligner order-modification-status">--}}
                                                {{--{{__('app.approved')}}--}}
                                            {{--</button>--}}
                                        @endif

                                        @if( $item->status == 13 )
                                            {{--<p class="mb-1 order_canceled">--}}
                                                {{--{{__('app.client_rejected_new_cost')}}--}}
                                            {{--</p>--}}
                                            {{--<button data-id="{{$item->id}}" data-status="0" data-total="{{$item->price}}" class="btn btn-success aligner order-modification-status">--}}
                                                {{--{{__('app.approved')}}--}}
                                            {{--</button>--}}
                                        @endif

                                        <a href="{{route("orders.show" , $item->id)}}"
                                           class="btn btn-success btn-icon aligner">
                                            <i data-feather="eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <div class="modal fade" id="editOrderModal" tabindex="-1" role="dialog" aria-labelledby="editOrderLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editOrderLabel"> {{__('app.edit_request')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="" id="edit_request_form" >
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label for="add_category" class="col-form-label"> {{__('app.total_order')}}  :</label>
                            </div>
                            <div class="col-lg-4">
                                <input class="form-control" id="old_cost" value="" readonly  />
                                <input type="hidden" id="order_id_" value="" />
                            </div>
                            <div class="col-lg-3">
                                <span class="mt-3">
                                    {{__('app.SAR')}}
                                </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label for="new_cost" class="col-form-label"> {{__('app.new_cost')}}  :</label>
                            </div>
                            <div class="col-lg-4">
                                <input class="form-control" type="number" id="new_cost" value="" required  />
                            </div>
                            <div class="col-lg-3">
                                <span class="mt-3">
                                    {{__('app.SAR')}}
                                </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary"> {{__('app.send')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel"> {{__('app.technical_assign')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="" id="assign-form">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_category" class="col-form-label"> {{__('app.providers_')}}  :</label>
                            </div>
                            <div class="col-lg-9">
                                <select class="form-control select2" name="category" id="providers_va">
                                    {{--<option value="0"> -- {{__('app.choose')}} -- </option>--}}
                                    {{--@foreach( $providers as $provider )--}}
                                    {{--<option value="{{$provider->id}}"> {{$provider->full_name}} </option>--}}
                                    {{--@endforeach--}}
                                </select>

                                <input type="hidden" id="order_id" value="" />
                                <input type="hidden" id="re_assign" value="" />
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModalLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalLabel"> {{__('app.rejectModel')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="" id="reject-form">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="management_reject_reson" class="col-form-label"> {{__('app.reason')}}  :</label>
                            </div>
                            <div class="col-lg-9">
                                <input type="hidden" id="order_reject_id" value="">
                                 <textarea class="form-control" rows="4" name="management_reject_reson" id="management_reject_reson"></textarea>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>







    <script>
        $(".approve-status").click(function () {

            $("#providers_va").val(0).trigger("change");

            let date    = $(this).data("date") ,
                time    = $(this).data("time") ,
                lat     = $(this).data("lat") ,
                reasign = $(this).data("reasign") ,
                lon     = $(this).data("lon") ;


            let route   = "{{route('order.getProviders')}}";
            axios.post(route, {
                date  : date ,
                lat  : lat ,
                lon  : lon ,
                reasign  : reasign ,
                time  : time
            }).then((response) => {
                console.log("response >> " , response )
                if (response.data.status == true) {
                    $("#providers_va").empty().append(response.data.data)
                }
            })



            let id = $(this).data('id')
            $("#order_id").val(id) ;
            $("#re_assign").val(reasign) ;
            $('#createModal').modal('show');
        });


        //===========================================================

        $(".order-modification").click(function (e) {
            e.preventDefault();
            let orderId = $(this).data('id') ;
            let total = $(this).data('total') ;
            $("#old_cost").val(total) ;
            $("#order_id_").val(orderId) ;
            $('#editOrderModal').modal('show');
        });


        $("#edit_request_form").on("submit", function (e) {
            e.preventDefault();
            let newCost = $("#new_cost").val(),
                orderId = $("#order_id_").val(),
                messageStatus= "تم ارسال التكلفه الجديده للعميل"
                route = "{{route('order.newCost')}}";

            axios.post(route, {
                orderId: orderId,
                newCost: newCost
            }).then((response) => {
                console.log("response >> " , response )
                if (response.data.status == true) {
                    $("#messages_").empty().append("<p class='alert alert-success'> {{ __('تم إرسال التكلفه الجديدة العميل') }} </p>")
                    setTimeout(function() {
                        $("#messages_").empty()
                    }, 8000);

                    $('#editOrderModal').modal('hide');

                    $("tr.order-id-"+orderId).removeClass("table-warning")
                    $("tr.order-id-"+orderId).find(".approve-status").remove()
                    $("tr.order-id-"+orderId).find(".order_canceled").remove()
                    $("tr.order-id-"+orderId).find(".status").text(messageStatus)
                    $("tr.order-id-"+orderId).find(".reject-status").remove()
                    $("tr.order-id-"+orderId).find(".order-modification").remove()

                }
            })

        });

        //===========================================================

        $(".order-modification-status").click(function (e) {
            e.preventDefault();
            let status_ = $(this).data("status");
            var message ="";
            var messageStatus ="";
            if (status_){
                message = "<p class='alert alert-success'> {{ __('تم ارسال اشعار للفني بانه تم الموفقه طلب التعديل') }} </p>"
                messageStatus = "تم ارسال الموافقه علي التعديل للفني"
            } else{
                message = "<p class='alert alert-success'> {{ __('تم ارسال اشعار للفني بانه تم رفض طلب التعديل') }} </p>"
                messageStatus = "تم ارسال الرفض علي التعديل للفني"
            }
            let orderId = $(this).data("id"),
                status  =  status_,
                that  = $(this) ,
                route   = "{{route('order.modification-status')}}"

            axios.post(route, {
                orderId   : orderId,
                status    : status
            }).then((response) => {
                console.log("responsesa 11>> " , response )
                if (response.data.status == true) {
                    that.css("display","none")
                    $("#messages_").empty().append(message)
                    setTimeout(function() {
                        $("#messages_").empty()
                    }, 8000);

                    $("tr.order-id-"+orderId).removeClass("table-warning")
                    $("tr.order-id-"+orderId).find(".approve-status").remove()
                    $("tr.order-id-"+orderId).find(".order_canceled").remove()
                    $("tr.order-id-"+orderId).find(".status").text(messageStatus)
                    $("tr.order-id-"+orderId).find(".reject-status").remove()

                }
            })
        })


        //===========================================================

        $(".reject-status").click(function () {
            let id = $(this).data("id")
            $("#order_reject_id").val(id);
            $('#rejectModal').modal('show');
            $("#management_reject_reson").val('');
        });

        $("#reject-form").on("submit" , function (e) {
            e.preventDefault();
            let orderId = $('#order_reject_id').val() ,
                reason = $('#management_reject_reson').val() ,
                route = "{{route('order.reject')}}";

            console.log("orderId >> ",orderId)
            console.log("reson >> ", reason)

            axios.post(route, {
                orderId   : orderId,
                reason: reason
            }).then((response) => {
                console.log("response >> " , response )
                if (response.data.status == true) {
                    $("#messages_").empty().append("<p class='alert alert-success'> {{ __('تم رفض طلب العميل') }} </p>")
                    setTimeout(function() {
                        $("#messages_").empty()
                    }, 8000);

                    $('#rejectModal').modal('hide');

                    $("tr.order-id-"+orderId).addClass("table-danger")
                    $("tr.order-id-"+orderId).find(".approve-status").remove()
                    $("tr.order-id-"+orderId).find(".reject-status").remove()

                    $("tr.order-id-"+orderId).find(".status").empty().text("مرفوض من قبل الاداره")

                }
            })
        }) ;


        $('#providers_va').select2({
            dropdownParent: $('#createModal')
        });

        $('.technician_item').change(function(){

            let id = $(this).data('id') ,
                val = $(this).val() ,
                route = $(this).data('route') ;

            axios.post(route, {
                val  : val ,
                id   : id
            }).then((response) => {
                console.log("response >> " , response )
                if (response.data.status == true) {
                    $("#messages_").empty().append("<p class='alert alert-success'> {{ __('تم ارسال الطلب لفني و في انتظار الموافقه .. الطلب تحت المراجعه الان') }} </p>")
                    setTimeout(function() {
                        $("#messages_").empty()
                    }, 2000);
                }
            })
        });


        $('.reject-canceled').click(function(){

            let id = $(this).data('id') ,
                route = $(this).data('route') ;

            console.log(id , route)
            axios.post(route, {
                id   : id
            }).then((response) => {
                console.log("response >> " , response )
                if (response.data.status == true) {
                    $("#messages_").empty().append("<p class='alert alert-success'> {{ __('app.request_was_returned_to_technician_again') }} </p>");


                    $("tr.order-id-"+id).removeClass("table-danger");
                    $("tr.order-id-"+id).find(".order_canceled").remove();
                    $("tr.order-id-"+id).find(".reject-canceled").remove();

                    $("tr.order-id-"+id).find(".approve-status").remove();

                    $("tr.order-id-"+id).find(".reject-status").remove();
                    $("tr.order-id-"+id).find(".status").empty().text("{{__('app.technician_assigned')}}")



                    setTimeout(function() {
                        $("#messages_").empty()
                    }, 2000);
                }
            })
        });





        $("#assign-form").on("submit" , function (e) {
            e.preventDefault();
            let orderId = $(this).find('#order_id').val() ,
                providerId = $(this).find('#providers_va').val() ,
                reAssign = $(this).find('#re_assign').val() ,
                route = "{{route('order.assign')}}";

            axios.post(route, {
                orderId   : orderId,
                reAssign   : reAssign,
                providerId: providerId
            }).then((response) => {
                console.log("response >> " , response )
                if (response.data.status == true) {
                    $("#messages_").empty().append("<p class='alert alert-success'> {{ __('تم ارسال الطلب لفني و في انتظار الموافقه .. الطلب تحت المراجعه الان') }} </p>")
                    setTimeout(function() {
                        $("#messages_").empty()
                    }, 8000);

                    $('#createModal').modal('hide');

                    $("tr.order-id-"+orderId).find(".approve-status").remove()
                    $("tr.order-id-"+orderId).removeClass("table-danger")
                    $("tr.order-id-"+orderId).find(".reject-status").remove()
                    $("tr.order-id-"+orderId).find(".order_canceled").remove()
                    $("tr.order-id-"+orderId).find(".reject-canceled").remove()


                    $("tr.order-id-"+orderId).find(".status").empty().text("{{__('app.under_revision')}}")

                }
            })
        }) ;

    </script>

@endpush

