@extends('dashboard.layouts.app', ['title' => __('app.show_order')])

@section('content')

    @push('button')

        <div class="d-flex align-items-center flex-wrap text-nowrap btn-back">

            <a href="{{route('orders.index')}}" class="btn btn-primary">

                <i data-feather="skip-back"></i>

            </a>

        </div>

    @endpush

    <div class="col-lg-12 col-xl-12 stretch-card">

        <div class="card">

            <div class="card-body ">

                <div class="row">

                    <div class="col-md-12">

                        <div id="messages_">

                        </div>

                    </div>



                    <div class="col-sm-12">

                        <div class="row">

                            <div class="col-md-1 col-sm-4 col-4 status_item">

                                <strong>  {{__('app.status')}} :  </strong>

                            </div>

                            <div class="col-md-3 col-sm-8 col-8 ">

                                {{--{{optional($data->status)->name}}--}}

                                {{ $data->status_order}}

                                @if( $data->status == App\Enums\OrderStatusEnum::RejectFromMangment->value )

                                @endif

                            </div>



                            @if( $data->status == App\Enums\OrderStatusEnum::RejectFromMangment->value )

                            <div class="col-md-1 col-sm-4 col-4 status_item">

                                <strong>  {{__('app.reason')}} :  </strong>

                            </div>

                            <div class="col-md-6 col-sm-8 col-8 ">

                                @if( $data->status == App\Enums\OrderStatusEnum::RejectFromMangment->value )

                                    {{$data->management_reject_reson}}

                                @endif

                            </div>

                            @endif



                            @if( $data->status == App\Enums\OrderStatusEnum::Cancel->value )

                                <div class="col-md-1 col-sm-4 col-4 status_item">

                                    <strong>  {{__('app.reason')}} :  </strong>

                                </div>

                                <div class="col-md-3 col-sm-8 col-8 ">

                                    {{$data->reject_reson}}

                                </div>

                            @endif

                            <div class="col-md-12">

                                <hr class="w-100">

                            </div>

                            <div class="col-md-2  col-sm-6 col-6"><strong> {{__('app.price')}} :  </strong></div>

                            <div class="col-md-2 col-sm-6 col-6">

                                {{ $data->total}}



                                @if( $data->total )

                                    {{__('app.SAR')}}

                                @endif

                            </div>



                            <div class="col-md-2  col-sm-6 col-6"><strong> {{__('app.price_paid')}} :  </strong></div>

                            <div class="col-md-2 col-sm-6 col-6">

                                @if( $data->is_paid == 1 )

                                {{ $data->total}}



                                    @if( $data->total )

                                        {{__('app.SAR')}}

                                    @endif

                                @endif

                            </div>



                            <div class="col-md-2  col-sm-6 col-6"><strong> {{__('app.service_date')}} :  </strong></div>

                            <div class="col-md-2 col-sm-6 col-6">



                                <span id="timeLocal"> </span>

                                <script>



                                    var utcTime = "{!! $data->service_date . " " . $data->service_time !!}"

                                    var utcDate = new Date( utcTime + " UTC");

                                    var localDate = new Date(utcDate.toLocaleString());



                                    // var dateString = "Sat Nov 25 2023 13:28:38 GMT+0200 (Eastern European Standard Time)";



                                    // Create a Date object from the string

                                    var dateObject = new Date(localDate);

                                    // Extract date components

                                    var day = dateObject.getDate();

                                    var month = dateObject.getMonth() + 1; // Months are zero-based

                                    var year = dateObject.getFullYear();

                                    var hours = dateObject.getHours();

                                    var minutes = dateObject.getMinutes();

                                    var seconds = dateObject.getSeconds();



                                    // Format the date

                                    var formattedDate = `${year}-${month}-${day}  ${hours}:${minutes}:${seconds}`;



                                    console.log("formattedDate >> " ,formattedDate);

                                    console.log("LLLL.." , utcTime , "--", localDate)



                                    {{--let doc = document.getElementById("time-"+{!! $item->id !!})--}}



                                    let d = document.getElementById('timeLocal')

                                    d.innerText = formattedDate;

                                </script>





                            </div>





                            <div class="col-md-12">

                                <hr class="w-100">

                            </div>



                            <div class="col-md-2 col-sm-6 col-6"><strong> {{__('app.date_of_registration')}} :    </strong></div>

                            <div class="col-md-4 col-sm-6 col-6">  {{ $data->created_at }} </div>





                            <div class="col-md-2 col-sm-6 col-6"><strong> {{__('app.city')}} :    </strong></div>

                            <div class="col-md-4 col-sm-6 col-6">  {{ ( $city ) ? $city->name : "لم تحدد" }} </div>



                            <div class="col-md-12">

                                <hr class="w-100">

                            </div>



                            <div class="col-md-2 col-sm-6 col-6"><strong> {{__('app.management_comment')}} : </strong></div>

                            <div class="col-md-10 col-sm-6 col-6">

                                <div id="management_comment" style="display: inline-block;">{{ $data->management_comment }} </div>

                                <span class="btn btn-primary btn-icon aligner">

                                <i class="link-icon" data-feather="edit-2" id="edit-3" ></i>

                                </span>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    @if( count($data->trackingOrder) )

    <div class="col-lg-12 mt-4 ">

        <div class="row">

            <div class="col-md-12">

                <div class="card ">

                    <h4 class="mb-3 mt-3" style="padding: 0 20px 0 20px;margin-bottom: 0 !important;">

                        {{__('app.cases_of_rejection_and_cancellation')}}

                    </h4>

                    <div class="card-body row  pb-0">

                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"> {{__('app.status')}} </th>
                                <th scope="col"> {{__('app.provider')}} </th>
                                <th scope="col"> {{__('app.reason')}} </th>
                                <th scope="col"> {{__('app.created_at')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $data->trackingOrder as $tracking )
                                <tr>
                                    <th scope="row">
                                        {{ $loop->iteration }}</th>
                                    <td>
                                    @if( $tracking->status_id == 9 )
                                        {{__('app.reject')}}
                                        @else
                                        {{__('app.cancel')}}
                                        @endif
                                    </td>
                                    <td>
                                        {{ optional($tracking->provider)->full_name}}
                                    </td>
                                    <td>
                                        {{$tracking->reason}}
                                    </td>
                                    <td>
                                        {{$tracking->created_at}}
                                    </td>
                                </tr>
                            @empty
                                <tr >
                                    <td colspan="4" class="text-center">{{__('app.no_services')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="col-lg-12 mt-4 ">

        <div class="row">

            <div class="col-md-12">

                <div class="card ">

                    <h4 class="mb-3 mt-3" style="padding: 0 20px 0 20px;margin-bottom: 0 !important;">

                        تفاصيل الطلب

                    </h4>

                    <div class="card-body row  pb-0">
                        <table class="table mb-0">

                            <thead>

                            <tr>

                                <th scope="col">#</th>

                                <th scope="col">{{__('app.basic_service')}} </th>

                                <th scope="col">{{__('app.sup_category')}} </th>

                                <th scope="col"> {{__('app.quantity')}} </th>
                                <th scope="col"> {{__('app.unit_price')}} </th>
                                <th scope="col"> {{__('app.price')}} </th>
                                <th scope="col"> {{__('app.notes')}} </th>
                                <th scope="col"> {{__('app.status')}} </th>
                                {{--<th scope="col"> {{__('app.status_edit_request')}} </th>--}}

                            </tr>

                            </thead>

                            <tbody>

                            @forelse( $data->orderDetailsItems as $orderDetails )

                                <tr
                                    @if( $orderDetails->admin_status == 'rejected_client' )
                                        class="table table-danger"
                                    @endif
                                >

                                    <th scope="row">{{ $loop->iteration }}</th>

                                    <td>{{optional( $orderDetails->service_parent )->name}}</td>

                                    <td>{{optional( $orderDetails->service_item )->name}}</td>
                                    <td>

                                        {{ $orderDetails->quantity }}

                                    </td>
                                    <td>{{ $orderDetails->unit_price }}</td>

                                    <td>
                                        @if( $orderDetails->admin_status == 'rejected_client' )
                                            {{$orderDetails->visit}} {{__('app.SAR')}}
                                        @else
                                            {{ $orderDetails->price }} {{ $orderDetails->price ? __('app.SAR') : "" }}
                                        @endif
                                    </td>



                                    <td>

                                        {{ $orderDetails->description }}

                                    </td>



                                    <td>
                                        @if( $orderDetails->admin_status == 'rejected_client' )
                                           رسوم الزياره
                                        @elseif ( $orderDetails->notes )

                                            {{__('app.'.$orderDetails->notes)}}

                                        @endif

                                    </td>

                                    {{--<td>--}}
                                        {{--{{  $orderDetails->admin_status }}--}}
                                    {{--</td>--}}

                                </tr>

                            @empty

                                <tr >

                                    <td colspan="9" class="text-center">{{__('app.no_services')}}</td>

                                </tr>

                            @endforelse



                            </tbody>

                        </table>
                    </div>



                    @if( count( $data->orderDetailsItemsEdit ) )

                    <h4 class="mb-3 mt-3" style="padding: 0 20px 0 20px;margin-bottom: 0 !important;">
                        {{__('app.edit_request')}}

                    </h4>



                    <div class="card-body row">

                        <table class="table mb-0">

                            <thead>

                            <tr>

                                <th scope="col">#</th>

                                {{--<th scope="col">الخدمه الاساسيه </th>--}}

                                <th scope="col"> {{__('app.sub_service')}} </th>

                                <th scope="col"> {{__('app.description_problem')}}  </th>

                                <th scope="col"> {{__('app.used_spare_parts')}} </th>

                                <th scope="col"> {{__('app.edit_images')}}  </th>

                                <th scope="col"> {{__('app.price')}}  </th>

                                <th scope="col">{{__('app.status_edit_request')}} </th>

                                {{--<th scope="col">{{__('app.actions')}} </th>--}}

                            </tr>

                            </thead>

                            <tbody>

                            @forelse( $data->orderDetailsItemsEdit as $orderDetails )

                                <tr>

                                    <th scope="row">{{ $loop->iteration }}</th>

                                    <td>



                                        @foreach( $orderDetails->getServices() as $supService )

                                            {{$supService->name}} <br>

                                        @endforeach

                                            {{--{{ $orderDetails->service_id }}--}}

                                    </td>

                                    <td>{{ $orderDetails->description }}</td>



                                    <td>

                                        @forelse(  $orderDetails->sparePartsOrderDetials as $sparePartsOrderDetials )

                                            {{ optional($sparePartsOrderDetials->sparePart)->name  }} . <br><br>

                                        @empty

                                            {{__('app.no_spare_parts_used')}}

                                        @endforelse

                                    </td>



                                    <td>

                                        @if( $orderDetails->images )

                                            @foreach( $orderDetails->images as $image )

                                                <img src="{{asset($image)}}" style="width: 100px; height: 100px" class="myImg">

                                            @endforeach

                                        @endif

                                    </td>



                                    <td>

                                        {{$orderDetails->price}} {{__('app.SAR')}}

                                    </td>



                                    {{--<td>--}}

                                        {{--{{__("app.".$orderDetails->admin_status)}}--}}

                                    {{--</td>--}}



                                    <td>

                                        @if( $orderDetails->admin_status == 'under_revision' )

                                        <button data-id="{{$data->id}}" data-total="{{$data->basic_total}}" data-status="1" class="btn btn-success aligner order-modification"

                                                data-service-price="{{$orderDetails->price}}"

                                                data-service-name="{{$supService->name}}"

                                                data-service-details-id="{{$orderDetails->id}}"

                                        >

                                            {{__('app.approve_edit_request')}}

                                        </button>



                                        <button data-id="{{$data->id}}" data-total="{{$data->basic_total}}" data-status="0" class="btn btn-danger aligner order-modification-reject">

                                            {{__('app.approve_edit_request_reject')}}

                                        </button>

                                        @else

                                            {{__("app.".$orderDetails->admin_status)}}

                                        @endif

                                    </td>

                                </tr>

                            @empty

                                <tr >

                                    <td colspan="9" class="text-center">{{__('app.no_services')}}</td>

                                </tr>

                            @endforelse

                            </tbody>

                        </table>

                    </div>

                    @endif

                </div>

            </div>

        </div>

    </div>





    <div class="col-lg-12 mt-4 ">

        <div class="row">

            <div class="col-md-6">

                <div class="card ">

                <h4 class="mb-3 mt-3" style="padding: 0 20px 0 20px;margin-bottom: 0 !important;"> {{__('app.service_requester_information')}} </h4>

                <hr style="margin-bottom: 0 !important;">

                @if( $data->client )

                <div class="card-body row">

                    <div class="col-sm-5 col-4"><strong> {{__('app.name_of_service_applicant')}} :  </strong></div>

                    <div class="col-sm-7 col-8">  {{ optional($data->client)->full_name}}  </div>



                    <div class="col-md-12">

                        <hr class="w-100">

                    </div>



                    <div class="col-md-5 col-sm-6 col-6"><strong>  {{__('app.rating')}}  :  </strong></div>

                    <div class="col-md-7 col-sm-6 col-6"> {{$data->client_rate}} </div>


                    <div class="col-md-12">

                        <hr class="w-100">

                    </div>

                    <div class="col-md-5 col-sm-6 col-6"><strong>  {{__('app.client_comment')}}  :  </strong></div>

                    <div class="col-md-7 col-sm-6 col-6"> {{$data->client_rate_comment}} </div>


                    <div class="col-md-12">

                        <hr class="w-100">

                    </div>

                    <div class="col-md-5 col-sm-6 col-6"><strong>  {{__('app.address')}}  :  </strong></div>

                    <div class="col-md-7 col-sm-6 col-6"> {{optional($data->addressItem)->address}} {{optional($data->addressItem)->region}}  </div>

                </div>

                @else

                    <div class="card">

                        <div class="card-body row">

                            {{__('app.no_customer')}}

                        </div>

                    </div>

                @endif

            </div>

        </div>

        <div class="col-md-6">

            <div class="card ">

                <h4 class="mb-3 mt-3" style="padding: 0 20px 0 20px;margin-bottom: 0 !important;"> {{__('app.technical_information')}} </h4>

                <hr style="margin-bottom: 0 !important;">

                @if( $data->provider )

                    <div class="card-body row">

                        <div class="col-sm-5 col-4"><strong> {{__('app.name_of_technical')}} :  </strong></div>

                        <div class="col-sm-7 col-8">  {{ optional($data->provider)->full_name}}  </div>



                        <div class="col-md-12">

                            <hr class="w-100">

                        </div>



                        <div class="col-md-5 col-sm-6 col-6"><strong>  {{__('app.rating')}}  :  </strong></div>

                        <div class="col-md-7 col-sm-6 col-6"> {{$data->provider_rate}} </div>





                        <div class="col-md-12">

                            <hr class="w-100">

                        </div>



                        <div class="col-md-5 col-sm-6 col-6"><strong>  {{__('app.technical_comment')}}  :  </strong></div>

                        <div class="col-md-7 col-sm-6 col-6"> {{$data->provider_comment}} </div>



                    </div>

                @else

                    <div class="card">

                        <div class="card-body row">

                            {{__('app.no_customer')}}

                        </div>

                    </div>

                @endif

            </div>

        </div>

    </div>

        <div class="row mt-4">

            <div class="col-md-12">

                <div class="card ">

                    <h4 class="mb-3 mt-3" style="padding: 0 20px 0 20px;margin-bottom: 0 !important;"> {{__('app.titles.survey')}} </h4>

                    <hr style="margin-bottom: 0 !important;">

                    @forelse( $answers as $item )

                        <div class="col-md-12">

                        <h6 class="mt-2 mr-2">

                            {{optional($item->question)->text}}

                            @if( $item->question )

                                @if( $item->question->type == 'choose' )

                                    <small class="text-info"> {{__('app.survey.select_choose')}}  </small>

                                @else

                                    <small class="text-info"> {{__('app.survey.ans_text')}} </small>

                                @endif

                            @endif

                        </h6>



                        @if( optional( $item->question )->type == 'choose' )

                            <p class="mr-2">  {{optional($item->answer)->text}} </p>

                        @else

                            <p class="mr-2">  {{ $item->text}} </p>

                        @endif

                        <hr>

                        </div>

                    @empty

                        <p class="text-center text-danger pt-3 pb-3">

                            {{__('app.survey.no_survey_yet')}}

                        </p>

                    @endforelse

                </div>

            </div>

        </div>

    </div>





    <div class="col-lg-12 mt-4">

        <div class="row">

            <div class="col-md-6">

                <div class="card ">

                    <h4 class="mb-3 mt-3" style="padding: 0 20px 0 20px;margin-bottom: 0 !important;"> {{__('app.order_information')}} </h4>

                    <hr style="margin-bottom: 0 !important;">

                    <div class="card-body row">

                        <div class="col-sm-5 col-4"><strong> {{__('app.time_taken_for_maintenance')}} :  </strong></div>

                        <div class="col-sm-7 col-8">  {{  $data->completion_time }}  </div>



                        <div class="col-md-12">

                            <hr class="w-100">

                        </div>



                        <div class="col-sm-5 col-4"><strong> {{__('app.amount_paid')}} :  </strong></div>

                        <div class="col-sm-7 col-8">

                            @if( $data->is_paid == 1 )

                                {{ $data->total }}

                                @if( $data->total )
                                    {{__('app.SAR')}}
                                @endif

                            @endif

                        </div>



                        <div class="col-md-12">

                            <hr class="w-100">

                        </div>





                        <div class="col-sm-5 col-4"><strong> {{__('app.payment_method')}} :  </strong></div>

                        <div class="col-sm-7 col-8">

                            @if( $data->method_payment == 1 )

                                {{ __('app.cash')}}



                            @endif

                        </div>



                        <div class="col-md-12">

                            <hr class="w-100">

                        </div>

                        <div class="col-md-5 col-sm-6 col-6"><strong>  {{__('app.what_been_accomplished')}}  :  </strong></div>

                        <div class="col-md-7 col-sm-6 col-6"> {{$data->what_done}} </div>

                    </div>

                </div>

            </div>


            <div class="col-md-6">
                <div class="card ">

                    <h4 class="mb-3 mt-3" style="padding: 0 20px 0 20px;margin-bottom: 0 !important;"> مرفقات السله </h4>

                    <hr style="margin-bottom: 0 !important;">

                    <div class="card-body row">
                @foreach( $images as $image )
                    <img src="{{asset($image->image)}}" width="200" height="200" >
                @endforeach
                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection

@push('scripts')

    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"

         aria-hidden="true">

        <div class="modal-dialog " role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="createModalLabel"> {{__('app.management_comment')}}  </h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                    </button>

                </div>

                <div class="modal-body">



                    <form method="post" id="edit-form" action="{{route("orders.update", $data->id)}}">

                        @method('put')

                        @csrf

                        <div class="form-group row">

                            <div class="col-lg-12">

                                <textarea class="form-control" name="management_comment" rows="5" id="comment_mang"></textarea>

                            </div>

                        </div>



                        <div class="form-group row">

                            <div class="col-md-6 offset-md-3">

                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>

                            </div>

                        </div>

                    </form>

                </div>

            </div>

        </div>

    </div>





    <!-- The Modal -->

    <div id="myModal" class="modal">



        <!-- The Close Button -->

        <span class="close">&times;</span>



        <!-- Modal Content (The Image) -->

        <img class="modal-content" id="img01">

        <!-- Modal Caption (Image Text) -->

        <div id="caption"></div>

    </div>



    <div class="modal fade" id="editOrderModal" tabindex="-1" role="dialog" aria-labelledby="editOrderLabel"

         aria-hidden="true">

        <div class="modal-dialog " role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h5 class="modal-title" id="editOrderLabel"> {{__('app.edit_request')}} </h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                    </button>

                </div>

                <div class="modal-body">

                    <form method="post" action="{{route('order.updates' , $data->id)}}" id="edit_request_form" >

                        @csrf



                        <div class="form-group row">

                            <div class="col-lg-3">

                                <label>

                                    أضف خدمة جديده

                                </label>

                            </div>

                            <div class="col-lg-7">

                                <select class="form-control" name="" id="new_services">

                                    @foreach( $services as $service )

                                    <option value="{{$service->id}}" data-price="{{$service->price}}"> {{$service->name}} </option>

                                    @endforeach

                                </select>



                                <div class="error" id="errorItemService"></div>

                            </div>

                            <div class="col-lg-2">

                                <button type="button" class="btn btn-success btn-icon aligner" id="addService"

                                        {{--onclick="showSwal('passing-parameter-execute-cancel' , '{{route("sliders.destroy", $item->id)}}' )" --}}

                                >

                                    +

                                </button>

                            </div>

                        </div>

                        <hr class="w-100" style="    margin-top: 10px; margin-bottom: 10px;">



                        <div id="services__items">

                        {{--@forelse( $data->orderDetails as $orderDetails )--}}



                        <div id="content-js">



                        </div>



                        <div class="form-group row service-item">

                            <div class="col-lg-7" id="service_text">

                                {{--{{optional( $orderDetails->service_item )->name}}--}}

                            </div>

                            <div class="col-lg-3" >

                                <input class="form-control" id="service_price" name="price" value="" />

                                {{--{{$orderDetails->price}}--}}

                                <input type="hidden" name="old_service_id" id="old_service_id" value="" />

                                {{--{{$orderDetails->id}}--}}

                            </div>

                            <div class="col-lg-2">

                                <button type="button" class="btn btn-danger btn-icon aligner btn-service-item btn-service-item js-remove-btn"

                                    {{--data-service-id="{{$orderDetails->service_id}}"--}}

                                    {{--data-details-id="{{$orderDetails->id}}"--}}

                                >

                                    <i data-feather="trash"></i>

                                </button>

                            </div>

                        </div>

                        {{--@endforeach--}}

                        </div>

                        <hr class="w-100" style=" margin-top: 10px; margin-bottom: 10px;">

                        <div class="form-group row">

                            <div class="col-md-12 text-center">

                                <button type="submit" class="btn btn-primary"> {{__('app.send')}} </button>

                            </div>

                        </div>

                    </form>

                </div>

            </div>

        </div>

    </div>





    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.26.0/axios.min.js"></script>

    <script type="text/javascript">



        $(function() {

            let lengthNavItems = $(".sidebar-body").find("li.nav-item").hasClass("active")

            // alert(lengthNavItems)



            if (lengthNavItems )  {

                $(".sidebar-body").find(".nav-item").removeClass("active")

            }





            $("#edit-3").click(function () {

                let managementComment= $("#management_comment").text();

                $("#comment_mang").text(managementComment)

                $("#createModal").modal("show")

            });

        });



    </script>

    <script>





        $(document).ready(function(){

            // Get the modal

            var modal = $("#myModal");



            // Get the image and insert it inside the modal - use its "alt" text as a caption

            var img = $(".myImg");

            var modalImg = $("#img01");

            var captionText = $("#caption");



            // img.on("click", function(){

            $(document).on("click",".myImg",function () {



                // alert(12121) ;

                console.log("?>? class >",  $(this).attr("class") )



                console.log("?>? src >",  $(this).attr("src") )



                // alert(12121) ;

                modal.css("display", "block");

                modalImg.attr("src", $(this).attr("src"));

                captionText.html($(this).attr("alt"));

            });



            // Get the <span> element that closes the modal

            var span = $(".close");



            // When the user clicks on <span> (x), close the modal

            span.on("click", function() {

                modal.css("display", "none");

            });

        });

        //===========================================================

        $(".order-modification-reject").click(function (e) {

            e.preventDefault();



            let messageStatus = "تم وصول الفني";

            let message = "<p class='alert alert-success'>  تم رفض التعديل بنجاح  </p>";

            let orderId = $(this).data("id"),

                that  = $(this) ,

                route   = "{{route('order.modificationStatusRejectAdmin')}}"



            axios.post(route, {

                orderId   : orderId,

                status    : status

            }).then((response) => {

                console.log("responsesa 11>> " , response )

                if ( response.data.status == true) {



                    // alert( response.data.status )

                    that.css("display","none")

                    $("#messages_").empty().append(message)

                    setTimeout(function() {

                        $("#messages_").empty()

                    }, 8000);



                    $(this).closest("td").empty().append("{{__('app.reject_from_admin')}}")

                    // $("tr.order-id-"+orderId).removeClass("table-warning")

                    // $("tr.order-id-"+orderId).find(".order-modification").remove()

                    // $("tr.order-id-"+orderId).find(".order-modification-reject").remove()

                    // $("tr.order-id-"+orderId).find(".order_canceled").remove()

                    // $("tr.order-id-"+orderId).find(".status").text(messageStatus)

                    // $("tr.order-id-"+orderId).find(".reject-status").remove()





                    // مرفوض من الادمن



                }

            })

        })





        //===========================================================





        $(".order-modification").click(function (e) {

            e.preventDefault();
            let orderId = $(this).data('id') ;
            let total = $(this).data('total') ;

            let detailsId = $(this).data('service-details-id') ;





            $("#old_cost").val(total) ;

            $("#order_id_").val(orderId) ;

            let serviceName = $(this).data("service-name");

            let servicePrice = $(this).data("service-price");

            $("#service_text").empty().text(serviceName);

            $("#service_price").val(servicePrice);

            $("#old_service_id").val(detailsId);



            $('#editOrderModal').modal('show');

        });





        //===========================================================



        $("#addService").on("click" , function (event) {

            event.preventDefault() ;



            $(document).find("#errorItemService").empty()



            let services__items = $("#services__items").find(".service-item").length



            if ( services__items < 1 ) {



                let serviceVal = $("#new_services").find("option:selected").val();

                let serviceText = $("#new_services").find("option:selected").text();

                let price = $("#new_services").find("option:selected").data('price');





                let newServiceItem = `<div class="form-group row service-item" >

                                    <div class="col-lg-7">

                                        ${serviceText}

                                    </div>

                                    <div class="col-lg-3">

                                        <input class="form-control" name="price" value="${price}"   />



                                        <input type="hidden" name="service_id" value="${serviceVal}" />

                                    </div>

                                    <div class="col-lg-2">

                                        <button type="button" class="btn btn-danger btn-icon aligner btn-service-item"

                                            data-service-id="12"

                                            data-details-id="222"
                                        >

                                            <i data-feather="trash"></i>

                                        </button>

                                    </div>

                                </div>`



                $("#services__items").append(newServiceItem)



            } else{

                $(document).find("#errorItemService").empty().append("لا يمكن إضافه أكثر من خدمه عليك بحذف الخدمه اولا")

            }







        })



        //=================================================



        $(document).on("click" , ".btn-service-item" ,function () {

            if ( $(this).hasClass('js-remove-btn') ){
                $("#old_service_id").val()
                // let detailsId = $(this).data('details-id')
                let detailsId = $("#old_service_id").val()

                $("#content-js").append(`<input type='hidden' name='remove_item' value="${detailsId}" >`)

            }



            $(this).closest(".service-item").remove();

        })

    </script>

@endpush

@push('styles')

    <style>

        .logo img{

            width: 100% ;

            height: 200px;

        }



        .cover{

            width: 100%;

        }



        .cover img{

            width: 100%;

            height: 200px    ;

        }



        #edit-2{

            display: inline-block;

            cursor: pointer;

        }



        .status_item ,.float-left-desc{

            float: left;

        }



        #edit-3{



        }



        @media (max-width: 768px) {

            .status_item {

                display: none;

            }

            .logo{

                margin-top: 30px;

            }

        }

    </style>

@endpush