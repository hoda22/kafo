@extends('dashboard.layouts.app', ['title' => __('app.clients.show')])
@section('content')
    @push('button')
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <a href="{{route('clients.index')}}" class="btn btn-primary">
                <i data-feather="skip-back"></i>
            </a>
        </div>
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 ">
            <div class="card">
                <div class="card-body resize_section">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="{{ $item->avatar_image }}" class="avatar" />
                        </div>
                        <div class="col-md-2 mb-3">
                            {{__('app.name')}} :
                        </div>
                        <div class="col-md-10 mb-3">
                            {{$item->full_name}}
                        </div>

                        <div class="col-md-2 mb-3">
                            {{__('app.email')}} :
                        </div>
                        <div class="col-md-4 mb-3">
                            {{$item->email}}
                        </div>

                        <div class="col-md-2 mb-3">
                            {{__('app.phone')}} :
                        </div>
                        <div class="col-md-4 mb-3 phone">
                            {{$item->phone}}
                        </div>
                        <div class="col-md-2 mb-3">
                            {{__('app.date_joining')}} :
                        </div>
                        <div class="col-md-4 mb-3">
                            {{$item->created_at}}
                        </div>

                        <div class="col-md-2 mb-3">
                            {{__('app.balance')}} :
                        </div>
                        <div class="col-md-4 mb-3">
                            {{$item->balance}}
                        </div>

                        <div class="col-md-2 mb-3">
                            {{__('app.block_statue')}} :
                        </div>
                        <div class="col-md-4 mb-3">
                            @if( $item->block_status == "block" )
                                {{__('app.block')}}
                            @else
                                {{__('app.unblock')}}
                            @endif
                        </div>


                        @if( $item->block_status == "block" )
                        <div class="col-md-2 mb-3">
                            {{__('app.reason')}} :
                        </div>
                        <div class="col-md-4 mb-3">
                            {{$item->block_reason}}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            {{--Orders--}}
            <div class="card mt-4">
                <div class="card-body resize_section">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="mb-4"> {{__('app.client_requests')}} </h5>
                        </div>
                        <div class="col-md-12">

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover mb-0">
                                        <thead>
                                        <tr>
                                            <th class="pt-0"> {{__('app.order_id')}} </th>
                                            <th class="pt-0"> {{__('app.total')}} </th>
                                            <th class="pt-0"> {{__('app.payment_type')}} </th>
                                            <th class="pt-0"> {{__('app.payment_status')}} </th>

                                            <th class="pt-0"> {{__('app.processes')}} </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse( $orders as $order )
                                            <tr>
                                                <td> {{ $order->id }}</td>
                                                <td> {{ $order->total }} </td>
                                                <td>
                                                    @if( $order->method_payment )
                                                        {{$order->payment_methods[$order->method_payment] }}
                                                    @endif
                                                </td>
                                                <td> {{ $order->is_paid ? __("app.paid"):__("app.not_paid") }} </td>
                                                <td>
                                                    <a href="{{route("orders.show", $order->id)}}"
                                                       class="btn btn-success btn-icon aligner ">
                                                        <i data-feather="eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="8" class="danger text-center">
                                                    {{__('app.no_requests')}}
                                                </td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                    {{--<div class="text-center">--}}
                                        {{--{!! $items->appends(request()->query())->render() !!}--}}
                                    {{--</div>--}}
                                </div>
                            </div>

                            {{--@forelse( $orders as $order )--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-2 mb-3">--}}
                                        {{--{{__('app.service_name')}} :--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-10 mb-3">--}}
                                        {{--{{optional( $order->service )->name}}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-2 mb-3">--}}
                                        {{--{{__('app.price')}} :--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 mb-3">--}}
                                        {{--{{$order->price ?? "--"}}--}}
                                    {{--</div>--}}

                                    {{--<div class="col-md-2 mb-3">--}}
                                        {{--{{__('app.service_date')}} :--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 mb-3">--}}
                                        {{--{{$order->service_date ?? "--"}}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                    {{----}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-2 mb-3">--}}
                                        {{--{{__('app.description')}} :--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 mb-3">--}}
                                        {{--{{$order->description  ?? "--"}}--}}
                                    {{--</div>--}}


                                    {{--<div class="col-md-2 mb-3">--}}
                                        {{--{{__('app.created_at')}} :--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 mb-3">--}}
                                        {{--{{$order->created_at}}--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="row">--}}
                                    {{--<div class="col-md-2 mb-3">--}}
                                        {{--{{__('app.client_comment')}} :--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 mb-3">--}}
                                        {{--{{$order->client_comment  ?? "--"}}--}}
                                    {{--</div>--}}


                                    {{--<div class="col-md-2 mb-3">--}}
                                        {{--{{__('app.provider_rate')}} :--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 mb-3">--}}
                                        {{--{{$order->provider_rate}}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<hr>--}}
                            {{--@empty--}}
                                {{--<div class="text-danger text-center">--}}
                                    {{--{{__('app.no_requests')}}--}}
                                {{--</div>--}}
                            {{--@endforelse--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push("styles")
    <style>
            /*.resize_section .col-md-2 {*/
                /*max-width: 11%;!**!*/
            /*}*/

            /*.resize_section .col-md-4 {*/
                /*max-width: 46%;*/
            /*}*/
    </style>
@endpush
