
<script>
    // jQuery.validator.addMethod("isEmail", function(sEmail) {
    //
    //     var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    //     if (filter.test(sEmail)) {
    //         return true;
    //     }
    //     else {
    //         return false;
    //     }
    // })
    //
    // jQuery.validator.addMethod("phoneStartingWithPlus", function(phone_number, element) {
    //
    //     if ( phone_number.charAt(0) == "+" ) {
    //         return true
    //     }else{
    //         return false
    //     }
    // });


    jQuery.validator.addMethod("phoneStartingNumber", function(value, element) {
        let phoneNumber = value.substring(1);

        console.log("phoneNumber >>> ", phoneNumber)
        if ( isNumeric(phoneNumber) ) {
            return true
        }else{
            return false
        }

    }, "{{__('app.validation.phoneNumberIsNumber')}}" );



    $("#add-form").validate({
        rules: {
            first_name: {
                required: true,
            },
            last_name :{
                required: true,
            },
            phone :{
                phoneStartingWithPlus:true,
                phoneStartingNumber:true,
                required: true,
                minlength: 10,
                maxlength: 14,
            },
            password :{
                minlength: 8
            },

            password_confirmation :{
                equalTo: "#password"
            },
            email :{
                required: true,
                email: true,
                isEmail:true
            },
            action: "required"
        },
        messages: {
            first_name: {
                required: "{{__('app.validation.field_first_name_required')}}"
            },
            last_name: {
                required: "{{__('app.validation.field_last_name_required')}}"
            },

            phone: {
                required: "{{__('app.validation.field_phone_required')}}",
                minlength: "{{__('app.validation.field_phone_minlength')}}",
                maxlength: "{{__('app.validation.field_phone_maxlength')}}",
                phoneStartingWithPlus:"{{__('app.validation.phoneNumberShouldStartWithPlus')}}" ,
                phoneStartingNumber:"{{__('app.validation.phoneNumberIsNumber')}}"
            },

            email: {
                required: "{{__('app.validation.field_email_required')}}",
                email: "{{__('app.validation.field_email_email')}}",
                isEmail:"{{__('app.validation.field_email_email')}}",
            },

            password: {
                required: "{{__('app.validation.field_password_required')}}" ,
                minlength: "{{__('app.validation.field_password_min')}}" ,
            },
            password_confirmation: {
                required: "{{__('app.validation.field_password_confirmation_required')}}" ,
                equalTo: "{{__('app.validation.field_equalTo')}}" ,
            },


            action: "Please provide some data"
        }
    });


    $("#edit-form").validate({
        rules: {
            first_name: {
                required: true,
            },
            last_name :{
                required: true,
            },
            phone :{
                phoneStartingWithPlus:true,
                phoneStartingNumber:true,
                required: true,
                minlength: 10,
                maxlength: 14,
            },
            password :{
                minlength: 8
            },
            email :{
                required: true,
                email: true,
                isEmail:true
            },
            action: "required"
        },
        messages: {
            first_name: {
                required: "{{__('app.validation.field_first_name_required')}}"
            },
            last_name: {
                required: "{{__('app.validation.field_last_name_required')}}"
            },

            phone: {
                required: "{{__('app.validation.field_phone_required')}}",
                minlength: "{{__('app.validation.field_phone_minlength')}}",
                maxlength: "{{__('app.validation.field_phone_maxlength')}}",
                phoneStartingWithPlus:"{{__('app.validation.phoneNumberShouldStartWithPlus')}}" ,
                phoneStartingNumber:"{{__('app.validation.phoneNumberIsNumber')}}"
            },

            email: {
                required: "{{__('app.validation.field_email_required')}}",
                email: "{{__('app.validation.field_email_email')}}",
                isEmail:"{{__('app.validation.field_email_email')}}",
            },

            password: {
                required: "{{__('app.validation.field_password_required')}}" ,
                minlength: "{{__('app.validation.field_password_min')}}" ,
            },
            password_confirmation: {
                required: "{{__('app.validation.field_password_confirmation_required')}}" ,
                equalTo: "{{__('app.validation.field_equalTo')}}" ,
            },


            action: "Please provide some data"
        }
    });
</script>