@extends('dashboard.layouts.app', ['title' => __('app.clients.add')])
@section('content')
    @push('button')
        <div class="d-flex align-items-center flex-wrap text-nowrap btn-back">
            <a href="{{route('clients.index')}}" class="btn btn-primary">
                <i data-feather="skip-back"></i>
            </a>
        </div>
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="{{route("clients.store")}}" id="add-form">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_name" class="col-form-label"> {{__('app.first_name')}}<span class="required_star">*</span> : </label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{old('first_name')}}" name="first_name" id="add_name">
                            </div>

                            <div class="col-lg-2">
                                <label for="add_name" class="col-form-label"> {{__('app.last_name')}} <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="{{old('last_name')}}" name="last_name" id="last_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_phone" class="col-form-label">{{__('app.phone')}}  <span class="required_star">*</span> :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control phone" value="{{old('phone')}}" name="phone" id="add_phone">
                            </div>


                            <div class="col-lg-2">
                                <label for="add_email" class="col-form-label">  {{__('app.email')}}  <span class="required_star">*</span>  :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="email" class="form-control" value="{{old('email')}}" name="email" id="add_email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="add_active" class="col-form-label">  {{__('app.status')}} :</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="hidden" name="active" value="inactive">
                                <label class="switch">
                                    <input type="checkbox" name="active" value="active">
                                    <span class="slider round"></span>
                                </label>
                            </div>



                        </div>

                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="password" class="col-form-label">  {{__('app.password')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="password" class="form-control" name="password" id="password">
                            </div>

                            <div class="col-lg-2">
                                <label for="re_password" class="col-form-label">  {{__('app.re_password')}} <span class="required_star">*</span>:</label>
                            </div>
                            <div class="col-lg-4">
                                <input type="password" class="form-control" name="password_confirmation" id="re_password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <button type="submit" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    @include('dashboard.clients.script')
@endpush