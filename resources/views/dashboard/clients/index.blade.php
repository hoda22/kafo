@extends('dashboard.layouts.app', ['title' => __('app.titles.clients')])
@section('content')
    @push('button')
        @can("clients-create")
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <a href="{{route('clients.create')}}" class="btn btn-primary">
                {{__('app.clients.add')}}
            </a>
        </div>
        @endcan
    @endpush
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card mb-3 ">
                <div class="card-body filter">
                    <form class="filter_form">
                        <div class="form-group row">
                            <label for="place" class="col-sm-2 col-form-label"> {{__('app.name')}} </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="name" value="{{ request('name') }}"/>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-primary submit"> {{__('app.search')}} </button>
                                <button type="button" class="btn clear_search btn-danger btn-icon aligner">
                                    <i class="link-icon" data-feather="refresh-cw"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                @include("dashboard.includes.info_page",['items' => $items])
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th class="pt-0">#</th>
                                <th class="pt-0"> {{__('app.first_name')}} </th>
                                <th class="pt-0"> {{__('app.last_name')}} </th>
                                <th class="pt-0"> {{__('app.email')}} </th>
                                <th class="pt-0"> {{__('app.phone')}} </th>
                                <th class="pt-0"> {{__('app.status')}} </th>
                                @can("client-approve-reject")
                                <th class="pt-0"> {{__('app.block_statue')}} </th>
                                @endcan
                                <th class="pt-0"> {{__('app.processes')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $items as $item )
                                <tr>
                                    <td> {{ $loop->iteration }}</td>
                                    <td> {{ $item->first_name }} </td>
                                    <td> {{ $item->last_name }} </td>
                                    <td> {{ $item->email }} </td>
                                    <td class="phone"> {{ $item->phone }} </td>
                                    <td>
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="active" value="active" class="change_status"
                                                       data-route="{{route("client.updateAxios" , $item->id )}}"
                                                       data-id="{{$item->id}}"
                                                       @if($item->active == 'active')
                                                       checked
                                                        @endif
                                                >
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </td>

                                    @can("client-approve-reject")
                                    <td>
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="block_status" value="block" class="change_block_status"
                                                       data-route="{{route("client.updateAxiosBlock" , $item->id )}}"
                                                       data-id="{{$item->id}}"
                                                       data-block-id="{{$item->id}}"
                                                       @if($item->block_status == 'block')
                                                       checked
                                                        @endif
                                                >
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </td>
                                    @endcan

                                    <td>
                                        @can("clients-show")
                                        <a href="{{route("clients.show", $item->id)}}"
                                           class="btn btn-success btn-icon aligner ">
                                            <i data-feather="eye"></i>
                                        </a>
                                        @endcan
                                        @can("clients-edit")
                                        <a href="{{route("clients.edit", $item->id)}}"
                                           class="btn btn-primary btn-icon aligner ">
                                            <i data-feather="edit"></i>
                                        </a>
                                        @endcan
                                        @can("clients-delete")
                                            <button type="submit" class="btn btn-danger btn-icon aligner"
                                                    onclick="showSwal('passing-parameter-execute-cancel' , '{{route("clients.destroy", $item->id)}}' )" >
                                                <i data-feather="trash"></i>
                                            </button>

                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="danger text-center">{{__('app.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $items->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

    <script type="text/javascript">


        $(".change_block_status").change(function () {

            let statusAdmin = 'unblock' ,
                id = $(this).data('id') ,
                route = $(this).data('route') ;


            if ( $(this).is(":checked") ){
                statusAdmin = 'block'
                id = $(this).data('id') ;

                $(this).prop('checked',false)

                $("#reason").val("")
                $("#createModal").modal("show") ;
                $("#route_id").val(route)
                $("#client_id").val(id)
            }else {

                axios.post(route, {
                    block_status : "unblock"
                }).then((response) => {
                    if (response.data.status == true) {
                        $("#messages_").empty().append("<p class='alert alert-success'> {{__("app.block_successfully_delete")}} </p>")

                    }
                    $("#createModal").modal("hide") ;
                })
            }
        });

        $(document).on("click" , "#btn-add-block" ,function(){

            let route = $("#route_id").val(),
                client_id = $("#client_id").val(),
                reason = $("#reason").val()

            console.log("reason", reason)
            axios.post(route, {
                block_status: "block" ,
                block_reason: reason
            }).then((response) => {
                if (response.data.status == true) {

                    // if ( statusAdmin == 'active' ) {
                        $("#messages_").empty().append("<p class='alert alert-success'> {{__("app.block_successfully_deactivated")}} </p>")
                    {{--}else {--}}
                        {{--$("#messages_").empty().append("<p class='alert alert-success'> {{__("app.activation_successfully_deactivated")}} </p>")--}}
                    {{--}--}}

                    $(`[data-block-id="${client_id}"]`).prop('checked',true);
                    setTimeout(function() {
                        $("#messages_").empty()
                    }, 2000);
                }

                $("#createModal").modal("hide") ;
            })
        })









    </script>




    <div class="modal fade" id="createModal" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createSubServiceModalLabel">{{__('app.reason')}} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add-block" >
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label for="add_name" class="col-form-label">{{__('app.reason')}} :</label>
                            </div>
                            <div class="col-lg-9">
                                <textarea class="form-control" rows="3" id="reason" name=""></textarea>
                                <input type="hidden" id="route_id">
                                <input type="hidden" id="client_id">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="button" id="btn-add-block" class="btn btn-primary"> {{__('app.save')}} </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endpush


