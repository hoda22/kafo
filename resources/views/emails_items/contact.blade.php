<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<title>الدكتور اوتو</title>
</head>
<body style="direction: rtl;">
<table cellpadding="0" cellspacing="0" width="630" align="center" style="font-family: 'Open Sans', Arial, Helvetica, sans-serif; font-size:14px; color:#000; border:10px solid #4caf50  ;">
    <tr>
        <td style="padding:20px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td><img style="width:60px" src="{{ url('public/images/logo.png') }}" alt=""></td>
                    <td align="left" valign="middle"><?php echo date("j-m-Y");?></td>
                </tr>
            </table>         
        </td>
    </tr>
    <tr>
        <td valign="top">
            <div style="font-size:18px; border-bottom:3px solid #4caf50; border-top:3px solid #4caf50; margin:0 20px; padding:12px 0; font-weight:bold;">تواصل معنا</div>
            <div style="padding:35px 30px; line-height:20px;">
            الهاتف : {{ $mobile }}<br>
            الايميل : {{ $email }}<br>
            الرسالة : {{ $body }}<br>
            </div>            
        </td>
    </tr>
    <tr>
        <td bgcolor="#4caf50" style="padding:30px 0 20px 0; font-size:12px; color:#fff;">
            <div align="right" style="padding:0 10px;"><img src="" alt="" /></div>
            <div style="padding:0 30px;">            
            الدكتور,  جميع حقوق الطبع والنشر محفوظة &copy; {{ Carbon\Carbon::now()->year }} 
            </div>
        </td>
    </tr>    
</table>
</body>
</html>
