<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',


    'you_receiving_email_because_received_password_reset_request_your_account' => 'You are receiving this email because we received a password reset request for your account.',
    'reset_password' => 'Reset Password',
    'did_request_password_reset_further_action_required' => 'If you did not request a password reset, no further action is required.',


    'sent' => 'System will send an email to reset your password',
];
