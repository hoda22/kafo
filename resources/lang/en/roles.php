<?php

return[

    'roles-list' => 'roles list',

    'all' => 'all',
    'roles-create' => 'add role',
    'roles-edit' => 'edit role',
    'roles-delete' => 'delete role',

    'clients-list' => 'clients list',
    'clients-show' => 'client show',
    'clients-create' => 'add client',
    'clients-edit' => 'edit client',
    'clients-delete' => 'delete client',

    'users-list' => 'users list',
    'users-create' => 'add user',
    'users-edit' => 'edit users',
    'users-delete' => 'delete user',

    'cities-list' => 'cities list',
    'cities-create' => 'add city',
    'cities-edit' => 'edit city',
    'cities-delete' => 'delete city',


    'categories-list' => 'categories list',
    'categories-create' => 'add category',
    'categories-edit' => 'edit category',
    'categories-delete' => 'delete category',


    'orders-list' => 'orders list',
    'orders-create' => 'orders create',
    'orders-edit' => 'orders edit',
    'orders-delete' => 'orders delete',

    'spare-parts-list' => 'spare parts list',
    'spare-parts-create' => 'add spare parts',
    'spare-parts-edit' => 'edit spare parts',
    'spare-parts-delete' => 'delete spare parts',


    'brands-list' => 'brands-list',
    'brands-create' => 'add brands',
    'brands-edit' => 'edit brands',
    'brands-delete' => 'delete brands',

    'pages-list' => 'pages list',
    'pages-edit' => 'edit pages',

    'settings-edit' => 'edit settings',


    'providers-list' => 'providers list',
    'providers-create' => 'add providers',
    'providers-show' => 'show providers',
    'providers-edit' => 'edit providers',
    'providers-delete' => 'delete providers',


    'sliders-list' => 'sliders list',
    'sliders-create' => 'add sliders',
    'sliders-edit' => 'edit slider',
    'sliders-delete' => 'delete slider',

    'survey-list' => 'survey list',
    'survey-create' => 'add survey',
    'survey-edit' => 'edit survey',
    'survey-delete' => 'delete survey',

    'services-list' => 'services list',
    'services-create' => 'add services ',
    'services-edit' => 'edit services',
    'services-delete' => 'delete services',

    'notifications-list' => 'notifications list' ,
    'notifications-send' => 'notifications send' ,


    "performance-panel-settings" => "Show Performance board",
    "complaints-suggestions" =>"Complaints Suggestions",
    "complaints-suggestions-list" =>"complaints suggestions list",
    "complaints-suggestions-replay" =>"complaints suggestions replay",

];
