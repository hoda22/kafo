<?php

return [
    'name_en'           => 'English Name',
    'name_ar'           => 'Name Arabic',
    'export_data'       => 'Export Data',
    'hours' => 'hours' ,
    'answer' => 'answer' ,
    'new_chat' => 'New Chat' ,
    'ar' => 'Arabic' ,
    'en' => 'English' ,
    'quantity' => 'quantity' ,
    'days'                  => 'Days' ,

    //===============
    'approved_client'        => 'Approved by the client' ,
    'rejected_client'        => 'Rejected by customer' ,
    'status_edit_request'        => 'Modification status' ,
    'edit_images'        => 'images' ,
    'used_spare_parts'        => 'Used spare parts' ,
    'description_problem'        => 'Description of the problem' ,
    'no_spare_parts_used'        => 'No spare parts used' ,
    'cases_of_rejection_and_cancellation'        => 'Cases of rejection and cancellation' ,
    //=======================
    'modify_appointment' => 'Modify the appointment' ,


    're_password'       => 'Reset password',
    'email'             => 'E-mail',
    'sent' => 'System will send an email to reset your password',
    'Search' => 'Search' ,


    'cash'                      => 'cash',
    'number_answers_must_2_or_more' => 'The number of answers must be 2 or more' ,
    'status_active'                      => 'Activation status',
    'deleted_his_account'                      => 'He deleted his account',


    'active_successfully' => 'The technician has been activated successfully' ,
    'unactive_successfully' => 'The technician was successfully stopped' ,



    'management_comment'                      => 'Management comment',
    'visa'                      => 'visa',
    'mada'                      => 'Mada',
    'not_yet'                      => 'not yet',
    'paid'                      => 'paid',
    'not_paid'                      => 'not paid',
    'is_paid'                      => 'paid status',
    'collection_done'                      => 'The technician received the request successfully',

    "how_send"  => "how send" ,
    "request_cancel_order_was_rejected_by_administration"  => "The request to cancel the order was rejected by the administration" ,
    "request_was_returned_to_technician_again"  => "The request was returned to the technician again" ,
    "under_revision"  => "under revision" ,
    "technician_assigned"  => "technician assigned" ,

    //===============
    "new_request_been_sent_to_you_and_awaiting_your_approval_of_the_request_" => "A new request has been sent to you and is awaiting your approval of the request" ,
    "new_request_been_sent_to_you_and_awaiting_your_approval_of_the_request" => "A new request has been sent to you and is awaiting your approval of the request" ,
    "note_new_request" => "New order" ,
    "note_reject_request_from_admin" => "Rejection of request to cancel the order" ,
    "note_response_edit_request_from_admin" => "Response to amend request" ,
    'note_approve_modify_request' => 'He agreed to amend the request' ,
    'note_reject_modify_request' => 'Refusal to modify the request' ,
    'note_accept_cancellation_request' => 'Accept cancellation request' ,
    'suspended_account' => 'The account has been suspended by the administration' ,

    'suspended_account_mang' => '"This account is still under review by the Kafo app administration' ,

    'stped_account_mang' => 'This account has been suspended from managing the Kafo app administration' ,

    'application_management' => 'Kafo app Management' ,


    'your_request_rejected_because' => 'Your request was rejected because' ,


    'The request was rejected by the technician' => 'The request was rejected by the technician' ,



    'enter'             => 'enter',
    'send'             => 'send',
    'edit_request' => 'Edit Request' ,
    'name'              => 'name',
    'pending'              => 'pending',
    'providers&clients' => 'Customers and technicians',
    'order_id'          => 'order number',
    'canceled_orders'          => 'canceled orders',
    'requests_are_progress'          => 'requests are progress',
    'finished_orders'          => 'finished orders',

    'client'            => 'client',
    'description'       => 'description',
    'active'            => 'active',
    'inactive'          => 'inactive',
    'service_date'      => 'Service date',
    'created_at'        => 'created at',
    'order_status'      => 'order status',
    'order_type'        => 'order type',
    'client_comment'    => 'client comment',
    'provider_rate'     => 'provider rate',
    'provider'          => 'Technical',
    'first_name'        => 'first name',
    'last_name'         => 'last name',
    'login'             => 'login',
    'image'             => 'image',
    'price'             => 'price',
    'main'              => 'main',
    'SAR'               => "SAR",
    'title_site'        => 'kafo',
    'welcome_back'      => 'welcome back! Log in to your account.',
    'status'            => 'status',
    'processes'         => 'processes',
    'search'            => 'search',
    'service_technician'=> 'service technician',
    'service_requester' => 'service requester',
    'jop'               => 'jop',
    'password'          => 'password',
    'role'              => 'role',
    'service'           => 'service',
    'choose'            => 'choose',
    'save'              => 'save',
    'clients_'          => 'clients',
    'phone'             => 'phone',
    'nationality'       => 'nationality',
    'residency_number'  => 'residency number',
    'text'              => 'text',
    'years_experience'  => 'years of Experience',
    'date_birth'        => 'date of birth',
    'select_category'   => 'Choose the category',
    'all'               => 'all',
    'code'              => 'code',
    'enter_code'        => 'enter code',
    'ordering'          => 'ordering',
    'category_type'     => 'category',
    'notification'      => 'notification',
    'cancel'            => 'cancel',
    'yes_deleted'       => 'Yes, I want to delete!',
    'AC_type'           => 'Air conditioner type',
    'AC_brand'          => 'Air conditioning brand',
    'no_data'           => 'No results for your search',
    'number_elements_page' => 'The number of items per page',
    'number_elements'   => 'Total items',
    'logout'            => 'logout',
    'really_deleted'    => 'Do you really want to delete?',
    'copyright'         => 'All Rights Reserved',
    'service_name'      => 'service name',
    'services'          => 'services',
    'how_many_services' => 'number of sub services',
    'add_new_service'   => 'add new service',
    'service_add'       => 'add service',
    'service_update'    => 'service update',
    'category'          => 'category',
    'old_image'         => 'old_image',
    'city_add'          => 'city add',
    'city_name'         => 'city name',
    'city_edit'         => 'city_edit',
    'cities'            => 'cities',
    'duration'            => 'duration',
    'client_requests'   => 'Client Orders',
    'posting_time'      => 'posting time',
    'not_fond_data'      => 'not found data',
    'not_fond_orders'      => 'No orders found',
    'order_rejected'      => 'order rejected',
    'order_approved'      => 'order approved',


    'spare_part_add'      => 'Add a spare part',
    'spare_part_name'     => 'Spare part name',
    'spare_part_edit'     => 'Edit spare part',

    'assigned_to_order'     => 'A technician is assigned to order',


    'welcome_dashboard'  => 'Welcome to the control panel',
    'recently_enrolled_technicians'  => 'Recently enrolled technicians',
    'recently_registered_customers'  => 'Recently registered customers',


    'facebook'                    => 'Facebook',
    'twitter'                    => 'Twitter',
    'linkedin'                    => 'Linkedin',
    'instagram'                    => 'Instagram',

    'type' => 'type' ,
    'replay' => 'replay' ,


    'from'                    => 'From Date',
    'to'                    => 'To Date',
    'minutes' => 'minutes' ,


    'date_joining'                    => 'Date of joining',
    'sent_to_him'                    => 'Sent To',
    'message'                    => 'Message',
    'userMessagesAdd'                => 'Add Messages',


    'sub_service'                   => 'sub service',
    'technical_assign'              => 'technical assign',
    'show_order'                    => 'show order',
    'basic_service'                 => 'basic service',
    'price_paid'                    => 'price paid',
    'service_history'               => 'service history',
    'date_of_registration'          => 'date of registration',
    'service_requester_comment'     => 'service requester comment',
    'technical_comment'             => 'technical comment',
    'name_of_technical'             => 'Service technician information',

    'service_requester_information' => 'service requester information',
    'name_of_service_applicant'     => 'name of service applicant',
    'rating'                        => 'rating',
    'technical_information'         => 'technical information',
    'no_customer'                   => 'no customer',
    'edit_role'                   => 'edit role',
    'it_is_not_determined_yet'                   => 'it is not determined yet',

    'available' => 'available' ,
    'block_statue' => 'block statue' ,
    'block' => 'block' ,
    'unblock' => 'un block',
    'ordersNum' => 'number of orders' ,
    'hoursNum' => 'number of hours' ,
    'payment_method' => 'payment method' ,
    //============================
    'no_services' => 'There are no services added to the order' ,
    'previous_conversations' => 'Previous conversations' ,
    'no_conversations' => 'There are no conversations' ,




    'spare-part_name'                   => 'spare part_name',
    'spare-part_num'                   => 'Number of spare parts',

    //=========================

    "request_done" => "request is done",
    "under_revision_" => "under revision",
    "reject_from_admin" => "reject from admin",
    "technician_assigned_" => "A technician has been assigned",
    "technician_on_way" => "The technician is on the way",
    "technician_arrived" => "The technician has arrived",
    "implemented" => "Implemented",
    "canceled_technician" => "Canceled by technician",
    "rejected_by_technician" => "Rejected by the technician",
    "rejected_by_administration" => "Rejected by the administration",
    "modification" => "Modification request",

    "cost_approved_by_client" => "The cost has been approved by the client",
    "new_cost_rejected_by_customer" => "The new cost was rejected by the customer",
    "Approval_amendment_been_sent_to_technician" => "Approval for the amendment has been sent to the technician",
    "rejection_the_new_cost_been_sent_technician" => "A rejection of the new cost has been sent to the technician",
    "new_cost_been_sent_to_customer" => "The new cost has been sent to the customer",
    "rejected_customer" => "Rejected by customer",

    //===========================
    'no_requests'                   => 'No orders found',
    'balance'                   => 'balance',
    'activation_successfully' => 'activation successfully' ,
    'activation_successfully_deactivated' => 'activation successfully deactivated' ,
    'block_successfully_deactivated' => 'Client blocked successfully' ,
    'block_successfully_agent_deactivated' => 'The technician has been successfully blocked' ,
    'block_successfully_delete' => 'Client unblocked successfully' ,
    'client_name' => 'client name' ,
    'provider_name' => 'provider name' ,
    'reason' => 'reason' ,
    'rejectModel' => 'reject client Request' ,
    'providers_' => 'Technician' ,
    'unavailable' => 'un available' ,

    'spare_parts'         => [
        'brand'     => 'brand' ,
        'items'     => 'Air conditioner type' ,
        "edit"      => 'edit a spare part' ,
        "add"       => 'add a spare part'
    ],




    'pages'         => [
        'name'  => 'Page name' ,
        'add'   => 'Add page',
        'page_title'   => 'Page name',
        'page_content_ar'   => 'Page title',
        'page_content_en'   => 'Page content English',
    ],


    'survey'         => [
        'question_ar'     => 'question Arabic' ,
        'answers'     => 'answers' ,
        'question_en'     => 'question English' ,
        "ans_ar"      => 'answer Arabic' ,
        "ans_en"      => 'answer English' ,
        "question_type"      => 'question type' ,
        "ans_text"      => 'answer text' ,
        "select_choose"      => 'choose' ,
        "ans_type"      => 'answer type' ,
        "edit"      => 'edit question' ,
        "add"      => 'add question' ,
        "no_survey_yet"      => 'no survey yet' ,
        "cannot_add_more"      => 'cannot add more' ,
    ],

    'roles'         => [
        'name'  => 'Role name' ,
        'add'   => 'Add Role',
    ],

    'users'         => [
        'add'   => 'add user',
        'name'  => 'user name' ,
        'update'  => 'user edit' ,
        'first_name'  => 'first name' ,
        'last_name'  => 'last name' ,
    ],


    'notifications'     =>[
        'add'       => 'Add notice'
    ] ,

    'points'     =>[
        'add'       => 'Add to Point' ,
        'edit'       => 'Point edit' ,
        'num_points'       => 'Number points' ,
        'num_rus'       => 'Number Sar' ,
    ] ,

    'air_conditioner_brands' => [
        'add'       => 'Add Brand' ,
        'name'      => 'Brand name'
    ],

    'sliders' => [
        'add'        => 'Add image' ,
        'edit_image' => 'edit image'
    ],

    'settings' => [
        'meta_description'  => 'meta description' ,
        'meta_keywords'     => 'meta keywords',
        'site_name'         => 'website'
    ],

    'clients' => [
        'add'               => 'Add client',
        'show'               => 'technician data',
        'edit'              => 'Technical edit',
    ],

    'providers' => [
        'add'               => 'Add technician',
        'show'              => 'technician data',
        'edit'              => 'Technical edit',
    ],

    'titles'      => [
        'spare-parts'               => 'spare parts',
        'sup_services'          => 'sup services',
        'points_'               => 'points',
        "performance-panel" =>"Performance board" ,
        'complaints-suggestions'               => 'complaints & suggestions',
        'notifications'             => 'notifications',
        'payment_request'             => 'payment request',
        'pages'                     => 'sliders',
        'sliders'                   => 'sliders',
        'roles'                     => 'roles',
        'orders'                    => 'orders',
        'paymentRequest'            => 'Payment Request',
        'users'                     => 'users',
        'services'                  => 'services',
        'providers'                 => 'providers',
        'cities'                    => 'cities',
        'clients'                   => 'clients',
        'settings'                  => 'settings',
        'survey'                    => 'survey',
        'air_conditioner_brands'    => 'brands',
        'brands'                    => 'brands',
        'users_permissions'         => 'users & permissions',
        'statistics'                => 'Dashboard',
        'userMessages'              => 'User Messages',
        'reports'                   => 'reports',
        'chat'                      => 'technical support',
        'reportSparePart'                      => 'report Spare Part',


        'reportClients'               => 'Customer report',
        'reportFinance'               => 'Financial report',
        'reportServices'              => 'Services report',
        'reportRequests'              => 'Orders report',
        'reportProviders'             => 'Technicians report',


    ] ,

    'validation' => [
        'field_name_required' => 'The name field in Arabic is required' ,
        'field_first_name_required' => 'The first name field is required' ,
        'field_last_name_required' => 'The second name field is required' ,
        'field_equalTo' => 'The password does not match' ,
        'field_name_en_required' => 'The English name field is required' ,
        'field_text_required' => 'Text field is required' ,
        'field_email_required' => 'Email field is required' ,
        'field_email_email' => 'Email is incorrect' ,
        'field_phone_maxlength' => 'The mobile phone field must not exceed 14 numbers' ,
        'field_phone_minlength' => 'The mobile field is not less than 10 digits' ,
        'field_password_required' => 'Password field is required' ,
        'field_password_min' => 'The password field is at least eight characters long' ,
        'field_password_confirmation_required' => 'Reset password field is required' ,


        'field_service_id_type_required' => 'The service field is not less than 10 digits' ,
        'field_sub_service_id_required' => 'The sub service field is not less than 10 digits' ,

        'field_ordering_required' => 'The order field is required' ,
        'field_role_id_required' => 'The validity field is required' ,
        'field_src_required' => 'Image field is required' ,
        'field_site_name_required' => 'The site name field is required' ,
        'field_phone_required' => 'The phone field is required' ,
        'field_phone2_required' => 'The Phone field 2 is required' ,
        'field_ac_type_required' => 'Air conditioner type field is required' ,
        'field_air_conditioner_type_required' => 'Air conditioner brand field is required' ,


        'field_jop_required' => 'jop field required' ,
        'field_date_birth_required' => 'date birth field required' ,
        'field_date_birth_greaterThanToDay' => 'date birth field greater than to day' ,
        'field_years_experience_required' => 'years experience field required' ,

        'phoneNumberShouldStartWithPlus' => 'The mobile phone field must begin with a sign (+).' ,



        'field_date_birth_less_than_today' => 'The Date of Birth field is less than today' ,


        'field_residency_number_required' =>'The ID card field is required' ,
        'field_years_experience_maxlength' =>'The maximum number of years of experience is 3 numbers' ,
        'field_years_experience_number' =>'The years of experience field must be numerical' ,
        'field_residency_number_number' =>'The Residency Number field must be numerical' ,
        'field_residency_number_maxlength' =>'The Residency Number has a maximum number of 10 numbers' ,
    ]
];