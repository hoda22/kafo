<?php
//messages.added_successfully
//messages.updated_successfully
//messages.deleted_successfully
//messages.cant_deleted
//messages.added_rating
return [
    "added_successfully" => 'Added Successfully' ,
    "commented_administration" => 'Commented by administration' ,
    "successfully" => 'Successfully Done' ,
    "massege_send_successfully" => 'The message was sent successfully' ,
    'updated_successfully' => 'Modified Successfully',
    'cant_deleted' => 'This record cannot be cleared',
    'not_found' => 'This record does not exist',
    'already_rating' => 'Previously evaluated',
    'added_rating' => 'Rating Added Duccessfully',
    "deleted_successfully" => "Deleted Successfully" ,
    "phone_tacked" => "Mobile number taken from before" ,
    "phone_no_tacked" => "Mobile number not taken" ,
    "order_cannot_canceled_half_hour_before_execution" => "The order cannot be canceled half an hour before execution" ,
    "code_not_match" => "code not match" ,
    "coupon_is_invalid" => "The coupon is invalid" ,
    "cannot_deleted_user" => "This user cannot be deleted" ,
    "add_rating_successfully" => "Rating added successfully" ,


    "invalid_login_details" => "Invalid login details" ,

    "no_page_name" => "There is no page with this name" ,
    "no_phone_number" => "Mobile number is not exist" ,
    "account_inactive" => "This account is suspended" ,
    "activation_code_sent" => "The activation code has been sent" ,
    "activation_code_sent_phone" => "“The code was sent to the phone" ,
    "password_modified_successfully" => "The password has been modified successfully" ,
    "device_number_not_exist" => "The device number does not exist" ,
    "logged_successfully" => "You have logged in successfully" ,
    "account_not_activated" => "The account has not been activated" ,
    "account_successfully_activated_log_now" => "Your account has been successfully activated. Log in now" ,

    "account_successfully_activated_not_now" => "Your account has been created successfully, waiting for Kafo app administration to activate it" ,

    "account_has_been_activated_successfully" => "Your account has been activated successfully" ,

    "login_information_incorrect" => "Incorrect mobile number or password" ,
    "account_registered_successfully_activate_enter_sent_code" => "Your account has been registered successfully. To activate, enter the sent code" ,


    "code_incorrect" => "The code is incorrect" ,
    "device_number_not_found" => "Device number not found" ,
    "old_password_incorrect" => "The old password is incorrect" ,
    "service_time_has_expired" => "Service time has expired" ,
];
