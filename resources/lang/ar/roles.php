<?php

return [
    'roles-list' => 'عرض الصلاحيات',

    'all' => 'الكل',
    'roles-create' => 'أضف صلاحيه',
    'roles-edit' => 'تعديل صلاحيه',
    'roles-delete' => 'حذف صلاحيه',

    'clients-list' => 'قائمة العملاء',
    'clients-show' => 'عرض العميل',
    'clients-create' => 'أضف عميل',
    'clients-edit' => 'تعديل عميل',
    'clients-delete' => 'حذف عميل',

    'users-list' => 'قائمة المستخدمين',
    'users-create' => 'أضف مسنخدمين',
    'users-edit' => 'تعديل المستخدمين',
    'users-delete' => 'حذف المستخدمين',

    'answers-list' => 'عرض الحالات',
    'answers-create' => 'أضف حاله',
    'answers-edit' => 'تعديل الحالات',
    'answers-delete' => 'حذف الحالات',

    'cities-list' => 'قائمة المدن',
    'cities-create' => 'أضف مدن',
    'cities-edit' => 'تعديل مدن',
    'cities-delete' => 'حذف مدن',

    'regions-list' => 'قائمة المناطق',
    'regions-create' => 'أضف المناطق',
    'regions-edit' => 'تعديل المناطق',
    'regions-delete' => 'حذف المناطق',

    'categories-list' => 'قائمة تصنيفات',
    'categories-create' => 'أضف تصنيفات',
    'categories-edit' => 'تعديل تصنيفات',
    'categories-delete' => 'حذف تصنيفات',


    'orders-list' => 'قائمة طلبات العملاء',
    'orders-create' => 'أضف طلبات العملاء',
    'orders-edit' => 'تعديل طلبات العملاء',
    'orders-delete' => 'حذف طلبات العملاء',

    'spare-parts-list' => 'عرض قطع غيار المكيفات',
    'spare-parts-create' => 'أضف قطعه غيار',
    'spare-parts-edit' => 'تعديل قطعه غيار',
    'spare-parts-delete' => 'حذف قطعه غيار',


    'brands-list' => 'قائمة ماركه المكييف',
    'brands-create' => 'أضف  ماركه المكييف',
    'brands-edit' => 'تعديل ماركه المكييف',
    'brands-delete' => 'حذف ماركه المكييف',
    'pages-list' => 'عرض الصفحات',
    'pages-edit' => 'تعديل الصفحات',

    'settings-edit' => 'تعديل الاعدادات',


    'providers-list' => 'قائمه الفنيين',
    'providers-create' => 'أضف الفني',
    'providers-show' => 'عرض الفني',
    'providers-edit' => 'تعديل الفني',
    'providers-delete' => 'حذف الفني',


    'sliders-list' => 'قائمة السليدر',
    'sliders-create' => 'أضف السليدر',
    'sliders-edit' => 'تعديل السليدر',
    'sliders-delete' => 'حذف السليدر',

    'survey-list' => 'قائمة الاستبيان',
    'survey-create' => 'أضف الاستبيان',
    'survey-edit' => 'تعديل الاستبيان',
    'survey-delete' => 'حذف الاستبيان',

    'services-list' => 'عرض خدمه',
    'services-create' => 'أضف خدمه',
    'services-edit' => 'تعديل خدمه',
    'services-delete' => 'حذف خدمه',

    'notifications-list' => 'الاشعارات' ,
    'notifications-create' => 'ارسال اشعار' ,
    'notification-show' => 'عرض الاشعار' ,

    'messages-list' => 'رسائل المستخدمين' ,
    'messages-send' => 'ارسال رساله للمستخدم' ,
    //
    'messages-show' => 'عرض رساله للمستخدم' ,

    'subscriptions-lists' => 'عرض طلبات الاشتراكات',
    'subscriptions-processes' => 'عمليات طلبات الاشتراكات',


    //
    'chat-list' => 'الدعم الفني' ,


    'payment-requests-list' => 'قائمة طلبات الدفع' ,
    'payment-requests-collection' => 'تحصيل طلبات الدفع' ,








    'reports-list' => 'عرض التقارير' ,
    'report-clients' => 'تقرير العملاء' ,
    'export-report-clients' => 'تصدير بيانات العملاء' ,


    'report-services' => 'تقرير الخدمات' ,
    'export-report-services' => 'تصدير بيانات الخدمات' ,


    'report-request' => 'تقرير الطلبات' ,
    'export-report-request' => 'تصدير بيانات الطلبات' ,


    'report-finance' => 'تقرير الماليه' ,
    'export-report-finance' => 'تصدير بيانات الماليه' ,


    'report-provider' => 'تقرير الفنيين' ,
    'export-report-provider' => 'تصدير بيانات الفنيين' ,


    'report-spare-pats' => 'تقرير قطع غيار' ,
    'export-report-spare-pats' => 'تصدير قطع غيار' ,



    'sup-services-list'     => 'قائمة الخدمات الفرعيه' ,
    'sup-services-create'   => 'أضف خدمة فرعيه' ,
    'sup-services-edit'     => 'تعديل خدمة فرعيه' ,
    'sup-services-delete'   => 'حذف خدمة فرعيه' ,



    'loyalty-points-list'   => 'قائمة النقاط' ,
    'loyalty-points-create' => 'أضف نقطة' ,
    'loyalty-points-edit'   => 'تعديل نقطة' ,
    'loyalty-points-delete' => 'حذف نقطة' ,



    'coupons-list'   => 'قائمة الكوبونات' ,
    'coupons-create' => 'أضف كوبون' ,
    'coupons-edit'   => 'تعديل كوبون' ,
    'coupons-delete' => 'حذف كوبون' ,


    'orders-actions' => 'الردود علي الطلبات' ,

    "performance-panel" =>"لوحه الاداء" ,
    "performance-panel-settings" => "عرض لوحه الاداء",
    "complaints-suggestions" =>"الشكاوي و الاقتراحات",
    "complaints-suggestions-list" =>"عرض",
    "complaints-suggestions-replay" =>"رد",
    "client-approve-reject" =>"موافقه / رفض العميل",
    "provider-approve-reject" =>"موافقه / رفض الفني",



];