<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'بيانات الاعتماد هذه لا تتطابق مع سجلاتنا.',
    'throttle' => 'عدد كبير جدًا من محاولات تسجيل الدخول. يرجى المحاولة مرة أخرى :seconds seconds.',

    'you_receiving_email_because_received_password_reset_request_your_account' => 'لقد تلقيت هذا البريد الإلكتروني لأننا تلقينا طلبًا لإعادة تعيين كلمة المرور لحسابك .',
    'reset_password' => 'إعادة تعيين كلمة المرور',
    'did_request_password_reset_further_action_required' => 'إذا لم تطلب إعادة تعيين كلمة المرور، فلا يلزم اتخاذ أي إجراء آخر .',


    'sent' => 'سوف يقوم النظام بارسال بريد الكتروني لإعادة تعيين كلمة المرور',



];
