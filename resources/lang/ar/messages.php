<?php
//messages.added_successfully
//messages.updated_successfully
//messages.deleted_successfully
//messages.cant_deleted
//messages.added_rating
//messages.code_not_match
return [
    "added_successfully" => 'تمت الاضافه بنجاح' ,
    //===
    "commented_administration" => 'تم التعليق من قبل الاداره' ,


    "edited_successfully" => 'تمت ارسال طلب التعديل بنجاح' ,
    "successfully" => 'تمت بنجاح' ,
    "massege_send_successfully" => 'تمت إرسال الرساله بنجاح' ,
    'updated_successfully' => 'تم التعديل بنجاح',
    'cant_deleted' => 'لا يمكن مسح هذا السجل',
    'not_found' => 'لا يوجد هذا السجل',
    'already_rating' => 'تم التقييم مسبقا',
    'added_rating' => 'تم إضافه التقييم بنجاح',
    "deleted_successfully" => "تم الحذف بنجاح" ,
    "phone_tacked" => "رقم الجوال مأخوذ من قبل" ,
    "phone_no_tacked" => "رقم الجوال غير مأخوذ" ,
    "order_cannot_canceled_half_hour_before_execution" => "لا يمكن إلغاء الطلب قبل التنفيذ بنصف ساعه" ,
//    =============
    "code_not_match" => "الكود غير متطابق" ,
    "coupon_is_invalid" => "الكوبون غير صحيح" ,
    "cannot_deleted_user" => "لا يمكن حذف هذا المستخدم" ,
    "add_rating_successfully" => "تم إضافه التقييم بنجاح" ,


    "no_page_name" => "لا يوجد صفحه بهذا الاسم" ,
    "no_phone_number" => "رقم الجوال غير موجود" ,
    "account_inactive" => "هذا الحساب موقوف" ,
    "activation_code_sent" => "تم ارسال كود التفعيل" ,
    "activation_code_sent_phone" => "تم ارسال الكود علي الهاتف" ,
    "password_modified_successfully" => "تم تعديل كلمه المرور بنجاح" ,
    "device_number_not_exist" => "رقم ال device غير موجود" ,
    "logged_successfully" => "تم التسجيل الدخول بنجاح" ,
    "account_not_activated" => "لم يتم تفعيل الحساب" ,
    "account_successfully_activated_log_now" => "تم تفعيل حسابك بنجاح .. سجل دخول الان" ,
    "account_successfully_activated_not_now" => "لقد تم إنشاء حسابك بنجاح و فى انتظار التفعيل من إدارة تطبيق كفو" ,
    "account_has_been_activated_successfully" => "تم تفعيل حسابك بنجاح" ,
    "login_information_incorrect" => "رقم الجوال أو كلمة المرور غير صحيحة" ,
    "account_registered_successfully_activate_enter_sent_code" => "تم تسجيل حسابك بنجاح .. للتفعيل ادخل الكود المرسل" ,
    "invalid_login_details" => "معلومات الدخول غير صحيحه",

    "code_incorrect" => "الكود غير صحيح" ,
    "device_number_not_found" => "لم يتم العثور على رقم الجهاز" ,
    "old_password_incorrect" => "كلمه المرور القديمه غير صحيحه" ,


    "remove_item_from_cart" => "تم حذف العنصر من السله" ,
    "updated_item_cart_successfully" => "تم تعديل الكميه بنجاح" ,
    "add_to_cart" => "تم الاضافه للسله بنجاح" ,
    "service_time_has_expired" => "تم انتهاء وقت الخدمه" ,
];
