<?php

return [
    'name_en'           => 'الاسم بالانجليزي',
    'name_ar'           => 'الاسم بالعربي',
    'reset_password_wo'        => 'إعادة تعيين' ,
    'reset_password'        => 'إعادة تعيين' ,
    'send_password_reset_link' => 'إرسال' ,
    'hours' => 'ساعات' ,

    'answer' => 'الإجابة' ,
    'new_chat' => 'رساله جديده' ,
    'no_city' => 'لا يوجد مدينه معرفه علي هذا الموقع' ,
    'approved' => 'موافق' ,
    'rejected' => 'مرفوض' ,
    'ar' => 'بالعربي' ,
    'en' => 'بالانجليزي' ,
    'quantity' => 'الكميه' ,
    'Search' => 'البحث' ,
    'forget_password' => 'نسيت كلمة المرور' ,
    'confirm_password' => 'تأكيد كلمة المرور' ,
    'export_data'       => 'تصدير البيانات',
    're_password'       => 'إعاده كلمة المرور',
    'email'             => 'البريد الإلكتروني',
    'sent'          =>'سوف يقوم النظام بارسال بريد الكتروني لإعادة تعيين كلمة المرور' ,
    //======
    'number_answers_must_2_or_more' => 'لا بد ان يكون عدد الاجابات 2 او أكثر' ,
    'cash'                      => 'نقدي',
    'status_active'                      => 'حالة التنشيط',
    'deleted_his_account'                      => 'قام بحذف حسابه',
    'management_comment'                      => 'تعليق الاداره',
    'visa'                      => 'بطاقه',
    'mada'                      => 'مدي',
    'not_yet'                      => 'لم يحدد بعد',
    'paid'                      => 'تم الدفع',
    'not_paid'                      => 'لم يتم الدفع',
    'is_paid'                      => 'حالة الدفع',
    'collection_done'                      => 'تم التحصيل من الفني بنجاح الطلب',

    "how_send"  => "طريقة الإرسال" ,
    "request_cancel_order_was_rejected_by_administration"  => "تم رفض طلب إلغائك للطلب من قبل الادارة" ,
    "request_was_returned_to_technician_again"  => "تم إرجاع الطلب الي الفني مره اخري" ,
    "under_revision"  => "تحت المراجعه" ,
    "reject_from_admin"  => "مرفوض من الادمن" ,
    "technician_assigned"  => "تم تخصيص فني" ,

    "admin_modified_status"  => "تعديل السعر بموافقه العميل" ,
    "modified_status"  => "تم التعديل" ,
    "remove_status"  => "محذوفه" ,
    "add_service_status"  => "تمت الاضافه من الادمن" ,

    //===============
    "new_request_been_sent_to_you_and_awaiting_your_approval_of_the_request_" => "تم ارسال طلب جديد لك و في انتظار موافقتك علي الطلب" ,
    "new_request_been_sent_to_you_and_awaiting_your_approval_of_the_request" => "تم ارسال طلب جديد لك و في انتظار موافقتك علي الطلب" ,
    "note_new_request" => "طلب جديد" ,
    "_order_id" => "رقم الطلب" ,
    "note_reject_request_from_admin" => "رفض طلب إلغاء الطلب" ,
    "note_response_edit_request_from_admin" => "قبول طلب التعديل" ,
    "note_response_reject_edit_request_from_admin" => "رفض طلب التعديل" ,
    'note_approve_modify_request' => 'قبول طلب التعديل' ,
    'note_reject_modify_request' => 'رفض تعديل الطلب' ,
    'note_accept_cancellation_request' => 'قبول إلغاء الطلب من إدارة كفو' ,
    'suspended_account' => 'الحساب موقوف من قبل الاداره' ,
    'suspended_account_mang' => 'هذا الحساب ما زال قيد المراجعة من إدارة تطبيق كفو' ,
    'stped_account_mang' => 'تم إيقاف هذا الحساب من إدارة تطبيق كفو' ,
    'application_management' => 'إدارة تطبيق كفو' ,


    'your_request_rejected_because' => 'تم رفض طلبك بسبب' ,

    'The request was rejected by the technician' => 'تم رفض الطلب من قبل الفني' ,



    'enter'             => 'دخول',
    'unit_price'             => 'سعر الوحده',
    'no_image'          => 'لا يوجد صوره',
    'no_balance'        =>  'رصيد غير كافي' ,

    //=========================
    "request_done" => "تم الطلب",
    "under_revision_" => "تحت المراجعه",
    "technician_assigned_" => "تم تخصيص فني",
    "technician_on_way" => "الفني في الطريق",
    "technician_arrived" => "تم وصول الفني",
    "implemented" => "تم التنفيذ",
    "canceled_technician" => "تم الإلغاء من قبل الفني",
    "rejected_by_technician" => "رفض من الفني",
    "rejected_by_administration" => "مرفوض من قبل الاداره",
    "modification" => "طلب تعديل",

    "cost_approved_by_client" => "تم الموافقه علي التكلفه من العميل",
    //=============================
    "approved_modification_admin" => "تم الموافقه علي التعديل من الادمن",

    "new_cost_rejected_by_customer" => "تم رفض التكلفه الجديده من قبل العميل",
    "Approval_amendment_been_sent_to_technician" => "تم ارسال الموافقه علي التعديل للفني",
    "rejection_the_new_cost_been_sent_technician" => "تم ارسال رفض التكلفه الجديده للفني",
    "new_cost_been_sent_to_customer" => "تم ارسال التكلفه الجديده للعميل",
    "rejected_customer" => "تم الرفض من قبل العميل",

    // =========================
    'select_work_city'             => 'إختار مدينه عمل الفني',
    'select_work_city_successfully'             => 'تم اختيار مدينه عمل الفني بنجاح',
    'city'             => 'المدينه',
    'city_work' => 'مدينه العمل' ,


    'remove'             => 'حذف',
    'send'             => 'ارسال',
    'name'              => 'الاسم',
    'pending'              => 'معلق',
    'providers&clients' => 'العملاء و الفنيين',
    'order_id'          => 'رقم الطلب',

    'canceled_orders'          => 'الطلبات الملغاه',
    'requests_are_progress'          => 'الطلبات قيد التنفيذ',
    'finished_orders'          => 'الطلبات المنتهيه',


    'client'            => 'عميل',
    'description'       => 'وصف',
    'active'            => 'نشط',
    'inactive'          => 'غير نشط',
    'service_date'      => 'تاريخ الخدمه',
    'created_at'        => 'تاريخ الطلب',
    'order_status'      => 'حالة الطلب',
    'order_type'        => 'نوع الطلب',
    'client_comment'    => 'تعليق العميل',
    'provider_rate'     => 'تقييم العميل',
    'provider'          => 'فني',
    'first_name'        => 'الاسم الأول',
    'last_name'         => 'الاسم الثاني',
    'login'             => 'تسجيل الدخول',
    'image'             => 'الصورة',
    'price'             => 'سعر الخدمة',
    'main'              => 'رئيسي',
    'SAR'               => "ر.س",
    'title_site'        => 'كفو',
    'welcome_back'      => 'مرحبا بعودتك! تسجيل الدخول إلي  حسابك.',
    'status'            => 'الحالة',
    'processes'         => 'العمليات',
    'search'            => 'بحث',
    'service_technician'=> 'فني الخدمة',
    'service_requester' => 'طالب الخدمة',
    'jop'               => 'مهنة',
    'password'          => 'كلمة المرور',
    'role'              => 'الصلاحية',
    'service'           => 'الخدمة',
    'choose'            => 'اختار',
    'save'              => 'حفظ',
    'clients_'          => 'عملاء',
    'phone'             => 'الجوال',
    'nationality'       => 'الجنسية',
    'residency_number'  => 'رقم بطاقة الاحوال',
    'text'              => 'النص',
    'years_experience'  => 'عدد سنوات الخبرة',
    'date_birth'        => 'تاريخ الميلاد',
    'select_category'   => 'اختار الفئة',
    'all'               => 'الكل',
    'code'              => 'الكود',
    'enter_code'        => 'أدخل الكود',
    'ordering'          => 'الترتيب',
    'category_type'     => 'الفئة',
    'notification'      => 'الإشعار',
    'cancel'            => 'إلغاء',
    'yes_deleted'       => 'نعم , اريد الحذف !',
    'AC_type'           => 'نوع المكيف	',
    'AC_brand'           => 'ماركة المكيف',
    'no_data'           => 'لا يوجد نتائج للبحث',
    'number_elements_page' => 'عدد العناصر في الصفحة',
    'number_elements'   => 'مجموع العناصر',
    'logout'            => 'تسجيل الخروج',
    'really_deleted'    => 'هل تريد الحذف حقا ؟',
    'copyright'         => 'جميع حقوق الطبع و النشر محفوظة',
    'service_name'      => 'اسم الخدمة',
    'services'          => 'الخدمات',
    'how_many_services' => 'عدد الخدمات الفرعيه',
    'add_new_service'   => 'إضافة خدمة جديدة',
    'service_add'       => 'إضافة خدمة',
    'service_update'    => 'تعديل الخدمة',
    'category'          => 'فرعي من',
    'old_image'         => 'الصورة القديمة',
    'city_add'          => 'إضافة مدينة',
    'city_name'         => 'اسم المدينة',
    'city_edit'         => 'تعديل المدينة',
    'cities'            => 'المدن',
    'duration'            => 'المده الزمنيه',
    'client_requests'            => 'طلبات العميل',
    'posting_time'      => 'وقت الإرسال',
    'not_fond_data'      => 'لا يوجد بيانات',
    'not_fond_orders'      => 'لا يوجد طلبات',
    'order_rejected'      => 'تم رفض الطلب',
    'order_approder'      => 'تم الموافقه الطلب',

    'address'      => 'العنوان',
    'total'      => 'المجموع',
    'payment_type'      => 'نوع الدفع',
    'payment_status'      => 'حالة الدفع',


    'spare_part_add'      => 'إضافة قطعة غيار',
    'spare_part_name'     => 'اسم قطعة الغيار',
    'spare_part_edit'     => 'تعديل المدينة',

    'assigned_to_order'     => 'تم تخصيص فني للطلب',
    'welcome_dashboard'  => 'لوحة التحكم',
    'recently_enrolled_technicians'  => 'الفنيين الفنيين مؤخرا',
    'recently_registered_customers'  => 'العملاء المسجلين مؤخرا',

    'facebook'                    => 'حساب فيس بوك',
    'twitter'                    => 'حساب تويتر',
    'linkedin'                    => 'حساب لينكدان',
    'instagram'                    => 'حساب إنستغرام',

    'from'                    => 'تاريخ من',
    'to'                    => 'تاريخ إلي',


    'date_joining'                    => 'تاريخ الانضمام',
    'sent_to_him'                    => 'المرسل له',
    'message'                       => 'الرساله',
    'userMessagesAdd'                => 'إضافه رسالة',

    'sub_service'                   => 'الخدمه الفرعيه',
    'technical_assign'             => 'تخصيص فني',
    'show_order'                   => 'عرض الطلب',
    'basic_service'                 => 'الخدمه الاساسيه',
    'price_paid'                    => 'السعر المدفوع',
    'service_history'               => 'تاريخ الخدمه',
    'date_of_registration'          => 'تاريخ التسجيل',
    'service_requester_comment'     => 'تعليق طالب الخدمه',
    'technical_comment'             => 'تعليق الفني',
//    'management_comment'            => 'تعليق الاداره',
    'service_requester_information' => 'معلومات طالب الخدمه',
    'name_of_service_applicant'     => 'اسم طالب الخدمه',
    'name_of_technical'             => 'اسم الفني',
    'rating'                        => 'التقييم',
    'technical_information'         => 'معلومات الفني',
    // ==========
    'order_information'             => 'معلومات الطلب',
    'time_taken_for_maintenance'    => 'الوقت المستغرق للصيانه',
    'amount_paid'                   => 'المبلغ المدفوع',
    'what_been_accomplished'        => 'ماتم انجازه',

    'no_customer'                   => 'لا يوجد عميل',
    'edit_role'                   => 'تعديل صلاحيه',
    'it_is_not_determined_yet'                   => 'لم يحدد بعد',
    'full_name'                   => 'الاسم كامل',
    'create_at'                   => 'تاريخ التسجيل',
    'spare-part_name'                   => 'اسم قطعة الغيار',
    'spare-part_num'                   => 'عدد قطع الغيار المستخدمة',

    //===========================
    'no_requests'                   => 'لا يوجد طلبات',
    'balance'                   => 'الرصيد المتاح',
    'activation_successfully' => 'تم التفعيل بنجاح' ,
    'activation_successfully_deactivated' => 'تم إلغاء التفعيل بنجاح' ,
    'block_successfully_deactivated' => 'تم حظر العميل بنجاح' ,
    'block_successfully_agent_deactivated' => 'تم حظر الفني بنجاح' ,
    'block_successfully_delete' => 'تم إيقاف حظر العميل بنجاح' ,
    'active_successfully' => 'تم تنشيط الفني بنجاح' ,
    'unactive_successfully' => 'تم ايقاف تنشيط الفني بنجاح' ,
    'client_name' => 'اسم العميل' ,
    'provider_name' => 'اسم الفني' ,
    'reason' => 'السبب' ,
    'rejectModel' => 'رفض طلب العميل' ,
    'providers_' => 'الفنيين' ,
    'unavailable' => 'غير متاح' ,
    'available' => 'متاح' ,
    'block_statue' => 'حالة الحظر' ,
    'block' => 'محظور' ,
    'unblock' => 'غير محظور' ,
    'ordersNum' => 'عدد الطلبات' ,
    'hoursNum' => 'عدد الساعات' ,
    'open' => 'مفتوح' ,
    'close' => 'مغلق' ,
    'assigned_agent' => 'فني الطلب' ,
    'invoiced_amount' => 'مجموع الفاتوره' ,
    'sup_category' => 'الخدمه الفرعيه' ,
    'category_' => 'الخدمه' ,

    'payment_method' => 'طريقه الدفع' ,
    'notes' => 'ملاحظات' ,
    //============================
    'no_services' => 'لا يوجد خدمات مضافه للطلب' ,
    'previous_conversations' => 'المحادثات السابقه' ,
    'no_conversations' => 'لا يوجد محادثات' ,
    'send_message' => 'ارسال الرسالة' ,
    'send_conversations' => 'اختار المحادثه' ,
    'approve' => 'موافق' ,
    'suggestion' => 'اقتراح' ,
    'reject' => 'رفض' ,
    'approve_edit_request' => 'قبول' ,
    'approve_edit_request_reject' => 'رفض' ,
    'edit_request' => 'تعديل الطلب' ,
    'order_canceled_by_technician' => 'تم الغاء الطلب من قبل الفني' ,
    'order_modification_technician' => 'هناك تعديل مطلوب' ,
    'total_order' => 'التكلفة السابقة' ,
    'new_cost' => 'تكلفة التعديل الجديد' ,
    'type' => 'النوع' ,
    'replay' => 'الرد' ,
    'we_send_new_cost_in_order' => 'تم ارسال التكلفه الجديده علي الطلب لتصبح ' ,
    'client_approved_new_cost' => 'وافق العميل علي التكلفه الجديده' ,
    'client_rejected_new_cost' => ' رفض العميل التكلفه الجديده' ,
    'reject_modify_request' => 'تم رفض التعديل المطلوب' ,
    'approve_modify_request' => 'تم الموافقه علي التعديل المطلوب' ,

    'do_it'                 => 'نفذ الان' ,
    'complaint'             => 'شكوي',
    'replay_management'     => 'رد  الاداره',

    'minutes'               => 'دقائق' ,
    'days'                  => 'ايام' ,
    //===============
    'approved_client'        => 'تمت الموافقه من العميل' ,
    'rejected_client'        => 'تم الرفض من العميل' ,
    'status_edit_request'        => 'حالة التعديل' ,
    'edit_images'        => 'صور التعديل' ,
    'used_spare_parts'        => 'قطع الغيار المستخدمه' ,
    'description_problem'        => 'وصف المشكله' ,
    'no_spare_parts_used'        => 'لا يوجد قطع غيار مستخدمة' ,
    'cases_of_rejection_and_cancellation'        => 'حالات الرفض و الالغاء' ,
    //=======================
    'modify_appointment' => 'تعديل الموعد' ,


    'spare_parts'         => [
        'brand'     => 'ماركة المكيف' ,
        'items'     => 'نوع المكيف' ,
        "edit"      => 'تعديل قطعة غيار' ,
        "add"       => 'إضافة قطعة غيار'
    ],

    'coupons'         => [
        'items'     => 'الكوبونات' ,
        "add"      => 'إضافه كوبون' ,
        "edit"      => 'تعديل كوبون' ,
        'code'     => 'الكود' ,
        'expire_date'     => 'تاريخ الانتهاء' ,
        "type"      => 'النوع' ,
        "percent"      => 'نسبه مئويه' ,

        "amount"      => 'المبلغ' ,
        "start_date"      => 'تاريخ البدايه' ,
        "discount"      => 'قيمه الخصم'
    ],


    'complaints_suggestions'         => [
        "title_replay"       => 'إضافة رد علي الشكوي'
    ],

    'survey'         => [
        'question_ar'     => 'السؤال بالعربي' ,
        'answers'     => 'الإجابات' ,
        'question_en'     => 'السؤال بالإنجليزي' ,
        "ans_ar"      => 'الإجابة بالعربي' ,
        "ans_en"      => 'الإجابة بالإنجليزي' ,
        "question_type"      => 'نوع السؤال' ,
        "ans_text"      => 'اجابه نصيه' ,
        "select_choose"      => 'اختيار من متعدد' ,
        "ans_type"      => 'نوع الاجابه' ,
        "edit"      => 'تعديل سؤال' ,
        "add"      => 'إضافه سؤال' ,
        "no_survey_yet"      => 'لا يوجد استبيان الي الان' ,
        "cannot_add_more"      => 'لا يمكن أضافه أكثر من 5 أجابات' ,
    ],

    'pages'         => [
        'name'  => 'اسم الصفحة' ,
        'add'   => 'إضافة صفحه تعريفيه',
        'page_title'   => 'اسم الصفحة',
        'page_content_ar'   => 'محتوى الصفحة بالعربي',
        'page_content_en'   => 'محتوى الصفحة بالإنجليزي',
    ],

    'roles'         => [
        'name'  => 'اسم الصلاحية' ,
        'add'   => 'إضافة صلاحية',
    ],

    'users'         => [
        'add'   => 'إضافة مستخدم',
        'name'  => 'اسم المستخدم' ,
        'update'  => 'تعديل المستخدم' ,
        'first_name'  => 'الاسم الأول' ,
        'last_name'  => 'الاسم الثاني' ,
    ],


    'notifications'     =>[
        'add'       => 'إضافة رسالة' ,
        'show'       => 'عرض الرسالة'
    ] ,

    'points'     =>[
        'add'       => 'إضافة للنقاط' ,
        'edit'       => 'تعديل نقطة' ,
        'num_points'       => 'عدد النقاط' ,
        'num_rus'       => 'عدد الريالات' ,
    ] ,

    'air_conditioner_brands' => [
        'add'       => 'إضافة ماركة' ,
        'name'      => 'اسم الماركة'
    ],

    'sliders' => [
        'add'        => 'إضافة صورة' ,
        'edit_image' => 'تعديل الصورة'
    ],

    'settings' => [
        'meta_description'  => 'الكلمات الوصفية' ,
        'meta_keywords'     => 'الكلمات المفتاحية',
        'visit'     => 'رسوم الزياره',
        'site_name'         => 'الموقع الإلكتروني'
    ],

    'clients' => [
        'add'               => 'إضافة عميل',
        'show'               => 'بيانات العميل',
        'edit'              => 'تعديل عميل',
    ],

    'providers' => [
        'add'               => 'إضافة فني',
        'show'              => 'بيانات فني',
        'edit'              => 'تعديل فني',
    ],




    'titles'      => [
        'spare-parts'               => 'قطع الغيار',
        'points_'               => 'النقاط',
        'pricePoint'               => 'أسعار النقاط',
        'points'               => 'النقاط',
        'coupons'               => 'الكوبونات',
        "performance-panel" =>"لوحه الاداء" ,
        'complaints-suggestions'               => 'الشكاوي و الاقتراحات',

        'sup_services'              => 'الخدمات الفرعيه',
        'notifications'             => 'الإشعارات',
        'payment_request'           => 'طلبات الدفع',
        'pages'                     => 'الصفحات التعريفيه',
        'sliders'                   => 'الصور',
        'survey'                   => 'الاستبيان',
        'roles'                     => 'الصلاحيات',
        'orders'                    => 'طلبات العملاء',
        'paymentRequest'            => 'طلبات دفع',
        'users'                     => 'المستخدمين',
        'services'                  => 'الخدمات',
        'providers'                 => 'الفنيين',
        'cities'                    => 'المدن',
        'clients'                   => 'العملاء',
        'settings'                  => 'اتصل بنا',
        'air_conditioner_brands'    => 'ماركات المكيفات',
        'brands'                    => 'ماركات المكيفات',
        'users_permissions'         => 'المستخدمين و الصلاحيات',
        'statistics'                => 'لوحة الأداء',
        'userMessages'              => 'رسائل المستخدمين',
        'payment-requests' => 'طلبات الدفع' ,
        'reports'                   => 'التقارير',
//        ============
        'reportClients'               => 'تقرير العملاء',
        'reportFinance'               => 'تقرير المالية',
        'reportServices'              => 'تقرير الخدمات',
        'reportRequests'              => 'تقرير الطلبات',
        'reportProviders'             => 'تقرير الفنيين',
        'reportSpareParts'             => 'تقرير قطع الغيار',
        'chat'                      => 'الدعم الفني',
        'showNotifications'                      => 'عرض الاشعار',


        //============
        'reports-finance'               => 'تقرير المالية',
        'reports-request'               => 'تقرير الطلبات',
        'reports-spare-pats'           => 'تقرير قطع الغيار',
        'reports-services'               => 'تقرير الخدمات',
        'reports-provider'               => 'تقرير الفنيين',
        'reports-client'               => 'تقرير العملاء',
    ] ,

    'pricesPoints' => [
        "from" => "المجموع من" ,
        "to" => "المجموع الي" ,
        "sar" => "عدد الريالات" ,
        "points_num" => "عدد النقاط" ,
    ] ,

    'validation' => [
        'field_name_required' => 'حقل الاسم بالعربي مطلوب' ,
        'field_first_name_required' => 'حقل الاسم الأول مطلوب' ,
        'field_last_name_required' => 'حقل الاسم الثاني مطلوب' ,
        'field_equalTo' => 'كلمة المرور غير متطابقه' ,
        //===
        'field_name_en_required' => 'حقل الاسم بالانجليزي مطلوب' ,
        'field_text_en_required' => 'حقل السؤال بالانجليزي مطلوب' ,
        'field_text_q_required' => 'حقل السؤال بالعربي مطلوب' ,
        'field_text_required' => 'حقل النص مطلوب' ,
        'field_email_required' => 'حقل البريد الإلكتروني مطلوب' ,
        'field_email_email' => 'البريد الإلكتروني غير صحيح' ,
        'field_phone_maxlength' => 'حقل  الجوال  لا يزيد عن 14 ارقام' ,
        'field_phone_minlength' => 'حقل  الجوال  لا يقل عن 10 ارقام' ,
        'field_password_required' => 'حقل كلمة المرور مطلوبة' ,
        'field_password_min' => 'حقل  كلمة المرور لا يقل عن ثمانية أحروف' ,
        'field_password_confirmation_required' => 'حقل إعاده كلمة المرور مطلوبة' ,

        'field_service_id_type_required' => 'حقل الخدمة الاساسية مطلوبة' ,
        'field_sub_service_id_required' => 'حقل الخدمة الفرعية مطلوبة' ,

        'field_ordering_required' => 'حقل الترتيب مطلوب' ,
        'field_role_id_required' => 'حقل الصلاحيه مطلوبة' ,
        'field_src_required' => 'حقل الصورة مطلوبة' ,
        'field_src_extension' => 'امتداد الصوره خطأ' ,
        'field_site_name_required' => 'حقل اسم الموقع مطلوب' ,
        'field_phone_required' => 'حقل الجوال مطلوب' ,

        'field_phone2_required' => 'حقل الفون 2 مطلوب' ,
        'field_ac_type_required' => 'حقل نوع المكيف مطلوب' ,
        'field_air_conditioner_type_required' => 'حقل ماركة المكيف مطلوبة' ,
        //======
        'phoneNumberShouldStartWithPlus' => 'حقل الجوال لابد ان يبدا ب علامه ( + )' ,
        'phoneNumberIsNumber' => 'حقل الجوال لابد ان يكون عددي' ,
        'field_url_required' => 'يجب ان يكون رابط' ,
        //======

        'field_answer_en_required' => 'حقل الاجابه بالانجليزيه مطلوب' ,
        'field_answer_ar_required' => 'حقل الاجابه بالعربي مطلوب' ,
        //======
        'field_code_required' => 'حقل الكود مطلوب' ,
        'field_code_maxlength' => 'حقل الكود لابد الا يزيد عن 4 ارقام' ,
        'field_number_required' => 'حقل قيمه الخصم مطلوب' ,
        'field_num_point_required' => 'حقل عدد النقاط مطلوب' ,
        'field_num_sar_required' => 'حقل عدد الريالات مطلوب' ,
        'field_expire_date_required' => 'تاريخ انتهاء الكوبون مطلوب' ,


        'field_jop_required' => 'حقل المهنة مطلوبة' ,
        'field_from_required' => 'حقل من مطلوبة' ,
        'field_to_required' => 'حقل إلي مطلوبة' ,
        'field_points_num_required' => 'حقل عدد النقاط مطلوبة' ,
        'field_greater_than_required' => 'حقل من لابد ان يكون أكبر' ,
        'field_nationality_required' => 'حقل الجنسيه مطلوبة' ,
        'field_city_id_required' => 'حقل المدينة مطلوبة' ,
        'field_date_birth_required' => 'حقل تاريخ الميلاد مطلوب' ,
        'field_years_experience_required' => 'حقل عدد سنوات الخبرة مطلوبة ' ,
        'field_date_birth_less_than_today' => 'حقل تاريخ الميلاد لابد ان يكون أقل من تاريخ اليوم' ,


        'field_residency_number_required' =>'حقل بطاقة الأحوال مطلوبة' ,
        'field_years_experience_maxlength' =>'أقصي حد لسنوات الخبره 3 ارقام' ,
        'field_years_experience_number' =>'حقل سنوات الخبرة لابد ان يكون عددي' ,
        'field_residency_number_number' =>'حقل بطاقة الأحوال لابد ان يكون عددي' ,

        'field_to_range' =>'حقل إلي مطابق من قبل' ,
        'field_from_range' =>'حقل من مطابق من قبل' ,



    ]

];